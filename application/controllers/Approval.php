<?php
defined('BASEPATH') or exit('No direct script access allowed');

use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
use PhpOffice\PhpSpreadsheet\Writer\Xls;
use PhpOffice\PhpSpreadsheet\Writer\Csv;

class Approval extends CI_Controller
{

	public function __construct()
	{
		parent::__construct();
		if (!$this->ion_auth->logged_in()) {
			redirect('auth');
		} else if (!$this->ion_auth->is_admin()) {
			show_error('Hanya Administrator yang diberi hak untuk mengakses halaman ini, <a href="' . base_url('dashboard') . '">Kembali ke menu awal</a>', 403, 'Akses Terlarang');
		}
		$this->load->library(['datatables', 'form_validation']); // Load Library Ignited-Datatables
		$this->load->model('Master_model', 'master');
		$this->form_validation->set_error_delimiters('', '');
	}

	public function output_json($data, $encode = true)
	{
		if ($encode) $data = json_encode($data);
		$this->output->set_content_type('application/json')->set_output($data);
	}

	public function index()
	{
		$data = [
			'user' => $this->ion_auth->user()->row(),
			'judul'	=> 'Pendaftaran',
			'subjudul' => 'Data Pendaftaran'
		];
		$this->load->view('_templates/dashboard/_header', $data);
		$this->load->view('approval/data');
		$this->load->view('_templates/dashboard/_footer');
	}

	public function data()
	{
		$this->output_json($this->master->getDataPendaftaran(), false);
    }
    
	public function aksi($id)
	{
		$data=$this->master->getDataPendaftaranId($id);
		$id_program=($data[0]->id_program);
		
		$data = [
			'user' => $this->ion_auth->user()->row(),
			'judul'	=> 'Approval',
            'subjudul' => 'Pendaftaran Approval',
            'data'=>$data,
            'kelas'=>$this->master->getAllKelasn($id_program),
		];
		$this->load->view('_templates/dashboard/_header', $data);
		$this->load->view('approval/form');
		$this->load->view('_templates/dashboard/_footer');
    }
    public function terima(){

		$data=$this->master->getDataPendaftaranId($_POST['id']);
        $data=json_decode(json_encode($data), true);
        	$input = [
				'email' 		=>$data[0]['email'],
				'nim' 			=>$data[0]['username'], 
				'nickname' 		=>$data[0]['username'], 
				'nama' 			=>$data[0]['nama_lengkap'], 
				'jenis_kelamin' =>$data[0]['jenis_kelamin'],
				'id_kabupaten'  =>$data[0]['kabupaten'], 
				'alamat'  		=>$data[0]['alamat'], 
				'kelas_id' 		=> $_POST['kelas_id'],
			];
			$action = $this->master->create('mahasiswa', $input);

			if ($action) {
				$nama = explode(' ', $data[0]['nama_lengkap']);
				$first_name = $nama[0];
				$last_name = end($nama);
				$username=$data[0]['username'];
				$password=$data[0]['password'];
				$email=$data[0]['email'];
				$additional_data = [
					'first_name'	=> $first_name,
					'last_name'		=> $last_name
				];
            $group = array('3'); // Sets user to dosen.
			$this->ion_auth->register($username, $password, $email, $additional_data, $group);
				$data = [
					'status'	=> true,
					'msg'	 => 'User berhasil dibuat Silahkan Menggunkan email untuk login'
				];
            
                $update[] = array(
                    'id'=>$_POST['id'],
                    'flag_registrasi'=> 'Y'
                );
                $this->master->update('pendaftaran', $update, 'id', null, true);
        }
        die(json_encode($data));

    }
}
