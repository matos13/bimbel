<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class HasilUjian extends CI_Controller {

	public function __construct(){
		parent::__construct();
		if (!$this->ion_auth->logged_in()){
			redirect('auth');
		}
		
		$this->load->library(['datatables']);// Load Library Ignited-Datatables
		$this->load->model('Master_model', 'master');
		$this->load->model('Ujian_model', 'ujian');
		
		$this->user = $this->ion_auth->user()->row();
	}

	public function output_json($data, $encode = true)
	{
		if($encode) $data = json_encode($data);
		$this->output->set_content_type('application/json')->set_output($data);
	}

	public function data()
	{
		$nip_dosen = null;
		$id_kelas = null;
		
		if( $this->ion_auth->in_group('dosen') ) {
			$nip_dosen = $this->user->username;
		}
		if( $this->ion_auth->in_group('mahasiswa') ) {
			$nim = $this->user->username;
			$mhs =$this->master->getMahasiswaByNim($nim);
			$id_kelas=$mhs->kelas_id;
		}

		$this->output_json($this->ujian->getHasilUjian($nip_dosen,$id_kelas), false);
	}

	public function NilaiMhs($id)
	{
		$this->output_json($this->ujian->HslUjianById($id, true), false);
	}

	public function index()
	{
		$data = [
			'user' => $this->user,
			'judul'	=> 'TO',
			'subjudul'=> 'Hasil TO',
		];
		$this->load->view('_templates/dashboard/_header.php', $data);
		$this->load->view('ujian/hasil');
		$this->load->view('_templates/dashboard/_footer.php');
	}
	
	public function detail($id)
	{
		$ujian = $this->ujian->getUjianById($id);
		$nilai = $this->ujian->bandingNilai($id);
		$total = $this->ujian->totalNilai($id);

		$data = [
			'user' => $this->user,
			'judul'	=> 'TO',
			'subjudul'=> 'Detail Hasil TO',
			'ujian'	=> $ujian,
			'nilai'	=> $nilai,
			'total'	=> $total
		];

		$this->load->view('_templates/dashboard/_header.php', $data);
		$this->load->view('ujian/detail_hasil');
		$this->load->view('_templates/dashboard/_footer.php');
	}

	public function cetak($id)
	{
		$this->load->library('Pdf');

		$mhs 	= $this->ujian->getIdMahasiswa($this->user->username);
		$hasil 	= $this->ujian->HslUjian($id, $mhs->id_mahasiswa)->row();
		$ujian 	= $this->ujian->getUjianById($id);
		$peserta = $this->ujian->peringkatByUjianId($id);
		$peringkat = $this->ujian->peringkatByPeserta($id);


		$ranking = 1;
		foreach ($peringkat as $key => $value) {
				$data = $value+$peserta;
				$urutan = array('ranking'=>$ranking++);

				if ($mhs->id_mahasiswa == $value['mahasiswa_id']) {
					$juara = array_merge($data,$urutan);
				}

		}

		$data = [
			'ujian' => $ujian,
			'hasil' => $hasil,
			'mhs'	=> $mhs,
			'ranking' => $juara
		];

		
		$this->load->view('ujian/cetak', $data);
	}

	public function cetak_detail($id)
	{
		$this->load->library('Pdf');

		$ujian = $this->ujian->getUjianById($id);
		$nilai = $this->ujian->bandingNilai($id);
		$hasil = $this->ujian->HslUjianByIdCetak($id)->result();

		$data = [
			'ujian'	=> $ujian,
			'nilai'	=> $nilai,
			'hasil'	=> $hasil
		];

		$this->load->view('ujian/cetak_detail', $data);
	}
	
}