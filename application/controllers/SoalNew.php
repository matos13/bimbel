<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class SoalNew extends CI_Controller {

	public function __construct(){
		parent::__construct();
		if (!$this->ion_auth->logged_in()){
			redirect('auth');
		}else if ( !$this->ion_auth->is_admin() && !$this->ion_auth->in_group('dosen') ){
			show_error('Hanya Administrator dan dosen yang diberi hak untuk mengakses halaman ini, <a href="'.base_url('dashboard').'">Kembali ke menu awal</a>', 403, 'Akses Terlarang');
		}
		$this->load->library(['datatables', 'form_validation']);// Load Library Ignited-Datatables
		$this->load->helper('my');// Load Library Ignited-Datatables
		$this->load->model('Master_model', 'master');
		$this->load->model('Soal_model', 'soal');
		$this->form_validation->set_error_delimiters('','');
	}

	public function output_json($data, $encode = true)
	{
        if($encode) $data = json_encode($data);
        $this->output->set_content_type('application/json')->set_output($data);
    }

    public function index()
	{
        $user = $this->ion_auth->user()->row();
		$data = [
			'user' => $user,
			'judul'	=> 'Soal',
			'subjudul'=> 'Bank Soal'
        ];
        
        if($this->ion_auth->is_admin()){
            //Jika admin maka tampilkan semua matkul
            $data['matkul'] = $this->master->getAllMatkul();
        }else{
            //Jika bukan maka matkul dipilih otomatis sesuai matkul dosen
            // $data['matkul'] = $this->soal->getMatkulDosen($user->username);
        }

		$this->load->view('_templates/dashboard/_header.php', $data);
		$this->load->view('soal-new/data');
		$this->load->view('_templates/dashboard/_footer.php');
    }
    
    public function detail($id)
    {
        $user = $this->ion_auth->user()->row();
		$data = [
			'user'      => $user,
			'judul'	    => 'Soal',
            'subjudul'  => 'Detail Paket',
            'soal'      => $this->soal->getSoalById($id),
            'id'        => $id,
            'paket'     =>$this->soal->getPaketSoalid($id),
        ];

        $this->load->view('_templates/dashboard/_header.php', $data);
		$this->load->view('soal-new/detail');
		$this->load->view('_templates/dashboard/_footer.php');
    }
    
    public function add()
	{
        $user = $this->ion_auth->user()->row();
		$data = [
			'user'      => $user,
			'judul'	    => 'Tambah',
            'subjudul'  => 'data'
        ];
        $data['dosen'] = $this->soal->getAllDosen();
        $data['paket'] = $this->master->getAllPaket();
		$this->load->view('_templates/dashboard/_header.php', $data);
		$this->load->view('soal-new/add');
		$this->load->view('_templates/dashboard/_footer.php');
    }

    public function edit($id)
	{
		$user = $this->ion_auth->user()->row();
		$data = [
			'user'      => $user,
			'judul'	    => 'Edit',
            'subjudul'  => 'Edit data',
            'soal'      => $this->soal->getSoalPaketById($id),
        ];
        $data['dosen'] = $this->soal->getAllDosen();
        $data['paket'] = $this->master->getAllPaket();
		$this->load->view('_templates/dashboard/_header.php', $data);
		$this->load->view('soal-new/edit');
		$this->load->view('_templates/dashboard/_footer.php');
	}

	public function data()
	{
        $this->output_json($this->soal->getDataPaketSoal(), false);
    }
    public function datadetail()
	{
		$this->output_json($this->soal->getDataSoalPaket($_POST['id']), false);
    }
    public function validasi()
    {
        if($this->ion_auth->is_admin()){
            $this->form_validation->set_rules('dosen_id', 'Dosen', 'required');
        }
        // $this->form_validation->set_rules('soal', 'Soal', 'required');
        // $this->form_validation->set_rules('jawaban_a', 'Jawaban A', 'required');
        // $this->form_validation->set_rules('jawaban_b', 'Jawaban B', 'required');
        // $this->form_validation->set_rules('jawaban_c', 'Jawaban C', 'required');
        // $this->form_validation->set_rules('jawaban_d', 'Jawaban D', 'required');
        // $this->form_validation->set_rules('jawaban_e', 'Jawaban E', 'required');
        $this->form_validation->set_rules('jawaban', 'Kunci Jawaban', 'required');
        // $this->form_validation->set_rules('bobot', 'Bobot Soal', 'required|max_length[2]');
    }

    public function file_config()
    {
        $allowed_type 	= [
            "image/jpeg", "image/jpg", "image/png", "image/gif",
            "audio/mpeg", "audio/mpg", "audio/mpeg3", "audio/mp3", "audio/x-wav", "audio/wave", "audio/wav",
            "video/mp4", "application/octet-stream"
        ];
        $config['upload_path']      = FCPATH.'uploads/bank_soal/';
        $config['allowed_types']    = 'jpeg|jpg|png|gif|mpeg|mpg|mpeg3|mp3|wav|wave|mp4';
        $config['encrypt_name']     = TRUE;
        
        return $this->load->library('upload', $config);
    }
    
    public function save()
    {
            
 		    $mode = $this->input->post('method', true);
			$paket 	= $this->input->post('paket', true);
            $dosen  = $this->input->post('dosen', true);
            // $dasar  = $this->input->post('dasar', true);
            // $nilai  = $this->input->post('nilai', true);
            // $soal   = $this->input->post('soal', true);
            // $nilai  = 'nilai' ? $nilai : 0 ;
            // $soal   = 'soal' ? $soal : 0 ;
            //add new parameter
            $jenis_soal 	= $this->input->post('jenis_soal', true);
				if ($mode == 'add') {
					$insert[] = [
						'id_paket' 	=> $this->input->post('paket', true),
                        'id_dosen'  => $this->input->post('dosen', true),
                         //add new parameter
						'jenis_soal' 	=> $this->input->post('jenis_soal', true),
						// 'dasar_kriteria'=> $dasar,
						// 'nilai' 	    => $nilai,
						// 'jml_soal' 	    => $soal,
					];
				} else if ($mode == 'edit') {
					$update[] = array(
						'id'		=> $this->input->post('id', true),
						'id_paket' 	=> $this->input->post('paket', true),
						'id_dosen' 	=> $this->input->post('dosen', true),
                        //add new parameter
                        'jenis_soal'    => $this->input->post('jenis_soal', true),
                        // 'dasar_kriteria'=> $dasar,
						// 'nilai' 	    => $nilai,
						// 'jml_soal' 	    => $soal,
					);
				}
				$status = TRUE;
		if ($status) {
			if ($mode == 'add') {
				$this->master->create('soal_paket', $insert, true);
                $data['insert']	= $insert;
			} else if ($mode == 'edit') {
				$this->master->update('soal_paket', $update, 'id', null, true);
				$data['update'] = $update;
			}
		} else {
			if (isset($error)) {
				$data['errors'] = $error;
			}
        }
		redirect('SoalNew');
    }

    public function delete()
    {
        $chk = $this->input->post('checked', true);
        
        // Delete File
        foreach($chk as $id){
            $abjad = ['a', 'b', 'c', 'd', 'e'];
            $path = FCPATH.'uploads/bank_soal/';
            $soal = $this->soal->getSoalById($id);
            // Hapus File Soal
            if(!empty($soal->file)){
                if(file_exists($path.$soal->file)){
                    unlink($path.$soal->file);
                }
            }
            //Hapus File Opsi
            $i = 0; //index
            foreach ($abjad as $abj) {
                $file_opsi = 'file_'.$abj;
                if(!empty($soal->$file_opsi)){
                    if(file_exists($path.$soal->$file_opsi)){
                        unlink($path.$soal->$file_opsi);
                    }
                }
            }
        }

        if(!$chk){
            $this->output_json(['status'=>false]);
        }else{
            if($this->master->delete('tbl_soal_tryout', $chk, 'id_soal')){
                $this->output_json(['status'=>true, 'total'=>count($chk)]);
            }
        }
    }
    public function adddetail($id)
	{
        $user = $this->ion_auth->user()->row();
		$data = [
			'user'      => $user,
			'judul'	    => 'Soal',
            'subjudul'  => 'Buat Soal',
            'id'        => $id,
            'paket'     =>$this->soal->getPaketSoalid($id),
        ];

        if($this->ion_auth->is_admin()){
            $data['dosen'] = $this->soal->getAllDosen();
        }else{
            $data['dosen'] = $this->soal->getMatkulDosen($user->username);
        }

		$this->load->view('_templates/dashboard/_header.php', $data);
		$this->load->view('soal-new/adddetail');
		$this->load->view('_templates/dashboard/_footer.php');
    }
    public function savedetail()
    {

        $method     = $this->input->post('method', true);
        $jenis_soal = $this->input->post('jenis_soal', true);

        if ($jenis_soal=='tkp') { 
            $jawaban    = 'null';
        } else {
            $jawaban    = $this->input->post('jawaban', true);
        }

        //bobot non tkp
        $bobot_benar = $this->input->post('bobot_benar', true);
        $bobot_salah = $this->input->post('bobot_salah', true);
        $bobot_kosong = $this->input->post('bobot_kosong', true);
        $pembahasan = $this->input->post('pembahasan', true);

        $this->validasi();
        $this->file_config();

        $id=$this->input->post('id_soal_paket', true);
        if($this->form_validation->run() === FALSE){
            $method==='add'? $this->add() : $this->edit();
        }else{
            $data = [
                'soal'      => $this->input->post('soal', true),
                'jawaban'   => $jawaban,
                'bobot'     => 0,
                'id_soal_paket'     => $this->input->post('id_soal_paket', true),
            ];
            
            $abjad = ['a', 'b', 'c', 'd', 'e'];
            
            // Inputan Opsi
            foreach ($abjad as $abj) {
                $data['opsi_'.$abj]    = $this->input->post('jawaban_'.$abj, true);
            }

            // Inputan bobot jawaban
            if ($jenis_soal=='tkp') {
                foreach ($abjad as $abjd) {
                    //new parameter here
                    $data['bobot_'.$abjd]    = $this->input->post('bobot_'.$abjd, true);
                }
            } else {
                //value non tkp

                if ($jawaban=='A') {
                    $data['bobot_a']    = $bobot_benar;
                    $data['bobot_b']    = $bobot_salah;
                    $data['bobot_c']    = $bobot_salah;
                    $data['bobot_d']    = $bobot_salah;
                    $data['bobot_e']    = $bobot_salah;
                    $data['bobot_kosong']    = $bobot_kosong;
                } elseif ($jawaban=='B') {
                    $data['bobot_a']    = $bobot_salah;
                    $data['bobot_b']    = $bobot_benar;
                    $data['bobot_c']    = $bobot_salah;
                    $data['bobot_d']    = $bobot_salah;
                    $data['bobot_e']    = $bobot_salah;
                    $data['bobot_kosong']    = $bobot_kosong;
                } elseif ($jawaban=='C') {
                    $data['bobot_a']    = $bobot_salah;
                    $data['bobot_b']    = $bobot_salah;
                    $data['bobot_c']    = $bobot_benar;
                    $data['bobot_d']    = $bobot_salah;
                    $data['bobot_e']    = $bobot_salah;
                    $data['bobot_kosong']    = $bobot_kosong;
                } elseif ($jawaban=='D') {
                    $data['bobot_a']    = $bobot_salah;
                    $data['bobot_b']    = $bobot_salah;
                    $data['bobot_c']    = $bobot_salah;
                    $data['bobot_d']    = $bobot_benar;
                    $data['bobot_e']    = $bobot_salah;
                    $data['bobot_kosong']    = $bobot_kosong;
                } elseif ($jawaban=='E') {
                    $data['bobot_a']    = $bobot_salah;
                    $data['bobot_b']    = $bobot_salah;
                    $data['bobot_c']    = $bobot_salah;
                    $data['bobot_d']    = $bobot_salah;
                    $data['bobot_e']    = $bobot_benar;
                    $data['bobot_kosong']    = $bobot_kosong;
                }
            }



            $i = 0;
            foreach ($_FILES as $key => $val) {
                $img_src = FCPATH.'uploads/bank_soal/';
                $getsoal = $this->soal->getSoalById($this->input->post('id_soal', true));
                
                $error = '';
                if($key === 'file_soal'){
                    if(!empty($_FILES['file_soal']['name'])){
                        if (!$this->upload->do_upload('file_soal')){
                            $error = $this->upload->display_errors();
                            show_error($error, 500, 'File Soal Error');
                            exit();
                        }else{
                            if($method === 'edit'){
                                if(!unlink($img_src.$getsoal->file)){
                                    show_error('Error saat delete gambar <br/>'.var_dump($getsoal), 500, 'Error Edit Gambar');
                                    exit();
                                }
                            }
                            $data['file'] = $this->upload->data('file_name');
                            $data['tipe_file'] = $this->upload->data('file_type');
                        }
                    }
                }else{
                    $file_abj = 'file_'.$abjad[$i];
                    if(!empty($_FILES[$file_abj]['name'])){    
                        if (!$this->upload->do_upload($key)){
                            $error = $this->upload->display_errors();
                            show_error($error, 500, 'File Opsi '.strtoupper($abjad[$i]).' Error');
                            exit();
                        }else{
                            if($method === 'edit'){
                                if(!unlink($img_src.$getsoal->$file_abj)){
                                    show_error('Error saat delete gambar', 500, 'Error Edit Gambar');
                                    exit();
                                }
                            }
                            $data[$file_abj] = $this->upload->data('file_name');
                        }
                    }
                    $i++;
                }
            }
                
            // if($this->ion_auth->is_admin()){
            //     $pecah = $this->input->post('dosen_id', true);
            //     $pecah = explode(':', $pecah);
            //     $data['dosen_id'] = $pecah[0];
            //     $data['matkul_id'] = end($pecah);
            // }else{
                $data['dosen_id'] = $this->input->post('dosen_id', true);
                $data['matkul_id'] = $this->input->post('matkul_id', true);
                $data['pembahasan']=$pembahasan;
            // }


            if($method==='add'){
                //push array
                $data['created_on'] = time();
                $data['updated_on'] = time();
                //insert data
                $this->master->create('tbl_soal_tryout', $data);
            }else if($method==='edit'){
                //push array
                $data['updated_on'] = time();
                //update data
                $id_soal = $this->input->post('id_soal', true);
                $this->master->update('tbl_soal_tryout', $data, 'id_soal', $id_soal);
            }else{
                show_error('Method tidak diketahui', 404);
            }
            redirect('SoalNew/detail/'.$id.'');
        }
    }
    public function editdetail($id)
	{
		$user = $this->ion_auth->user()->row();
        $soal = $this->soal->getSoalById($id);
        $id_paket_soal = $soal->id_soal_paket;
        
		$data = [
			'user'      => $user,
			'judul'	    => 'Soal',
            'subjudul'  => 'Edit Soal',
            'soal'      => $this->soal->getSoalById($id),
            'paket_soal' => $this->soal->getSoalPaketById($id_paket_soal),
        ];
        
        if($this->ion_auth->is_admin()){
            //Jika admin maka tampilkan semua matkul
            $data['dosen'] = $this->soal->getAllDosen();
        }else{
            //Jika bukan maka matkul dipilih otomatis sesuai matkul dosen
            $data['dosen'] = $this->soal->getMatkulDosen($user->username);
        }

		$this->load->view('_templates/dashboard/_header.php', $data);
		$this->load->view('soal-new/editdetail');
		$this->load->view('_templates/dashboard/_footer.php');
    }
    public function detailsoal($id)
    {
        $user = $this->ion_auth->user()->row();
		$data = [
			'user'      => $user,
			'judul'	    => 'Soal',
            'subjudul'  => 'Detail Soal',
            'soal'      => $this->soal->getSoalById($id),
        ];

        $this->load->view('_templates/dashboard/_header.php', $data);
		$this->load->view('soal-new/detailsoal');
		$this->load->view('_templates/dashboard/_footer.php');
    }

}