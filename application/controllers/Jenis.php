<?php
defined('BASEPATH') or exit('No direct script access allowed');

use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
use PhpOffice\PhpSpreadsheet\Writer\Xls;
use PhpOffice\PhpSpreadsheet\Writer\Csv;

class Jenis extends CI_Controller
{

	public function __construct()
	{
		parent::__construct();
		if (!$this->ion_auth->logged_in()) {
			redirect('auth');
		} else if (!$this->ion_auth->is_admin()) {
			show_error('Hanya Administrator yang diberi hak untuk mengakses halaman ini, <a href="' . base_url('dashboard') . '">Kembali ke menu awal</a>', 403, 'Akses Terlarang');
		}
		$this->load->library(['datatables', 'form_validation']); // Load Library Ignited-Datatables
		$this->load->model('Master_model', 'master');
		$this->form_validation->set_error_delimiters('', '');
	}

	public function output_json($data, $encode = true)
	{
		if ($encode) $data = json_encode($data);
		$this->output->set_content_type('application/json')->set_output($data);
	}

	public function index()
	{
		$data = [
			'user' => $this->ion_auth->user()->row(),
			'judul'	=> 'Jenis Soal',
			'subjudul' => 'Data Jenis Soal'
		];
		$this->load->view('_templates/dashboard/_header', $data);
		$this->load->view('master/jenis/data');
		$this->load->view('_templates/dashboard/_footer');
	}

	public function add()
	{
		$data = [
			'user' 		=> $this->ion_auth->user()->row(),
			'judul'		=> 'Tambah Jenis Soal',
			'subjudul'	=> 'Tambah Data Jenis Soal',
			'banyak'	=> $this->input->post('banyak', true)
		];
		$this->load->view('_templates/dashboard/_header', $data);
		$this->load->view('master/jenis/add');
		$this->load->view('_templates/dashboard/_footer');
	}

	public function data()
	{
		$this->output_json($this->master->getDatajenis(), false);
	}

	public function edit()
	{
		$chk = $this->input->post('checked', true);
		if (!$chk) {
			redirect('jenis');
		} else {
			$jenis = $this->master->getjenisById($chk);
			$data = [
				'user' 		=> $this->ion_auth->user()->row(),
				'judul'		=> 'Edit Jenis Soal',
				'subjudul'	=> 'Edit Data Jenis Soal',
				'jenis'	=> $jenis
			];
			$this->load->view('_templates/dashboard/_header', $data);
			$this->load->view('master/jenis/edit');
			$this->load->view('_templates/dashboard/_footer');
		}
	}

	public function save()
	{
		// print_r("sini");
		// exit;
		$rows = count($this->input->post('jenis_soal', true));
		$mode = $this->input->post('mode', true);
		for ($i = 1; $i <= $rows; $i++) {
			$jenis_soal = 'jenis_soal[' . $i . ']';
			$bobot_benar = 'bobot_benar[' . $i . ']';
			$bobot_salah = 'bobot_salah[' . $i . ']';
			$bobot_kosong = 'bobot_kosong[' . $i . ']';
			$waktu_pengerjaan = 'waktu_pengerjaan[' . $i . ']';
			$this->form_validation->set_rules($jenis_soal, 'jenis', 'required');
			$this->form_validation->set_rules($bobot_benar, 'jenis', 'required');
			$this->form_validation->set_rules($bobot_salah, 'jenis', 'required');
			$this->form_validation->set_rules($bobot_kosong, 'jenis', 'required');
			$this->form_validation->set_rules($waktu_pengerjaan, 'jenis', 'required');
			$this->form_validation->set_message('required', '{field} Wajib diisi');
// print_r($bobot_benar);
					// exit;
			if ($this->form_validation->run() === FALSE) {
				$error[] = [
					$jenis_soal => form_error($jenis_soal)
				];
				$status = FALSE;
			} else {
				// print_r($mode);
				if ($mode == 'add') {
					$insert[] = [
						'jenis_soal' => $this->input->post($jenis_soal, true),
						'bobot_benar' => $this->input->post($bobot_benar, true),
						'bobot_salah' => $this->input->post($bobot_salah, true),
						'bobot_kosong' => $this->input->post($bobot_kosong, true),
						'waktu_pengerjaan' => $this->input->post($waktu_pengerjaan, true)
					];
					// print_r($insert);
					// exit;
				} else if ($mode == 'edit') {
					$update[] = array(
						'id'	=> $this->input->post('id[' . $i . ']', true),
						'jenis_soal' => $this->input->post($jenis_soal, true),
						'bobot_benar' => $this->input->post($bobot_benar, true),
						'bobot_salah' => $this->input->post($bobot_salah, true),
						'bobot_kosong' => $this->input->post($bobot_kosong, true),
						'waktu_pengerjaan' => $this->input->post($waktu_pengerjaan, true)
					);
				}
				$status = TRUE;
			}
		}
		if ($status) {
			if ($mode == 'add') {
				$this->master->create('jenis_soal', $insert, true);
				$data['insert']	= $insert;
			} else if ($mode == 'edit') {
				$this->master->update('jenis_soal', $update, 'id', null, true);
				$data['update'] = $update;
			}
		} else {
			if (isset($error)) {
				$data['errors'] = $error;
			}
		}
		$data['status'] = $status;
		$this->output_json($data);
	}

	public function delete()
	{
		$chk = $this->input->post('checked', true);
		if (!$chk) {
			$this->output_json(['status' => false]);
		} else {
			if ($this->master->delete('jenis_soal', $chk, 'id')) {
				$this->output_json(['status' => true, 'total' => count($chk)]);
			}
		}
	}

	public function load_jenis()
	{
		$data = $this->master->getjenis();
		$this->output_json($data);
	}

	public function import($import_data = null)
	{
		$data = [
			'user' => $this->ion_auth->user()->row(),
			'judul'	=> 'Jenis Soal',
			'subjudul' => 'Import Jenis Soal'
		];
		if ($import_data != null) $data['import'] = $import_data;

		$this->load->view('_templates/dashboard/_header', $data);
		$this->load->view('master/jenis/import');
		$this->load->view('_templates/dashboard/_footer');
	}

	public function preview()
	{
		$config['upload_path']		= './uploads/import/';
		$config['allowed_types']	= 'xls|xlsx|csv';
		$config['max_size']			= 2048;
		$config['encrypt_name']		= true;

		$this->load->library('upload', $config);

		if (!$this->upload->do_upload('upload_file')) {
			$error = $this->upload->display_errors();
			echo $error;
			die;
		} else {
			$file = $this->upload->data('full_path');
			$ext = $this->upload->data('file_ext');

			switch ($ext) {
				case '.xlsx':
					$reader = new \PhpOffice\PhpSpreadsheet\Reader\Xlsx();
					break;
				case '.xls':
					$reader = new \PhpOffice\PhpSpreadsheet\Reader\Xls();
					break;
				case '.csv':
					$reader = new \PhpOffice\PhpSpreadsheet\Reader\Csv();
					break;
				default:
					echo "unknown file ext";
					die;
			}

			$spreadsheet = $reader->load($file);
			$sheetData = $spreadsheet->getActiveSheet()->toArray();
			$jenis = [];
			for ($i = 1; $i < count($sheetData); $i++) {
				if ($sheetData[$i][0] != null) {
					$jenis[] = $sheetData[$i][0];
				}
			}

			unlink($file);

			$this->import($jenis);
		}
	}
	public function do_import()
	{
		$data = json_decode($this->input->post('jenis', true));
		$jenis = [];
		foreach ($data as $j) {
			$jenis[] = ['nama_jenis' => $j];
		}

		$save = $this->master->create('jenis', $jenis, true);
		if ($save) {
			redirect('jenis');
		} else {
			redirect('jenis/import');
		}
	}
}
