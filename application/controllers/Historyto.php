<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Historyto extends CI_Controller {

	public $mhs, $user;

	public function __construct(){
		parent::__construct();
		if (!$this->ion_auth->logged_in()){
			redirect('auth');
		}
		$this->load->library(['datatables', 'form_validation']);// Load Library Ignited-Datatables
		$this->load->helper('my');
		$this->load->model('Master_model', 'master');
		$this->load->model('Soal_model', 'soal');
		$this->load->model('Ujian_model', 'ujian');
		$this->form_validation->set_error_delimiters('','');

		$this->user = $this->ion_auth->user()->row();
		$this->mhs 	= $this->ujian->getIdMahasiswa($this->user->username);
    }


    public function akses_mahasiswa()
    {
        if ( !$this->ion_auth->in_group('mahasiswa') ){
			show_error('Halaman ini khusus untuk mahasiswa mengikuti ujian, <a href="'.base_url('dashboard').'">Kembali ke menu awal</a>', 403, 'Akses Terlarang');
		}
    }


	/**
	 * BAGIAN MAHASISWA
	 */
    public function output_json($data, $encode = true)
	{
        if($encode) $data = json_encode($data);
        $this->output->set_content_type('application/json')->set_output($data);
	}
	public function list_json()
	{
		$this->akses_mahasiswa();
		$list = $this->ujian->getListUjian($this->mhs->id_mahasiswa, $this->mhs->id_program);
		$this->output_json($list, false);
	}

	public function index()
	{
		$this->akses_mahasiswa();

		$user = $this->ion_auth->user()->row();
		
		$data = [
			'user' 		=> $user,
			'judul'		=> 'History',
			'subjudul'	=> 'Tryout',
			'mhs' 		=> $this->ujian->getIdMahasiswa($user->username),
		];
		$this->load->view('_templates/dashboard/_header.php', $data);
		$this->load->view('ujian/historyto');
		$this->load->view('_templates/dashboard/_footer.php');
	}
	public function detail($id)
	{
        $this->akses_mahasiswa();
        $ujian = $this->ujian->getUjianById($id);

        $user = $this->ion_auth->user()->row();
        $mhs=$this->ujian->getIdMahasiswa($user->username);
		$hasil=$this->ujian->getHistoryTo($id,$mhs->id_mahasiswa);
		$jawaban=$this->ujian->getListSoal($id,$mhs->id_mahasiswa);

		$data = [
			'user' 		=> $user,
			'judul'		=> 'Detail Hasil',
			'subjudul'	=> 'Tryout',
            'mhs' 		=> $mhs,
            'ujian'	    => $ujian,
            'hasil'	    => $hasil,
            'jawaban'	=> $jawaban,
		];
		$this->load->view('_templates/dashboard/_header.php', $data);
		$this->load->view('ujian/detailhistoryto');
		$this->load->view('_templates/dashboard/_footer.php');
	}
	public function cetak_detail($id)
	{
        $this->akses_mahasiswa();
        $ujian = $this->ujian->getUjianById($id);
        $this->load->library('Pdf');
        $user = $this->ion_auth->user()->row();
        $mhs=$this->ujian->getIdMahasiswa($user->username);
        // print_r($mhs);
		$hasil=$this->ujian->getHistoryTo($id,$mhs->id_mahasiswa);
		$jawaban=$this->ujian->getListSoal($id,$mhs->id_mahasiswa);
		$data = [
			'user' 		=> $user,
			'judul'		=> 'Detail Hasil',
			'subjudul'	=> 'Tryout',
            'mhs' 		=> $mhs,
            'ujian'	    => $ujian,
            'hasil'	    => $hasil,
            'jawaban'	=> $jawaban,
		];
		$this->load->view('ujian/cetak_history', $data);
	}
}