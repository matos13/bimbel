<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Kabupaten extends CI_Controller
{

	public function __construct()
	{
		parent::__construct();
		if (!$this->ion_auth->logged_in()) {
			redirect('auth');
		} else if (!$this->ion_auth->is_admin()) {
			show_error('Hanya Administrator yang diberi hak untuk mengakses halaman ini, <a href="' . base_url('dashboard') . '">Kembali ke menu awal</a>', 403, 'Akses Terlarang');
		}
		$this->load->library(['datatables', 'form_validation']); // Load Library Ignited-Datatables
		$this->load->model('Master_model', 'master');
		$this->form_validation->set_error_delimiters('', '');
	}

	public function output_json($data, $encode = true)
	{
		if ($encode) $data = json_encode($data);
		$this->output->set_content_type('application/json')->set_output($data);
	}

	public function index()
	{
		$data = [
			'user' => $this->ion_auth->user()->row(),
			'judul'	=> 'Kabupaten',
			'subjudul' => 'Data Kabupaten'
		];
		$this->load->view('_templates/dashboard/_header.php', $data);
		$this->load->view('master/kabupaten/data');
		$this->load->view('_templates/dashboard/_footer.php');
	}

	public function data()
	{
		$this->output_json($this->master->getDatakabupaten(), false);
	}

	public function add()
	{
		$data = [
			'user' 		=> $this->ion_auth->user()->row(),
			'judul'		=> 'Tambah Kabupaten',
			'subjudul'	=> 'Tambah Data Kabupaten',
			'banyak'	=> $this->input->post('banyak', true),
			'provinsi'	=> $this->master->getAllprovinsi()
		];
		$this->load->view('_templates/dashboard/_header.php', $data);
		$this->load->view('master/kabupaten/add');
		$this->load->view('_templates/dashboard/_footer.php');
	}

	public function edit()
	{
		
		$chk = $this->input->post('checked', true);
		if (!$chk) {
			redirect('admin/kabupaten');
		} else {
			$kabupaten = $this->master->getkabupatenById($chk);
			$data = [
				'user' 		=> $this->ion_auth->user()->row(),
				'judul'		=> 'Edit Kabupaten',
				'subjudul'	=> 'Edit Data Kabupaten',
				'provinsi'	=> $this->master->getAllprovinsi(),
				'kabupaten'		=> $kabupaten
			];
			$this->load->view('_templates/dashboard/_header.php', $data);
			$this->load->view('master/kabupaten/edit');
			$this->load->view('_templates/dashboard/_footer.php');
		}
	}

	public function save()
	{
		$rows = count($this->input->post('nama', true));
		$mode = $this->input->post('mode', true);
		for ($i = 1; $i <= $rows; $i++) {
			$nama 	= 'nama[' . $i . ']';
			$provinsi_id 	= 'provinsi_id[' . $i . ']';
			$this->form_validation->set_rules($nama, 'kabupaten', 'required');
			$this->form_validation->set_rules($provinsi_id, 'provinsi', 'required');
			$this->form_validation->set_message('required', '{field} Wajib diisi');

			if ($this->form_validation->run() === FALSE) {
				$error[] = [
					$nama 	=> form_error($nama),
					$provinsi_id 	=> form_error($provinsi_id),
				];
				$status = FALSE;
			} else {
				if ($mode == 'add') {
					$insert[] = [
						'nama' 	=> $this->input->post($nama, true),
						'id_provinsi' 	=> $this->input->post($provinsi_id, true)
					];
				} else if ($mode == 'edit') {
					$update[] = array(
						'id'		=> $this->input->post('id[' . $i . ']', true),
						'nama' 	=> $this->input->post($nama, true),
						'id_provinsi' 	=> $this->input->post($provinsi_id, true)
					);
				}
				$status = TRUE;
			}
		}
		if ($status) {
			if ($mode == 'add') {
				$this->master->create('kabupaten', $insert, true);
				$data['insert']	= $insert;
			} else if ($mode == 'edit') {
				$this->master->update('kabupaten', $update, 'id', null, true);
				$data['update'] = $update;
			}
		} else {
			if (isset($error)) {
				$data['errors'] = $error;
			}
		}
		$data['status'] = $status;
		$this->output_json($data);
	}

	public function delete()
	{
		$chk = $this->input->post('checked', true);
		if (!$chk) {
			$this->output_json(['status' => false]);
		} else {
			if ($this->master->delete('kabupaten', $chk, 'id')) {
				$this->output_json(['status' => true, 'total' => count($chk)]);
			}
		}
	}

	public function kabupaten_by_provinsi($id)
	{
		$data = $this->master->getkabupatenByprovinsi($id);
		$this->output_json($data);
	}

	public function import($import_data = null)
	{
		$data = [
			'user' => $this->ion_auth->user()->row(),
			'judul'	=> 'Kabupaten',
			'subjudul' => 'Import Kabupaten',
			'provinsi' => $this->master->getAllprovinsi()
		];
		if ($import_data != null) $data['import'] = $import_data;

		$this->load->view('_templates/dashboard/_header', $data);
		$this->load->view('master/kabupaten/import');
		$this->load->view('_templates/dashboard/_footer');
	}

	public function preview()
	{
		$config['upload_path']		= './uploads/import/';
		$config['allowed_types']	= 'xls|xlsx|csv';
		$config['max_size']			= 2048;
		$config['encrypt_name']		= true;

		$this->load->library('upload', $config);

		if (!$this->upload->do_upload('upload_file')) {
			$error = $this->upload->display_errors();
			echo $error;
			die;
		} else {
			$file = $this->upload->data('full_path');
			$ext = $this->upload->data('file_ext');

			switch ($ext) {
				case '.xlsx':
					$reader = new \PhpOffice\PhpSpreadsheet\Reader\Xlsx();
					break;
				case '.xls':
					$reader = new \PhpOffice\PhpSpreadsheet\Reader\Xls();
					break;
				case '.csv':
					$reader = new \PhpOffice\PhpSpreadsheet\Reader\Csv();
					break;
				default:
					echo "unknown file ext";
					die;
			}

			$spreadsheet = $reader->load($file);
			$sheetData = $spreadsheet->getActiveSheet()->toArray();
			$data = [];
			for ($i = 1; $i < count($sheetData); $i++) {
				$data[] = [
					'kabupaten' => $sheetData[$i][0],
					'provinsi' => $sheetData[$i][1]
				];
			}

			unlink($file);

			$this->import($data);
		}
	}
	public function do_import()
	{
		$input = json_decode($this->input->post('data', true));
		$data = [];
		foreach ($input as $d) {
			$data[] = ['nama' => $d->kabupaten, 'id_provinsi' => $d->provinsi];
		}

		$save = $this->master->create('kabupaten', $data, true);
		if ($save) {
			redirect('kabupaten');
		} else {
			redirect('kabupaten/import');
		}
	}
	
}
