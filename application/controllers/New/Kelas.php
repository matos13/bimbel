<?php
defined('BASEPATH') or exit('No direct script access allowed');

use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
use PhpOffice\PhpSpreadsheet\Writer\Xls;
use PhpOffice\PhpSpreadsheet\Writer\Csv;

class Kelas extends CI_Controller
{

	public function __construct()
	{
		parent::__construct();
		if (!$this->ion_auth->logged_in()) {
			redirect('auth');
		} else if (!$this->ion_auth->is_admin()) {
			show_error('Hanya Administrator yang diberi hak untuk mengakses halaman ini, <a href="' . base_url('dashboard') . '">Kembali ke menu awal</a>', 403, 'Akses Terlarang');
		}
		$this->load->library(['datatables', 'form_validation']); // Load Library Ignited-Datatables
		$this->load->model('new/Master_model', 'master');
		$this->form_validation->set_error_delimiters('', '');
	}

	public function output_json($data, $encode = true)
	{
		if ($encode) $data = json_encode($data);
		$this->output->set_content_type('application/json')->set_output($data);
	}

	public function index()
	{
		$data = [
			'user' => $this->ion_auth->user()->row(),
			'judul'	=> 'Kelas',
			'subjudul' => 'Data Kelas'
		];
		$this->load->view('_templates/dashboard/_header', $data);
		$this->load->view('new/kelas/data');
		$this->load->view('_templates/dashboard/_footer');
	}

	public function add()
	{
		$data = [
			'user' 		=> $this->ion_auth->user()->row(),
			'judul'		=> 'Tambah Kelas',
			'subjudul'	=> 'Tambah Data Kelas',
			'banyak'	=> $this->input->post('banyak', true),
			'program'	=> $this->master->getAllProgramBelajar()
		];
		$this->load->view('_templates/dashboard/_header', $data);
		$this->load->view('new/kelas/add');
		$this->load->view('_templates/dashboard/_footer');
	}

	public function data()
	{
		$this->output_json($this->master->getDatakelas(), false);
	}

	public function edit()
	{
		$chk = $this->input->post('checked', true);
		if (!$chk) {
			redirect('new/Mata_pelajaran');
		} else {
			$kelas = $this->master->getkelasById($chk);
			$data = [
				'user' 		=> $this->ion_auth->user()->row(),
				'judul'		=> 'Edit Kelas',
				'subjudul'	=> 'Edit Data Kelas',
				'kelas'		=> $kelas,
				'program'	=> $this->master->getAllProgramBelajar()
			];
			$this->load->view('_templates/dashboard/_header.php', $data);
			$this->load->view('new/kelas/edit');
			$this->load->view('_templates/dashboard/_footer.php');
		}
	}

	public function save()
	{
		$rows = count($this->input->post('nama', true));
        $mode = $this->input->post('mode', true);
        // print_r($_POST); 
		for ($i = 1; $i <= $rows; $i++) {
			$nama 	= 'nama[' . $i . ']';
			$program 	= 'program[' . $i . ']';
			$this->form_validation->set_rules($nama, 'Kelas', 'required');
			$this->form_validation->set_rules($program, 'Kelas', 'required');
			$this->form_validation->set_message('required', '{field} Wajib diisi');

			if ($this->form_validation->run() === FALSE) {
				$error[] = [
					$nama 	=> form_error($nama),
					$program 	=> form_error($program)
				];
				$status = FALSE;
			} else {
                
				if ($mode == 'add') {
					$insert[] = [
						'nama' 	=> $this->input->post($nama, true),
						'id_program' 	=> $this->input->post($program, true)
					];
				} else if ($mode == 'edit') {
					$update[] = array(
						'id'   => $this->input->post('id[' . $i . ']', true),
						'nama' 	=> $this->input->post($nama, true),
						'id_program' 	=> $this->input->post($program, true)
					);
				}
				$status = TRUE;
			}
        }
		if ($status) {
			if ($mode == 'add') {
				$this->master->create('tbl_kelas', $insert, true);
				$data['insert']	= $insert;
			} else if ($mode == 'edit') {
				$this->master->update('tbl_kelas', $update, 'id', null, true);
				$data['update'] = $update;
			}
		} else {
			if (isset($error)) {
				$data['errors'] = $error;
			}
		}
		$data['status'] = $status;
		$this->output_json($data);
	}

	public function delete()
	{
		$chk = $this->input->post('checked', true);
		if (!$chk) {
			$this->output_json(['status' => false]);
		} else {
			if ($this->master->delete('tbl_kelas', $chk, 'id')) {
				$this->output_json(['status' => true, 'total' => count($chk)]);
			}
		}
	}


}
