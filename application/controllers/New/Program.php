<?php
defined('BASEPATH') or exit('No direct script access allowed');

use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
use PhpOffice\PhpSpreadsheet\Writer\Xls;
use PhpOffice\PhpSpreadsheet\Writer\Csv;

class Program extends CI_Controller
{

	public function __construct()
	{
		parent::__construct();
		if (!$this->ion_auth->logged_in()) {
			redirect('auth');
		} else if (!$this->ion_auth->is_admin()) {
			show_error('Hanya Administrator yang diberi hak untuk mengakses halaman ini, <a href="' . base_url('dashboard') . '">Kembali ke menu awal</a>', 403, 'Akses Terlarang');
		}
		$this->load->library(['datatables', 'form_validation']); // Load Library Ignited-Datatables
		$this->load->model('new/Master_model', 'master');
		$this->form_validation->set_error_delimiters('', '');
	}

	public function output_json($data, $encode = true)
	{
		if ($encode) $data = json_encode($data);
		$this->output->set_content_type('application/json')->set_output($data);
	}

	public function index()
	{
		$data = [
			'user' => $this->ion_auth->user()->row(),
			'judul'	=> 'Program Belajar',
			'subjudul' => 'Data Program Belajar'
		];
		$this->load->view('_templates/dashboard/_header', $data);
		$this->load->view('new/program/data');
		$this->load->view('_templates/dashboard/_footer');
	}

	public function add()
	{
		$data = [
			'user' 		=> $this->ion_auth->user()->row(),
			'judul'		=> 'Tambah Program Belajar',
			'subjudul'	=> 'Tambah Data Program Belajar',
			'banyak'	=> $this->input->post('banyak', true),
		];
		$this->load->view('_templates/dashboard/_header', $data);
		$this->load->view('new/program/add');
		$this->load->view('_templates/dashboard/_footer');
	}

	public function data()
	{
		$this->output_json($this->master->getDataProgram(), false);
	}

	public function edit()
	{
		$chk = $this->input->post('checked', true);
		if (!$chk) {
			redirect('new/Program');
		} else {
			$program = $this->master->getProgramById($chk);
			$data = [
				'user' 		=> $this->ion_auth->user()->row(),
				'judul'		=> 'Edit Program Belajar',
				'subjudul'	=> 'Edit Data Program Belajar',
				'program'		=> $program
			];
			$this->load->view('_templates/dashboard/_header.php', $data);
			$this->load->view('new/program/edit');
			$this->load->view('_templates/dashboard/_footer.php');
		}
	}

	public function save()
	{
		$rows = count($this->input->post('nama', true));
        $mode = $this->input->post('mode', true);
        // print_r($_POST); 
		for ($i = 1; $i <= $rows; $i++) {
			$nama 	= 'nama[' . $i . ']';
			$this->form_validation->set_rules($nama, 'Program Belajar', 'required');
			$this->form_validation->set_message('required', '{field} Wajib diisi');

			if ($this->form_validation->run() === FALSE) {
				$error[] = [
					$nama 	=> form_error($nama)
				];
				$status = FALSE;
			} else {
                
				if ($mode == 'add') {
					$insert[] = [
						'nama' 	=> $this->input->post($nama, true)
					];
				} else if ($mode == 'edit') {
					$update[] = array(
						'id'   => $this->input->post('id[' . $i . ']', true),
						'nama' 	=> $this->input->post($nama, true)
					);
				}
				$status = TRUE;
			}
        }
		if ($status) {
			if ($mode == 'add') {
				$this->master->create('tbl_program_belajar', $insert, true);
				$data['insert']	= $insert;
			} else if ($mode == 'edit') {
				$this->master->update('tbl_program_belajar', $update, 'id', null, true);
				$data['update'] = $update;
			}
		} else {
			if (isset($error)) {
				$data['errors'] = $error;
			}
		}
		$data['status'] = $status;
		$this->output_json($data);
	}

	public function delete()
	{
		$chk = $this->input->post('checked', true);
		if (!$chk) {
			$this->output_json(['status' => false]);
		} else {
			if ($this->master->delete('tbl_program_belajar', $chk, 'id')) {
				$this->output_json(['status' => true, 'total' => count($chk)]);
			}
		}
	}


}
