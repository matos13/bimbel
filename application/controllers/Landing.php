<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Landing extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */

	public function __construct()
	{
		parent::__construct();
		$this->load->library(['datatables', 'form_validation']); // Load Library Ignited-Datatables
		$this->load->model('Master_model', 'master');
		$this->form_validation->set_error_delimiters('', '');
	}
	public function index()
	{
		$this->load->view('landing/index');
	}

	public function kompetisi()
	{
		$this->load->view('landing/kompetisi/index');
	}
	public function kontak()
	{
		$this->load->view('landing/kontak/index');
	}
	public function registrasi()
	{

		$data = [
			'user' => $this->ion_auth->user()->row(),
			'prov' =>$this->master->getAllProv(), 
			'kab' =>$this->master->getAllKab(), 
			'kat' =>$this->master->getAllJur(), 
		];
		$this->load->view('landing/registrasi/index',$data);
	}
	public function ProsesRegistrasi()
	{
		// print_r($_POST);
		// exit;
		$input = [
			'email' 		=> $this->input->post('email', true),
			'nim' 			=> $this->input->post('username', true),
			'nama' 			=> $this->input->post('nama_lengkap', true),
			'jenis_kelamin' => $this->input->post('jenis_kelamin', true),
			'id_provinsi'   => $this->input->post('provinsi', true),
			'id_kabupaten'  => $this->input->post('kabupaten', true),
			'jurusan_id' 	=> $this->input->post('alamat', true),
		];
		$action = $this->master->create('mahasiswa', $input);

		if ($action) {
			# code...
			$nama = explode(' ', $_POST['nama_lengkap']);
			$first_name = $nama[0];
			$last_name = end($nama);

			$username=$_POST['username'];
			$password=$_POST['password'];
			$email=$_POST['email'];
			$additional_data = [
				'first_name'	=> $first_name,
				'last_name'		=> $last_name
			];
		$group = array('3'); // Sets user to dosen.

		if ($this->ion_auth->username_check($username)) {
			$data = [
				'status' => false,
				'msg'	 => 'Username tidak tersedia (sudah digunakan).'
			];
		} else if ($this->ion_auth->email_check($email)) {
			$data = [
				'status' => false,
				'msg'	 => 'Email tidak tersedia (sudah digunakan).'
			];
		} else {
			$this->ion_auth->register($username, $password, $email, $additional_data, $group);
			$data = [
				'status'	=> true,
				'msg'	 => 'User berhasil dibuat Silahkan Menggunkan email untuk login'
			];
		}

	}
	// print_r($data);
	redirect('/landing/registrasi',$data);
		// $this->output_json($data);

		// $data = array(
		// 	'username' => $_POST['username'],
		// 	'password' => md5($_POST['password']),
		// 	'email' =>$_POST['email'] ,
		// 	'first_name' => $_POST['nama_lengkap'],
		// 	'phone' => $_POST['nomor_hp'],
		//  );
		//  $insert = $this->db->insert('user', $data);
		// print_r($data);
		// exit;
}
public function getKabByidprovinsi(){
	// print_r($_POST);
	// exit();
	$data=$this->master->getAllKabProv($_POST['id']);
	echo json_encode($data);
}
}
