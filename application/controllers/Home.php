<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */

	public function __construct()
	{
		parent::__construct();
		$this->load->library(['datatables', 'form_validation']); // Load Library Ignited-Datatables
		$this->load->model('Master_model', 'master');
		$this->form_validation->set_error_delimiters('', '');
	}
	public function index()
	{
		$this->load->view('landingv2/index');
    }
	public function daftar()
	{

		$data = [
			'user' => $this->ion_auth->user()->row(),
			'prov' =>$this->master->getAllProv(),
			'kab' =>$this->master->getAllKab(),
			'kat' =>$this->master->getAllProgram(),
		];
		$this->load->view('landingv2/register',$data);
    }
		public function ProsesRegistrasi()
		{
			
			$username=$this->input->post('username');
			$email=$this->input->post('email');
			
			if ($this->ion_auth->username_check($username)) {
				$msg = [
					'status' => false,
					'msg'	 => 'Username tidak tersedia (sudah digunakan).'
				];
			} else if ($this->ion_auth->email_check($email)) {
				$msg = [
					'status' => false,
					'msg'	 => 'Email tidak tersedia (sudah digunakan).'
				];
			} else {
			$mhsdata = [
				'id_program' => $this->input->post('jurusan', true),	
				'nama_lengkap' => $this->input->post('nama_lengkap', true),
				'username' => $this->input->post('username'),
				'password' => $this->input->post('password'),
				'email' => $this->input->post('email'),
				'nomor_hp' => $this->input->post('nomor_hp'),
				'jenis_kelamin' => $this->input->post('jenis_kelamin'),
				'kabupaten' => $this->input->post('kabupaten'),
				'alamat' => $this->input->post('alamat'),
			];    

			$simpan= $this->db->insert('pendaftaran', $mhsdata);

			if ($simpan) {
				$msg=[
					"status"=>"200",
					"msg"=>"Pendaftaran Berhasil, Silahkan Menunggu Approve dari Admin"
				];
				// die (json_encode($msg));
			}else{
				$msg=[
					"status"=>"201",
					"msg"=>"Pendaftaran gagal, Silahkan Cek Data Anda Kembali"
				];
				// die (json_encode($msg));
			}
		}
			
		die (json_encode($msg));
	}
}
