<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class HasilUjianKuis extends CI_Controller {

	public function __construct(){
		parent::__construct();
		if (!$this->ion_auth->logged_in()){
			redirect('auth');
		}
		
		$this->load->library(['datatables']);// Load Library Ignited-Datatables
		$this->load->model('Master_model', 'master');
		$this->load->model('Ujian_model', 'ujian');
		
		$this->user = $this->ion_auth->user()->row();
	}

	public function output_json($data, $encode = true)
	{
		if($encode) $data = json_encode($data);
		$this->output->set_content_type('application/json')->set_output($data);
	}

	public function data()
	{
		$nip_dosen = null;
		$id_kelas = null;
		
		if( $this->ion_auth->in_group('dosen') ) {
			$nip_dosen = $this->user->username;
		}
		if( $this->ion_auth->in_group('mahasiswa') ) {
			$nim = $this->user->username;
			$mhs =$this->master->getMahasiswaByNim($nim);
			$id_kelas=$mhs->kelas_id;
		}

		$this->output_json($this->ujian->getHasilUjianKuis($nip_dosen,$id_kelas), false);
	}

	public function NilaiMhs($id)
	{
		$this->output_json($this->ujian->HslUjianByIdKuis($id, true), false);
	}

	public function index()
	{
		$data = [
			'user' => $this->user,
			'judul'	=> 'Kuis',
			'subjudul'=> 'Hasil Kuis',
		];
		$this->load->view('_templates/dashboard/_header.php', $data);
		$this->load->view('ujiankuis/hasil_kuis');
		$this->load->view('_templates/dashboard/_footer.php');
	}
	
	public function detail($id)
	{
		$ujian = $this->ujian->getUjianKuisById($id);
		$nilai = $this->ujian->bandingNilaiKuis($id);

		$data = [
			'user' => $this->user,
			'judul'	=> 'Kuis',
			'subjudul'=> 'Detail Hasil Kuis',
			'ujian'	=> $ujian,
			'nilai'	=> $nilai
		];

		$this->load->view('_templates/dashboard/_header.php', $data);
		$this->load->view('ujiankuis/detail_hasil_kuis');
		$this->load->view('_templates/dashboard/_footer.php');
	}

	public function cetak($id)
	{
		$this->load->library('Pdf');

		$mhs 	= $this->ujian->getIdMahasiswa($this->user->username);
		$hasil 	= $this->ujian->HslUjianKuis($id, $mhs->id_mahasiswa)->row();
		$ujian 	= $this->ujian->getUjianKuisById($id);
		
		$data = [
			'ujian' => $ujian,
			'hasil' => $hasil,
			'mhs'	=> $mhs
		];
		
		$this->load->view('ujiankuis/cetak_kuis', $data);
	}

	public function cetak_detail($id)
	{
		$this->load->library('Pdf');

		$ujian = $this->ujian->getUjianKuisById($id);
		$nilai = $this->ujian->bandingNilaiKuis($id);
		$hasil = $this->ujian->HslUjianByIdKuis($id)->result();

		$data = [
			'ujian'	=> $ujian,
			'nilai'	=> $nilai,
			'hasil'	=> $hasil
		];

		$this->load->view('ujiankuis/cetak_detail_kuis', $data);
	}
	
}