<?php
defined('BASEPATH') or exit('No direct script access allowed');

use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
use PhpOffice\PhpSpreadsheet\Writer\Xls;
use PhpOffice\PhpSpreadsheet\Writer\Csv;

class Bab extends CI_Controller
{

	public function __construct()
	{
		parent::__construct();
		if (!$this->ion_auth->logged_in()) {
			redirect('auth');
		} else if (!$this->ion_auth->is_admin()) {
			show_error('Hanya Administrator yang diberi hak untuk mengakses halaman ini, <a href="' . base_url('dashboard') . '">Kembali ke menu awal</a>', 403, 'Akses Terlarang');
		}
		$this->load->library(['datatables', 'form_validation']); // Load Library Ignited-Datatables
		$this->load->model('Master_model', 'master');
		$this->form_validation->set_error_delimiters('', '');
	}

	public function output_json($data, $encode = true)
	{
		if ($encode) $data = json_encode($data);
		$this->output->set_content_type('application/json')->set_output($data);
	}

	public function index()
	{
		$data = [
			'user' => $this->ion_auth->user()->row(),
			'judul'	=> 'Bab',
			'subjudul' => 'Data Bab'
		];
		$this->load->view('_templates/dashboard/_header', $data);
		$this->load->view('master/bab/data');
		$this->load->view('_templates/dashboard/_footer');
	}

	public function add()
	{
		$data = [
			'user' 		=> $this->ion_auth->user()->row(),
			'judul'		=> 'Tambah Bab',
			'subjudul'	=> 'Tambah Data Bab',
			'banyak'	=> $this->input->post('banyak', true),
			'kelas'		=> $this->master->getallmapel()
		];
		$this->load->view('_templates/dashboard/_header', $data);
		$this->load->view('master/bab/add');
		$this->load->view('_templates/dashboard/_footer');
	}

	public function data()
	{
		$this->output_json($this->master->getDatabab(), false);
	}

	public function edit()
	{
		$chk = $this->input->post('checked', true);
		if (!$chk) {
			redirect('admin/bab');
		} else {
			$bab = $this->master->getbabById($chk);
			$data = [
				'user' 		=> $this->ion_auth->user()->row(),
				'judul'		=> 'Edit bab',
				'subjudul'	=> 'Edit Data bab',
				'kelas'		=> $this->master->getallmapel(),
				'bab'		=> $bab
			];
			$this->load->view('_templates/dashboard/_header.php', $data);
			$this->load->view('master/bab/edit');
			$this->load->view('_templates/dashboard/_footer.php');
		}
	}

	public function save()
	{
		$rows = count($this->input->post('nama_bab', true));
		$mode = $this->input->post('mode', true);
		for ($i = 1; $i <= $rows; $i++) {
			$nama_bab 	= 'nama_bab[' . $i . ']';
			$kelas_id 	= 'kelas_id[' . $i . ']';
			$this->form_validation->set_rules($nama_bab, 'Bab', 'required');
			$this->form_validation->set_rules($kelas_id, 'Mata Pelajaran', 'required');
			$this->form_validation->set_message('required', '{field} Wajib diisi');

			if ($this->form_validation->run() === FALSE) {
				$error[] = [
					$nama_bab 	=> form_error($nama_bab),
					$kelas_id 	=> form_error($kelas_id),
				];
				$status = FALSE;
			} else {
				if ($mode == 'add') {
					$insert[] = [
						'nama_bab' 	=> $this->input->post($nama_bab, true),
						'id_mapel' 	=> $this->input->post($kelas_id, true)
					];
				} else if ($mode == 'edit') {
					$update[] = array(
						'id'		=> $this->input->post('id[' . $i . ']', true),
						'nama_bab' 	=> $this->input->post($nama_bab, true),
						'id_mapel' 	=> $this->input->post($kelas_id, true)
					);
				}
				$status = TRUE;
			}
		}
		if ($status) {
			if ($mode == 'add') {
				$this->master->create('bab', $insert, true);
				$data['insert']	= $insert;
			} else if ($mode == 'edit') {
				$this->master->update('bab', $update, 'id', null, true);
				$data['update'] = $update;
			}
		} else {
			if (isset($error)) {
				$data['errors'] = $error;
			}
		}
		$data['status'] = $status;
		$this->output_json($data);
	}

	public function delete()
	{
		$chk = $this->input->post('checked', true);
		if (!$chk) {
			$this->output_json(['status' => false]);
		} else {
			if ($this->master->delete('bab', $chk, 'id')) {
				$this->output_json(['status' => true, 'total' => count($chk)]);
			}
		}
	}

	public function load_jurusan()
	{
		$data = $this->master->getJurusan();
		$this->output_json($data);
	}

	public function import($import_data = null)
	{
		$data = [
			'user' => $this->ion_auth->user()->row(),
			'judul'	=> 'Kategori Mapel',
			'subjudul' => 'Import Kategori Mapel'
		];
		if ($import_data != null) $data['import'] = $import_data;

		$this->load->view('_templates/dashboard/_header', $data);
		$this->load->view('master/jurusan/import');
		$this->load->view('_templates/dashboard/_footer');
	}

	public function preview()
	{
		$config['upload_path']		= './uploads/import/';
		$config['allowed_types']	= 'xls|xlsx|csv';
		$config['max_size']			= 2048;
		$config['encrypt_name']		= true;

		$this->load->library('upload', $config);

		if (!$this->upload->do_upload('upload_file')) {
			$error = $this->upload->display_errors();
			echo $error;
			die;
		} else {
			$file = $this->upload->data('full_path');
			$ext = $this->upload->data('file_ext');

			switch ($ext) {
				case '.xlsx':
					$reader = new \PhpOffice\PhpSpreadsheet\Reader\Xlsx();
					break;
				case '.xls':
					$reader = new \PhpOffice\PhpSpreadsheet\Reader\Xls();
					break;
				case '.csv':
					$reader = new \PhpOffice\PhpSpreadsheet\Reader\Csv();
					break;
				default:
					echo "unknown file ext";
					die;
			}

			$spreadsheet = $reader->load($file);
			$sheetData = $spreadsheet->getActiveSheet()->toArray();
			$jurusan = [];
			for ($i = 1; $i < count($sheetData); $i++) {
				if ($sheetData[$i][0] != null) {
					$jurusan[] = $sheetData[$i][0];
				}
			}

			unlink($file);

			$this->import($jurusan);
		}
	}
	public function do_import()
	{
		$data = json_decode($this->input->post('jurusan', true));
		$jurusan = [];
		foreach ($data as $j) {
			$jurusan[] = ['nama_jurusan' => $j];
		}

		$save = $this->master->create('jurusan', $jurusan, true);
		if ($save) {
			redirect('jurusan');
		} else {
			redirect('jurusan/import');
		}
	}
}
