<?php

defined('BASEPATH') or exit('No direct script access allowed');

use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
use PhpOffice\PhpSpreadsheet\Writer\Xls;
use PhpOffice\PhpSpreadsheet\Writer\Csv;

class Materi extends CI_Controller
{

	public function __construct()
	{
		parent::__construct();
		if (!$this->ion_auth->logged_in()) {
			redirect('auth');
		} 
		$this->load->library(['datatables', 'form_validation']); // Load Library Ignited-Datatables
		$this->form_validation->set_error_delimiters('', '');
		$this->load->model('Master_model', 'master');
		$this->load->model('Ujian_model', 'ujian');

		$this->user = $this->ion_auth->user()->row();
		$this->mhs 	= $this->ujian->getIdMahasiswa($this->user->username);
	}

	public function output_json($data, $encode = true)
	{
		if ($encode) $data = json_encode($data);
		$this->output->set_content_type('application/json')->set_output($data);
	}

	public function index()
	{
		$data = [
			'user' => $this->ion_auth->user()->row(),
			'judul'	=> 'Materi',
			'subjudul' => 'Data Materi'
		];
		$this->load->view('_templates/dashboard/_header', $data);
		$this->load->view('master/materi/data');
		$this->load->view('_templates/dashboard/_footer');
	}

	public function jenjang()
	{
		$data = [
			'user' => $this->ion_auth->user()->row(),
			'judul'	=> 'Materi',
			'subjudul'=> 'Materi Ujian',
		];
		$this->load->view('_templates/dashboard/_header.php', $data);
		$this->load->view('ujian/materi');
		$this->load->view('_templates/dashboard/_footer.php');
	}

	public function jenjang_detail($id)
	{
		$ujian = $this->ujian->getDetailMateriById($id);

		$data = [
			'user' 			=> $this->ion_auth->user()->row(),
			'judul'			=> 'Materi',
			'subjudul' 		=> 'Detail Materi Ujian',
			'detail_materi'	=> $ujian,
		];

		$this->load->view('_templates/dashboard/_header.php', $data);
		$this->load->view('ujian/detail_materi');
		$this->load->view('_templates/dashboard/_footer.php');
	}

	public function download_materi($id, $id_bab)
	{
		$ujian = $this->ujian->download_materi_soal($id,$id_bab);

		$data = [
			'user' 			=> $this->ion_auth->user()->row(),
			'judul'			=> 'Materi',
			'subjudul' 		=> 'Download Materi Ujian',
			'download_materi'	=> $ujian,
		];

		$this->load->view('_templates/dashboard/_header.php', $data);
		$this->load->view('ujian/download_materi');
		$this->load->view('_templates/dashboard/_footer.php');
	}

	public function add()
	{
		$data = [
			'user' 		=> $this->ion_auth->user()->row(),
			'judul'		=> 'Tambah materi',
			'subjudul'	=> 'Tambah Data materi',
			'bab'		=> $this->master->getAllBab()
		];
		$this->load->view('_templates/dashboard/_header', $data);
		$this->load->view('master/materi/add');
		$this->load->view('_templates/dashboard/_footer');
	}

	public function data()
	{
		$this->output_json($this->master->getDatamateri(), false);
	}

	public function jenjang_data()
	{
		$this->output_json($this->ujian->getDataMateriJenjang($this->mhs->id_program), false);
	}

	public function jenjang_data_detail($id)
	{
		$this->output_json($this->ujian->getDataMateriJenjangDetail($id,true), false);
	}

	public function download_materi_soal($id,$id_bab)
	{
		$this->output_json($this->ujian->download_materi_soal($id,$id_bab), false);
	}

	public function edit($id)
	{
		$materi = $this->master->getmateriById($id);

		$data = [
			'user' 		=> $this->ion_auth->user()->row(),
			'judul'		=> 'Edit materi',
			'subjudul'	=> 'Edit Data materi',
			'bab'		=> $this->master->getAllBab(),
			'materi'	=> $materi
		];
		$this->load->view('_templates/dashboard/_header.php', $data);
		$this->load->view('master/materi/edit');
		$this->load->view('_templates/dashboard/_footer.php');
		
	}

	public function save()
	{
		$config['upload_path'] = './uploads/materi/';
		$config['allowed_types'] = 'pdf';

		$this->load->library('upload', $config);
		if (!$this->upload->do_upload('file_pdf')) {
			$error = $this->upload->display_errors();
			print_r("Pilih file dengan extension .pdf");
			exit;
		} else {
			$result = $this->upload->data();
			if ($result) {
				$materi = [
					'id_bab' => $this->input->post('dosen_id'),
					'link_youtube' => $this->input->post('youtube'),
					'file_pdf' => $result['file_name'],
					'isi_materi' => $this->input->post('materi'),
					'status' => $this->input->post('status'),
				];
				$insert=$this->db->insert('materi', $materi);
			}
			// print_r($insert);
		}
		redirect('materi');
	}	
	public function saveedit()
	{
		if ($_FILES['file_pdf']['name']) {

			$config['upload_path'] = './uploads/materi/';
			$config['allowed_types'] = 'pdf';

			$this->load->library('upload', $config);
			if (!$this->upload->do_upload('file_pdf')) {
				$error = $this->upload->display_errors();
				print_r($error);
			} else {
				$result = $this->upload->data();
				if ($result) {
					$materi = [
						'id_bab' => $this->input->post('dosen_id'),
						'link_youtube' => $this->input->post('youtube'),
						'file_pdf' => $result['file_name'],
						'isi_materi' => $this->input->post('materi'),
						'status' => $this->input->post('status'),
					];
					$this->db->where('id',$this->input->post('id'));
    				$this->db->update('materi', $materi);
				}
			}
			redirect('materi');
		}else{
			$materi = [
				'id_bab' => $this->input->post('dosen_id'),
				'link_youtube' => $this->input->post('youtube'),
				'isi_materi' => $this->input->post('materi'),
				'status' => $this->input->post('status'),
			];
			$this->db->where('id',$this->input->post('id'));
    		$this->db->update('materi', $materi);
			
			redirect('materi');
		}
	}

	public function delete($id)
	{
		$this->db->where('id',$id);
	    $this->db->delete('materi');
	    redirect('materi');
	}

		public function cetak($id)
	{
		$this->load->library('Pdf');

		$mhs 	= $this->ujian->getIdMahasiswa($this->user->username);
		$hasil 	= $this->ujian->HslUjian($id, $mhs->id_mahasiswa)->row();
		$ujian 	= $this->ujian->getUjianById($id);
		
		$data = [
			'ujian' => $ujian,
			'hasil' => $hasil,
			'mhs'	=> $mhs
		];
		
		$this->load->view('ujian/cetak', $data);
	}

	public function cetak_detail($id)
	{
		$this->load->library('Pdf');

		$ujian = $this->ujian->getUjianById($id);
		$nilai = $this->ujian->bandingNilai($id);
		$hasil = $this->ujian->HslUjianById($id)->result();

		$data = [
			'ujian'	=> $ujian,
			'nilai'	=> $nilai,
			'hasil'	=> $hasil
		];

		$this->load->view('ujian/cetak_detail', $data);
	}

	
}
