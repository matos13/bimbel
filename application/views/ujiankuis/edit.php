<div class="box">
    <div class="box-header with-border">
        <h3 class="box-title"><?=$subjudul?></h3>
        <div class="box-tools pull-right">
            <a href="<?=base_url()?>kuis/master_kuis" class="btn btn-sm btn-flat btn-warning">
                <i class="fa fa-arrow-left"></i> Batal
            </a>
        </div>
    </div>
    <div class="box-body">
        <div class="row">
            <div class="col-sm-4">
                <div class="alert bg-purple">
                    <h4>Mata Kuliah <i class="fa fa-book pull-right"></i></h4>
                    <p><?=$matkul->nama?></p>
                </div>
                <div class="alert bg-purple">
                    <h4>Dosen <i class="fa fa-address-book-o pull-right"></i></h4>
                    <p><?=$dosen->nama_dosen?></p>
                </div>
            </div>
            <div class="col-sm-4">
                <?=form_open('kuis/save_kuis', array('id'=>'formujian'), array('method'=>'edit','dosen_id'=>$dosen->id_dosen, 'matkul_id'=>$matkul->matkul_id, 'id_kuis'=>$ujian->id_kuis))?>
                <div class="form-group">
                    <label for="nama_kuis">Nama Ujian</label>
                    <input value="<?=$ujian->nama_kuis?>" autofocus="autofocus" onfocus="this.select()" placeholder="Nama Ujian" type="text" class="form-control" name="nama_kuis">
                    <small class="help-block"></small>
                </div>
                <div class="form-group">
                    <label for="jumlah_soal">Jumlah Soal</label>
                    <input value="<?=$ujian->jumlah_soal?>" placeholder="Jumlah Soal" type="number" class="form-control" name="jumlah_soal">
                    <small class="help-block"></small>
                </div>
                <div class="form-group">
                    <label for="tgl_mulai">Tanggal Mulai</label>
                    <input id="tgl_mulai" name="tgl_mulai" type="text" class="datetimepicker form-control" placeholder="Tanggal Mulai">
                    <small class="help-block"></small>
                </div>
                <div class="form-group">
                    <label for="tgl_selesai">Tanggal Selesai</label>
                    <input id="tgl_selesai" name="tgl_selesai" type="text" class="datetimepicker form-control" placeholder="Tanggal Selesai">
                    <small class="help-block"></small>
                </div>
                <div class="form-group">
                    <label for="waktu">Waktu</label>
                    <input value="<?=$ujian->waktu?>" placeholder="menit" type="number" class="form-control" name="waktu">
                    <small class="help-block"></small>
                </div>
                <div class="form-group">
                    <label for="jenis">Acak Soal</label>
                    <select name="jenis" class="form-control">
                        <option value="" disabled selected>--- Pilih ---</option>
                        <option <?=$ujian->jenis==="acak"?"selected":"";?> value="acak">Acak Soal</option>
                        <option <?=$ujian->jenis==="urut"?"selected":"";?> value="urut">Urut Soal</option>
                    </select>
                    <small class="help-block"></small>
                </div>

                <div class="form-group">
                  <label for="">Dasar Kriteria Lulus</label>
                  <select name="dasar" id="dasar" class="form-control form-group">
                    <option value="">Pilih Dasar Kriteria</option>
                    <option <?= $ujian->dasar_kriteria == 'nilai' ? 'selected' :  null ;?> value="nilai">Nilai Minimal</option>
                    <option <?= $ujian->dasar_kriteria == 'soal' ? 'selected' :  null ;?> value="soal">Jumlah Soal Benar</option>
                  </select>
                </div> 
                 <div class="form-group" id="nl" <?= $ujian->dasar_kriteria == 'soal' ? "style='display:none'" :  null ;?> >
                  <label for="">Nilai</label>
                  <input type="text" name="nilai" id="nilai" class="form-control" value="<?= $ujian->nilai ?>">
                </div> 
                 <div class="form-group" id="jml" <?= $ujian->dasar_kriteria == 'nilai' ? "style='display:none'" :  null ;?>>
                  <label for="">Jumlah Soal</label>
                  <input type="text" name="soal" id="soal" class="form-control" value="<?= $ujian->jml_soal ?>">
                </div> 

                <div class="form-group pull-right">
                    <button type="reset" class="btn btn-default btn-flat">
                        <i class="fa fa-rotate-left"></i> Reset
                    </button>
                    <button id="submit" type="submit" class="btn btn-flat bg-purple"><i class="fa fa-save"></i> Simpan</button>
                </div>
                <?=form_close()?>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
    var tgl_mulai = '<?=$ujian->tgl_mulai?>';
    var terlambat = '<?=$ujian->terlambat?>';
</script>

<script src="<?=base_url()?>assets/dist/js/app/ujian/edit_kuis.js"></script>
<script>
$(function() {
    $('#dasar').change(function(){
        if($('#dasar').val() == 'nilai') {
            $('#nl').show(); 
            $('#jml').hide(); 
        } else if($('#dasar').val() == 'soal') {
            $('#nl').hide(); 
            $('#jml').show(); 
        } else{
          $('#nl').hide(); 
          $('#jml').hide(); 
        }
    });
});
</script>