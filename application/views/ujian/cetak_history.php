<?php 
// Extend the TCPDF class to create custom Header and Footer
class MYPDF extends TCPDF {

    public function Header() {
        $image_file = K_PATH_IMAGES.'logo.jpg';
        $this->Image($image_file, 10, 10, 15, '', 'JPG', '', 'T', false, 300, '', false, false, 0, false, false, false);
        $this->SetFont('helvetica', 'B', 18);
        $this->SetY(13);
        $this->Cell(0, 15, 'Hasil Ujian Bersama Belajar', 0, false, 'C', 0, '', 0, false, 'M', 'M');
    }

    public function Footer() {
        $this->SetY(-15);
        $this->SetFont('helvetica', 'I', 8);
        $this->Cell(0, 10, 'Page '.$this->getAliasNumPage().'/'.$this->getAliasNbPages(), 0, false, 'R', 0, '', 0, false, 'T', 'M');
    }
}

// create new PDF document
$pdf = new MYPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

// set document information
$pdf->SetCreator(PDF_CREATOR);
$pdf->SetAuthor('Tutech Dev');
$pdf->SetTitle('Hasil Ujian Bersama Belajar');

// set default header data
$pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, PDF_HEADER_TITLE, PDF_HEADER_STRING);

// set header and footer fonts
$pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
$pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));

// set default monospaced font
$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

// set margins
$pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
$pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
$pdf->SetFooterMargin(PDF_MARGIN_FOOTER);

// set auto page breaks
$pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);

// set image scale factor
$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

// set some language-dependent strings (optional)
if (@file_exists(dirname(__FILE__).'/lang/eng.php')) {
    require_once(dirname(__FILE__).'/lang/eng.php');
    $pdf->setLanguageArray($l);
}

// ---------------------------------------------------------

// set font
$pdf->SetFont('helvetica', '', 10);

// add a page
$pdf->AddPage();

$mulai = strftime('%A, %d %B %Y', strtotime($ujian->tgl_mulai));
$selesai = strftime('%A, %d %B %Y', strtotime($ujian->terlambat));

// create some HTML content
$html = <<<EOD
<p>
</p>
<table>
    <tr>
        <th>Nama Ujian</th>
        <td>{$ujian->nama_ujian}</td>
        <th>Mata Pelajaran</th>
        <td>{$ujian->nama}</td> 
    </tr>
    <tr>
        <th>Jumlah Soal</th>
        <td>{$ujian->jumlah_soal}</td>
        <th>Pengajar</th>
        <td>{$ujian->nama_dosen}</td>
    </tr>
    <tr>
        <th>Waktu</th>
        <td>{$ujian->waktu} Menit</td>
    </tr>
    <tr>
        <th>Tanggal Mulai</th>
        <td>{$mulai}</td>
    </tr>
    <tr>
        <th>Tanggal Selasi</th>
        <td>{$selesai}</td>
    </tr>
</table>
EOD;

$html .= <<<EOD
<br><br><br>
<table border="1" style="border-collapse:collapse">
    <thead>
        <tr>
            <th width="9%">No Soal</th>
            <th>Soal</th>
            <th>Jawaban</th>
            <th>Jawaban Benar</th>
            <th width="30%">Pembahasan</th>
        </tr>         
    </thead>
    <tbody>
EOD;
$no=1;
$jawaban=explode(',',$jawaban->list_jawaban);
foreach ($hasil as $key => $value) { 
    $jwb=getBetween($jawaban[$key],':',':');
    if ($jwb==':N') {
        $jwb='Kosong';
    }
    if ($jwb==='A') {
        $jwb .= '. '.$value->opsi_a;
    }else if ($jwb==='B') {
        $jwb .= '. '.$value->opsi_b;
    }else if ($jwb==='C') {
        $jwb .= '. '.$value->opsi_c;
    }else if ($jwb==='D') {
        $jwb .= '. '.$value->opsi_d;
    }else if ($jwb==='E'){
        $jwb .= '. '.$value->opsi_e;
    }
    $benar=$value->jawaban;
    if ($benar==='A') {
        $benar.='.'.$value->opsi_a;
    }else if ($value->jawaban==='B') {
        $benar.='.'.$value->opsi_b;
    }else if ($value->jawaban==='C') {
        $benar.='.'.$value->opsi_c;
    }else if ($value->jawaban==='D') {
        $benar.='.'.$value->opsi_d;
    }else{
        $benar.=', '.$value->opsi_e;
    }
$html .= <<<EOD
    <tr>
        <td align="center" width="9%">{$no}</td>
        <td>{$value->soal}</td>
        <td>{$jwb}</td>
        <td>{$benar}</td>
        <td width="30%">{$value->pembahasan}</td>
    </tr>
EOD;
$no++;
}

$html .= <<<EOD
    </tbody>
</table>
EOD;

// output the HTML content
$pdf->writeHTML($html, true, 0, true, 0);
// reset pointer to the last page
$pdf->lastPage();
// ---------------------------------------------------------

//Close and output PDF document
$pdf->Output('tes.pdf', 'I');
