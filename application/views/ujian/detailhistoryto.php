<div class="box">
    <div class="box-header with-border">
        <h3 class="box-title"><?=$subjudul?></h3>
        <div class="box-tools pull-right">
            <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
            </button>
        </div>
    </div>
    <div class="box-body">
        <div class="row">
            <div class="col-sm-12 mb-4">
                <button type="button" onclick="reload()" class="btn btn-flat btn-sm bg-purple"><i class="fa fa-refresh"></i> Reload</button>
                <div class="pull-right">
                    <!-- disuruh dihilangkan dulu, revisi nomor 12 -->

                    <!-- <a target="_blank" href="<?=base_url()?>Historyto/cetak_detail/<?=$this->uri->segment(3)?>" class="btn bg-maroon btn-flat btn-sm">
                        <i class="fa fa-print"></i> Print
                    </a> -->

                    <!-- end revisi  -->
                    
                </div>
            </div>
            <div class="col-sm-6">
                <table class="table w-100">
                    <tr>
                        <th>Nama Ujian</th>
                        <td><?=$ujian->nama_ujian?></td>
                    </tr>
                    <tr>
                        <th>Jumlah Soal</th>
                        <td><?=$ujian->jumlah_soal?></td>
                    </tr>
                    <tr>
                        <th>Waktu</th>
                        <td><?=$ujian->waktu?> Menit</td>
                    </tr>
                    <tr>
                        <th>Tanggal Mulai</th>
                        <td><?=strftime('%A, %d %B %Y', strtotime($ujian->tgl_mulai))?></td>
                    </tr>
                    <tr>
                        <th>Tanggal Selasi</th>
                        <td><?=strftime('%A, %d %B %Y', strtotime($ujian->terlambat))?></td>
                    </tr>
                </table>
            </div>
            <div class="col-sm-6">
                <table class="table w-100">
                    <tr>
                        <th>Program Belajar</th>
                        <td><?=$ujian->nama?></td>
                    </tr>
                    <tr>
                        <th>Pengajar</th>
                        <td><?=$ujian->nama_dosen?></td>
                    </tr>
                </table>
            </div>
        </div>
    <div class="table-responsive px-4 pb-3" style="border: 0">
        <table id="detail_hasil" class="w-100 table table-striped table-bordered table-hover">
        <thead>
            <tr>
                <!-- <th width="8%">No Soal</th> -->
                <th>Soal</th>
                <th>Jawaban</th>
                <th>Jawaban Benar</th>
                <th width="40%">Pembahasan</th>
            </tr>        
        </thead>
        <?php 
        $no=1;
        if (!empty($jawaban)) {
        $jawaban=explode(',',$jawaban->list_jawaban);
        // print_r($hasil);
        foreach ($hasil as $key => $value) { 
            ?>
            <tr>
                <!-- <td align="center"><?php echo $no ++ ?></td> -->
                <td><?php echo $value->soal ?></td>
                <td>
                <?php
                    $jwb=getBetween($jawaban[$key],':',':');
                    if ($jwb==':N') {
                        $jwb='Kosong';
                    }
                    echo $jwb;
                    if ($jwb==='A') {
                        echo ". ".$value->opsi_a;
                    }else if ($jwb==='B') {
                        echo ". ".$value->opsi_b;
                    }else if ($jwb==='C') {
                        echo ". ".$value->opsi_c;
                    }else if ($jwb==='D') {
                        echo ". ".$value->opsi_d;
                    }else if ($jwb==='E'){
                        echo ". ".$value->opsi_e;
                    }
                ?>
                </td>
                <td><?php echo $value->jawaban;
                        if ($value->jawaban==='A') {
                            echo ". ".$value->opsi_a;
                        }else if ($value->jawaban==='B') {
                            echo ". ".$value->opsi_b;
                        }else if ($value->jawaban==='C') {
                            echo ". ".$value->opsi_c;
                        }else if ($value->jawaban==='D') {
                            echo ". ".$value->opsi_d;
                        }else{
                            echo ". ".$value->opsi_e;
                        }
                
                ?></td>
                <td><?php echo $value->pembahasan ?></td>
            </tr>
       <?php } // end foreach
        } else { ?>
             <tr>
                <td colspan="6" align="center">Belum ada history ujian</td>
            </tr>
        <?php } ?>
        </table>
    </div>
</div>
<script>
    function reload(){
        window.location.reload()
    }
</script>