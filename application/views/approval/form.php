<div class="box">
    <div class="box-header with-border">
        <h3 class="box-title">Form <?=$judul?></h3>
        <div class="box-tools pull-right">
            <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
            </button>
        </div>
    </div>
    <?php 
    $data=json_decode(json_encode($data), true);
        // print_r($data);
    ?>
    <div class="box-body">
        <div class="row">
            <div class="col-sm-offset-3 col-sm-6">
                <div class="my-2">
                
                </div>
                <!-- <?=form_open('bab/save', array('id'=>'bab'), array('mode'=>'add'))?> -->
                <form action="" id="form">
                <div class="form-group">
                <label for="">Nama</label>
                <input type="text"  class="form-control" name="nama_lengkap" value="<?php echo $data[0]['nama_lengkap']?>" readonly>
                <input type="hidden"  class="form-control"  name="id" value="<?php echo $data[0]['id']?>">
                <input type="hidden" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>">
                </div>
                <div class="form-group">
                <label for="">Jenis Kelamin</label>
                <input type="text"  class="form-control"  name="jenis_kelamin" value="<?php echo $data[0]['jenis_kelamin']?>" readonly>
                </div>
                <div class="form-group">
                <label for="">Alamat</label>
                <input type="text"  class="form-control"  name="alamat" value="<?php echo $data[0]['alamat']?>" readonly>
                </div>
                <div class="form-group">
                <label for="">Nomor Hp</label>
                <input type="text"  class="form-control"  name="nomor_hp" value="<?php echo $data[0]['nomor_hp']?>" readonly>
                </div>
                <div class="form-group">
                <label for="">Program Belajar</label>
                <input type="text"  class="form-control"  name="nama" value="<?php echo $data[0]['nama']?>" readonly>
                </div>
                <div class="form-group">
                <label for="">Kelas</label>
                <select required="required" name="kelas_id" id="kelas" class="form-control input-sm select2" style="width: 100%!important">
                    <option value="" disabled selected>-- Pilih --</option>
                        <?php foreach ($kelas as $j) : ?>
                            <option value="<?=$j['id']?>"><?=$j['nama'] ?></option>
                            <?php endforeach; ?>
                        </select>
                </div>
                </form>
                <center>
                    <button id="button" type="button"  onclick="batal()"  class="btn btn-flat bg-red">
                        <i class="fa fa-arrow-left"></i> Batal
                    </button>
                    <button id="submit" type="button" onclick="simpan()" class="btn btn-flat bg-purple">
                        <i class="fa fa-save"></i> Simpan
                    </button>
                </center>
            </div>
        </div>
    </div>
</div>
<script>
    function simpan() {
        // alert($('#kelas').val());
        if ($('#kelas').val()==null) {
            alert("Kelas hasrus dipilih ");
            return false;
        }else{
            Swal.fire({
                title: 'Apa anda yakin?',
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Yes'
            }).then((result) => {
                var formdata=$("#form").serialize();
                console.log(formdata);
                $.ajax(
                    {
                        type:'POST',
                        url:'<?= base_url('approval/terima')?>',
                        data:formdata,
                        dataType:'json',
                        success:function(data){
                            if (data.status == true) {
                                Swal({
                                    "title": "Sukses",
                                    "text": "Data Berhasil disimpan",
                                    "type": "success"
                                }).then((result) => {
                                    if (result.value) {
                                        window.location.href = base_url+'Approval';
                                    }
                                });
                            } else {
                                Swal({
                                    "title": "Eror",
                                    "text": data.msg,
                                    "type": "warning"
                                })
                            }
                        }
                    }
                    );
                })
    }
    }
    function batal() {
        window.location.href = base_url+'Approval';
    }
</script>