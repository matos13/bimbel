<div class="box">
    <div class="box-header with-border">
        <h3 class="box-title"><?=$subjudul?></h3>
        <div class="box-tools pull-right">
            <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
            </button>
        </div>
	</div>
    <div class="box-body">
		<div class="mt-2 mb-4">
        <table id="approval" class="w-100 table table-striped table-bordered table-hover">
            <thead>
                <tr>
                    <th>No.</th>
                    <th>Nama</th>
                    <th>Jenis Kelamin</th>
                    <th>Alamat</th>
                    <th>No Telp</th>
                    <th>Program Belajar</th>
					<th class="text-center">Aksi
					</th>
				</tr>
            </thead>
        </table>
    </div>
</div>
</div>
<script>
    $(document).ready(function() {
  ajaxcsrf();

  table = $("#approval").DataTable({
    initComplete: function() {
      var api = this.api();
      $("#approval_filter input")
        .off(".DT")
        .on("keyup.DT", function(e) {
          api.search(this.value).draw();
        });
    },
    dom:
      "<'row'<'col-sm-3'l><'col-sm-6 text-center'B><'col-sm-3'f>>" +
      "<'row'<'col-sm-12'tr>>" +
      "<'row'<'col-sm-5'i><'col-sm-7'p>>",
    buttons: [
      {
        extend: "copy",
        exportOptions: { columns: [1, 2,3,4,5] }
      },
      {
        extend: "print",
        exportOptions: { columns: [1, 2,3,4,5] }
      },
      {
        extend: "excel",
        exportOptions: { columns: [1, 2,3,4,5] }
      },
      {
        extend: "pdf",
        exportOptions: { columns: [1, 2,3,4,5] }
      }
    ],
    oLanguage: {
      sProcessing: "loading..."
    },
    processing: true,
    serverSide: true,
    ajax: {
      url: base_url + "approval/data",
      type: "POST"
      //data: csrf
    },
    columns: [
      {
        data: "id",
        orderable: false,
        searchable: false
      },
      { data: "nama_lengkap" },
      { data: "jenis_kelamin" },
      { data: "alamat" },
      { data: "nomor_hp" },
      { data: "nama" },
      {
        targets: 6,
        data: "id",
        render: function(data, type, row, meta) {
          return `<div class="text-center">
                                <a href="${base_url}Approval/aksi/${data}" class="btn btn-xs btn-success">
                                    <i class="fa fa-check"></i> Terima
                                </a>
                            </div>`;
        }
      }
    ],
    order: [[1, "asc"]],
    rowId: function(a) {
      return a;
    },
    rowCallback: function(row, data, iDisplayIndex) {
      var info = this.fnPagingInfo();
      var page = info.iPage;
      var length = info.iLength;
      var index = page * length + (iDisplayIndex + 1);
      $("td:eq(0)", row).html(index);
    }
  });

  table
    .buttons()
    .container()
    .appendTo("#bab_wrapper .col-md-6:eq(0)");

});
</script>
<!-- <script src="<?=base_url()?>assets/dist/js/app/master/bab/data.js"></script> -->