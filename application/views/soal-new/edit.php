<?php 
// print_r($soal);

?>
<div class="row">
    <div class="col-sm-12">    
        <?=form_open_multipart('SoalNew/save', array('id'=>'formsoal'), array('method'=>'edit'));?>
        <!-- <input type="hidden" name="<?php echo $this->security->get_csrf_token_name();?>" value="<?php echo $this->security->get_csrf_hash();?>"> -->
        <input type="hidden" name="id" value="<?php echo $soal->id ?>">
        <div class="box">
            <div class="box-header with-border">
                <h3 class="box-title"><?=$subjudul?></h3>
                <div class="box-tools pull-right">
                    <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                    </button>
                </div>
            </div>
            <div class="box-body">
                <div class="row">
                    <div class="col-sm-8">
                        <div class="form-group">
                          <label for="">Paket</label>
                          <select name="paket" class="select2 form-group">
                            <option value="">Pilih Paket</option>
                            <?php foreach ($paket as $d) : ?>
                                <option value="<?=$d['id']?>" <?= $soal->id_paket == $d['id'] ? 'selected' : null ;?>><?=$d['nama_paket']?></option>
                            <?php endforeach; ?>
                        </select>
                    </div>
                    <div class="form-group">
                      <label for="">Dosen</label>
                      <select name="dosen" class="select2 form-group">
                        <option value="">Pilih Paket</option>
                        <?php foreach ($dosen as $d) : ?>
                            <option value="<?=$d->id_dosen?>" <?= $soal->id_dosen == $d->id_dosen ? 'selected' : null ;?>><?=$d->nama_dosen?></option>
                        <?php endforeach; ?>
                    </select>
                </div> 

                <div class="form-group">
                  <label for="">Jenis Soal</label>
                  <select name="jenis_soal" class="select2 form-group">
                    <?php if ($soal->jenis_soal=='tkp') { ?>
                        <option disabled="" value="">Pilih Jenis Soal</option>
                        <option value="tkp" selected="">TKP</option>
                        <option value="nontkp">NON-TKP</option>
                    <?php } else { ?>
                        <option disabled="" value="">Pilih Jenis Soal</option>
                        <option value="tkp">TKP</option>
                        <option value="nontkp" selected="">NON-TKP</option>
                    <?php } ?>  
                </select>
            </div>
            <!-- <div class="form-group">
                  <label for="">Dasar Kriteria Lulus</label>
                  <select name="dasar" id="dasar" class="form-control form-group">
                    <option value="">Pilih Dasar Kriteria</option>
                    <option <?= $soal->dasar_kriteria == 'nilai' ? 'selected' :  null ;?> value="nilai">Nilai Minimal</option>
                    <option <?= $soal->dasar_kriteria == 'soal' ? 'selected' :  null ;?> value="soal">Jumlah Soal Benar</option>
                  </select>
                </div> 
                 <div class="form-group" id="nl" <?= $soal->dasar_kriteria == 'soal' ? "style='display:none'" :  null ;?> >
                  <label for="">Nilai</label>
                  <input type="text" name="nilai" id="nilai" class="form-control" value="<?= $soal->nilai ?>">
                </div> 
                 <div class="form-group" id="jml" <?= $soal->dasar_kriteria == 'nilai' ? "style='display:none'" :  null ;?>>
                  <label for="">Jumlah Soal</label>
                  <input type="text" name="soal" id="soal" class="form-control" value="<?= $soal->jml_soal ?>">
                </div>  -->
            <div class="form-group pull-right">
                <a href="<?=base_url('SoalNew')?>" class="btn btn-flat btn-default"><i class="fa fa-arrow-left"></i> Batal</a>
                <button type="submit" id="submit" class="btn btn-flat bg-purple"><i class="fa fa-save"></i> Simpan</button>
            </div>
        </div>
    </div>
</div>
</div>
<?=form_close();?>
</div>
</div>
<!-- <script>
    $(function() {
    $('#dasar').change(function(){
        if($('#dasar').val() == 'nilai') {
            $('#nl').show(); 
            $('#jml').hide(); 
        } else if($('#dasar').val() == 'soal') {
            $('#nl').hide(); 
            $('#jml').show(); 
        } else{
          $('#nl').hide(); 
          $('#jml').hide(); 
        }
    });
});
</script> -->