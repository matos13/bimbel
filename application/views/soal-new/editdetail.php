<?php
    // print_r($soal);
    // exit;
?>
<div class="row">
    <div class="col-sm-12">    
        <?=form_open_multipart('SoalNew/savedetail', array('id'=>'formsoal'), array('method'=>'edit', 'id_soal'=>$soal->id_soal));?>
        <input type="hidden" name="id_soal_paket" value="<?php echo $soal->id_soal_paket ?>">
        <div class="box">
            <div class="box-header with-border">
                <h3 class="box-title"><?=$subjudul?></h3>
                <div class="box-tools pull-right">
                    <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                    </button>
                </div>
            </div>
            <div class="box-body">
                <div class="row">
                    <div class="col-sm-8 col-sm-offset-2">
                        <div class="row">
                            <div class="form-group col-sm-12">
                                <label for="dosen_id" class="control-label">Dosen (Mata Kuliah)</label>
                                <?php if( $this->ion_auth->is_admin() ) : ?>
                                <select required="required" name="dosen_id" id="dosen_id" class="select2 form-group" style="width:100% !important">
                                    <option value="" disabled selected>Pilih Dosen</option>
                                    <?php 
                                    $sdm = $soal->dosen_id.':'.$soal->matkul_id;
                                    foreach ($dosen as $d) : 
                                        $dm = $d->id_dosen.':'.$d->matkul_id;?>
                                        <option <?=$sdm===$dm?"selected":"";?> value="<?=$dm?>"><?=$d->nama_dosen?> (<?=$d->nama?>)</option>
                                    <?php endforeach; ?>
                                </select>
                                <small class="help-block" style="color: #dc3545"><?=form_error('dosen_id')?></small>
                                <?php else : ?>
                                <input type="hidden" name="dosen_id" value="<?=$dosen->id_dosen;?>">
                                <input type="hidden" name="matkul_id" value="<?=$dosen->matkul_id;?>">
                                <input type="text" readonly="readonly" class="form-control" value="<?=$dosen->nama_dosen; ?> (<?=$dosen->nama; ?>)">
                                <?php endif; ?>
                            </div>

                            <div class="form-group col-sm-12">
                            <label>Jenis Soal</label>
                            <?php if ($paket_soal->jenis_soal=='tkp') { ?>
                                <input type="text" readonly="readonly" class="form-control" value="tkp">
                            <?php } else { ?>
                                <input type="text" readonly="readonly" class="form-control" value="nontkp">
                            <?php } ?>
                        </div>
                            
                            <div class="form-group col-sm-12">
                                <label for="soal" class="control-label text-center">Soal</label>
                                <!-- <div class="row"> -->
                                    <div class="form-group">
                                        <input type="file" name="file_soal" class="form-control">
                                        <small class="help-block" style="color: #dc3545"><?=form_error('file_soal')?></small>
                                        <?php if(!empty($soal->file)) : ?>
                                            <?=tampil_media('uploads/bank_soal/'.$soal->file);?>
                                        <?php endif;?>
                                    </div>
                                    <div class="form-group">
                                        <textarea name="soal" id="soal" class="form-control froala-editor"><?=$soal->soal?></textarea>
                                        <small class="help-block" style="color: #dc3545"><?=form_error('soal')?></small>
                                    </div>
                                <!-- </div> -->
                            </div>
                            
                            <!-- 
                                Membuat perulangan A-E 
                            -->
                            <?php
                            $abjad = ['a', 'b', 'c', 'd', 'e']; 
                            foreach ($abjad as $abj) :
                                $ABJ = strtoupper($abj); // Abjad Kapital
                                $file = 'file_'.$abj;
                                $opsi = 'opsi_'.$abj;
                                $bobot = 'bobot_'.$abj;
                                
                            ?>
                            
                            <div class="col-sm-12">
                                <div class="row">
                                <div class="form-group col-sm-12">
                                    <label for="jawaban_<?= $abj; ?>" class="control-label text-center">Jawaban <?= $ABJ; ?></label>
                                        <input type="file" name="<?= $file; ?>" class="form-control">
                                        <small class="help-block" style="color: #dc3545"><?=form_error($file)?></small>
                                        <?php if(!empty($soal->$file)) : ?>
                                            <?=tampil_media('uploads/bank_soal/'.$soal->$file);?>
                                        <?php endif;?>
                                    </div>

                                    <?php if ($paket_soal->jenis_soal=='tkp') { 
                                        // print_r($soal->$bobot);
                                        ?>
                                    <div class="form-group col-sm-7">
                                    <label for="file">Bobot Jawaban <?= $ABJ; ?></label>
                                        <select required="required" name="bobot_<?= $abj; ?>" id="bobot_<?= $abj; ?>" class="form-control select2" style="width:100%!important">
                                            <option value="" disabled selected>Pilih Bobot Jawaban</option>
                                            <option <?=$soal->$bobot==1?"selected":""?> value="1">1</option>
                                            <option <?=$soal->$bobot==2?"selected":""?> value="2">2</option>
                                            <option <?=$soal->$bobot==3?"selected":""?> value="3">3</option>
                                            <option <?=$soal->$bobot==4?"selected":""?> value="4">4</option>
                                            <option <?=$soal->$bobot==5?"selected":""?> value="5">5</option>
                                        </select>
                                    </div>
                                    <?php } ?>
                                </div>
                            </div>

                            <div class="col-sm-12">
                                    <div class="form-group">
                                        <textarea name="jawaban_<?= $abj; ?>" id="jawaban_<?= $abj; ?>" class="form-control froala-editor"><?=$soal->$opsi?></textarea>
                                        <small class="help-block" style="color: #dc3545"><?=form_error('jawaban_'.$abj)?></small>
                                    </div>
                            </div>
                            
                            <?php endforeach; ?>
                            <input type="hidden" name="jawaban" value="null">
                            <!-- tidak pakai kunci jawaban -->

                            <?php if ($paket_soal->jenis_soal=='nontkp') { ?>
                            <div class="form-group col-sm-12">
                                <label for="jawaban" class="control-label">Kunci Jawaban</label>
                                <select required="required" name="jawaban" id="jawaban" class="form-control select2" style="width:100%!important">
                                    <option value="" disabled selected>Pilih Kunci Jawaban</option>
                                    <option <?=$soal->jawaban==="A"?"selected":""?> value="A">A</option>
                                    <option <?=$soal->jawaban==="B"?"selected":""?> value="B">B</option>
                                    <option <?=$soal->jawaban==="C"?"selected":""?> value="C">C</option>
                                    <option <?=$soal->jawaban==="D"?"selected":""?> value="D">D</option>
                                    <option <?=$soal->jawaban==="E"?"selected":""?> value="E">E</option>
                                </select>                
                                <small class="help-block" style="color: #dc3545"><?=form_error('jawaban')?></small>
                            </div>

                                <div class="form-group col-sm-12">  
                                    <small class="help-block" style="color: #dc3545"><i>Silakan ganti nilai bobot sesuai dengan kebutuhan.</i></small>
                                </div>
                                <?php if ($soal->jawaban=='A') { ?>
                                    <div class="form-group col-sm-12">
                                        <label for="bobot" class="control-label">Bobot Benar</label>
                                        <input required="required" value="<?php echo $soal->bobot_a;?>" type="number" name="bobot_benar" placeholder="Bobot Benar" id="bobot" class="form-control" required="">
                                    </div>
                                    <div class="form-group col-sm-12">
                                        <label for="bobot" class="control-label">Bobot Salah</label>
                                        <input required="required" value="<?php echo $soal->bobot_b;?>" type="number" name="bobot_salah" placeholder="Bobot Salah" id="bobot" class="form-control" required="">
                                    </div>
                                <?php } elseif ($soal->jawaban=='B') { ?>
                                    <div class="form-group col-sm-12">
                                        <label for="bobot" class="control-label">Bobot Benar</label>
                                        <input required="required" value="<?php echo $soal->bobot_b;?>" type="number" name="bobot_benar" placeholder="Bobot Benar" id="bobot" class="form-control" required="">
                                    </div>
                                    <div class="form-group col-sm-12">
                                        <label for="bobot" class="control-label">Bobot Salah</label>
                                        <input required="required" value="<?php echo $soal->bobot_c;?>" type="number" name="bobot_salah" placeholder="Bobot Salah" id="bobot" class="form-control" required="">
                                    </div>
                                 <?php } elseif ($soal->jawaban=='C') { ?>
                                    <div class="form-group col-sm-12">
                                        <label for="bobot" class="control-label">Bobot Benar</label>
                                        <input required="required" value="<?php echo $soal->bobot_c;?>" type="number" name="bobot_benar" placeholder="Bobot Benar" id="bobot" class="form-control" required="">
                                    </div>
                                    <div class="form-group col-sm-12">
                                        <label for="bobot" class="control-label">Bobot Salah</label>
                                        <input required="required" value="<?php echo $soal->bobot_d;?>" type="number" name="bobot_salah" placeholder="Bobot Salah" id="bobot" class="form-control" required="">
                                    </div>
                                 <?php } elseif ($soal->jawaban=='D') { ?>
                                    <div class="form-group col-sm-12">
                                        <label for="bobot" class="control-label">Bobot Benar</label>
                                        <input required="required" value="<?php echo $soal->bobot_d;?>" type="number" name="bobot_benar" placeholder="Bobot Benar" id="bobot" class="form-control" required="">
                                    </div>
                                    <div class="form-group col-sm-12">
                                        <label for="bobot" class="control-label">Bobot Salah</label>
                                        <input required="required" value="<?php echo $soal->bobot_d;?>" type="number" name="bobot_salah" placeholder="Bobot Salah" id="bobot" class="form-control" required="">
                                    </div>
                                 <?php } elseif ($soal->jawaban=='E') { ?>
                                    <div class="form-group col-sm-12">
                                        <label for="bobot" class="control-label">Bobot Benar</label>
                                        <input required="required" value="<?php echo $soal->bobot_e;?>" type="number" name="bobot_benar" placeholder="Bobot Benar" id="bobot" class="form-control" required="">
                                    </div>
                                    <div class="form-group col-sm-12">
                                        <label for="bobot" class="control-label">Bobot Salah</label>
                                        <input required="required" value="<?php echo $soal->bobot_a;?>" type="number" name="bobot_salah" placeholder="Bobot Salah" id="bobot" class="form-control" required="">
                                    </div>

                                <?php } ?>

                                <div class="form-group col-sm-12">
                                    <label for="bobot" class="control-label">Bobot Kosong</label>
                                    <input required="required" value="<?php echo $soal->bobot_kosong;?>" type="number" name="bobot_kosong" placeholder="Bobot Kosong" id="bobot" class="form-control" required="">
                                </div>

                            <?php } ?>
                            <br>
                            <div class="form-group col-sm-5">
                                <label for="file">Pembahasan</label>
                                <small class="help-block" style="color: #dc3545"></small>
                            </div>
                            <div class="col-sm-12">
                                <div class="form-group">
                                    <textarea name="pembahasan" id="pembahasan" class="form-control"><?=$soal->pembahasan?></textarea>
                                </div>
                            </div>
                            <div class="col-sm-12">
                                <div class="form-group pull-right">
                                    <a href="<?=base_url()?>SoalNew" class="btn btn-flat btn-default"><i class="fa fa-arrow-left"></i> Batal</a>
                                    <button type="submit" id="submit" class="btn btn-flat bg-purple"><i class="fa fa-save"></i> Simpan</button>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
        <?=form_close();?>
    </div>
</div>