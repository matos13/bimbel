<!-- <?= $id ?> -->
<?php 
$paket = json_decode(json_encode($paket), true);
// print_r ($paket);
?>
<div class="row">
    <div class="col-sm-12">    
        <?=form_open_multipart('SoalNew/savedetail', array('id'=>'formsoal'), array('method'=>'add'));?>
        <input type="hidden" name="id_soal_paket" value="<?php echo $id ?>">
        <div class="box">
            <div class="box-header with-border">
                <h3 class="box-title"><?=$subjudul?></h3>
                <div class="box-tools pull-right">
                    <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                    </button>
                </div>
            </div>
            <div class="box-body">
                <div class="row">
                    <div class="col-sm-8 col-sm-offset-2">
                        <div class="form-group col-sm-12">
                            <label>Pengajar (Mata Pelajaran)</label>
                            <input type="hidden" name="dosen_id" value="<?=$paket['id_dosen'];?>">
                            <input type="hidden" name="matkul_id" value="<?=$paket['matkul_id'];?>">
                            <input type="hidden" name="id_soal_paket" value="<?=$paket['id'];?>">
                            <input type="text" readonly="readonly" class="form-control" value="<?=$paket['nama_dosen']; ?> (<?=$paket['nama']; ?>)">
                        </div>

                        <div class="form-group col-sm-12">
                            <label>Jenis Soal</label>
                            <?php if ($paket['jenis_soal']=='tkp') { ?>
                                <input name="jenis_soal" type="text" readonly="readonly" class="form-control" value="tkp">
                            <?php } else { ?>
                                <input name="jenis_soal" type="text" readonly="readonly" class="form-control" value="nontkp">
                            <?php } ?>
                        </div>
                        
                        <div class="col-sm-12">
                            <label for="soal" class="control-label">Soal</label>
                            <div class="form-group">
                                <input type="file" name="file_soal" class="form-control">
                                <small class="help-block" style="color: #dc3545"><?=form_error('file_soal')?></small>
                            </div>
                            <div class="form-group">
                                <textarea name="soal" id="soal" class="form-control froala-editor"><?=set_value('soal')?></textarea>
                                <small class="help-block" style="color: #dc3545"><?=form_error('soal')?></small>
                            </div>
                        </div>
                        
                        <!-- 
                            Membuat perulangan A-E 
                        -->
                        <?php
                        $abjad = ['a', 'b', 'c', 'd', 'e']; 
                        foreach ($abjad as $abj) :
                            $ABJ = strtoupper($abj); // Abjad Kapital
                        ?>

                        <div class="col-sm-12">
                            <div class="row">
                                 <?php if ($paket['jenis_soal']=='tkp') { ?>
                                    <div class="form-group col-sm-5">
                                        <label for="file">Jawaban <?= $ABJ; ?></label>
                                        <input type="file" name="file_<?= $abj; ?>" class="form-control">
                                        <small class="help-block" style="color: #dc3545"><?=form_error('file_'.$abj)?></small>
                                    </div>

                                    <div class="form-group col-sm-7">
                                    <label for="file">Bobot Jawaban <?= $ABJ; ?></label>
                                    <select required="required" name="bobot_<?= $abj; ?>" id="bobot" class="form-control select2" style="width:100%!important">
                                        <option value="" disabled selected>Pilih Bobot Jawaban</option>
                                        <option value="1">1</option>
                                        <option value="2">2</option>
                                        <option value="3">3</option>
                                        <option value="4">4</option>
                                        <option value="5">5</option>
                                    </select>
                                </div>

                                <?php } else { ?>
                                    <div class="form-group col-sm-12">
                                <label for="file">Jawaban <?= $ABJ; ?></label>
                                    <input type="file" name="file_<?= $abj; ?>" class="form-control">
                                    <small class="help-block" style="color: #dc3545"><?=form_error('file_'.$abj)?></small>
                                </div>
                                <?php } ?>

                            </div>
                        </div>

                        <div class="col-sm-12">
                            <div class="form-group">
                                <textarea name="jawaban_<?= $abj; ?>" id="jawaban_<?= $abj; ?>" class="form-control froala-editor"><?=set_value('jawaban_a')?></textarea>
                                <small class="help-block" style="color: #dc3545"><?=form_error('jawaban_'.$abj)?></small>
                            </div>


                        </div>

                        <?php endforeach; ?>
                        <input type="hidden" name="jawaban" value="null">
                        <!-- tidak pakai kunci jawaban -->

                        <?php if ($paket['jenis_soal']=='nontkp') { ?>
                            
                        <div class="form-group col-sm-12">
                            <label for="jawaban" class="control-label">Kunci Jawaban</label>
                            <select required="required" name="jawaban" id="jawaban" class="form-control select2" style="width:100%!important">
                                <option value="" disabled selected>Pilih Kunci Jawaban</option>
                                <option value="A">A</option>
                                <option value="B">B</option>
                                <option value="C">C</option>
                                <option value="D">D</option>
                                <option value="E">E</option>
                            </select>                
                            <small class="help-block" style="color: #dc3545"><?=form_error('jawaban')?></small>
                        </div>

                        <div class="form-group col-sm-12">  
                            <small class="help-block" style="color: #dc3545"><i>Silakan ganti nilai bobot sesuai dengan kebutuhan.</i></small>
                        </div>
                        <div class="form-group col-sm-12">
                            <label for="bobot" class="control-label">Bobot Benar</label>
                            <input required="required" value="2" type="number" name="bobot_benar" placeholder="Bobot Benar" id="bobot" class="form-control" required="">
                        </div>
                        <div class="form-group col-sm-12">
                            <label for="bobot" class="control-label">Bobot Salah</label>
                            <input required="required" value="-1" type="number" name="bobot_salah" placeholder="Bobot Salah" id="bobot" class="form-control" required="">
                        </div>
                        <div class="form-group col-sm-12">
                            <label for="bobot" class="control-label">Bobot Kosong</label>
                            <input required="required" value="-2" type="number" name="bobot_kosong" placeholder="Bobot Kosong" id="bobot" class="form-control" required="">
                        </div>
                        <?php } ?>
                        <br>
                        <div class="form-group col-sm-5">
                            <label for="file">Pembahasan</label>
                            <small class="help-block" style="color: #dc3545"></small>
                        </div>
                        <div class="col-sm-12">
                            <div class="form-group">
                                <textarea name="pembahasan" id="pembahasan" class="form-control"></textarea>
                            </div>
                        </div>
                        <div class="form-group pull-right">
                            <a href="<?=base_url('soal')?>" class="btn btn-flat btn-default"><i class="fa fa-arrow-left"></i> Batal</a>
                            <button type="submit" id="submit" class="btn btn-flat bg-purple"><i class="fa fa-save"></i> Simpan</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <?=form_close();?>
    </div>
</div>