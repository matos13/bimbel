<div class="row">
    <div class="col-sm-12">    
        <?=form_open_multipart('SoalNew/save', array('id'=>'formsoal'), array('method'=>'add'));?>
        <input type="hidden" name="<?php echo $this->security->get_csrf_token_name();?>" value="<?php echo $this->security->get_csrf_hash();?>">
        <div class="box">
            <div class="box-header with-border">
                <h3 class="box-title"><?=$subjudul?></h3>
                <div class="box-tools pull-right">
                    <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                    </button>
                </div>
            </div>
            <div class="box-body">
                <div class="row">
                <div class="col-sm-8">
                <div class="form-group">
                  <label for="">Paket</label>
                  <select name="paket" class="select2 form-group">
                    <option value="">Pilih Paket</option>
                    <?php foreach ($paket as $d) : ?>
                                    <option value="<?=$d['id']?>"><?=$d['nama_paket']?></option>
                    <?php endforeach; ?>
                  </select>
                </div>

                <div class="form-group">
                  <label for="">Pengajar</label>
                  <select name="dosen" class="select2 form-group">
                    <option value="">Pilih Pengajar</option>
                    <?php foreach ($dosen as $d) : ?>
                                    <option value="<?=$d->id_dosen?>"><?=$d->nama_dosen?> - <?=$d->nama?></option>
                    <?php endforeach; ?>
                  </select>
                </div>

                 <div class="form-group">
                  <label for="">Jenis Soal</label>
                  <select name="jenis_soal" class="select2 form-group">
                    <option value="">Pilih Jenis Soal</option>
                    <option value="tkp">TKP</option>
                    <option value="nontkp">NON-TKP</option>
                  </select>
                </div> 
                
                 <!-- <div class="form-group">
                  <label for="">Dasar Kriteria Lulus</label>
                  <select name="dasar" id="dasar" class="form-control form-group">
                    <option value="">Pilih Dasar Kriteria</option>
                    <option value="nilai">Nilai Minimal</option>
                    <option value="soal">Jumlah Soal Benar</option>
                  </select>
                </div> 
                 <div class="form-group" id="nl">
                  <label for="">Nilai</label>
                  <input type="text" name="nilai" id="nilai" class="form-control">
                </div> 
                 <div class="form-group" id="jml">
                  <label for="">Jumlah Soal</label>
                  <input type="text" name="soal" id="soal" class="form-control">
                </div>  -->

                <div class="form-group pull-right">
                            <a href="<?=base_url('SoalNew')?>" class="btn btn-flat btn-default"><i class="fa fa-arrow-left"></i> Batal</a>
                            <button type="submit" id="submit" class="btn btn-flat bg-purple"><i class="fa fa-save"></i> Simpan</button>
                        </div>
                </div>
                </div>
            </div>
        </div>
        <?=form_close();?>
    </div>
</div>
<!-- <script>
$(function() {
    $('#nl').hide(); 
    $('#jml').hide(); 
    $('#dasar').change(function(){
        if($('#dasar').val() == 'nilai') {
            $('#nl').show(); 
            $('#jml').hide(); 
        } else if($('#dasar').val() == 'soal') {
            $('#nl').hide(); 
            $('#jml').show(); 
        } else{
          $('#nl').hide(); 
          $('#jml').hide(); 
        }
    });
});
</script> -->