<?php
include 'template/header.php';
?>
<div class="page-title parallax parallax1">
        <div class="overlay"></div>
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="page-title-heading">
                        <h1 class="title">Register</h1>
                        <div class="breadcrumbs">
                            <ul>
                                <li><a href="<?php echo base_url();?>home">Home</a></li>
                            </ul>
                        </div>
                    </div>
                    <!-- /.page-title-captions -->

                </div>
                <!-- /.col-md-12 -->
            </div>
            <!-- /.row -->
        </div>
        <!-- /.container -->
    </div>
<div class="flat-row pad-top90px">
        <div class="container">
            <div class="row">
                <div class="col-md-6">
                  <form  method="post" id="form" role="form" class="contactForm">
                    <input type="hidden" id="crfs" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>">
                    <div class="form-group">
                      <label style="color:#000000">Kategori Mapel</label>
                      <select name="jurusan" id="jurusan" class="form-control">
                        <option value="">--Pilih Kategori--</option>
                        <?php foreach ($kat as $key => $value): ?>
                          <option value="<?= $value['id'] ?>"><?= $value['nama'] ?></option>
                        <?php endforeach ?>
                      </select>
                      <div class="validation"></div>
                    </div>
                    <div class="form-group">
                      <label style="color:#000000">Nama Lengkap</label>

                      <input type="text" name="nama_lengkap" class="form-control" id="name" placeholder="Nama Lengkap" data-rule="minlen:4" data-msg="Please enter at least 4 chars" required="" />
                      <div class="validation"></div>
                    </div>
                    <div class="form-group">
                      <label style="color:#000000">Username</label>

                      <input type="text" name="username" class="form-control" id="name" placeholder="Username" data-rule="minlen:4" data-msg="Please enter at least 4 chars" required="" />
                      <div class="validation"></div>
                    </div>
                    <div class="form-group">
                      <label style="color:#000000">Password</label>

                      <input type="password" name="password" class="form-control" id="name" placeholder="Password" data-rule="minlen:4" data-msg="Please enter at least 4 chars" required="" />
                      <div class="validation"></div>
                    </div>
                    <div class="form-group">
                      <label style="color:#000000">Re-Password</label>

                      <input type="password" name="re_password" class="form-control" id="name" placeholder="Retype Password" data-rule="minlen:4" data-msg="Please enter at least 4 chars" required="" />
                      <div class="validation"></div>
                    </div>
                    <div class="form-group">
                      <label style="color:#000000">Email</label>

                      <input type="email" class="form-control" name="email" id="email" placeholder="Email" data-rule="email" data-msg="Please enter a valid email" />
                      <div class="validation"></div>
                    </div>

                  </div>
                  <div class="col-md-6 ">
                    <div class="form-group">
                      <label style="color:#000000">Nomor HP</label>

                      <input type="text" name="nomor_hp" class="form-control" id="name" placeholder="Nomor HP/Whatsapp" data-rule="minlen:4" data-msg="Please enter at least 4 chars" required="" />
                      <div class="validation"></div>
                    </div>
                    <div class="form-group">
                      <label style="color:#000000">Jenis Kelamin</label>
                      <select name="jenis_kelamin" class="form-control">
                        <option value="">--Pilih Jenis Kelamin--</option>
                        <option value="L">Laki-laki</option>
                        <option value="P">Perempuan</option>
                      </select>
                      <div class="validation"></div>
                    </div>
                    <div class="form-group">
                      <label style="color:#000000">Provinsi</label>
                      <select name="provinsi" id="provinsi" class="form-control">
                        <option value="">--Pilih Provinsi--</option>
                        <?php foreach ($prov as $key => $value): ?>
                          <option value="<?= $value['id'] ?>"><?= $value['nama'] ?></option>
                        <?php endforeach ?>
                      </select>
                      <div class="validation"></div>
                    </div>
                    <div class="form-group">
                      <label style="color:#000000">Kabupaten</label>
                      <select name="kabupaten" id="kabupaten" class="form-control">
                        <option value="">--Pilih Kabupaten--</option>
                        <!-- <?php foreach ($kab as $key => $value): ?>
                          <option value="<?= $value['id'] ?>"><?= $value['nama'] ?></option>
                        <?php endforeach ?> -->
                      </select>
                      <div class="validation"></div>
                    </div>
                    <div class="form-group">
                      <label style="color:#000000">Alamat</label>

                      <textarea class="form-control" name="alamat" rows="5" data-rule="required" data-msg="Please write something for us" placeholder="Alamat lengkap"></textarea>
                      <div class="validation"></div>
                    </div>
                    </div>
                    <div class="col-md-12">


                  </div>
                </form>
                    <div class="text-right"><button type="submit" id="submit" name="submit" class="btn btn-primary btn-lg" required="required">Registrasi</button></div>
                </div><!-- /.col-md-6 .col-md-offset-3 -->
            </div><!-- /.row -->
        </div><!-- /.container -->
    </div>
<?php
include 'template/footer.php';
?>
<script>
// $('#form').on('submit', function (e) {
  $('#submit').click(function() {
  var formdata=$("#form").serialize();
        console.log(formdata);
        $.ajax(
            {
                type:'POST',
                url:'<?= base_url('home/ProsesRegistrasi')?>',
                data:formdata,
                dataType:'json',
                success:function(data){
                    if (data.status == 200) {
                        toastr.success(
                        data.msg, 'Berhasil ',
                        {
                            timeOut: 3000
                        },
                        );
                        setTimeout(function () {
                          window.location.reload();
                        }, 3300);

                    } else {
                            toastr.error(data.msg, 'Gagal !',
                            {
                                timeOut: 5000
                            }
                            );
                    }
                }
            }
            );
});
function ajaxcsrf() {
					var csrfname = '<?= $this->security->get_csrf_token_name() ?>';
					var csrfhash = '<?= $this->security->get_csrf_hash() ?>';
					var csrf = {};
					csrf[csrfname] = csrfhash;
					$.ajaxSetup({
						"data": csrf
					});
				}
$(document).ready(function(){
  ajaxcsrf();
		$('#provinsi').change(function(){
      var id=$(this).val();
      var crsf=$('#crfs').val();

			$.ajax({
				url : "<?php echo base_url();?>landing/getKabByidprovinsi",
				method : "POST",
        data : {id: id
        },
				async : false,
		    dataType : 'json',
				success: function(data){
          console.log(data);
					var html = '';
		            var i;
		            for(i=0; i<data.length; i++){
		                html += '<option name="kabupaten" value="'+data[i].id+'">'+data[i].nama+'</option>';
		            }
		            $('#kabupaten').html(html);

				}
			});
		});
	});
</script>
