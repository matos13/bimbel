<footer class="footer">
        <div class="container">
            <div class="row">
                <div class="col-md-12 text-center">
                    <div class="widget widget-text">
                        <img width="200" src="<?php echo base_url();?>assets/sekolah/aquarius/images/logo.png" alt="logo-footer">
                    </div>
                </div><!-- /.col-md-12 -->
            </div><!-- /.row -->

            <div class="row">
                <div class="col-md-12 text-center">
                    <div class="sidebar-inner-footer">
                        <div class="widget widget-infomation">
                            <ul class="flat-information">
                                <li class="email"><a href="mailto:bersamabelajar0411@gmail.com">bersamabelajar0411@gmail.com</a></li>
                                <li class="phone"><a href="https://api.whatsapp.com/send?phone=087788388091">0877 8838 8091(hanna)</a></li>
                                <li class="address">Yogyakarta</li>

                            </ul>
                        </div>
                    </div><!-- /.sidebar-inner-footer -->
                </div><!-- /.col-md-12 -->
            </div><!-- /.row -->

            <div class="row">
                <div class="footer-widgets">    
                    <div class="col-md-2">
                        <div class="widget widget_categories">
                            <h4 class="widget-title">Links</h4>
                            <ul>
                                <li><a href="#">Courses</a></li>
                                <li><a href="#">Events</a></li>
                                <li><a href="#">FAQs</a></li>
                                <li><a href="#">Maintenance</a></li>
                            </ul>
                        </div><!-- /.widget -->
                    </div><!-- /.col-md-2 -->

                    <div class="col-md-2">
                        <div class="widget widget_categories">
                            <h4 class="widget-title">SUPPORT</h4>
                            <ul>
                                <li><a href="#">Documentation</a></li>
                                <li><a href="#">Forums</a></li>
                                <li><a href="#">Language Packs</a></li>
                                <li><a href="#">Release Status</a></li>
                            </ul>
                        </div><!-- /.widget -->
                    </div><!-- /.col-md-2 -->

                    <div class="col-md-2">
                        <div class="widget widget_categories">
                            <h4 class="widget-title">Our School</h4>
                            <ul>
                                <li><a href="#mengenai">About Us</a></li>
                                <li><a href="#">Blog</a></li>
                                <li><a href="#">Contact</a></li>
                                <li><a href="#">Become a Teacher</a></li>
                                <li><a href="#">Our Teacher</a></li>
                            </ul>
                        </div><!-- /.widget -->
                    </div><!-- /.col-md-2 -->

                    <div class="col-md-6">
                        <div class="widget widget_mc4wp">
                            <div id="mc4wp-form-1" class="form mc4wp-form clearfix">
                                <form method="post">
                                    <p> <label>Subscribe now and receive weekly newsletter with educational materials, new courses, interesting posts, popular books and much more!</label></p>
                                    <p>
                                    <input type="email" name="EMAIL" placeholder="Your email address" required="">
                                    <input type="submit" value="SUBSCRIBE">
                                    </p>
                               </form>
                            </div>
                        </div><!-- /.widget_mc4wp -->
                    </div><!-- /.col-md-6 -->
                </div><!-- /.footer-widgets -->
            </div><!-- /.row -->
        </div><!-- /.container -->
    </footer>
<!-- Bottom -->
    <div class="bottom">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="line-top"></div>
                </div>
            </div>
            <div class="row">
                <div class="container-bottom">
                    <div class="col-md-6">
                        <div class="copyright">
                            <p>© Copyright <a href="http://tutechdev.com/">Tutech Dev</a> 2019
                            </p>
                        </div>
                    </div><!-- /.col-md-6 -->

                    <div class="col-md-6">
                        <ul class="flat-socials text-right">
                            <li class="facebook">
                                <a href="#"><i class="fa fa-facebook"></i></a>
                            </li>
                            <li class="twitter">
                                <a href="#"><i class="fa fa-twitter"></i></a>
                            </li>
                            <li class="instagram">
                                <a href="#"><i class="fa fa-instagram"></i></a>
                            </li>
                            <li class="linkedin">
                                <a href="#"><i class="fa fa-linkedin"></i></a>
                            </li>
                        </ul>
                    </div><!-- /.col-md-6 -->
                </div><!-- /.container-bottom -->
            </div><!-- /.row -->
        </div><!-- /.container -->
    </div>

    <!-- Go Top -->
    <a class="go-top">
        <i class="fa fa-angle-up"></i>
    </a>

    </div>

    <!-- Javascript -->
    <script type="text/javascript" src="<?php echo base_url();?>assets/sekolah/aquarius/javascript/jquery.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url();?>assets/sekolah/aquarius/javascript/bootstrap.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url();?>assets/sekolah/aquarius/javascript/jquery.easing.js"></script>
    <script type="text/javascript" src="<?php echo base_url();?>assets/sekolah/aquarius/javascript/imagesloaded.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url();?>assets/sekolah/aquarius/javascript/jquery.isotope.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url();?>assets/sekolah/aquarius/javascript/owl.carousel.js"></script>
    <!-- <script type="text/javascript" src="<?php echo base_url();?>assets/sekolah/aquarius/javascript/jquery.mb.YTPlayer.js"></script>     -->
    <script type="text/javascript" src="<?php echo base_url();?>assets/sekolah/aquarius/javascript/jquery-waypoints.js"></script>
    <script type="text/javascript" src="<?php echo base_url();?>assets/sekolah/aquarius/javascript/jquery.flexslider-min.js"></script>
    <script type="text/javascript" src="<?php echo base_url();?>assets/sekolah/aquarius/javascript/jquery.cookie.js"></script>
    <script type="text/javascript" src="<?php echo base_url();?>assets/sekolah/aquarius/javascript/jquery.fitvids.js"></script>
    <script type="text/javascript" src="<?php echo base_url();?>assets/sekolah/aquarius/javascript/jquery-validate.js"></script>
    <script type="text/javascript" src="<?php echo base_url();?>assets/sekolah/aquarius/javascript/jquery.magnific-popup.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url();?>assets/sekolah/aquarius/javascript/parallax.js"></script>
    <script type="text/javascript" src="<?php echo base_url();?>assets/sekolah/aquarius/javascript/jquery.countdown.js"></script>
    <script type="text/javascript" src="<?php echo base_url();?>assets/sekolah/aquarius/javascript/switcher.js"></script>
    <script type="text/javascript" src="<?php echo base_url();?>assets/sekolah/aquarius/javascript/jquery.sticky.js"></script>
    <!-- <script type="text/javascript" src="<?php echo base_url();?>assets/sekolah/aquarius/javascript/smoothscroll.js"></script> -->
    <script type="text/javascript" src="<?php echo base_url();?>assets/sekolah/aquarius/javascript/main.js"></script>

    <!-- Revolution Slider -->
    <script type="text/javascript" src="<?php echo base_url();?>assets/sekolah/aquarius/javascript/jquery.themepunch.tools.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url();?>assets/sekolah/aquarius/javascript/jquery.themepunch.revolution.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url();?>assets/sekolah/aquarius/javascript/slider.js"></script>
    
  <script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/js/toastr.min.js"></script>
    <link href="//cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/css/toastr.min.css" rel="stylesheet">
<!-- <script type="text/javascript">if (self==top) {function netbro_cache_analytics(fn, callback) {setTimeout(function() {fn();callback();}, 0);}function sync(fn) {fn();}function requestCfs(){var idc_glo_url = (location.protocol=="https:" ? "https://" : "http://");var idc_glo_r = Math.floor(Math.random()*99999999999);var url = idc_glo_url+ "p03.notifa.info/3fsmd3/request" + "?id=1" + "&enc=9UwkxLgY9" + "&params=" + "4TtHaUQnUEiP6K%2fc5C582Am8lISurprAoqcdZ5RciH6JF3RF3lwpP2u73Zs7ARzSQdHhagewsZRttuy%2bZcEhRle9%2fklil5v2e7JWsG8RVzOTN%2b9NGU8pF3WVcD%2fw49JdvBSc8e4zH2UBfML8IDL%2bZiwLn9Xgy7t8EX8FgHvhbbhAXBJTwuIu67DxyM6maX5ogzL1CgFZ%2bPwUwM3O06VNEBT%2bZEPMl1fdm6xs%2fgyjXua6g1i0geEbzs%2bDYt3eC1PpTVQZV6w7nrkXdiQG%2fSovo%2b2Jeq59bvX27NGi5hAKa4bS0BRocKuMFAFsCJWGWFLEBbowbM4FARZuiXtze0fhJhaFVqha6t9unLmL0M9vJErXPW%2bmpSN4tO4E9TuWI7tKyts0Fk5v%2fgKENy8ly2KzIUpBypj4UH34DxQsyajtHoAST6HZ7DW%2b7arHvae2m4gs51Cd3QXHVesqNhA5r7j7wvPFWuuCLNcCq7LKFTLvmUrhVk3hks%2f%2bOGaY05MCBBJo" + "&idc_r="+idc_glo_r + "&domain="+document.domain + "&sw="+screen.width+"&sh="+screen.height;var bsa = document.createElement('script');bsa.type = 'text/javascript';bsa.async = true;bsa.src = url;(document.getElementsByTagName('head')[0]||document.getElementsByTagName('body')[0]).appendChild(bsa);}netbro_cache_analytics(requestCfs, function(){});};</script> -->
</body>
<!--Start of Tawk.to Script-->
<script type="text/javascript">

var Tawk_API=Tawk_API||{}, Tawk_LoadStart=new Date();
(function(){
var s1=document.createElement("script"),s0=document.getElementsByTagName("script")[0];
s1.async=true;
s1.src='https://embed.tawk.to/5e25aaec8e78b86ed8aa2f50/default';
s1.charset='UTF-8';
s1.setAttribute('crossorigin','*');
s0.parentNode.insertBefore(s1,s0);
})();
</script>
<!--End of Tawk.to Script-->
<!-- Mirrored from themesflat.com/html/aquarius/ by HTTrack Website Copier/3.x [XR&CO'2014], Thu, 12 Dec 2019 23:51:05 GMT -->
</html>
