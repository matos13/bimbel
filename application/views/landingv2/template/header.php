<!DOCTYPE html>
<!--[if IE 8 ]><html class="ie" xmlns="http://www.w3.org/1999/xhtml" xml:lang="en-US" lang="en-US"> <![endif]-->
<!--[if (gte IE 9)|!(IE)]><!--><html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en-US" lang="en-US"><!--<![endif]-->

<!-- Mirrored from themesflat.com/html/aquarius/ by HTTrack Website Copier/3.x [XR&CO'2014], Thu, 12 Dec 2019 23:46:47 GMT -->
<head>
    <!-- Basic Page Needs -->
    <meta charset="utf-8">
    <!--[if IE]><meta http-equiv='X-UA-Compatible' content='IE=edge,chrome=1'><![endif]-->
    <title>Bersama Belajar </title>

    <meta name="author" content="themesflat.com">

    <!-- Mobile Specific Metas -->
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">

    <!-- Bootstrap  -->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/sekolah/aquarius/stylesheets/bootstrap.css" >

    <!-- Theme Style -->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/sekolah/aquarius/stylesheets/style.css">

    <!-- Responsive -->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/sekolah/aquarius/stylesheets/responsive.css">

    <!-- Colors -->
    <!-- <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/sekolah/aquarius/stylesheets/colors/color1.css" id="colors"> -->

	<!-- Animation Style -->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/sekolah/aquarius/stylesheets/animate.css">

    <!-- Favicon and touch icons  -->
    <link href="icon/apple-touch-icon-48-precomposed.png" rel="apple-touch-icon-precomposed" sizes="48x48">
    <link href="icon/apple-touch-icon-32-precomposed.png" rel="apple-touch-icon-precomposed">
    <link href="<?php echo base_url();?>assets/sekolah/aquarius/images/logo.png" rel="shortcut icon">

    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body class="header-sticky">
    <div class="loader">
        <span class="loader1 block-loader"></span>
        <span class="loader2 block-loader"></span>
        <span class="loader3 block-loader"></span>
    </div>

    <!-- Boxed -->
    <div class="boxed">

    <div class="top">
        <div class="container">
            <div class="row">
                <div class="col-md-6">
                    <div class="language">
                        <ul class="flat-information">
                            <li class="email"><a href="mailto:bersamabelajar0411@gmail.com">bersamabelajar0411@gmail.com</a></li>
                            <li class="phone"><a href="https://api.whatsapp.com/send?phone=087788388091">087788388091(hanna)</a></li>
                            <!-- <li class="phone"></li> -->
                            <!-- <li class="current"><a href="#">English</a>
                                <ul>
                                    <li class="en"><a href="#">English</a></li>
                                    <li class="ge"><a href="#">German</a></li>
                                    <li class="pl"><a href="#">Polish</a></li>
                                    <li class="ru"><a href="#">Russian</a></li>
                                </ul>
                            </li> -->
                        </ul>
                    </div><!-- /.language -->
                </div><!-- /.col-md-6 -->
                <div class="col-md-6">
                    <div class="top-navigator">
                        <ul>
                            <li><a href="<?php echo base_url();?>home/daftar"">Register</a></li>
                            <li><a href="<?php echo base_url();?>login">Login</a></li>
                        </ul>
                    </div><!-- /.top-navigator -->
                </div><!-- /.col-md-6 -->
            </div><!-- /.row -->
        </div><!-- /.container -->
    </div><!-- /.top -->

    <!-- Header -->
    <header id="header" class="header clearfix">
        <div class="header-wrap clearfix">
            <div class="container">
                <div class="row">
                    <div class="col-md-2">
                        <div id="logo" class="logo">
                            <a href="<?php echo base_url();?>home"" rel="home">
                                <img width="100" src="<?php echo base_url();?>assets/sekolah/aquarius/images/logo.png" alt="image">
                            </a>
                        </div><!-- /.logo -->
                        <div class="btn-menu">
                            <span></span>
                        </div><!-- //mobile menu button -->
                    </div><!-- /.col-md-2 -->
                    <div class="col-md-10">
                        <div class="nav-wrap">

                            <nav id="mainnav" class="mainnav">
                                <ul class="menu">
                                    <li class="home">
                                        <a href="<?php echo base_url();?>home">Home</a>
                                        <!-- <ul class="submenu">
                                            <li><a href="index-2.html">Home version 1</a></li>
                                            <li><a href="index_2.html">Home version 2</a></li>
                                            <li><a href="index_3.html">Home version 3</a></li>
                                            <li><a href="index_4.html">Home version 4</a></li>
                                            <li><a href="index_animated.html">Home Animated</a></li>
                                        </ul> -->
                                        <!-- /.submenu -->
                                    </li>
                                    <li><a href="#">Courses</a>
                                        <ul class="submenu">
                                            <li><a href="#">Course</a></li>
                                            <li><a href="#">Course List</a></li>
                                            <li><a href="#">Course Detail</a></li>
                                            <li><a href="#">Course Categories</a></li>
                                        </ul><!-- /.submenu -->
                                    </li>
                                    <li><a href="#mengenai">About Us</a>
                                    </li>
                                    <li><a href="#">Contact Us</a>
                                    </li>
                                    <li><a href="#galery">Gallery</a>
                                    </li>
                                </ul><!-- /.menu -->
                            </nav><!-- /.mainnav -->
                        </div><!-- /.nav-wrap -->
                    </div><!-- /.col-md-10 -->
                </div><!-- /.row -->
            </div><!-- /.container -->
            <div class="show-search">
                <a href="#"><i class="fa fa-search"></i></a>
            </div><!-- /.show-search -->
            <div class="nav-search">
                <div class="container">
                    <div class="col-md-12">
                       <div class="top-search" id="s1">
                            <aside id="search-4" class="widget widget_search">
                                <form role="search" method="get" class="search-form">
                                    <label>
                                        <input type="search" class="search-field" placeholder="Search …" value="" name="s" title="Search for:">
                                    </label>
                                    <input type="submit" class="search-submit" value="Search">
                                </form>
                            </aside>
                        </div><!-- /.top-search -->
                    </div>
                </div>
            </div><!-- /.nav-search -->
        </div><!-- /.header-inner -->
    </header><!-- /.header -->
