
<?php
include 'template/header.php';
?>
    <!-- Slider -->
    <div class="tp-banner-container">
        <div class="tp-banner" >
            <ul>
                <li data-transition="slideup" data-slotamount="7" data-masterspeed="1000" data-saveperformance="on">
                    <img src="<?php echo base_url();?>assets/sekolah/aquarius/images/slides/1.jpg" alt="slider-image" />
                    <!-- <div class="tp-caption sfl title-slide center" data-x="379" data-y="245" data-speed="1000" data-start="1000" data-easing="Power3.easeInOut">
                        Selemat Datang di Bersama Belajar
                    </div>  
                    <br> -->
                    <!-- <div class="tp-caption sfl title-slide center" data-x="379" data-y="245" data-speed="1000" data-start="1000" data-easing="Power3.easeInOut">
                        <i>Bersiap Bersama, Sukses Bersama</i>
                    </div>   -->
                    <!-- <div class="tp-caption sfr desc-slide center" data-x="306" data-y="322" data-speed="1000" data-start="1500" data-easing="Power3.easeInOut">                       
                       Tentukan impian dan tujuan, dan mari belajar bersama
                    </div>                             -->
                </li>

                <li data-transition="random-static" data-slotamount="7" data-masterspeed="1000" data-saveperformance="on">
                    <img src="<?php echo base_url();?>assets/sekolah/aquarius/images/slides/2.jpg" alt="slider-image" />

                    <div class="tp-caption sfb title-slide color-white style1" data-x="15" data-y="254" data-speed="1000" data-start="1000" data-easing="Power3.easeInOut">Selamat Datang di ,<br>Belajar Bersama <br>Sukses Bersama.</div>

                    <div style="font-size:20px !important;"cl class="tp-caption sfb desc-slide color-white" data-x="15" data-y="398" data-speed="1000" data-start="1500" data-easing="Power3.easeInOut"><br>Tentukan impianmu dan tujuanmu,dan mari <br> bersama menggampainya</div>

                    <!-- <div class="tp-caption sfb center color-white flat-button-slide" data-x="15" data-y="528" data-speed="1000" data-start="2000" data-easing="Power3.easeInOut">
                        <a href="#">View Our courses</a>
                    </div> -->
                </li>

                <li data-transition="random-static" data-slotamount="7" data-masterspeed="1000" data-saveperformance="on">
                    <img src="<?php echo base_url();?>assets/sekolah/aquarius/images/slides/3.jpg" alt="slider-image" />

                    <div class="tp-caption sfb title-slide color-white center style2" data-x="100" data-y="285" data-speed="1000" data-start="1000" data-easing="Power3.easeInOut">Belajar Bersama dengan <br> Teman Dekatmu, Bersama <br> Belajar dengan Pengajar <br> Berpengalaman </div>
                
                    <div   style="font-size:20px !important;"class="tp-caption sft desc-slide color-white center" data-x="100" data-y="480" data-speed="1000" data-start="1500" data-easing="Power3.easeInOut">Peserta dapat membentuk kelompok belajar sendiri<br>dan menentukan jumlah peserta dalam kelasnya <br>sehingga proses belajar terasa nayaman dan maksimal. <br> Pelajaran tetap juga memandu proses belajar untuk <br> memastikan prose belajar berjalan dengan baik </div>

                    <!-- <div class="tp-caption sft center color-white flat-button-slide style1" data-x="484" data-y="463" data-speed="1000" data-start="2000" data-easing="Power3.easeInOut">
                        <a href="#">learn more</a>
                    </div> -->
                </li>

                <li data-transition="slidedown" data-slotamount="7" data-masterspeed="1000" data-saveperformance="on">
                    <img src="<?php echo base_url();?>assets/sekolah/aquarius/images/slides/4.jpg" alt="slider-image" />

                    <!-- <div class="tp-caption sft sub-title" data-x="480" data-y="232" data-speed="1000" data-start="1000" data-easing="Power3.easeInOut">Course, Online Courses Template</div> -->

                    <div class="tp-caption sft title-slide color-white center style3" data-x="218" data-y="300" data-speed="1000" data-start="1500" data-easing="Power3.easeInOut">Belajar Bersama Offline,<br> Bersama Belajar Online,<br> Kurikulum terbaik</div>

                    <div class="tp-caption sfb desc-slide color-white center style1" data-x="319" data-y="480" data-speed="1000" data-start="2000" data-easing="Power3.easeInOut">Proses belajar memadukan belajar offline (tatap muka di kelas)<br> dan Online (mandiri) dengan kurikulum <br>terbaik dan proses belajar yang menyenangkan.</div>                        
                </li>
            </ul>
        </div>
    </div>

    <div class="flat-divider d55px"></div>

    <!-- About us style1 -->
    <section class="flat-row" id="mengenai">
        <div class="container">
            <div class="row">
                <div class="flat-about-us clearfix">
                    <div class="col-md-7">
                        <div class="about-us-img clearfix">
                            <div class="v1">
                                <div class="overly-img"></div>  
                                <img src="<?php echo base_url();?>assets/sekolah/aquarius/images/about/about2.jpg" alt="image">
                            </div>
                            <div class="v2">
                                <div class="overly-img"></div>
                                <img src="<?php echo base_url();?>assets/sekolah/aquarius/images/about/about6.png" alt="image">
                            </div>
                            <div class="v3">
                                <div class="overly-img"> </div>  
                                <img src="<?php echo base_url();?>assets/sekolah/aquarius/images/about/about1.jpg" alt="image">
                            </div>                            
                        </div><!-- /.about-us-img -->
                    </div><!-- /.col-md-7 -->
                    
                    <div class="col-md-5">
                        <div class="about-us style1">
                            <h2 class="title-about-us">Mengenai Bersama Belajar
</h2>
                            <div class="desc">
                                <p>Bersama belajar merupakan mitra belajar yang membantu proses persiapan ujian masuk 
terkait instansi (Ujian CPNS) dan/atau sekolah tinggi kedinasan (USM PTK) yang sudah 
terbukti membantu peserta-pesertanya untuk mencapai tujuan dan impiannya.</p>
                                <p>Berawal dari perjuangan Founder pada saat menyiapkan ujian masuk DIV PKN STAN, 
Bersama Belajar memiliki semangat untuk tidak sekedar memberikan pelajaran saja, namun 
benar-benar total dalam memberikan panduan persiapan melalui kurikulum (Latihan, Mini Kuis, 
Kuiz, serta Tryout berkala) dan metode belajar terbaik yang memadukan metode belajar 
offline dan online. Untuk membuat proses belajar semakin nyaman, Bersama belajar juga 
memberikan kesempatan bagi peserta untuk menentukan temannya sendiri dan jumlah 
anggota per kelas.
</p>
<p>
Tim Bersama Belajar merupakan orang-orang yang berpengalaman di bidangnya yang 
sebagian besar berkerja di bidang kedinasan, yang pastinya akan sangat membantu proses 
belajar. Dan, Tim Bersama belajar juga benar-benar peduli terhadap peserta dan akan 
membantu secara offline maupun online, terkait akademis maupun non akademis yang 
ditunjukkan dengan adanya admin akademis dan nonakademis dalam struktrur tim.
Mari bergabung Bersama Belajar: bersiap Bersama, Sukses Bersama.

</p>
                            </div>
                        </div><!-- /.about-us -->
                    </div><!-- /.col-md-5 -->
                </div><!-- /.flat-about-us -->
            </div><!-- /.row -->
        </div><!-- /.container -->
    </section><!-- /.flat-row -->

    <div class="flat-divider d88px"></div>

    <section class="flat-row parallax parallax3">
        <div class="overlay"></div>
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="title-section cl-white">
                        <h1 class="title">Search For Courses</h1>
                        <div class="desc">
                           <!--  <p>Vut sagittis a, magna ridiculus! Scelerisque parturient! Cum duis nunc in, dignissim, porta porta enim, proin<br> turpis egestas! Mauris dapibus sed integer placerat, scelerisque!</p> -->
                        </div>
                    </div><!-- /.title-section -->

                    <div class="row">
                        <div class="flat-divider d50px"></div>
                    </div>

                    <!-- Fillter courses -->
                    <!-- <form class="flat-contact-form fillter-courses border-radius border-white text-center" id="contactform5" method="post" action="http://themesflat.com/html/aquarius/contact/contact-process.php">
                        <div class="field clearfix">      
                            <div class="wrap-type-input">
                                <div class="wrap categories-courses">
                                    <select class="select-field" name="appointment_services">
                                        <option value="" selected="selected">All categories </option>
                                        <option value="Categories 1">Categories 1</option>
                                        <option value="Categories 2"> Categories 2</option>
                                        <option value="Categories 3"> Categories 3</option>
                                    </select> 
                                </div> -->
                                <!-- /.wrap-select -->

                                <!-- <div class="wrap courses-level">
                                    <select class="select-field" name="appointment_services">
                                        <option value="" selected="selected">Courses level</option>
                                        <option value="Level 1">Level 1</option>
                                        <option value="Level 2">Level 2</option>
                                        <option value="Level 3">Level 3</option>
                                    </select> 
                                </div> -->
                                <!-- /.wrap-select -->
<!--                                 
                                <div class="wrap courses-keyword">
                                    <input type="text" value="" placeholder="Courses keyword" name="subject-flat" id="subject-flat" >
                                </div>

                                <div class="wrap all-categories">
                                    <button>All categories</button>
                                </div>   -->
                            <!-- </div> -->
                            <!-- /.wrap-type-input -->                            
                        <!-- </div> -->
                        <!-- /.field -->                        
                    <!-- </form> -->
                </div><!-- /.col-md-12 -->
            </div><!-- /.row -->
        </div><!-- /.container -->        
    </section>

    <!-- Flat courses -->


    <!-- Portfolio -->
    <section  class="flat-row portfolio-row-page" id="galery">
        <!-- Portfolio Fullwidth -->
        <div class="flat-portfolio">  
            <div class="portfolio-wrap clearfix">
                <div class="item">                                     
                    <a class="popup-gallery" href="<?php echo base_url();?>assets/sekolah/aquarius/images/portfolio/1.3.jpg"><img src="<?php echo base_url();?>assets/sekolah/aquarius/images/portfolio/1.3.jpg" alt="images"></a>
                    <div class="overlay">                            
                    </div>                    
                </div><!-- /.portfolio-item -->

                <div class="item">                                     
                    <a class="popup-gallery" href="<?php echo base_url();?>assets/sekolah/aquarius/images/portfolio/1.2.jpg"><img src="<?php echo base_url();?>assets/sekolah/aquarius/images/portfolio/1.2.jpg" alt="images"></a>
                    <div class="overlay">                            
                    </div>                    
                </div><!-- /.portfolio-item -->

                <div class="item w50">                                     
                    <a class="popup-gallery" href="<?php echo base_url();?>assets/sekolah/aquarius/images/portfolio/1.1.jpg"><img src="<?php echo base_url();?>assets/sekolah/aquarius/images/portfolio/1.1.jpg" alt="images"></a>
                    <div class="overlay">                            
                    </div>                    
                </div><!-- /.portfolio-item -->
     
                <div class="item">                                     
                    <a class="popup-gallery" href="<?php echo base_url();?>assets/sekolah/aquarius/images/portfolio/1.4.jpg"><img src="<?php echo base_url();?>assets/sekolah/aquarius/images/portfolio/1.4.jpg" alt="images"></a>
                    <div class="overlay">                            
                    </div>                   
                </div><!-- /.portfolio-item -->

                <div class="item">                                     
                    <a class="popup-gallery" href="<?php echo base_url();?>assets/sekolah/aquarius/images/portfolio/1.5.jpg"><img src="<?php echo base_url();?>assets/sekolah/aquarius/images/portfolio/1.5.jpg" alt="images"></a>
                    <div class="overlay">                            
                    </div>                    
                </div><!-- /.portfolio-item -->

                <div class="item">                                     
                    <a class="popup-gallery" href="<?php echo base_url();?>assets/sekolah/aquarius/images/portfolio/1.6.jpg"><img src="<?php echo base_url();?>assets/sekolah/aquarius/images/portfolio/1.6.jpg" alt="images"></a>
                    <div class="overlay">                            
                    </div>                    
                </div><!-- /.portfolio-item -->

                <div class="item">                                     
                    <a class="popup-gallery" href="<?php echo base_url();?>assets/sekolah/aquarius/images/portfolio/1.7.jpg"><img src="<?php echo base_url();?>assets/sekolah/aquarius/images/portfolio/1.7.jpg" alt="images"></a>
                    <div class="overlay">                            
                    </div>                    
                </div><!-- /.portfolio-item -->

                <div class="item">                                     
                    <a class="popup-gallery" href="<?php echo base_url();?>assets/sekolah/aquarius/images/portfolio/1.8.jpg"><img src="<?php echo base_url();?>assets/sekolah/aquarius/images/portfolio/1.8.jpg" alt="images"></a>
                    <div class="overlay">                            
                    </div>                    
                </div><!-- /.portfolio-item -->

                <div class="item">                                     
                    <a class="popup-gallery" href="<?php echo base_url();?>assets/sekolah/aquarius/images/portfolio/1.9.jpg"><img src="<?php echo base_url();?>assets/sekolah/aquarius/images/portfolio/1.9.jpg" alt="images"></a>
                    <div class="overlay">                            
                    </div>                    
                </div><!-- /.portfolio-item -->       

            </div><!-- /.portfolio-wrap -->
        </div><!-- /.flat-portfolio --> 
    </section>

    <div class="flat-divider d90px"></div>

    <!-- Team member style1-->
    <section class="flat-row">
        <div class="container">
            <div class="row">
                <div class="flat-teammember clearfix">
                    <div class="col-md-9 col-xs-12">               
                        <div class="flat-team-carosuel" data-item="1" data-nav="false" data-dots="false" data-auto="true">
                            <div class="flat-team style1">
                                <div class="descritption">
                                    <blockquote>Pada saat saya mempersiapkan diri untuk mengikuti ujian DIV STAN pada tahun 2017, saya
merasakan banyak tekanan dan kebingunan. Berbeda dengan pada saat mengikuti Ujian DIII 
ditahun 210, ujian PKN DIV PKN  STAN lebih berat karenga bersaing dengan sesama 
lulusan DIII PKN STAN. Pada saat itulah, saya memiliki keinginan apabila diterima di DIV, maka 
saya akan membatu adik-adik kelas untuk mempersiapkan  Ujian yang sama.
Setelah founder diterima di DIV PKN STAN, founder memulai kelompok Bersama Belajar dimana
Founder ingin menciptakan mitra bersama belajar yang bener-bener peduli terhadap pesertanya.
Maka lahirlah Bersama Belajar, sebuah mitra belajar benar-benar peduli terhadap pesertanya,
baik secara akademis maupun nonakademis. Kepedualian ini kami wujudkan dalam diskusi rutin
kelompok,latihan berkala,mini quiz berkala, kuiz berkala, tryout berkala, admin materi yang siap 
berdikusi setiap saat, admin konsultasi yang akan memberikan saran dan masukan terkain bidang nonakademis. 
Ditahun pertama, Bersama Belajar membatu 84% pesertanya untuk lulu, dan di tahun kedua membatu 42.5%

Bergabunglah Bersama kami, dan biarkan kami membantu mengeluarkan kemampuan terbaik 
dalam proses persapan menggapai mimpi, karna kami benar-benar peduli. </blockquote>
                                </div>
                                <div class="profile">
                                    <span class="name">MDSN , </span>
                                    <span class="position">Founder Belajar Bersama</span>
                                </div>
                                <!-- <div class="signature">
                                    <p>Jen Maroney</p>
                                </div> -->
                            </div><!-- /.flat-team --> 
                            <div class="flat-team style1">
                                <div class="descritption">
                                    <blockquote>Pada saat saya mempersiapkan diri untuk mengikuti ujian DIV STAN pada tahun 2017, saya
merasakan banyak tekanan dan kebingunan. Berbeda dengan pada saat mengikuti Ujian DIII 
ditahun 210, ujian PKN DIV PKN  STAN lebih berat karenga bersaing dengan sesama 
lulusan DIII PKN STAN. Pada saat itulah, saya memiliki keinginan apabila diterima di DIV, maka 
saya akan membatu adik-adik kelas untuk mempersiapkan  Ujian yang sama.
Setelah founder diterima di DIV PKN STAN, founder memulai kelompok Bersama Belajar dimana
Founder ingin menciptakan mitra bersama belajar yang bener-bener peduli terhadap pesertanya.
Maka lahirlah Bersama Belajar, sebuah mitra belajar benar-benar peduli terhadap pesertanya,
baik secara akademis maupun nonakademis. Kepedualian ini kami wujudkan dalam diskusi rutin
kelompok,latihan berkala,mini quiz berkala, kuiz berkala, tryout berkala, admin materi yang siap 
berdikusi setiap saat, admin konsultasi yang akan memberikan saran dan masukan terkain bidang nonakademis. 
Ditahun pertama, Bersama Belajar membatu 84% pesertanya untuk lulu, dan di tahun kedua membatu 42.5%

Bergabunglah Bersama kami, dan biarkan kami membantu mengeluarkan kemampuan terbaik 
dalam proses persapan menggapai mimpi, karna kami benar-benar peduli. </blockquote>
                                </div>
                                <div class="profile">
                                    <span class="name">MDSN , </span>
                                    <span class="position">Founder Belajar Bersama</span>
                                </div>
                                <!-- <div class="signature">
                                    <p>Jen Maroney</p>
                                </div> -->
                            </div>
                            <!-- /.flat-team -->                                      
                        </div><!-- /.flat-team-carosuel -->
                    </div><!-- /.col-md-9 --> 

                   <div class="col-md-3 col-xs-12">
                       <div class="team-image">
                           <img src="<?php echo base_url();?>assets/sekolah/aquarius/images/member/1.png" alt="image" 
                           style="max-width: 400%; height: 600px; opacity: 0.4;"
                           >
                       </div><!-- /.team-image -->
                   </div><!-- /.col-md-3 -->
                </div><!-- /.flat-teammember -->
            </div><!-- /.row -->
        </div><!-- /.container -->
    </section>

    <!-- Flat video parallax -->
 <!--    <section class="flat-row flat-video video-bg parallax bg-playvideo">
        <div class="overlay"></div>        
        <div class="video">
            <div id="bg-video">
                <div class="video-section" data-property="{videoURL:'https://youtu.be/raDmtsOB7n0',containment:'.flat-video.video-bg', autoPlay:false, mute:true, startAt:0, opacity:1, vol: 0, realfullscreen:true, quality: 'hd1080', startAt: 12}"></div>
            </div>
            <div id="video-controls" style="display: block;">
                <a class="fa fa-play text-color color-border" id="video-play" href="#"></a>
                <br />
                <h2 class="title">take the time to visit our school</h2>
            </div> 
        </div>
    </section> -->

    <!-- Team member style2 -->

    <!-- Register -->
    

    <!-- Testimonial -->
   
    <!-- Flat event -->
    

    <!-- Blog -->
    

    <div class="flat-divider d85px"></div>

<?php
include 'template/footer.php';
?>