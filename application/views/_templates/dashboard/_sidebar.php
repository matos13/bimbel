<!-- Left side column. contains the logo and sidebar -->
<aside class="main-sidebar">

	<!-- sidebar: style can be found in sidebar.less -->
	<section class="sidebar">

		<!-- Sidebar user panel (optional) -->
		<div class="user-panel">
			<div class="pull-left image">
				<img src="<?=base_url()?>assets/dist/img/user1.png" class="img-circle" alt="User Image">
			</div>
			<div class="pull-left info">
				<p><?=$user->username?></p>
				<small><?=$user->email?></small>
			</div>
		</div>
		
		<ul class="sidebar-menu" data-widget="tree">
			<li class="header">MAIN MENU</li>
			<!-- Optionally, you can add icons to the links -->
			<?php 
			$page = $this->uri->segment(1);
			$master = ["jurusan", "kelas", "matkul", "dosen", "mahasiswa","paket","bab","materi","jenis","mkelas"];
			$wilayah=['provinsi','kabupaten'];
			$relasi = ["kelasdosen", "jurusanmatkul"];
			$users = ["users"];
			?>
			<li class="<?= $page === 'dashboard' ? "active" : "" ?>"><a href="<?=base_url('dashboard')?>"><i class="fa fa-dashboard"></i> <span>Dashboard</span></a></li>
			<?php if($this->ion_auth->is_admin()) : ?>
			<li class="treeview <?= in_array($page, $master)  ? "active menu-open" : ""  ?>">
				<a href="#"><i class="fa fa-folder"></i> <span>Data Master</span>
					<span class="pull-right-container">
						<i class="fa fa-angle-left pull-right"></i>
					</span>
				</a>
				<ul class="treeview-menu">
					<li class="<?=$page==='jurusan'?"active":""?>">
						<a href="<?=base_url('New/Program')?>">
							<i class="fa fa-circle-o"></i> 
							Master Program Belajar
						</a>
					</li>
					<li class="<?=$page==='kelas'?"active":""?>">
						<a href="<?=base_url('New/Mata_pelajaran')?>">
							<i class="fa fa-circle-o"></i>
							Master Mata Pelajaran
						</a>
					</li>
					<li class="<?=$page==='kelas'?"active":""?>">
						<a href="<?=base_url('New/Kelas')?>">
							<i class="fa fa-circle-o"></i>
							Master Kelas
						</a>
					</li>
					<li class="<?=$page==='paket'?"active":""?>">
						<a href="<?=base_url('paket')?>">
							<i class="fa fa-circle-o"></i>
							Master Paket
						</a>
					</li>
					<li class="<?=$page==='bab'?"active":""?>">
						<a href="<?=base_url('bab')?>">
							<i class="fa fa-circle-o"></i>
							Master Bab
						</a>
					</li>
					<li class="<?=$page==='materi'?"active":""?>">
						<a href="<?=base_url('materi')?>">
							<i class="fa fa-circle-o"></i>
							Master Materi
						</a>
					</li>
					<!-- <li class="<?=$page==='jenis'?"active":""?>">
						<a href="<?=base_url('jenis')?>">
							<i class="fa fa-circle-o"></i>
							Master Jenis Soal
						</a>
					</li> -->
					<li class="<?=$page==='dosen'?"active":""?>">
						<a href="<?=base_url('dosen')?>">
							<i class="fa fa-circle-o"></i>
							Master Pengajar
						</a>
					</li>
					<li class="<?=$page==='mahasiswa'?"active":""?>">
						<a href="<?=base_url('mahasiswa')?>">
							<i class="fa fa-circle-o"></i>
							Master Siswa
						</a>
					</li>
				</ul>
			</li>
			<li class="treeview <?= in_array($page, $wilayah)  ? "active menu-open" : ""  ?>">
				<a href="#"><i class="fa fa-folder"></i> <span>Master wilayah</span>
					<span class="pull-right-container">
						<i class="fa fa-angle-left pull-right"></i>
					</span>
				</a>
				<ul class="treeview-menu">
					<li class="<?=$page==='provinsi'?"active":""?>">
						<a href="<?=base_url('provinsi')?>">
							<i class="fa fa-circle-o"></i> 
							Provinsi
						</a>
					</li>
					<li class="<?=$page==='kabupaten'?"active":""?>">
						<a href="<?=base_url('kabupaten')?>">
							<i class="fa fa-circle-o"></i>
							Kabupaten
						</a>
					</li>
				
				</ul>
			</li>
			<li class="treeview <?= in_array($page, $relasi)  ? "active menu-open" : ""  ?>">
				<a href="#"><i class="fa fa-link"></i> <span>Relasi</span>
					<span class="pull-right-container">
						<i class="fa fa-angle-left pull-right"></i>
					</span>
				</a>
				<ul class="treeview-menu">
					<li class="<?=$page==='kelasdosen'?"active":""?>">
						<a href="<?=base_url('kelasdosen')?>">
							<i class="fa fa-circle-o"></i>
							Kelas - Pengajar
						</a>
					</li>
<!-- 					<li class="<?=$page==='jurusanmatkul'?"active":""?>">
						<a href="<?=base_url('jurusanmatkul')?>">
							<i class="fa fa-circle-o"></i>
							Jurusan - Mata Kuliah
						</a>
					</li> -->
				</ul>
			</li>
			<?php endif; ?>
			<?php if($this->ion_auth->in_group('dosen') ) : ?>
			<li class="header">SOAL</li>
			<li class="<?=$page==='soal'?"active":""?>">
				<a href="<?=base_url('SoalNew')?>" rel="noopener noreferrer">
					<i class="fa fa-file-text-o"></i> <span>Paket Soal Tryout</span>
				</a>
			</li>
			<li class="<?=$page==='soalkuisnew'?"active":""?>">
				<a href="<?=base_url('SoalKuisNew')?>" rel="noopener noreferrer">
					<i class="fa fa-file-text-o"></i> <span>Paket Soal Quiz</span>
				</a>
			</li>
			<li class="<?=$page==='soallatihannew'?"active":""?>">
				<a href="<?=base_url('SoalLatihanNew')?>" rel="noopener noreferrer">
					<i class="fa fa-file-text-o"></i> <span>Paket Soal Latihan</span>
				</a>
			</li>
			<?php endif; ?>
			<?php if($this->ion_auth->in_group('dosen') ) : ?>
			<li class="header">UJIAN</li>
			<li class="<?=$page==='ujian'?"active":""?>">
				<a href="<?=base_url('ujian/master')?>" rel="noopener noreferrer">
					<i class="fa fa-chrome"></i> <span>Data Ujian Tryout</span>
				</a>
			</li>
			<li class="<?=$page==='kuis'?"active":""?>">
				<a href="<?=base_url('kuis/master_kuis')?>" rel="noopener noreferrer">
					<i class="fa fa-chrome"></i> <span>Data Ujian Quiz</span>
				</a>
			</li>
			<li class="<?=$page==='latihan'?"active":""?>">
				<a href="<?=base_url('latihan/master_latihan')?>" rel="noopener noreferrer">
					<i class="fa fa-chrome"></i> <span>Data Ujian Latihan</span>
				</a>
			</li>
			<?php endif; ?>
			<?php if( $this->ion_auth->in_group('mahasiswa') ) : ?>
			<li class="<?=$page==='materi'?"active":""?>">
				<a href="<?=base_url('materi/jenjang')?>" rel="noopener noreferrer">
					<i class="fa fa-chrome"></i> <span>Materi Pembelajaran</span>
				</a>
			</li>
			<li class="header">UJIAN</li>

			<!--<li class="<?=$page==='kuis'?"active":""?>">
				<a href="<?=base_url('kuis/kuis')?>" rel="noopener noreferrer">
					<i class="fa fa-chrome"></i> <span>Kuis</span>
				</a>
			</li> -->

			<li class="<?=$page==='ujian'?"active":""?>">
				<a href="<?=base_url('ujian/lists')?>" rel="noopener noreferrer">
					<i class="fa fa-chrome"></i> <span>Ujian</span>
				</a>
			</li>
			<?php endif; ?>
			<?php if( $this->ion_auth->in_group('dosen') || $this->ion_auth->in_group('mahasiswa')) : ?>
			<li class="header">REPORTS</li>
			<li class="<?=$page==='hasilujian'?"active":""?>">
				<a href="<?=base_url('hasilujian')?>" rel="noopener noreferrer">
					<i class="fa fa-file"></i> <span>Hasil Ujian</span>
				</a>
			</li>
			<li class="<?=$page==='hasilujiankuis'?"active":""?>">
				<a href="<?=base_url('hasilujiankuis')?>" rel="noopener noreferrer">
					<i class="fa fa-file"></i> <span>Hasil Quiz</span>
				</a>
			</li>
			<li class="<?=$page==='hasilujianlatihan'?"active":""?>">
				<a href="<?=base_url('hasilujianlatihan')?>" rel="noopener noreferrer">
					<i class="fa fa-file"></i> <span>Hasil Latihan</span>
				</a>
			</li>
			<?php endif; ?>
			<?php if( $this->ion_auth->in_group('mahasiswa') ) : ?>
			<li class="header">HISTORY</li>
			<li class="<?=$page==='Historyto'?"active":""?>">
				<a href="<?=base_url('Historyto')?>" rel="noopener noreferrer">
					<i class="fa fa-file"></i> <span>History Ujian</span>
				</a>
			</li>
			<?php endif; ?>
			<?php if($this->ion_auth->is_admin()) : ?>
			<li class="header">ADMINISTRATOR</li>
			<li class="<?=$page==='approval'?"active":""?>">
				<a href="<?=base_url('approval')?>" rel="noopener noreferrer">
					<i class="fa fa-users"></i> <span>Approv Pendaftaran</span>
				</a>
			</li>
			<li class="<?=$page==='users'?"active":""?>">
				<a href="<?=base_url('users')?>" rel="noopener noreferrer">
					<i class="fa fa-users"></i> <span>User Management</span>
				</a>
			</li>
			<li class="<?=$page==='settings'?"active":""?>">
				<a href="<?=base_url('settings')?>" rel="noopener noreferrer">
					<i class="fa fa-cog"></i> <span>Settings</span>
				</a>
			</li>
			<?php endif; ?>
		</ul>

	</section>
	<!-- /.sidebar -->
</aside>