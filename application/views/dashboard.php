<?php
if( $this->ion_auth->is_admin() ) : ?>
<div class="row">
    <?php foreach($info_box as $info) : ?>
    <div class="col-lg-3 col-xs-6">
        <div class="small-box bg-<?=$info->box?>">
        <div class="inner">
            <h3><?=$info->total;?></h3>
            <p><?=$info->title;?></p>
        </div>
        <div class="icon">
            <i class="fa fa-<?=$info->icon?>"></i>
        </div>
        <a href="<?=base_url().strtolower($info->title);?>" class="small-box-footer">
            More info <i class="fa fa-arrow-circle-right"></i>
        </a>
        </div>
    </div>
    <?php endforeach; ?>
</div>

<?php elseif( $this->ion_auth->in_group('dosen') ) : ?>

<div class="row">
    <div class="col-sm-4">
        <div class="box box-default">
            <div class="box-header with-border">
                <h3 class="box-title">Informasi Akun</h3>
            </div>
            <table class="table table-hover">
                <tr>
                    <th>Nama</th>
                    <td><?=$dosen->nama_dosen?></td>
                </tr>
                <tr>
                    <th>NIP</th>
                    <td><?=$dosen->nip?></td>
                </tr>
                <tr>
                    <th>Email</th>
                    <td><?=$dosen->email?></td>
                </tr>
                <tr>
                    <th>Mata Pelajaran</th>
                    <td><?=$dosen->nama?></td>
                </tr>
                <tr>
                    <th>Daftar Kelas</th>
                    <td>
                        <ol class="pl-4">
                        <?php foreach ($kelas as $k) : ?>
                            <li><?=$k->nama?></li>
                        <?php endforeach;?>
                        </ol>
                    </td>
                </tr>
            </table>
        </div>
    </div>
    <div class="col-sm-8">
        <div class="box box-solid">
            <div class="box-header bg-purple">
                <h3 class="box-title">Pemberitahuan</h3>
            </div>
            <div class="box-body">
                <p>Lorem ipsum, dolor sit amet consectetur adipisicing elit. Quidem in animi quibusdam nihil esse ratione, nulla sint enim natus, aut mollitia quas veniam, tempore quia!</p>
                <ul class="pl-4">
                    <li>Lorem ipsum, dolor sit amet consectetur adipisicing elit. Consectetur, culpa.</li>
                    <li>Provident dolores doloribus, fugit aperiam alias tempora saepe non omnis.</li>
                    <li>Doloribus sed eum et repellat distinctio a repudiandae quia voluptates.</li>
                    <li>Adipisci hic rerum illum odit possimus voluptatibus ad aliquid consequatur.</li>
                    <li>Laudantium sapiente architecto excepturi beatae est minus, labore non libero.</li>
                </ul>
            </div>
        </div>
    </div>
</div>

<?php else : ?>

<div class="row">
    <div class="col-sm-4">
        <div class="box box-default">
            <div class="box-header with-border">
                <h3 class="box-title">Informasi Akun</h3>
            </div>
            <table class="table table-hover">
                <tr>
                    <th>NIM</th>
                    <td><?=$mahasiswa->nim?></td>
                </tr>
                <tr>
                    <th>Nama</th>
                    <td><?=$mahasiswa->nama?></td>
                </tr>
                <tr>
                    <th>Jenis Kelamin</th>
                    <td><?=$mahasiswa->jenis_kelamin === 'L' ? "Laki-laki" : "Perempuan" ;?></td>
                </tr>
                <tr>
                    <th>Email</th>
                    <td><?=$mahasiswa->email?></td>
                </tr>
                <tr>
                    <th>Alamat</th>
                    <td><?=$mahasiswa->alamat?></td>
                </tr>
                <!-- <tr>
                    <th>Kelas</th>
                    <td><?=$mahasiswa->nama_kelas?></td>
                </tr> -->
            </table>
        </div>
    </div>
    <div class="col-sm-8">
        <div class="box box-solid">
            <div class="box-header bg-purple">
                <h3 class="box-title">Tryout</h3>
            </div>
            <div class="box-body">
                <table class="table">
                    <thead>
                        <tr>
                            <th>No.</th>
                            <th>Nama Ujian</th>
                            <th>Mata Pelajaran</th>
                            <th>Pengajar</th>
                            <th>Jumlah Soal</th>
                            <th>Waktu</th>
                            <th class="text-center">Aksi</th>
                        </tr>        
                    </thead>
                    <tbody>
                        <?php 
                            foreach ($tryout->result_array() as $key => $ty) { ?>
                            <tr>
                                <td><?php echo $key+1 ?></td>
                                <td><?php echo $ty['nama_ujian'] ?></td>
                                <td><?php echo $ty['nama_matkul'] ?></td>
                                <td><?php echo $ty['nama_dosen'] ?></td>
                                <td><?php echo $ty['jumlah_soal'] ?></td>
                                <td><?php echo $ty['waktu'] ?></td>
                                <td>
                                    <?php if ($ty['ada']>0) {
                                        ?>
                                        <a class="btn btn-xs btn-success" href="<?=base_url()?>hasilujian/cetak/<?=$ty['id_ujian']?>" target="_blank">
                                            <i class="fa fa-print"></i> Cetak Hasil
                                        </a>
                                    <?php } else{
                                      ?>
                                        <a class="btn btn-xs btn-primary" href="<?=base_url()?>ujian/token/<?=$ty['id_ujian']?>">
                                            <i class="fa fa-pencil"></i> Ikut Ujian
                                        </a>               
                                      <?php  
                                    }?>
                                </td>
                            </tr>
                            
                        <?php  } ?>
                    </tbody>

                </table>
            </div>
        </div>
    </div>
    <div class="col-sm-12">
        <div class="box box-solid">
            <div class="box-header bg-yellow">
                <h3 class="box-title">Latihan</h3>
            </div>
            <div class="box-body">
                <table class="table">
                    <thead>
                        <tr>
                            <th>No.</th>
                            <th>Nama Ujian</th>
                            <th>Mata Pelajaran</th>
                            <th>Pengajar</th>
                            <th>Jumlah Soal</th>
                            <th>Waktu</th>
                            <th class="text-center">Aksi</th>
                        </tr>        
                    </thead>
                    <tbody>
                        <?php 
                            foreach ($latihan->result_array() as $key => $lt) { ?>
                            <tr>
                                <td><?php echo $key+1 ?></td>
                                <td><?php echo $lt['nama_latihan'] ?></td>
                                <td><?php echo $lt['nama_matkul'] ?></td>
                                <td><?php echo $lt['nama_dosen'] ?></td>
                                <td><?php echo $lt['jumlah_soal'] ?></td>
                                <td><?php echo $lt['waktu'] ?></td>
                                <td>
                                    <div class="text-center">
                                        
                                    <a class="btn btn-xs btn-success" href="http://[::1]/tutec/bimbel/hasilujianlatihan/cetak/2" target="_blank">
                                        <i class="fa fa-print"></i> Cetak Hasil
                                    </a>
                                    <a class="btn btn-xs btn-danger" href="http://[::1]/tutec/bimbel/latihan/tokenlatihan/2">
                                        <i class="fa fa-pencil"></i> Retake Ujian
                                    </a>
                                    
								</div>
                                </td>
                            </tr>
                            
                        <?php  } ?>
                    </tbody>

                </table>
            </div>
        </div>
    </div>
    <div class="col-sm-12">
        <div class="box box-solid">
            <div class="box-header bg-red">
                <h3 class="box-title">Quiz</h3>
            </div>
            <div class="box-body">
                <table class="table">
                    <thead>
                        <tr>
                            <th>No.</th>
                            <th>Nama Ujian</th>
                            <th>Mata Pelajaran</th>
                            <th>Pengajar</th>
                            <th>Jumlah Soal</th>
                            <th>Waktu</th>
                            <th class="text-center">Aksi</th>
                        </tr>        
                    </thead>
                    <tbody>
                        <?php 
                            foreach ($kuis->result_array() as $key => $ks) { ?>
                            <tr>
                                <td><?php echo $key+1 ?></td>
                                <td><?php echo $ks['nama_kuis'] ?></td>
                                <td><?php echo $ks['nama_matkul'] ?></td>
                                <td><?php echo $ks['nama_dosen'] ?></td>
                                <td><?php echo $ks['jumlah_soal'] ?></td>
                                <td><?php echo $ks['waktu'] ?></td>
                            <td>
                            <div class="text-center">
									
                                    <a class="btn btn-xs btn-success" href="<?=base_url()?>hasilujiankuis/cetak/<?= $ks['id_kuis']?>" target="_blank">
                                        <i class="fa fa-print"></i> Cetak Hasil
                                    </a>
                                    <a class="btn btn-xs btn-danger" href="<?=base_url()?>kuis/tokenkuis/<?= $ks['id_kuis']?>">
                                    <i class="fa fa-pencil"></i> Retake Ujian
                                </a>
                                    
                                    </div>
                            </td>
                            </tr>
                            
                        <?php  } ?>
                    </tbody>

                </table>
            </div>
        </div>
    </div>
</div>

<?php endif; ?>