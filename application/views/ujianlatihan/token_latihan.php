<div class="callout callout-info">
    <h4>Peraturan Ujian!</h4>
    <p>1.  Berdoalah sebelum ujian agar ujian bermanfaat dan menjadi ilmu yang berguna.</p>
<p>2.  Tujuan utama dari Ujian adalah adalah untuk mengukur kemampuan diri sendiri, mengetahui bab mana yang masih kurang, serta untuk menjadikan kemampuan sendiri dengan orang lain. Maka kerjakanlah sendiri dengan sejujur-jujurnya dan sebaik-baiknya.</p>
<p>3.   Setelah mengerjakan, jangan lupa baca pembahasan dan tanyakan yang kurang jelas!
</p>
</div>
<div class="box box-primary">
    <div class="box-header with-border">
        <h3 class="box-title">Konfirmasi Data</h3>
    </div>
    <div class="box-body">
        <span id="id_ujian" data-key="<?=$encrypted_id?>"></span>
        <input type="hidden" id="id_bab" value="<?=$ujian->id_bab?>"></span>
        <div class="row">
            <div class="col-sm-6">
                <table class="table table-bordered">
                    <tr>
                        <th>Nama</th>
                        <td><?=$mhs->nama?></td>
                    </tr>
                    <tr>
                        <th>Pengajar</th>
                        <td><?=$ujian->nama_dosen?></td>
                    </tr>
                    <tr>
                        <th>Nama Ujian</th>
                        <td><?=$ujian->nama_latihan?></td>
                    </tr>
                    <tr>
                        <th>Jumlah Soal</th>
                        <td><?=$ujian->jumlah_soal?></td>
                    </tr>
                    <tr>
                        <th>Waktu</th>
                        <td><?=$ujian->waktu?> Menit</td>
                    </tr>
                    <tr>
                        <th>Batas Akhir Pengerjaan</th>
                        <td>
                            <?=strftime('%d %B %Y', strtotime($ujian->terlambat))?> 
                            <?=date('H:i:s', strtotime($ujian->terlambat))?>
                        </td>
                    </tr>
                    <tr>
                        <th style="vertical-align:middle">Token</th>
                        <td>
                            <input autocomplete="off" id="token" placeholder="Token" type="text" class="input-sm form-control">
                        </td>
                    </tr>
                </table>
            </div>
            <div class="col-sm-6">
                <div class="box box-solid">
                    <div class="box-body pb-0">
                        <div class="callout callout-info">
                            <p>
                                Waktu boleh mengerjakan ujian adalah saat tombol "MULAI" berwarna hijau.
                            </p>
                        </div>
                    
                        <button id="btncek" data-id="<?=$ujian->id_latihan?>" class="btn btn-success btn-lg mb-4">
                            <i class="fa fa-pencil"></i> Mulai
                        </button>
                        
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script src="<?=base_url()?>assets/dist/js/app/ujian/token_latihan.js"></script>