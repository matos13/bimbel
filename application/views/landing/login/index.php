<?php $this->load->view("landing/template/header.php") ?>
<?php
if (isset($this->session->userdata['logged_in'])) {
  $username = ($this->session->userdata['logged_in']['user_username']);
  $email = ($this->session->userdata['logged_in']['user_email']);

  $page = base_url();
  header("location: ".$page."dashboard"); 
}
?>
<div id="breadcrumb">
    <div class="container">
      <div class="breadcrumb">
        <li><a style="color:#fff" href="<?php echo base_url();?>">Home</a></li>
        <li>Login</li>
      </div>
    </div>
  </div>

  <div class="services">
    <div class="container">
      <div class="col-md-6">
        <img src="<?php echo base_url();?>/assets/images/4.jpg" class="img-responsive">
      </div>

      <div class="col-md-6">
        <!-- <section id="contact-page"> -->
    <!-- <div class="container"> -->
      <div class="center">
        <h2>Bersama belajar | Login</h2>
        <p>Masuk dengan akun yang telah tersedia.</p>
      </div>
      <div class="row contact-wrap">
        <div class="status alert alert-success" style="display: none"></div>
        <div class="col-md-6 col-md-offset-3">
          <div id="sendmessage">Your message has been sent. Thank you!</div>
          <div id="errormessage"></div>
          <!-- <form action="" method="post" role="form" class="contactForm"> -->
            <?php echo form_open('landing/login/process'); ?>
              
              <?php
              echo "<div class='error_msg'>";
                if (isset($error_message)) {
                echo $error_message;
              }
              echo validation_errors();
            echo "</div>";
            ?>  
            <div class="form-group">
              <input type="text" name="user_username" class="form-control" id="name" placeholder="Username" data-rule="minlen:4" data-msg="Please enter at least 4 chars" />
              <div class="validation"></div>
            </div>
            <div class="form-group">
              <input type="password" class="form-control" name="user_password" id="email" placeholder="Password" data-rule="email" data-msg="Please enter a valid email" />
              <div class="validation"></div>
            </div>
            <div class="text-center"><button type="submit" name="submit" class="btn btn-primary btn-lg" required="required">Login</button></div>
          </form>
        </div>
      </div>
      <!--/.row-->
    <!-- </div> -->
    <!--/.container-->
  <!-- </section> -->
      </div>
    </div>
  </div>

<?php $this->load->view("landing/template/footer.php") ?>