<?php $this->load->view("landing/template/header.php") ?>
<div id="breadcrumb">
  <div class="container">
    <div class="breadcrumb">
      <li><a style="color:#fff" href="<?php echo base_url();?>">Home</a></li>
      <li>Registrasi</li>
    </div>
  </div>
</div>

<div class="services">
  <div class="container">
    <section id="contact-page">
      <div class="container">
        <div class="center">
          <h2>Bersama Belajar | Registrasi</h2>
          <p>Masukan data diri Anda sesuai dengan field yang telah disediakan dan pastikan bahwa data yang Anda masukan adalah benar adanya.</p>
        </div>
        <div class="row contact-wrap">
          <div class="status alert alert-success" style="display: none"></div>
          <div class="col-md-6 ">
            <div id="sendmessage">Your message has been sent. Thank you!</div>
            <div id="errormessage"></div>
          <?php if (isset($data)): ?>
              <span><?= $data->msg ?></span>
          <?php endif ?>
            <form action="ProsesRegistrasi" method="post" role="form" class="contactForm">
              <input type="hidden" id="crfs" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>">
              <div class="form-group">
                <label style="color:#000000">Kategori Mapel</label>
                <select name="jurusan" id="jurusan" class="form-control">
                  <option value="">--Pilih Kategori--</option>
                  <?php foreach ($kat as $key => $value): ?>
                    <option value="<?= $value['id_jurusan'] ?>"><?= $value['nama_jurusan'] ?></option>
                  <?php endforeach ?>
                </select>
                <div class="validation"></div>
              </div>
              <div class="form-group">
                <label style="color:#000000">Nama Lengkap</label>

                <input type="text" name="nama_lengkap" class="form-control" id="name" placeholder="Nama Lengkap" data-rule="minlen:4" data-msg="Please enter at least 4 chars" required="" />
                <div class="validation"></div>
              </div>
              <div class="form-group">
                <label style="color:#000000">Username</label>

                <input type="text" name="username" class="form-control" id="name" placeholder="Username" data-rule="minlen:4" data-msg="Please enter at least 4 chars" required="" />
                <div class="validation"></div>
              </div>
              <div class="form-group">
                <label style="color:#000000">Password</label>

                <input type="password" name="password" class="form-control" id="name" placeholder="Password" data-rule="minlen:4" data-msg="Please enter at least 4 chars" required="" />
                <div class="validation"></div>
              </div>
              <div class="form-group">
                <label style="color:#000000">Re-Password</label>

                <input type="password" name="re_password" class="form-control" id="name" placeholder="Retype Password" data-rule="minlen:4" data-msg="Please enter at least 4 chars" required="" />
                <div class="validation"></div>
              </div>
              <div class="form-group">
                <label style="color:#000000">Email</label>

                <input type="email" class="form-control" name="email" id="email" placeholder="Email" data-rule="email" data-msg="Please enter a valid email" />
                <div class="validation"></div>
              </div>

            </div>
            <div class="col-md-6 ">
              <div class="form-group">
                <label style="color:#000000">Nomor HP</label>

                <input type="text" name="nomor_hp" class="form-control" id="name" placeholder="Nomor HP/Whatsapp" data-rule="minlen:4" data-msg="Please enter at least 4 chars" required="" />
                <div class="validation"></div>
              </div>
              <div class="form-group">
                <label style="color:#000000">Jenis Kelamin</label>
                <select name="jenis_kelamin" class="form-control">
                  <option value="">--Pilih Jenis Kelamin--</option>
                  <option value="L">Laki-laki</option>
                  <option value="P">Perempuan</option>
                </select>
                <div class="validation"></div>
              </div>
              <div class="form-group">
                <label style="color:#000000">Provinsi</label>
                <select name="provinsi" id="provinsi" class="form-control">
                  <option value="">--Pilih Provinsi--</option>
                  <?php foreach ($prov as $key => $value): ?>
                    <option value="<?= $value['id'] ?>"><?= $value['nama'] ?></option>
                  <?php endforeach ?>
                </select>
                <div class="validation"></div>
              </div>
              <div class="form-group">
                <label style="color:#000000">Kabupaten</label>
                <select name="kabupaten" id="kabupaten" class="form-control">
                  <option value="">--Pilih Kabupaten--</option>
                  <!-- <?php foreach ($kab as $key => $value): ?>
                    <option value="<?= $value['id'] ?>"><?= $value['nama'] ?></option>
                  <?php endforeach ?> -->
                </select>
                <div class="validation"></div>
              </div>
              <div class="form-group">
                <label style="color:#000000">Alamat</label>

                <textarea class="form-control" name="alamat" rows="5" data-rule="required" data-msg="Please write something for us" placeholder="Alamat lengkap"></textarea>
                <div class="validation"></div>
              </div>
              <div class="text-right"><button type="submit" name="submit" class="btn btn-primary btn-lg" required="required">Registrasi</button></div>
            </div>
          </form>
        </div>
        <!--/.row-->
      </div>
      <!--/.container-->
    </section>
  </div>
</div>

<?php $this->load->view("landing/template/footer.php") ?>
<script>
function ajaxcsrf() {
					var csrfname = '<?= $this->security->get_csrf_token_name() ?>';
					var csrfhash = '<?= $this->security->get_csrf_hash() ?>';
					var csrf = {};
					csrf[csrfname] = csrfhash;
					$.ajaxSetup({
						"data": csrf
					});
				}
$(document).ready(function(){
  ajaxcsrf();
		$('#provinsi').change(function(){
      var id=$(this).val();
      var crsf=$('#crfs').val();
    
			$.ajax({
				url : "<?php echo base_url();?>landing/getKabByidprovinsi",
				method : "POST",
        data : {id: id
        },
				async : false,
		    dataType : 'json',
				success: function(data){
          console.log(data);
					var html = '';
		            var i;
		            for(i=0; i<data.length; i++){
		                html += '<option name="kabupaten" value="'+data[i].id+'">'+data[i].nama+'</option>';
		            }
		            $('#kabupaten').html(html);
					
				}
			});
		});
	});
</script>