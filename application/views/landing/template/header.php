<!DOCTYPE html>
<html lang="en">
<?php
// if ($this->ion_auth->logged_in()){
//       $user_id = $this->ion_auth->user()->row()->id; // Get User ID
//       $group = $this->ion_auth->get_users_groups($user_id)->row()->name; // Get user group
//       // redirect('dashboard');
//     }
?>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title>TRYOUT ONLINE - CPNS SBMPTN STAN STPN IPDN SMA SMP SD | bersama-belajar.com</title>

  <!-- Bootstrap -->
  <link href="<?php echo base_url();?>assets/landing/css/bootstrap.min.css" rel="stylesheet">
  <link rel="stylesheet" href="<?php echo base_url();?>assets/landing/css/font-awesome.min.css">
  <link rel="stylesheet" href="<?php echo base_url();?>assets/landing/css/animate.css">
  <link href="<?php echo base_url();?>assets/landing/css/prettyPhoto.css" rel="stylesheet">
  <link href="<?php echo base_url();?>assets/landing/css/style.css" rel="stylesheet" />
  <!-- =======================================================
    Theme Name: Company
    Theme URL: https://bootstrapmade.com/company-free-html-bootstrap-template/
    Author: BootstrapMade
    Author URL: https://bootstrapmade.com
  ======================================================= -->
</head>

<body>
  <header>
    <nav class="navbar navbar-default navbar-fixed-top" role="navigation">
      <div class="navigation">
        <div class="container">
          <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target=".navbar-collapse.collapse">
              <span class="sr-only">Toggle navigation</span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
            </button>
            <div class="navbar-brand">
              <a href="<?php echo base_url();?>"><h1><span>Bersama</span>Belajar</h1></a>
            </div>
          </div>

          <div class="navbar-collapse collapse">
            <div class="menu">
              <ul class="nav nav-tabs" role="tablist">
                <li role="presentation"><a href="<?php echo base_url();?>">Beranda</a></li>
                <li role="presentation"><a href="<?php echo base_url();?>landing/kompetisi">Kompetisi</a></li>
                <li role="presentation"><a href="<?php echo base_url();?>landing/kontak">Kontak Kami</a></li>
                <?php
                if ($this->ion_auth->logged_in()) { ?>
                  <li role="presentation"><a href="<?php echo base_url();?>dashboard">Dashboard</a></li>
                <?php } else { ?>
                  <li role="presentation"><a href="<?php echo base_url();?>landing/registrasi">Registrasi</a></li>
                  <li role="presentation"><a href="<?php echo base_url();?>login">Login</a></li>
                <?php } ?>

              </ul>
            </div>
          </div>
        </div>
      </div>
    </nav>
  </header>