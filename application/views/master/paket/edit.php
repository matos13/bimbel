<div class="box">
    <div class="box-header with-border">
        <h3 class="box-title">Form <?=$judul?></h3>
        <div class="box-tools pull-right">
            <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
            </button>
        </div>
    </div>
    <div class="box-body">
        <div class="row">
            <div class="col-sm-offset-3 col-sm-6">
                <div class="my-2">
                    <div class="form-horizontal form-inline">
                        <a href="<?=base_url('paket')?>" class="btn btn-default btn-xs">
                            <i class="fa fa-arrow-left"></i> Batal
                        </a>
                        <div class="pull-right">
                            <span> Jumlah : </span><label for=""><?=count($paket)?></label>
                        </div>
                    </div>
                </div>
                <?=form_open('paket/save', array('id'=>'paket'), array('mode'=>'edit'))?>
                <table id="form-table" class="table text-center table-condensed">
                    <thead>
                        <tr>
                            <th># No</th>
                            <th>Mata Pelajaran</th>
                            <th>Kateori Mapel</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php 
                        $i = 1;
                        foreach($paket as $row) : 
                        // print_r($row);
                        ?> 
                            <tr>
                                <td><?=$i?></td>
                                <td>
                                    <div class="form-group">
                                        <?=form_hidden('id['.$i.']', $row->id);?>
                                        <input required="required" autofocus="autofocus" onfocus="this.select()" value="<?=$row->nama_paket?>" type="text" name="nama_paket[<?=$i?>]" class="form-control">
                                        <span class="d-none">DON'T DELETE THIS</span>
                                        <small class="help-block text-right"></small>
                                    </div>
                                </td>
                                <td>
                                    <!-- <div class="form-group">
                                        <select required="required" name="kelas_id[<?=$i?>]" class="input-sm form-control select2" style="width: 100%!important">
                                            <option value="" disabled>-- Pilih --</option>
                                            <?php foreach ($kelas as $j) : ?>
                                                <option <?= $row->id_mapel == $j->id_kelas ? "selected='selected'" : "" ?> value="<?=$j->id_kelas?>"><?=$j->nama_kelas?></option>
                                            <?php endforeach; ?>
                                        </select>
                                        <small class="help-block text-right"></small>
                                    </div> -->
                                    <div class="form-group">
                                        <select required="required" name="jenis[<?=$i?>]" class="form-control input-sm select2" style="width: 100%!important">
                                            <option value="" disabled selected>-- Pilih --</option>
                                            <option <?= $row->jenis == "TO" ? "selected='selected'" : "" ?>  value="TO">TO</option>
                                            <option <?= $row->jenis == "Latihan" ? "selected='selected'" : "" ?>value="Latihan">Latihan</option>
                                            <option <?= $row->jenis == "Kuis" ? "selected='selected'" : "" ?>value="Kuis">Kuis</option>
                                        </select>
                                        <small class="help-block text-right"></small>
                                    </div>
                                </td>
                            </tr>
                        <?php $i++;endforeach; ?>
                    </tbody>
                </table>
                <button id="submit"  type="submit" class="mb-4 btn btn-block btn-flat bg-purple">
                    <i class="fa fa-edit"></i> Simpan Perubahan
                </button>
                <?=form_close()?>
            </div>
        </div>
    </div>
</div>

<script src="<?=base_url()?>assets/dist/js/app/master/paket/edit.js"></script>