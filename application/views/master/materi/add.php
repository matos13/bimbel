<div class="row">
    <div class="col-sm-12">    
        <?=form_open_multipart('materi/save', array('id'=>'formmateri'), array('method'=>'add'));?>
        <div class="box">
            <div class="box-header with-border">
                <h3 class="box-title"><?=$subjudul?></h3>
                <div class="box-tools pull-right">
                    <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                    </button>
                </div>
            </div>
            <div class="box-body">
                <div class="row">
                    <div class="col-sm-8 col-sm-offset-2">
                        <div class="form-group col-sm-12">
                            <label>Bab</label>
                            <select name="dosen_id" required="required" id="dosen_id" class="select2 form-group" style="width:100% !important">
                                <option value="" name="bab" disabled selected>Pilih Bab</option>
                                <?php foreach ($bab as $d) : ?>
                                    <option value="<?=$d->id_bab ?>"><?=$d->nama_bab.' - '.$d->nama_mapel.' - '.$d->nama_program?></option>
                                <?php endforeach; ?>
                            </select>
                        </div>
                          <div class="form-group col-sm-12">
                            <label for="bobot" class="control-label">Link Youtube</label>
                            <input type="text" name="youtube" placeholder="Link Youtube (Optional)" id="bobot" class="form-control">
                            <small class="help-block" style="color: #dc3545"><?=form_error('bobot')?></small>
                        </div>
                        <div class="form-group col-sm-12">
                           <label for="materi" class="control-label">Pdf</label>
                            <div class="form-group">
                                <input type="file" name="file_pdf" class="form-control">
                                <small class="help-block" style="color: #dc3545"><?=form_error('file_pdf')?></small>
                            </div>
                        </div>
                        
                        <div class="col-sm-12">
                            <label for="materi" class="control-label">Isi Materi</label>
                            <div class="form-group">
                                <textarea name="materi" id="materi" class="form-control"><?=set_value('materi')?></textarea>
                                <small class="help-block" style="color: #dc3545"><?=form_error('materi')?></small>
                            </div>
                        </div>
                        <div class="col-sm-12">
                            <label for="jenis">Status Materi</label>
                            <select name="status" class="form-control">
                                <option value="" disabled selected>--- Pilih ---</option>
                                <option value="1">Buka Materi</option>
                                <option value="0">Kunci Materi</option>
                            </select>
                            <small class="help-block"></small>
                        </div>
                        <div class="form-group pull-right">
                            <a href="<?=base_url('materi')?>" class="btn btn-flat btn-default"><i class="fa fa-arrow-left"></i> Batal</a>
                            <button type="submit" id="submit" class="btn btn-flat bg-purple"><i class="fa fa-save"></i> Simpan</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <?=form_close();?>
    </div>
</div>