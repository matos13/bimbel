<div class="box">
    <div class="box-header with-border">
        <h3 class="box-title"><?=$subjudul?></h3>
        <div class="box-tools pull-right">
            <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
            </button>
        </div>
    </div>
    <div class="box-body">
		<div class="row">
        	<div class="col-sm-4">
			</div>
			<div class="form-group col-sm-4 text-center">
			</div>
			<div class="col-sm-4">
				<div class="pull-right">
					<a href="<?=base_url('SoalLatihanNew/add')?>" class="btn bg-purple btn-flat btn-sm"><i class="fa fa-plus"></i> Tambah</a>
					<button type="button" onclick="reload_ajax()" class="btn btn-flat btn-sm bg-maroon"><i class="fa fa-refresh"></i> Reload</button>
				</div>
			</div>
		</div>
    </div>
	<?=form_open('soal/delete', array('id'=>'bulk'))?>
    <div class="table-responsive px-4 pb-3" style="border: 0">
        <table id="SoalLatihanNew" class="w-100 table table-striped table-bordered table-hover">
        <thead>
            <tr>
                <th width="25">No.</th>
				<th>Pengajar</th>
				<th>Mapel</th>
                <th>Bab</th>
				<th>Jumlah Soal</th>
				<th class="text-center">Aksi</th>
				<!-- <th class="text-center">
					<input type="checkbox" class="select_all">
				</th> -->
            </tr>        
        </thead>
        </table>
    </div>
	<?=form_close();?>
</div>

<script src="<?=base_url()?>assets/dist/js/app/soalnew/data_latihan.js"></script>

<?php if ( $this->ion_auth->is_admin() ) : ?>
<script type="text/javascript">
$(document).ready(function(){
	$('#matkul_filter').on('change', function(){
		let id_matkul = $(this).val();
		let src = '<?=base_url()?>SoalLatihanNew/data_awal';
		let url;

		if(id_matkul !== 'all'){
			let src2 = src + '/' + id_matkul;
			url = $(this).prop('checked') === true ? src : src2;
		}else{
			url = src;
		}
		table.ajax.url(url).load();
	});
});
</script>
<?php endif; ?>
<?php if ( $this->ion_auth->in_group('dosen') ) : ?>
<script type="text/javascript">
$(document).ready(function(){
	let id_matkul = '<?=$matkul->matkul_id?>';
	let id_dosen = '<?=$matkul->id_dosen?>';
	let src = '<?=base_url()?>SoalLatihanNew/data_awal';
	let url = src + '/' + id_matkul + '/' + id_dosen;

	table.ajax.url(url).load();
});
</script>
<?php endif; ?>