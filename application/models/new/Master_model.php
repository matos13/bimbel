<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Master_model extends CI_Model {
    public function create($table, $data, $batch = false)
    {
        if($batch === false){
            $insert = $this->db->insert($table, $data);
        }else{
            $insert = $this->db->insert_batch($table, $data);
        }
        return $insert;
    }

    public function update($table, $data, $pk, $id = null, $batch = false)
    {
        if($batch === false){
            $insert = $this->db->update($table, $data, array($pk => $id));
        }else{
            $insert = $this->db->update_batch($table, $data, $pk);
        }
        return $insert;
    }

    public function delete($table, $data, $pk)
    {
        $this->db->where_in($pk, $data);
        return $this->db->delete($table);
    }
    public function getDataProgram()
    {
        $this->datatables->select('id, nama');
        $this->datatables->from('tbl_program_belajar');
        $this->datatables->add_column('bulk_select', '<div class="text-center"><input type="checkbox" class="check" name="checked[]" value="$1"/></div>', 'id, nama');        
        return $this->datatables->generate();
    }
    public function getProgramById($id)
    {
        $this->db->where_in('id', $id);
        $this->db->order_by('nama');
        $query = $this->db->get('tbl_program_belajar')->result();
        return $query;
    } 
    public function getMapelById($id)
    {
        $this->db->where_in('id', $id);
        $this->db->order_by('nama');
        $query = $this->db->get('tbl_mapel')->result();
        return $query;
    } 
    public function getkelasById($id)
    {
        $this->db->where_in('id', $id);
        $this->db->order_by('nama');
        $query = $this->db->get('tbl_kelas')->result();
        return $query;
    } 
    public function getAllProgramBelajar()
    {
        $this->db->select('id, nama');
        $this->db->from('tbl_program_belajar');
        $this->db->order_by('nama');
        return $this->db->get()->result();
    }

    

    public function getDataMapel()
    {
        $this->datatables->select('tbl_mapel.id, tbl_mapel.nama, tbl_program_belajar.nama as program');
        $this->datatables->from('tbl_mapel');
        $this->datatables->join('tbl_program_belajar','tbl_mapel.id_program=tbl_program_belajar.id');
        $this->datatables->add_column('bulk_select', '<div class="text-center"><input type="checkbox" class="check" name="checked[]" value="$1"/></div>', 'id, tbl_mapel.nama, tbl_program_belajar.nama as program');        
        return $this->datatables->generate();
    }
    public function getDatakelas()
    {
        $this->datatables->select('tbl_kelas.id, tbl_kelas.nama, tbl_program_belajar.nama as program');
        $this->datatables->from('tbl_kelas');
        $this->datatables->join('tbl_program_belajar','tbl_kelas.id_program=tbl_program_belajar.id');
        $this->datatables->add_column('bulk_select', '<div class="text-center"><input type="checkbox" class="check" name="checked[]" value="$1"/></div>', 'id, tbl_kelas.nama, tbl_program_belajar.nama as program');        
        return $this->datatables->generate();
    }

}