<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Master_model extends CI_Model {

    public function create($table, $data, $batch = false)
    {
        if($batch === false){
            $insert = $this->db->insert($table, $data);
        }else{
            $insert = $this->db->insert_batch($table, $data);
        }
        return $insert;
    }

    public function update($table, $data, $pk, $id = null, $batch = false)
    {
        if($batch === false){
            $insert = $this->db->update($table, $data, array($pk => $id));
        }else{
            $insert = $this->db->update_batch($table, $data, $pk);
        }
        return $insert;
    }

    public function delete($table, $data, $pk)
    {
        $this->db->where_in($pk, $data);
        return $this->db->delete($table);
    }

    /**
     * Data Kelas
     */

    public function getDataKelas()
    {
        $this->datatables->select('id_kelas, nama_kelas, id_jurusan, nama_jurusan');
        $this->datatables->from('kelas');
        $this->datatables->join('jurusan', 'jurusan_id=id_jurusan');
        $this->datatables->add_column('bulk_select', '<div class="text-center"><input type="checkbox" class="check" name="checked[]" value="$1"/></div>', 'id_kelas, nama_kelas, id_jurusan, nama_jurusan');        
        return $this->datatables->generate();
    }
    public function getDatakabupaten()
    {
        $this->datatables->select('kabupaten.id, kabupaten.nama, provinsi.id as provinsi_id, provinsi.nama as provinsi_nama');
        $this->datatables->from('kabupaten');
        $this->datatables->join('provinsi', 'kabupaten.id_provinsi=provinsi.id');
        $this->db->order_by('provinsi.nama');
        $this->datatables->add_column('bulk_select', '<div class="text-center"><input type="checkbox" class="check" name="checked[]" value="$1"/></div>', 'id, kabupaten.nama, provinsi.id as provinsi_id, provinsi.nama as provinsi_nama');        
        return $this->datatables->generate();
    }

    public function getKelasById($id)
    {
        $this->db->where_in('id_kelas', $id);
        $this->db->order_by('nama_kelas');
        $query = $this->db->get('kelas')->result();
        return $query;
    }
    public function getkabupatenById($id)
    {
        $this->db->where_in('id', $id);
        $this->db->order_by('nama');
        $query = $this->db->get('kabupaten')->result();
        return $query;
    }

    /**
     * Data Jurusan
     */

    public function getDataJurusan()
    {
        $this->datatables->select('id_jurusan, nama_jurusan');
        $this->datatables->from('jurusan');
        $this->datatables->add_column('bulk_select', '<div class="text-center"><input type="checkbox" class="check" name="checked[]" value="$1"/></div>', 'id_jurusan, nama_jurusan');
        return $this->datatables->generate();
    }
    public function getAllProv(){
         $query = $this->db->get('provinsi')->result();
        return json_decode(json_encode($query), True);
    }
    public function getAllKab(){
             $query = $this->db->get('kabupaten')->result();
            return json_decode(json_encode($query), True);
     }
    public function getAllKabProv($id){
            $this->db->where('id_provinsi',$id);
             $query = $this->db->get('kabupaten')->result();
            return json_decode(json_encode($query), True);
     }
     public function getAllJur(){
             $query = $this->db->get('jurusan')->result();
            return json_decode(json_encode($query), True);
     }
     

    public function getJurusanById($id)
    {
        $this->db->where_in('id_jurusan', $id);
        $this->db->order_by('nama_jurusan');
        $query = $this->db->get('jurusan')->result();
        return $query;
    }
    public function getprovinsiById($id)
    {
        $this->db->where_in('id', $id);
        $this->db->order_by('nama');
        $query = $this->db->get('provinsi')->result();
        // print_r($query);
        return $query;
    }
    public function getAllprovinsi(){
        $query = $this->db->get('provinsi')->result();
       return $query;
}

    /**
     * Data Mahasiswa
     */

    public function getDataMahasiswa()
    {
        $this->datatables->select('a.id_mahasiswa, a.nama,a.nickname, a.nim, a.email, b.nama as nama_kelas, c.nama as nama_jurusan');
        $this->datatables->select('(SELECT COUNT(id) FROM users WHERE username = a.nim) AS ada');
        $this->datatables->from('mahasiswa a');
        $this->datatables->join('tbl_kelas b', 'a.kelas_id=b.id','left');
        $this->datatables->join('tbl_program_belajar c', 'b.id_program=c.id','left');
        return $this->datatables->generate();
    }

    public function getMahasiswaById($id)
    {
        $this->db->select('m.*');
        $this->db->from('mahasiswa m');
        $this->db->join('tbl_kelas k', 'm.kelas_id=k.id');
        $this->db->join('tbl_program_belajar j', 'k.id_program=j.id');
        $this->db->where(['id_mahasiswa' => $id]);
        return $this->db->get()->row();
    }
    public function getMahasiswaByNim($nim)
    {
        $this->db->select('m.*');
        $this->db->from('mahasiswa m');
        $this->db->join('tbl_kelas k', 'm.kelas_id=k.id');
        $this->db->join('tbl_program_belajar j', 'k.id_program=j.id');
        $this->db->where(['nim' => $nim]);
        return $this->db->get()->row();
    }

    public function getJurusan()
    {
        $this->db->select('id_jurusan, nama_jurusan');
        $this->db->from('kelas');
        $this->db->join('jurusan', 'jurusan_id=id_jurusan');
        $this->db->order_by('nama_jurusan', 'ASC');
        $this->db->group_by('id_jurusan');
        $query = $this->db->get();
        return $query->result();
    }
    public function getKelasProgram()
    {
        $this->db->select('tbl_kelas.*, tbl_program_belajar.nama as program');
        $this->db->from('tbl_kelas');
        $this->db->join('tbl_program_belajar', 'tbl_program_belajar.id=tbl_kelas.id_program');
        $this->db->order_by('tbl_program_belajar.nama', 'ASC');
        $this->db->group_by('tbl_kelas.id');
        $query = $this->db->get();
        return $query->result();
    }
    public function getDataprovinsi()
    {
        $this->datatables->select('id, nama');
        $this->datatables->from('provinsi');
        $this->datatables->add_column('bulk_select', '<div class="text-center"><input type="checkbox" class="check" name="checked[]" value="$1"/></div>', 'id, nama');
        return $this->datatables->generate();
    }

    public function getAllJurusan($id = null)
    {
        if($id === null){
            $this->db->order_by('nama_jurusan', 'ASC');
            return $this->db->get('jurusan')->result();    
        }else{
            $this->db->select('jurusan_id');
            $this->db->from('jurusan_matkul');
            $this->db->where('matkul_id', $id);
            $jurusan = $this->db->get()->result();
            $id_jurusan = [];
            foreach ($jurusan as $j) {
                $id_jurusan[] = $j->jurusan_id;
            }
            if($id_jurusan === []){
                $id_jurusan = null;
            }
            
            $this->db->select('*');
            $this->db->from('jurusan');
            $this->db->where_not_in('id_jurusan', $id_jurusan);
            $matkul = $this->db->get()->result();
            return $matkul;
        }
    }

    public function getKelasByJurusan($id)
    {
        $query = $this->db->get_where('kelas', array('jurusan_id'=>$id));
        return $query->result();
    }

    /**
     * Data Dosen
     */

    public function getDataDosen()
    {
        $this->datatables->select('a.id_dosen,a.nip, a.nama_dosen, a.email, a.matkul_id, b.nama, (SELECT COUNT(id) FROM users WHERE username = a.nip OR email = a.email) AS ada');
        $this->datatables->from('dosen a');
        $this->datatables->join('tbl_mapel b', 'a.matkul_id=b.id');
        return $this->datatables->generate();
    }

    public function getDosenById($id)
    {
        $query = $this->db->get_where('dosen', array('id_dosen'=>$id));
        return $query->row();
    }

    /**
     * Data Matkul
     */

    public function getDataMatkul()
    {
        $this->datatables->select('id_matkul, nama_matkul');
        $this->datatables->from('matkul');
        return $this->datatables->generate();
    }

    public function getAllMatkul()
    {
        return $this->db->get('matkul')->result();
    }
    public function getAllMapell()
    {
        return $this->db->get('tbl_mapel')->result();
    }

    public function getMatkulById($id, $single = false)
    {
        if($single === false){
            $this->db->where_in('id_matkul', $id);
            $this->db->order_by('nama_matkul');
            $query = $this->db->get('matkul')->result();
        }else{
            $query = $this->db->get_where('matkul', array('id_matkul'=>$id))->row();
        }
        return $query;
    }

    /**
     * Data Kelas Dosen
     */

    public function getKelasDosen()
    {
        $this->datatables->select('kelas_dosen.id, dosen.id_dosen, dosen.nip, dosen.nama_dosen, GROUP_CONCAT(tbl_kelas.nama) as kelas');
        $this->datatables->from('kelas_dosen');
        $this->datatables->join('tbl_kelas', 'kelas_dosen.kelas_id=tbl_kelas.id');
        $this->datatables->join('dosen', 'kelas_dosen.dosen_id=dosen.id_dosen');
        $this->datatables->group_by('dosen.nama_dosen');
        return $this->datatables->generate();
    }

    public function getAllDosen($id = null)
    {
        $this->db->select('dosen_id');
        $this->db->from('kelas_dosen');
        if($id !== null){
            $this->db->where_not_in('dosen_id', [$id]);
        }
        $dosen = $this->db->get()->result();
        $id_dosen = [];
        foreach ($dosen as $d) {
            $id_dosen[] = $d->dosen_id;
        }
        if($id_dosen === []){
            $id_dosen = null;
        }

        $this->db->select('id_dosen, nip, nama_dosen');
        $this->db->from('dosen');
        $this->db->where_not_in('id_dosen', $id_dosen);
        return $this->db->get()->result();
    }

    
    public function getAllKelas()
    {
        $this->db->select('tbl_kelas.id, tbl_kelas.nama, tbl_program_belajar.nama as program');
        $this->db->from('tbl_kelas');
        $this->db->join('tbl_program_belajar', 'tbl_kelas.id_program=tbl_program_belajar.id');
        $this->db->order_by('tbl_kelas.nama');
        return $this->db->get()->result();
    }
    
    public function getKelasByDosen($id)
    {
        $this->db->select('tbl_kelas.id');
        $this->db->from('kelas_dosen');
        $this->db->join('tbl_kelas', 'kelas_dosen.kelas_id=tbl_kelas.id');
        $this->db->where('dosen_id', $id);
        $query = $this->db->get()->result();
        return $query;
    }
    /**
     * Data Jurusan Matkul
     */

    public function getJurusanMatkul()
    {
        $this->datatables->select('jurusan_matkul.id, matkul.id_matkul, matkul.nama_matkul, jurusan.id_jurusan, GROUP_CONCAT(jurusan.nama_jurusan) as nama_jurusan');
        $this->datatables->from('jurusan_matkul');
        $this->datatables->join('matkul', 'matkul_id=id_matkul');
        $this->datatables->join('jurusan', 'jurusan_id=id_jurusan');
        $this->datatables->group_by('matkul.nama_matkul');
        return $this->datatables->generate();
    }

    public function getMatkul($id = null)
    {
        $this->db->select('matkul_id');
        $this->db->from('jurusan_matkul');
        if($id !== null){
            $this->db->where_not_in('matkul_id', [$id]);
        }
        $matkul = $this->db->get()->result();
        $id_matkul = [];
        foreach ($matkul as $d) {
            $id_matkul[] = $d->matkul_id;
        }
        if($id_matkul === []){
            $id_matkul = null;
        }

        $this->db->select('id_matkul, nama_matkul');
        $this->db->from('matkul');
        $this->db->where_not_in('id_matkul', $id_matkul);
        return $this->db->get()->result();
    }

    public function getJurusanByIdMatkul($id)
    {
        $this->db->select('jurusan.id_jurusan');
        $this->db->from('jurusan_matkul');
        $this->db->join('jurusan', 'jurusan_matkul.jurusan_id=jurusan.id_jurusan');
        $this->db->where('matkul_id', $id);
        $query = $this->db->get()->result();
        return $query;
    }

    public function getDataPaket()
    {
        $this->datatables->select('id, nama_paket, jenis');
        $this->datatables->from('paket');
        $this->datatables->add_column('bulk_select', '<div class="text-center"><input type="checkbox" class="check" name="checked[]" value="$1"/></div>', 'id, nama_paket, jenis');        
        return $this->datatables->generate();
    }
    public function getAllPaket(){
        $query = $this->db->get('paket')->result();
       return json_decode(json_encode($query), True);
   }
    public function getPaketById($id)
    {
        $this->db->where_in('id', $id);
        $this->db->order_by('nama_paket');
        $query = $this->db->get('paket')->result();
        return $query;
    } 

    public function getDataBab()
    {
        $this->datatables->select('bab.id, bab.nama_bab, .tbl_mapel.id as id_mapel, tbl_mapel.nama');
        $this->datatables->from('bab');
        $this->datatables->join('tbl_mapel', 'bab.id_mapel=tbl_mapel.id');
        $this->datatables->add_column('bulk_select', '<div class="text-center"><input type="checkbox" class="check" name="checked[]" value="$1"/></div>', 'id, nama_bab, .tbl_mapel.id as id_mapel, tbl_mapel.nama');        
        return $this->datatables->generate();
    }
    
    public function getBabById($id)
    {
        // print_r();
        $this->db->where_in('id', $id);
        $this->db->order_by('nama_bab');
        $query = $this->db->get('bab')->result();
        return $query;
    }
    public function getDatamateri()
    {
        $this->datatables->select('m.id,b.id as id_bab, b.nama_bab,m.link_youtube,m.file_pdf,m.isi_materi, m.status');
        $this->datatables->from('materi m');
        $this->datatables->join('bab b','b.id=m.id_bab');                    
        $this->datatables->join('tbl_mapel tm','tm.id=b.id_mapel');                    
        $this->datatables->join('tbl_program_belajar tpb','tpb.id=tm.id_program');                    
        return $this->datatables->generate();
    }

    public function getAllBabOld()
    {
        $this->db->select('*');
        $this->db->from('bab');
        $this->db->join('kelas','kelas.id_kelas=bab.id_mapel');
        $this->db->join('jurusan','jurusan.id_jurusan=kelas.jurusan_id');
        $this->db->order_by('nama_bab');
        return $this->db->get()->result();
    }

    public function getAllBab()
    {
        $this->db->select('b.id as id_bab, b.nama_bab, tm.nama as nama_mapel, tpb.nama as nama_program');
        $this->db->from('bab b');
        $this->db->join('tbl_mapel tm','tm.id=b.id_mapel');                    
        $this->db->join('tbl_program_belajar tpb','tpb.id=tm.id_program');
        $this->db->order_by('nama_bab');
        // $data = $query->get_compiled_select();
        // echo $data;
        // exit();
        return $this->db->get()->result();
    }

      public function getDataJenis()
    {
        $this->datatables->select('id, jenis_soal,bobot_benar,bobot_salah,bobot_kosong,waktu_pengerjaan');
        $this->datatables->from('jenis_soal');
        $this->datatables->add_column('bulk_select', '<div class="text-center"><input type="checkbox" class="check" name="checked[]" value="$1"/></div>', 'id,jenis_soal,bobot_benar,bobot_salah,bobot_kosong,waktu_pengerjaan');
        return $this->datatables->generate();
    }
  public function getJenisById($id)
    {
        $this->db->where_in('id', $id);
        $this->db->order_by('jenis_soal');
        $query = $this->db->get('jenis_soal')->result();
        return $query;
    }  
    public function getmateriById($id)
    {
        $this->db->where_in('id', $id);
        $query = $this->db->get('materi')->result();
        return $query;
    }
    function get_bab_by_id($id_mapel){
        $query = $this->db->get_where('bab', array('bab.id_mapel' => $id_mapel));
        return $query;
    }
    public function getAllProgram(){
        $query = $this->db->get('tbl_program_belajar')->result();
       return json_decode(json_encode($query), True);
    }
    public function getallmapel(){
        $this->db->select('tbl_mapel.*,tbl_program_belajar.nama as program');
        $this->db->from('tbl_mapel');
        $this->db->join('tbl_program_belajar','tbl_program_belajar.id=tbl_mapel.id_program');      
        $this->db->order_by('tbl_program_belajar.nama');
        $query = $this->db->get()->result();
       return json_decode(json_encode($query), True);
    }

    public function getDataPendaftaran()
    {
        $this->datatables->select('pendaftaran.id,pendaftaran.nama_lengkap,pendaftaran.jenis_kelamin,pendaftaran.alamat,pendaftaran.nomor_hp,tbl_program_belajar.nama');
        $this->datatables->from('pendaftaran');
        $this->datatables->join('tbl_program_belajar','tbl_program_belajar.id=pendaftaran.id_program');       
        $this->datatables->where('pendaftaran.flag_registrasi','N');       
        return $this->datatables->generate();
    }
    public function getDataPendaftaranId($id)
    {
        $this->db->select('pendaftaran.*,tbl_program_belajar.nama');
        $this->db->from('pendaftaran');
        $this->db->join('tbl_program_belajar','tbl_program_belajar.id=pendaftaran.id_program');   
        $this->db->where('pendaftaran.id',$id);    
        $query = $this->db->get()->result();
        // print_r($this->db->last_query());    
        return $query;
    }
    public function getAllKelasn($id){
        $this->db->select('tbl_kelas.*');
        $this->db->from('tbl_kelas');
        $this->db->join('tbl_program_belajar','tbl_program_belajar.id=tbl_kelas.id_program');
        $this->db->where('tbl_program_belajar.id',$id);
        $query = $this->db->get()->result();
       return json_decode(json_encode($query), True);
}
}