<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Soal_model extends CI_Model {
    
    public function getDataSoal($id, $dosen)
    {
        $this->datatables->select('a.id_soal, a.soal, FROM_UNIXTIME(a.created_on) as created_on, FROM_UNIXTIME(a.updated_on) as updated_on, b.nama_matkul, c.nama_dosen');
        $this->datatables->from('tb_soal a');
        $this->datatables->join('matkul b', 'b.id_matkul=a.matkul_id');
        $this->datatables->join('dosen c', 'c.id_dosen=a.dosen_id');
        if ($id!==null && $dosen===null) {
            $this->datatables->where('a.matkul_id', $id);            
            $this->datatables->where('a.jenis', 'tryout');            
        }else if($id!==null && $dosen!==null){
            $this->datatables->where('a.dosen_id', $dosen);
            $this->datatables->where('a.jenis', 'tryout');            
        }
        return $this->datatables->generate();
    }

    public function getDataSoalKuis($id, $dosen)
    {
        $this->datatables->select('a.id_soal, a.soal, FROM_UNIXTIME(a.created_on) as created_on, FROM_UNIXTIME(a.updated_on) as updated_on, b.nama_matkul, c.nama_dosen');
        $this->datatables->from('tb_soal a');
        $this->datatables->join('matkul b', 'b.id_matkul=a.matkul_id');
        $this->datatables->join('dosen c', 'c.id_dosen=a.dosen_id');
        if ($id!==null && $dosen===null) {
            $this->datatables->where('a.matkul_id', $id);            
            $this->datatables->where('a.jenis', 'kuis');            
        }else if($id!==null && $dosen!==null){
            $this->datatables->where('a.dosen_id', $dosen);
            $this->datatables->where('a.jenis', 'kuis');            
        }
        return $this->datatables->generate();
    }

        public function getDataSoalLatihan($id, $dosen)
    {
        $this->datatables->select('a.id_soal, a.soal, FROM_UNIXTIME(a.created_on) as created_on, FROM_UNIXTIME(a.updated_on) as updated_on, b.nama_matkul, c.nama_dosen');
        $this->datatables->from('tb_soal a');
        $this->datatables->join('matkul b', 'b.id_matkul=a.matkul_id');
        $this->datatables->join('dosen c', 'c.id_dosen=a.dosen_id');
        if ($id!==null && $dosen===null) {
            $this->datatables->where('a.matkul_id', $id);            
            $this->datatables->where('a.jenis', 'latihan');            
        }else if($id!==null && $dosen!==null){
            $this->datatables->where('a.dosen_id', $dosen);
            $this->datatables->where('a.jenis', 'latihan');            
        }
        return $this->datatables->generate();
    }


    public function getSoalById($id)
    {
        return $this->db->get_where('tbl_soal_tryout', ['id_soal' => $id])->row();
    }

    public function getSoalLatihanById($id)
    {
        return $this->db->get_where('tb_soal', ['id_soal' => $id])->row();
    }

     public function getSoalKuisById($id)
    {
        return $this->db->get_where('tb_soal', ['id_soal' => $id])->row();
    }

    public function getSoalPaketById($id)
    {
        return $this->db->get_where('soal_paket', ['id' => $id])->row();
    }

    public function getMatkulDosen($nip)
    {
        $this->db->select('matkul_id, tbl_mapel.nama, id_dosen, nama_dosen');
        $this->db->join('tbl_mapel', 'matkul_id=tbl_mapel.id');
        $this->db->from('dosen')->where('nip', $nip);
        return $this->db->get()->row();
    }
    public function getPaketSoalid($id)
    {
        $this->db->select('paket.nama_paket,soal_paket.*,dosen.nama_dosen,dosen.matkul_id,tbl_mapel.nama');
        $this->db->from('soal_paket');
        $this->db->join('paket', 'paket.id=soal_paket.id_paket');
        $this->db->join('dosen', 'dosen.id_dosen=soal_paket.id_dosen');
        $this->db->join('tbl_mapel','tbl_mapel.id=dosen.matkul_id');
        $this->db->where('soal_paket.id', $id);
        return $this->db->get()->row();
    }

    public function getAllDosen()
    {
        $this->db->select('*');
        $this->db->from('dosen a');
        $this->db->join('tbl_mapel b', 'a.matkul_id=b.id');
        return $this->db->get()->result();
    }
    public function getDataPaketSoal()
    {
        $this->datatables->select('d.*,a.nama_paket,d.jenis_soal,b.nama_dosen');
        $this->datatables->select('(select count(tbl_soal_tryout.id_soal) from tbl_soal_tryout  where tbl_soal_tryout.id_soal_paket=d.id )as jumlah');
        $this->datatables->from('soal_paket d');
        $this->datatables->join('paket a','a.id=d.id_paket');
        $this->datatables->join('dosen b','b.id_dosen=d.id_dosen');
        return $this->datatables->generate();
    }
    public function getDataSoalKuisNew()
    {
       $this->datatables->select('count(tb_soal.id_soal) as jumlah,bab.id as id_bab,dosen.nama_dosen,tm.nama,bab.nama_bab');
        $this->datatables->from('tb_soal');
        $this->datatables->join('dosen','tb_soal.dosen_id=dosen.id_dosen');
        $this->datatables->join('tbl_mapel tm','tb_soal.matkul_id=tm.id');
        $this->datatables->join('bab','bab.id=tb_soal.id_bab');
        $this->datatables->where('tb_soal.jenis','kuis');  
        $this->datatables->group_by('bab.id');  
        return $this->datatables->generate();
    }
    public function getDataSoalLatihanNew()
    {
        $this->datatables->select('count(tb_soal.id_soal) as jumlah,bab.id as id_bab,dosen.nama_dosen,tm.nama,bab.nama_bab');
        $this->datatables->from('tb_soal');
        $this->datatables->join('dosen','tb_soal.dosen_id=dosen.id_dosen');
        $this->datatables->join('tbl_mapel tm','tb_soal.matkul_id=tm.id');
        $this->datatables->join('bab','bab.id=tb_soal.id_bab');
        $this->datatables->where('tb_soal.jenis','latihan');  
        $this->datatables->group_by('bab.id');  
        return $this->datatables->generate();
    }
    public function getDataSoalPaket($id)
    {
        $this->datatables->select('a.id_soal, a.soal, FROM_UNIXTIME(a.created_on) as created_on, FROM_UNIXTIME(a.updated_on) as updated_on, b.nama, c.nama_dosen');
        $this->datatables->from('tbl_soal_tryout a');
        $this->datatables->join('tbl_mapel b', 'b.id=a.matkul_id');
        $this->datatables->join('dosen c', 'c.id_dosen=a.dosen_id');
        $this->datatables->where('a.id_soal_paket', $id);           
        return $this->datatables->generate();
    }
    public function getDataDetailKuis($id)
    {
        $this->datatables->select('a.id_soal, a.soal, FROM_UNIXTIME(a.created_on) as created_on, FROM_UNIXTIME(a.updated_on) as updated_on, b.nama as nama_matkul, c.nama_dosen');
        $this->datatables->from('tb_soal a');
        $this->datatables->join('tbl_mapel b', 'b.id=a.matkul_id');
        $this->datatables->join('dosen c', 'c.id_dosen=a.dosen_id');
        $this->datatables->where('a.id_bab', $id);
        $this->datatables->where('a.jenis','kuis');             
        return $this->datatables->generate();
    }
    public function getDataDetailLatihan($id)
    {
        $this->datatables->select('a.id_soal, a.soal, FROM_UNIXTIME(a.created_on) as created_on, FROM_UNIXTIME(a.updated_on) as updated_on, b.nama as nama_matkul, c.nama_dosen');
        $this->datatables->from('tb_soal a');
        $this->datatables->join('tbl_mapel b', 'b.id=a.matkul_id');
        $this->datatables->join('dosen c', 'c.id_dosen=a.dosen_id');
        $this->datatables->where('a.id_bab', $id);
        $this->datatables->where('a.jenis','latihan');             
        return $this->datatables->generate();
    }
}
