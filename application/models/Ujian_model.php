<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Ujian_model extends CI_Model {
    
    public function getDataUjian($id)
    {
        $this->datatables->select('a.id_ujian, a.token, a.nama_ujian, b.nama, a.jumlah_soal, CONCAT(a.tgl_mulai, " <br/> (", a.waktu, " Menit)") as waktu, a.jenis');
        $this->datatables->from('m_ujian a');
        $this->datatables->join('tbl_mapel b', 'a.matkul_id = b.id');
        if($id!==null){
            $this->datatables->where('dosen_id', $id);
        }
        return $this->datatables->generate();
    }

    public function getDataUjianKuis($id)
    {
        $this->datatables->select('a.id_kuis, a.token, a.nama_kuis, b.nama as nama_matkul, a.jumlah_soal, CONCAT(a.tgl_mulai, " <br/> (", a.waktu, " Menit)") as waktu, a.jenis');
        $this->datatables->from('m_kuis a');
        $this->datatables->join('tbl_mapel b', 'a.matkul_id = b.id');
        if($id!==null){
            $this->datatables->where('dosen_id', $id);
        }
        return $this->datatables->generate();
    }

    public function getDataUjianLatihan($id)
    {
        $this->datatables->select('a.id_latihan, a.token, a.nama_latihan, b.nama as nama_matkul, a.jumlah_soal, CONCAT(a.tgl_mulai, " <br/> (", a.waktu, " Menit)") as waktu, a.jenis');
        $this->datatables->from('m_latihan a');
        $this->datatables->join('tbl_mapel b', 'a.matkul_id = b.id');
        if($id!==null){
            $this->datatables->where('dosen_id', $id);
        }
        return $this->datatables->generate();
    }
    
    public function getListUjian($id, $kelas)
    {
        $today=date('Y-m-d H:i:s');
        // print_r($today);
        $this->datatables->select("a.id_ujian, e.nama_dosen, a.nama_ujian, b.nama as nama_matkul, a.jumlah_soal, CONCAT(a.terlambat, ' <br/> (', a.waktu, ' Menit)') as waktu,  (SELECT COUNT(id) FROM h_ujian h WHERE h.mahasiswa_id = {$id} AND h.ujian_id = a.id_ujian) AS ada");
        $this->datatables->from('m_ujian a');
        $this->datatables->join('tbl_mapel b', 'a.matkul_id = b.id','left');
        $this->datatables->join('kelas_dosen c', 'a.dosen_id = c.dosen_id','left');
        $this->datatables->join('tbl_kelas d', 'c.kelas_id = d.id','left');
        $this->datatables->join('dosen e', 'e.id_dosen = c.dosen_id','left');
        $this->datatables->join('tbl_program_belajar j', 'j.id = d.id_program','left');
        $this->datatables->where('j.id', $kelas);
        $this->datatables->where('a.terlambat >=',$today);
        $this->datatables->group_by('a.id_ujian');
        $this->datatables->group_by('e.nama_dosen');
        // $this->datatables->group_by('d.nama');
        $this->datatables->group_by('a.nama_ujian');
        $this->datatables->group_by('b.nama');
        $this->datatables->group_by('a.jumlah_soal');
        return $this->datatables->generate();
    }

    public function getListUjianKuis($id, $kelas, $id_bab)
    {
        $this->datatables->select("a.id_kuis, e.nama_dosen, d.nama as nama_kelas, a.nama_kuis, b.nama as nama_matkul, a.jumlah_soal, CONCAT(a.tgl_mulai, ' <br/> (', a.waktu, ' Menit)') as waktu,  (SELECT COUNT(id) FROM h_kuis h WHERE h.mahasiswa_id = {$id} AND h.kuis_id = a.id_kuis) AS ada");
        $this->datatables->from('m_kuis a');
        $this->datatables->join('tbl_mapel b', 'a.matkul_id = b.id','left');
        $this->datatables->join('kelas_dosen c', 'a.dosen_id = c.dosen_id','left');
        $this->datatables->join('tbl_kelas d', 'c.kelas_id = d.id','left');
        $this->datatables->join('dosen e', 'e.id_dosen = c.dosen_id','left');
        $this->datatables->join('tbl_program_belajar j', 'j.id = d.id_program','left');
        // $this->datatables->where('j.id', $kelas);
        $this->datatables->where('a.id_bab', $id_bab);
        return $this->datatables->generate();
    }

     public function getListUjianLatihan($id, $kelas, $id_bab)
    {
        $this->datatables->select("a.id_latihan, e.nama_dosen, d.nama as nama_kelas, a.nama_latihan, a.status, b.nama as nama_matkul, a.jumlah_soal, CONCAT(a.tgl_mulai, ' <br/> (', a.waktu, ' Menit)') as waktu,  (SELECT COUNT(id) FROM h_latihan h WHERE h.mahasiswa_id = {$id} AND h.latihan_id = a.id_latihan) AS ada");
        $this->datatables->from('m_latihan a');
        $this->datatables->join('tbl_mapel b', 'a.matkul_id = b.id','left');
        $this->datatables->join('kelas_dosen c', 'a.dosen_id = c.dosen_id','left');
        $this->datatables->join('tbl_kelas d', 'c.kelas_id = d.id','left');
        $this->datatables->join('dosen e', 'e.id_dosen = c.dosen_id','left');
        $this->datatables->join('tbl_program_belajar j', 'j.id = d.id_program','left');
        // $this->datatables->where('j.id', $kelas);
        $this->datatables->where('a.id_bab', $id_bab);
        return $this->datatables->generate();
    }

    public function getUjianById($id)
    {
        $this->db->select('*');
        $this->db->from('m_ujian a');
        $this->db->join('dosen b', 'a.dosen_id=b.id_dosen');
        $this->db->join('tbl_mapel c', 'a.matkul_id=c.id');
        $this->db->join('soal_paket ps', 'a.id_soal_paket=ps.id');
        $this->db->where('id_ujian', $id);
        return $this->db->get()->row();
    }

    public function peringkatByUjianId($id){
        $this->db->select('count(*) as jumlah_peserta');
        $this->db->from('h_ujian');
        $this->db->where('ujian_id', $id);
        return $this->db->get()->row_array();
    }

    public function peringkatByPeserta($id_ujian){
        $this->db->select('ujian_id, mahasiswa_id, nilai');
        $this->db->from('h_ujian');
        $this->db->where('ujian_id', $id_ujian);
        $this->db->order_by('nilai','desc');
        return $this->db->get()->result_array();
    }    

    public function getUjianKuisById($id)
    {
        $this->db->select('a.*,b.*, tm.nama as nama_matkul,bb.nama_bab');
        $this->db->from('m_kuis a');
        $this->db->join('dosen b', 'a.dosen_id=b.id_dosen','left');
        $this->db->join('tbl_program_belajar c', 'a.matkul_id=c.id','left');
        $this->db->join('tbl_mapel tm', 'tm.id=a.matkul_id','left');
        $this->db->join('bab bb', 'bb.id=a.id_bab','left');
        $this->db->where('id_kuis', $id);
        return $this->db->get()->row();
    }

    public function getUjianLatihanById($id)
    {
        $this->db->select('a.*,b.*, tm.nama as nama_matkul,bb.nama_bab');
        $this->db->from('m_latihan a');
        $this->db->join('dosen b', 'a.dosen_id=b.id_dosen','left');
        $this->db->join('tbl_program_belajar c', 'a.matkul_id=c.id','left');
        $this->db->join('tbl_mapel tm', 'tm.id=a.matkul_id','left');
        $this->db->join('bab bb', 'bb.id=a.id_bab','left');
        $this->db->where('id_latihan', $id);
        return $this->db->get()->row();
    }

    public function getIdDosen($nip)
    {
        $this->db->select('id_dosen, nama_dosen')->from('dosen')->where('nip', $nip);
        return $this->db->get()->row();
    }
    public function soalpaketdosen($nip)
    {
        $this->db->select('c.nama_paket,a.id');
        $this->db->from("soal_paket a");
        $this->db->join('dosen b', 'a.id_dosen=b.id_dosen');
        $this->db->join('paket c', 'c.id=a.id_paket');
        $this->db->where('b.nip', $nip);
        return $this->db->get()->result();
    }

    public function getJumlahSoal($dosen,$jenis, $id_bab)
    {
        $query = $this->db->select('COUNT(id_soal) as jml_soal');
        $this->db->from('tb_soal');
        $this->db->where('dosen_id', $dosen);
        $this->db->where('jenis', $jenis);
        $this->db->where('id_bab', $id_bab);
        return $this->db->get()->row();
    }
    public function getJumlahSoalTo($dosen, $id_paket)
    {
        $query = $this->db->select('COUNT(id_soal) as jml_soal');
        $this->db->from('tbl_soal_tryout');
        $this->db->where('dosen_id', $dosen);
        $this->db->where('id_soal_paket', $id_paket);
        return $this->db->get()->row();
    }

    public function getIdMahasiswa($nim)
    {
        $this->db->select('a.*, k.nama as nama_kelas, c.nama as nama_jurusan,c.id as id_program');
        $this->db->from('mahasiswa a');
        $this->db->join('tbl_kelas k', 'a.kelas_id=k.id','left');
        $this->db->join('tbl_program_belajar c', 'k.id_program=c.id','left');
        $this->db->where('nim', $nim);
        return $this->db->get()->row();
    }

    public function HslUjian($id, $mhs)
    {
        // $this->db->select('*, UNIX_TIMESTAMP(tgl_selesai) as waktu_habis');
        $this->db->select('*');
        $this->db->from('h_ujian');
        $this->db->where('ujian_id', $id);
        $this->db->where('mahasiswa_id', $mhs);
        return $this->db->get();
    }

    public function HslUjianKuis($id, $mhs)
    {
        // $this->db->select('*, UNIX_TIMESTAMP(tgl_selesai) as waktu_habis');
        $this->db->select('*');
        $this->db->from('h_kuis');
        $this->db->where('kuis_id', $id);
        $this->db->where('mahasiswa_id', $mhs);
        return $this->db->get();
    }

    public function HslUjianLatihan($id, $mhs)
    {
        // $this->db->select('*, UNIX_TIMESTAMP(tgl_selesai) as waktu_habis');
        $this->db->select('*');
        $this->db->from('h_latihan');
        $this->db->where('latihan_id', $id);
        $this->db->where('mahasiswa_id', $mhs);
        return $this->db->get();
    }

    public function getSoal($id,$jenis,$id_bab)
    {
        if ($jenis=='kuis') {
            $ujian = $this->getUjianKuisById($id);
        } else if ($jenis=='latihan') {
            $ujian = $this->getUjianLatihanById($id);
        } else {
            $ujian = $this->getUjianById($id);
        }
        $order = $ujian->jenis==="acak" ? 'rand()' : 'id_soal';

        $this->db->select('id_soal, soal, file, tipe_file, opsi_a, opsi_b, opsi_c, opsi_d, opsi_e, jawaban');
        $this->db->from('tb_soal');
        $this->db->where('dosen_id', $ujian->dosen_id);
        $this->db->where('matkul_id', $ujian->matkul_id);
        $this->db->where('jenis', $jenis);
        $this->db->where('id_bab', $id_bab);
        $this->db->order_by($order);
        $this->db->limit($ujian->jumlah_soal);
        return $this->db->get()->result();
    }
    public function getSoalTo($id)
    {
 
        $ujian = $this->getUjianById($id);

        $order = $ujian->jenis==="acak" ? 'rand()' : 'id_soal';

        $this->db->select('id_soal, soal, file, tipe_file, opsi_a, opsi_b, opsi_c, opsi_d, opsi_e, jawaban');
        $this->db->from('tbl_soal_tryout');
        $this->db->where('dosen_id', $ujian->dosen_id);
        $this->db->where('matkul_id', $ujian->matkul_id);
        $this->db->where('id_soal_paket', $ujian->id_soal_paket);
        $this->db->order_by($order);
        $this->db->limit($ujian->jumlah_soal);
        return $this->db->get()->result();
    }

    public function ambilSoal($pc_urut_soal1, $pc_urut_soal_arr)
    {
        $this->db->select("*, {$pc_urut_soal1} AS jawaban");
        $this->db->from('tb_soal');
        $this->db->where('id_soal', $pc_urut_soal_arr);
        return $this->db->get()->row();
    }
    public function ambilSoalTo($pc_urut_soal1, $pc_urut_soal_arr)
    {
        $this->db->select("*, {$pc_urut_soal1} AS jawaban");
        $this->db->from('tbl_soal_tryout');
        $this->db->where('id_soal', $pc_urut_soal_arr);
        return $this->db->get()->row();
    }

    public function getJawaban($id_tes)
    {
        $this->db->select('list_jawaban');
        $this->db->from('h_ujian');
        $this->db->where('id', $id_tes);
        return $this->db->get()->row()->list_jawaban;
    }

    public function getJawabanKuis($id_tes)
    {
        $this->db->select('list_jawaban');
        $this->db->from('h_kuis');
        $this->db->where('id', $id_tes);
        return $this->db->get()->row()->list_jawaban;
    }

    public function getJawabanLatihan($id_tes)
    {
        $this->db->select('list_jawaban');
        $this->db->from('h_latihan');
        $this->db->where('id', $id_tes);
        return $this->db->get()->row()->list_jawaban;
    }

    public function getHasilUjian($nip = null,$id_kelas = null)
    {
        $this->datatables->select('b.id_ujian, b.nama_ujian, b.jumlah_soal, CONCAT(b.waktu, " Menit") as waktu, b.tgl_mulai');
        $this->datatables->select('c.nama as nama_matkul, d.nama_dosen');
        $this->datatables->from('h_ujian a');
        $this->datatables->join('m_ujian b', 'a.ujian_id = b.id_ujian');
        $this->datatables->join('tbl_mapel c', 'b.matkul_id = c.id');
        $this->datatables->join('dosen d', 'b.dosen_id = d.id_dosen');
        if($id_kelas !== null){
            $this->datatables->join('kelas_dosen kd', 'kd.dosen_id = d.id_dosen');
        }
        $this->datatables->group_by('b.id_ujian');
        if($nip !== null){
            $this->datatables->where('d.nip', $nip);
        }
        if($id_kelas !== null){
            $this->datatables->where('kd.kelas_id', $id_kelas);
        }
        return $this->datatables->generate();
    }

    public function getHasilUjianKuis($nip = null,$id_kelas = null)
    {
        $this->datatables->select('b.id_kuis, b.nama_kuis, b.jumlah_soal, CONCAT(b.waktu, " Menit") as waktu, b.tgl_mulai');
        $this->datatables->select('c.nama as nama_matkul, d.nama_dosen');
        $this->datatables->from('h_kuis a');
        $this->datatables->join('m_kuis b', 'a.kuis_id = b.id_kuis');
        $this->datatables->join('tbl_mapel c', 'b.matkul_id = c.id');
        $this->datatables->join('dosen d', 'b.dosen_id = d.id_dosen');
        if($id_kelas !== null){
            $this->datatables->join('kelas_dosen kd', 'kd.dosen_id = d.id_dosen');
        }
        $this->datatables->group_by('b.id_kuis');
        if($nip !== null){
            $this->datatables->where('d.nip', $nip);
        }
        if($id_kelas !== null){
            $this->datatables->where('kd.kelas_id', $id_kelas);
        }
        return $this->datatables->generate();
    }

    public function getHasilUjianLatihan($nip = null,$id_kelas=null)
    {
        $this->datatables->select('b.id_latihan, b.nama_latihan, b.jumlah_soal, CONCAT(b.waktu, " Menit") as waktu, b.tgl_mulai');
        $this->datatables->select('c.nama as nama_matkul, d.nama_dosen');
        $this->datatables->from('h_latihan a');
        $this->datatables->join('m_latihan b', 'a.latihan_id = b.id_latihan');
        $this->datatables->join('tbl_mapel c', 'b.matkul_id = c.id');
        $this->datatables->join('dosen d', 'b.dosen_id = d.id_dosen');
        if($id_kelas !== null){
            $this->datatables->join('kelas_dosen kd', 'kd.dosen_id = d.id_dosen');
        }
        $this->datatables->group_by('b.id_latihan');
        if($nip !== null){
            $this->datatables->where('d.nip', $nip);
        }
        if($id_kelas !== null){
            $this->datatables->where('kd.kelas_id', $id_kelas);
        }
        return $this->datatables->generate();
    }

    public function getDataMateriJenjang($id)
    {
        $this->datatables->select('tm.id, tm.nama, tbp.nama as nama_program');
        $this->datatables->from('tbl_mapel tm');        
        $this->datatables->join('tbl_program_belajar tbp', 'tbp.id=tm.id_program'); 
        $this->datatables->where('tbp.id',$id);       
        return $this->datatables->generate();
    }

    public function getDetailMateriById($id)
    {
        $this->datatables->select('j.id, j.nama, tbp.nama as nama_program, b.nama_bab');
        $this->datatables->from('tbl_mapel j');        
        $this->datatables->join('tbl_program_belajar tbp', 'tbp.id=j.id_program');        
        $this->datatables->join('bab b','j.id=b.id_mapel');  
        $this->datatables->where(['j.id' => $id]);  
  
        return $this->datatables->generate();
    }

    public function getDataMateriJenjangDetail($id, $dt=false)
    {
        if($dt===false){
            $db = "db";
            $get = "get";
        }else{
            $db = "datatables";
            $get = "generate";
        }

        $this->$db->select('j.id, j.nama, tbp.nama as nama_program, b.nama_bab, m.id_bab as id_bab, m.status');
        $this->$db->from('tbl_mapel j');        
        $this->$db->join('tbl_program_belajar tbp', 'tbp.id=j.id_program');        
        $this->$db->join('bab b','j.id=b.id_mapel');  
        $this->$db->join('materi m','m.id_bab=b.id');
        $this->$db->where(['j.id' => $id]);  
  
        return $this->$db->$get();
    }

    public function download_materi_soal($id, $id_bab, $dt=false)
    {

        $db = "datatables";
        $get = "generate";

        $this->$db->select('m.link_youtube, m.file_pdf, m.isi_materi');
        $this->$db->from('tbl_mapel j');        
        $this->$db->join('tbl_program_belajar k','k.id=j.id_program');        
        $this->$db->join('bab b','j.id=b.id_mapel');  
        $this->$db->join('materi m','m.id_bab=b.id');  
        $this->$db->where(['j.id' => $id,'b.id' => $id_bab]);  
  
        return $this->$db->$get();
    }

    public function HslUjianById($id, $dt=false)
    {
        if($dt===false){
            $db = "db";
            $get = "get";
        }else{
            $db = "datatables";
            $get = "generate";
        }
        
        $this->$db->select('d.id, a.nama as nama, a.nim as username, c.nama as nama_program, d.jml_benar as jml_benar,(m.jumlah_soal-d.jml_benar-d.jml_kosong) as jumlah_salah, d.jml_kosong as jumlah_kosong, d.nilai,m.dasar_kriteria,m.nilai as min,m.jml_soal');
        $this->$db->from('mahasiswa a');
        $this->$db->join('tbl_kelas b', 'a.kelas_id=b.id','left');
        $this->$db->join('tbl_program_belajar c', 'b.id_program=c.id','left');
        $this->$db->join('h_ujian d', 'a.id_mahasiswa=d.mahasiswa_id','left');
        $this->$db->join('m_ujian m', 'm.id_ujian=d.ujian_id');
        $this->$db->where(['d.ujian_id' => $id]);
        $this->db->order_by('d.nilai_bobot', 'desc');
        // $this->$db->order_by('jumlah_salah', 'desc');
        return $this->$db->$get();
    }

    public function HslUjianByIdCetak($id, $dt=false)
    {
        if($dt===false){
            $db = "db";
            $get = "get";
        }else{
            $db = "datatables";
            $get = "generate";
        }
        
        $this->$db->select('d.id, a.nama as nama, c.nama as nama_program, d.jml_benar as jml_benar,(m.jumlah_soal-d.jml_benar-d.jml_kosong) as jumlah_salah, d.jml_kosong as jumlah_kosong, d.nilai,m.dasar_kriteria,m.nilai as min,m.jml_soal');
        $this->$db->from('mahasiswa a');
        $this->$db->join('tbl_kelas b', 'a.kelas_id=b.id','left');
        $this->$db->join('tbl_program_belajar c', 'b.id_program=c.id','left');
        $this->$db->join('h_ujian d', 'a.id_mahasiswa=d.mahasiswa_id','left');
        $this->$db->join('m_ujian m', 'm.id_ujian=d.ujian_id');
        $this->$db->where(['d.ujian_id' => $id]);
        $this->$db->order_by('d.jml_benar', 'desc');
        $this->$db->order_by('jumlah_salah', 'desc');
        return $this->$db->$get();
    }

    public function HslUjianByIdKuis($id, $dt=false)
    {
        if($dt===false){
            $db = "db";
            $get = "get";
        }else{
            $db = "datatables";
            $get = "generate";
        }
        
        $this->$db->select('d.id, a.nama, c.nama as nama_jurusan, d.jml_benar, d.nilai');
        $this->$db->from('mahasiswa a');
        $this->$db->join('tbl_kelas b', 'a.kelas_id=b.id');
        $this->$db->join('tbl_program_belajar c', 'b.id_program=c.id');
        $this->$db->join('h_kuis d', 'a.id_mahasiswa=d.mahasiswa_id');
        $this->$db->where(['d.kuis_id' => $id]);
        return $this->$db->$get();
    }

    public function HslUjianByIdLatihan($id, $dt=false)
    {
        if($dt===false){
            $db = "db";
            $get = "get";
        }else{
            $db = "datatables";
            $get = "generate";
        }
        
        $this->$db->select('d.id, a.nama, c.nama as nama_jurusan, d.jml_benar, d.nilai');
        $this->$db->from('mahasiswa a');
        $this->$db->join('tbl_kelas b', 'a.kelas_id=b.id');
        $this->$db->join('tbl_program_belajar c', 'b.id_program=c.id');
        $this->$db->join('h_latihan d', 'a.id_mahasiswa=d.mahasiswa_id');
        $this->$db->where(['d.latihan_id' => $id]);
        return $this->$db->$get();
    }

    public function bandingNilai($id)
    {
        $this->db->select_min('nilai', 'min_nilai');
        $this->db->select_max('nilai', 'max_nilai');
        $this->db->select_avg('FORMAT(FLOOR(nilai),0)', 'avg_nilai');
        $this->db->where('ujian_id', $id);
        return $this->db->get('h_ujian')->row();
    }
    public function totalNilai($id)
    {
        $this->db->select('d.nilai,m.dasar_kriteria,m.nilai as min,m.jml_soal, d.id, a.nama as nama, c.nama as nama_program, d.jml_benar,(m.jumlah_soal-d.jml_benar-d.jml_kosong) as jumlah_salah,d.jml_kosong as jumlah_kosong, a.id_mahasiswa');
        $this->db->from('mahasiswa a');
        $this->db->join('tbl_kelas b', 'a.kelas_id=b.id','left');
        $this->db->join('tbl_program_belajar c', 'b.id_program=c.id','left');
        $this->db->join('h_ujian d', 'a.id_mahasiswa=d.mahasiswa_id','left');
        $this->db->join('m_ujian m', 'm.id_ujian=d.ujian_id');
        $this->db->where(['d.ujian_id' => $id]);
        return $this->db->get()->result();
    }

    public function bandingNilaiKuis($id)
    {
        $this->db->select_min('nilai', 'min_nilai');
        $this->db->select_max('nilai', 'max_nilai');
        $this->db->select_avg('FORMAT(FLOOR(nilai),0)', 'avg_nilai');
        $this->db->where('kuis_id', $id);
        return $this->db->get('h_kuis')->row();
    }

    public function bandingNilaiLatihan($id)
    {
        $this->db->select_min('nilai', 'min_nilai');
        $this->db->select_max('nilai', 'max_nilai');
        $this->db->select_avg('FORMAT(FLOOR(nilai),0)', 'avg_nilai');
        $this->db->where('latihan_id', $id);
        return $this->db->get('h_latihan')->row();
    }
    public function getListUjiantable($id, $kelas)
    {
        $this->db->select("a.id_ujian, e.nama_dosen, d.nama as nama_kelas, a.nama_ujian, b.nama as nama_matkul, a.jumlah_soal, CONCAT(a.tgl_mulai, ' <br/> (', a.waktu, ' Menit)') as waktu,  (SELECT COUNT(id) FROM h_ujian h WHERE h.mahasiswa_id = {$id} AND h.ujian_id = a.id_ujian) AS ada");
        $this->db->from('m_ujian a');
        $this->db->join('tbl_mapel b', 'a.matkul_id = b.id','left');
        $this->db->join('kelas_dosen c', 'a.dosen_id = c.dosen_id','left');
        $this->db->join('tbl_kelas d', 'c.kelas_id = d.id','left');
        $this->db->join('dosen e', 'e.id_dosen = c.dosen_id','left');
        $this->db->join('tbl_program_belajar j', 'j.id = d.id_program','left');
        $this->db->where('j.id', $kelas);
        return $this->db->get();
    }
    public function getListUjianKuistable($id, $kelas)
    {
        $this->db->select("a.id_kuis, e.nama_dosen, d.nama as nama_kelas, a.nama_kuis, b.nama as nama_matkul, a.jumlah_soal, CONCAT(a.tgl_mulai, ' <br/> (', a.waktu, ' Menit)') as waktu,  (SELECT COUNT(id) FROM h_kuis h WHERE h.mahasiswa_id = {$id} AND h.kuis_id = a.id_kuis) AS ada");
        $this->db->from('m_kuis a');
        $this->db->join('tbl_mapel b', 'a.matkul_id = b.id','left');
        $this->db->join('kelas_dosen c', 'a.dosen_id = c.dosen_id','left');
        $this->db->join('tbl_kelas d', 'c.kelas_id = d.id','left');
        $this->db->join('dosen e', 'e.id_dosen = c.dosen_id','left');
        $this->db->join('tbl_program_belajar j', 'j.id = d.id_program','left');
        $this->db->where('j.id', $kelas);
        return $this->db->get();
    }
    
    public function getListUjianLatihantabel($id, $kelas)
    {
        $this->db->select("a.id_latihan, e.nama_dosen, d.nama as nama_kelas, a.nama_latihan, b.nama as nama_matkul, a.jumlah_soal, CONCAT(a.tgl_mulai, ' <br/> (', a.waktu, ' Menit)') as waktu,  (SELECT COUNT(id) FROM h_latihan h WHERE h.mahasiswa_id = {$id} AND h.latihan_id = a.id_latihan) AS ada");
        $this->db->from('m_latihan a');
        $this->db->join('tbl_mapel b', 'a.matkul_id = b.id','left');
        $this->db->join('kelas_dosen c', 'a.dosen_id = c.dosen_id','left');
        $this->db->join('tbl_kelas d', 'c.kelas_id = d.id','left');
        $this->db->join('dosen e', 'e.id_dosen = c.dosen_id','left');
        $this->db->join('tbl_program_belajar j', 'j.id = d.id_program','left');
        $this->db->where('j.id', $kelas);
        return $this->db->get();
    }
    public function getListSoal($id,$id_mhs){
        $this->db->select('list_soal,list_jawaban');
        $this->db->from('h_ujian');
        $this->db->where('ujian_id', $id);
        $this->db->where('mahasiswa_id', $id_mhs);
        return $this->db->get()->row();

    }
    public function getHistoryTo($id,$id_mhs){
        $list=$this->getListSoal($id,$id_mhs)->list_jawaban;
        $id=[];
        $list=explode(',',$list);
        foreach ($list as $key => $value) {
            $value=explode(':',$value);
            $id[$key]=$value[0];
        }

        $this->db->select('*');
        $this->db->from('tbl_soal_tryout');
        $this->db->where_in('id_soal', $id);
        return $this->db->get()->result();

    }
}