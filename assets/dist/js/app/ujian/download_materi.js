var table;

$(document).ready(function() {
  ajaxcsrf();

  table = $("#download_materi").DataTable({
    initComplete: function() {
      var api = this.api();
      $("#download_materi_filter input")
        .off(".DT")
        .on("keyup.DT", function(e) {
          api.search(this.value).draw();
        });
    },
    dom:
      "<'row'<'col-sm-3'l><'col-sm-6 text-center'B><'col-sm-3'f>>" +
      "<'row'<'col-sm-12'tr>>" +
      "<'row'<'col-sm-5'i><'col-sm-7'p>>",
    buttons: [
      {
        extend: "copy",
        exportOptions: { columns: [0, 1, 2, 3] }
      },
      {
        extend: "print",
        exportOptions: { columns: [0, 1, 2, 3] }
      },
      {
        extend: "excel",
        exportOptions: { columns: [0, 1, 2, 3] }
      },
      {
        extend: "pdf",
        exportOptions: { columns: [0, 1, 2, 3] }
      }
    ],
    oLanguage: {
      sProcessing: "loading..."
    },
    processing: true,
    serverSide: true,
    ajax: {
      url: base_url + "materi/download_materi_soal/"+id_jurusan+"/"+id_bab,
      type: "POST"
    },
    columns: [
      {
        data: "link_youtube",
        orderable: false,
        searchable: false
      },
      { data: "link_youtube" },
      { data: "file_pdf" },
      { data: "isi_materi" },
      {
        orderable: false,
        searchable: false
      }
    ],
    columnDefs: [
      {
        targets: 4,
        data: "file_pdf",
        render: function(data, type, row, meta) {
          return `
                    <div class="text-center">
                        <a class="btn btn-xs bg-maroon" href="${base_url}uploads/materi/${data}" >
                            <i class="fa fa-download"></i> Unduh Materi
                        </a>
                         <a onclick="return confirm('Pastikan Anda sudah membaca dan memahami materi yang ada pada Latihan ini. Terimakasih.')" 
                         class="btn btn-xs bg-blue" href="${base_url}latihan/latihan/`+id_bab+`" >
                            <i class="fa fa-search"></i> Soal Latihan
                        </a>
                        <a onclick="return confirm('Pastikan Anda sudah membaca dan memahami materi yang ada pada Kuis ini. Terimakasih.')" 
                         class="btn btn-xs bg-green" href="${base_url}kuis/kuis/`+id_bab+`" >
                            <i class="fa fa-search"></i> Soal Kuis
                        </a>
                    </div>
                    `;
        }
      }
    ],
    order: [[1, "asc"]],
    rowId: function(a) {
      return a;
    },
    rowCallback: function(row, data, iDisplayIndex) {
      var info = this.fnPagingInfo();
      var page = info.iPage;
      var length = info.iLength;
      var index = page * length + (iDisplayIndex + 1);
      $("td:eq(0)", row).html(index);
    }
  });
});

table
  .buttons()
  .container()
  .appendTo("#download_materi_wrapper .col-md-6:eq(0)");
