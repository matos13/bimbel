var table;

$(document).ready(function () {

    ajaxcsrf();

    table = $("#ujian_latihan").DataTable({
        initComplete: function () {
            var api = this.api();
            $('#ujian_latihan_filter input')
                .off('.DT')
                .on('keyup.DT', function (e) {
                    api.search(this.value).draw();
                });
        },
        oLanguage: {
            sProcessing: "loading..."
        },
        processing: true,
        serverSide: true,
        ajax: {
            "url": base_url+"latihan/list_latihan_json/"+id_bab,
            "type": "POST",
        },
        columns: [
            {
                "data": "id_latihan",
                "orderable": false,
                "searchable": false
            },
            { "data": 'nama_latihan' },
            { "data": 'nama_matkul' },
            { "data": 'nama_dosen' },
            { "data": 'jumlah_soal' },
            { "data": 'waktu' },
            {
                "searchable": false,
                "orderable": false
            }
        ],
        columnDefs: [
            {
                "targets": 6,
                "data": {
                    "id_latihan": "id_latihan",
                    "ada": "ada",
                    "status": "status"
                },
                "render": function (data, type, row, meta) {
                    var btn;
                    // if (data.status == 0 ) {
                    //     btn = `<span class="btn btn-xs btn-danger">
                    //                 <i class="fa fa-lock"></i> Latihan terkunci
                    //             </span>`;
                    // } else {
                        if (data.ada > 0) {
                            btn = `
    								<a class="btn btn-xs btn-success" href="${base_url}hasilujianlatihan/cetak/${data.id_latihan}/`+id_bab+`" target="_blank">
    									<i class="fa fa-print"></i> Cetak Hasil
    								</a>
                                    <a class="btn btn-xs btn-danger" href="${base_url}latihan/tokenlatihan/${data.id_latihan}/`+id_bab+`">
                                    <i class="fa fa-pencil"></i> Retake Latihan
                                </a>
                                    `;
                        } else {
                            btn = `<a class="btn btn-xs btn-primary" href="${base_url}latihan/tokenlatihan/${data.id_latihan}/`+id_bab+`">
    								<i class="fa fa-pencil"></i> Ikut Latihan
    							</a>`;
                        }
                    // }

                    return `<div class="text-center">
									${btn}
								</div>`;
                }
            },
        ],
        order: [
            [1, 'asc']
        ],
        rowId: function (a) {
            return a;
        },
        rowCallback: function (row, data, iDisplayIndex) {
            var info = this.fnPagingInfo();
            var page = info.iPage;
            var length = info.iLength;
            var index = page * length + (iDisplayIndex + 1);
            $('td:eq(0)', row).html(index);
        }
    });
});