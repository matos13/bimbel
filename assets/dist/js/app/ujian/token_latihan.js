$(document).ready(function () {
    ajaxcsrf();

    $('#btncek').on('click', function () {
        var token   = $('#token').val();
        var idUjian = $(this).data('id');
        var id_bab  = $('#id_bab').val();
        console.log(id_bab);
        console.log(idUjian);

        if (token === '') {
            Swal('Gagal', 'Token harus diisi', 'error');
        } else {
            var key = $('#id_ujian').data('key');
            $.ajax({
                url: base_url + 'latihan/cektokenlatihan/',
                type: 'POST',
                data: {
                    id_latihan: idUjian,
                    token: token
                },
                cache: false,
                success: function (result) {
                    Swal({
                        "type": result.status ? "success" : "error",
                        "title": result.status ? "Berhasil" : "Gagal",
                        "text": result.status ? "Token Benar" : "Token Salah"
                    }).then((data) => {
                        if(result.status){
                            location.href = base_url + 'latihan/ujian_latihan/'+id_bab+'/?key=' + key;
                        }
                    });
                }
            });
        }
    });

    var time = $('.countdown');
    if (time.length) {
        countdown(time.data('time'));
    }
});