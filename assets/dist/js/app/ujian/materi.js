var table;

$(document).ready(function() {
  ajaxcsrf();

  table = $("#materi").DataTable({
    initComplete: function() {
      var api = this.api();
      $("#materi_filter input")
        .off(".DT")
        .on("keyup.DT", function(e) {
          api.search(this.value).draw();
        });
    },
    dom:
      "<'row'<'col-sm-3'l><'col-sm-6 text-center'B><'col-sm-3'f>>" +
      "<'row'<'col-sm-12'tr>>" +
      "<'row'<'col-sm-5'i><'col-sm-7'p>>",
    buttons: [
      {
        extend: "copy",
        exportOptions: { columns: [0, 1] }
      },
      {
        extend: "print",
        exportOptions: { columns: [0, 1] }
      },
      {
        extend: "excel",
        exportOptions: { columns: [0, 1] }
      },
      {
        extend: "pdf",
        exportOptions: { columns: [0, 1] }
      }
    ],
    oLanguage: {
      sProcessing: "loading..."
    },
    processing: true,
    serverSide: true,
    ajax: {
      url: base_url + "materi/jenjang_data",
      type: "POST"
    },
    columns: [
      {
        data: "id",
        orderable: false,
        searchable: false
      },
      { data: "nama" },
      { data: "nama_program" },
      {
        orderable: false,
        searchable: false
      }
    ],
    columnDefs: [
      {
        targets: 3,
        "data": {
                    "id": "id",
                    "status": "status"
                },
        render: function(data, type, row, meta) {
          var btn;
          if (data.status == 0 ) {
              btn = `<span class="btn btn-xs btn-danger">
                          <i class="fa fa-lock"></i> Materi terkunci
                      </span>`;
          } else {
            btn = `<a class="btn btn-xs bg-maroon" href="${base_url}materi/jenjang_detail/${data.id}" >
                            <i class="fa fa-search"></i> Detail Materi
                        </a>`;
          }
          return `<div class="text-center">
                  ${btn}
                </div>`;
        }
      }
    ],
    order: [[1, "asc"]],
    rowId: function(a) {
      return a;
    },
    rowCallback: function(row, data, iDisplayIndex) {
      var info = this.fnPagingInfo();
      var page = info.iPage;
      var length = info.iLength;
      var index = page * length + (iDisplayIndex + 1);
      $("td:eq(0)", row).html(index);
    }
  });
});

table
  .buttons()
  .container()
  .appendTo("#materi_wrapper .col-md-6:eq(0)");
