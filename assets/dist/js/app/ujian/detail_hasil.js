var table;

$(document).ready(function () {

    ajaxcsrf();

    table = $("#detail_hasil").DataTable({
        initComplete: function () {
            var api = this.api();
            $('#detail_hasil_filter input')
                .off('.DT')
                .on('keyup.DT', function (e) {
                    api.search(this.value).draw();
                });
        },
        oLanguage: {
            sProcessing: "loading..."
        },
        processing: true,
        serverSide: true,
        ajax: {
            "url": base_url + "hasilujian/NilaiMhs/"+id,
            "type": "POST",
        },
        columns: [
            {
                "data": "id",
                "searchable": false
            },
            { 
                "data": 'username', 
                "searchable": false
            },
            { 
                "data": 'jml_benar', 
                "searchable": false
             },
            {
                 "data": 'jumlah_salah', 
                 "searchable": false
             },
             {
                 "data": 'jumlah_kosong', 
                 "searchable": false
             },
            { 
                "data": 'nilai',
                "searchable": false
            },
        ],
        columnDefs: [
            {
              targets: 6,
              "data": {
                "dasar_kriteria": "dasar_kriteria",
                "min": "min",
                "jml_soal": "jml_soal",
                "nilai": "nilai",
                "jml_benar": "jml_benar"
            },
              render: function(data, type, row, meta) {
                var btn;
                if (data.dasar_kriteria=='nilai' ) {
                    if (parseInt(data.nilai) >= parseInt(data.min)) {
                        btn = `<strong class="badge bg-blue">Lulus</strong>`;
                    }else{
                        btn = `<strong class="badge bg-red">Nilai Mati</strong>`;
                    }
                } else {
                    if (parseInt(data.jml_benar) >= parseInt(data.jml_soal)) {
                        btn = `<strong class="badge bg-blue">Lulus</strong>`;
                    }else{
                        btn = `<strong class="badge bg-red">Nilai Mati</strong>`;
                    }
                }
                return `<div class="text-center">
                        ${btn}
                      </div>`;
              }
            }
          ],
        order: [
            [4, 'desc']
        ],
        rowId: function (a) {
            return a;
        },
        rowCallback: function (row, data, iDisplayIndex) {
            var info = this.fnPagingInfo();
            var page = info.iPage;
            var length = info.iLength;
            var index = page * length + (iDisplayIndex + 1);
            $('td:eq(0)', row).html(index);
        }
    });
});