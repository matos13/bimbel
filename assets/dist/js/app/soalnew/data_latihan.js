var save_label;
var table;

$(document).ready(function() {
  ajaxcsrf();

  table = $("#SoalLatihanNew").DataTable({
    initComplete: function() {
      var api = this.api();
      $("#paket_filter input")
        .off(".DT")
        .on("keyup.DT", function(e) {
          api.search(this.value).draw();
        });
    },
    dom:
      "<'row'<'col-sm-3'l><'col-sm-6 text-center'B><'col-sm-3'f>>" +
      "<'row'<'col-sm-12'tr>>" +
      "<'row'<'col-sm-5'i><'col-sm-7'p>>",
    buttons: [
      {
        extend: "copy",
        exportOptions: { columns: [0,1, 2,3] }
      },
      {
        extend: "print",
        exportOptions: { columns: [0,1, 2,3] }
      },
      {
        extend: "excel",
        exportOptions: { columns: [0,1, 2,3] }
      },
      {
        extend: "pdf",
        exportOptions: { columns: [0,1, 2,3] }
      }
    ],
   
    oLanguage: {
      sProcessing: "loading..."
    },
    processing: true,
    serverSide: true,
    ajax: {
      url: base_url + "SoalLatihanNew/data_awal",
      type: "POST"
      //data: csrf
    },
    columns: [
      {
        data: "id_bab",
        orderable: false,
        searchable: false
      },
      { data: "nama_dosen" },
      { data: "nama" },
      { data: "nama_bab" },
      { data: "jumlah" },
    ],
    columnDefs: [
    //   {
    //     targets: 6,
    //     data: "id_bab",
    //     render: function(data, type, row, meta) {
    //       return `<div class="text-center">
	// 								<input name="checked[]" class="check" value="${data}" type="checkbox">
	// 							</div>`;
    //     }
    //   },
      {
        targets: 5,
        data: "id_bab",
        render: function(data, type, row, meta) {
          return `<div class="text-center">
                                <a href="${base_url}SoalLatihanNew/detail/${data}" class="btn btn-xs btn-default">
                                    <i class="fa fa-eye"></i> Detail
                                </a>
                            </div>`;
        }
      }
    ],
    order: [[1, "asc"]],
    rowId: function(a) {
      return a;
    },
    rowCallback: function(row, data, iDisplayIndex) {
      var info = this.fnPagingInfo();
      var page = info.iPage;
      var length = info.iLength;
      var index = page * length + (iDisplayIndex + 1);
      $("td:eq(0)", row).html(index);
    }
  });

  table
    .buttons()
    .container()
    .appendTo("#paket_wrapper .col-md-6:eq(0)");

  $("#myModal").on("shown.modal.bs", function() {
    $(':input[name="banyak"]').select();
  });

  $("#select_all").on("click", function() {
    if (this.checked) {
      $(".check").each(function() {
        this.checked = true;
      });
    } else {
      $(".check").each(function() {
        this.checked = false;
      });
    }
  });

  $("#paket tbody").on("click", "tr .check", function() {
    var check = $("#paket tbody tr .check").length;
    var checked = $("#paket tbody tr .check:checked").length;
    if (check === checked) {
      $("#select_all").prop("checked", true);
    } else {
      $("#select_all").prop("checked", false);
    }
  });

  $("#bulk").on("submit", function(e) {
    if ($(this).attr("action") == base_url + "paket/delete") {
      e.preventDefault();
      e.stopImmediatePropagation();

      $.ajax({
        url: $(this).attr("action"),
        data: $(this).serialize(),
        type: "POST",
        success: function(respon) {
          if (respon.status) {
            Swal({
              title: "Berhasil",
              text: respon.total + " data berhasil dihapus",
              type: "success"
            });
          } else {
            Swal({
              title: "Gagal",
              text: "Tidak ada data yang dipilih",
              type: "error"
            });
          }
          reload_ajax();
        },
        error: function() {
          Swal({
            title: "Gagal",
            text: "Ada data yang sedang digunakan",
            type: "error"
          });
        }
      });
    }
  });
});

function load_jurusan() {
  var jurusan = $('select[name="nama_jurusan"]');
  jurusan.children("option:not(:first)").remove();

  ajaxcsrf(); // get csrf token
  $.ajax({
    url: base_url + "jurusan/load_jurusan",
    type: "GET",
    success: function(data) {
      //console.log(data);
      if (data.length) {
        var dataJurusan;
        $.each(data, function(key, val) {
          dataJurusan = `<option value="${val.id_jurusan}">${val.nama_jurusan}</option>`;
          jurusan.append(dataJurusan);
        });
      }
    }
  });
}

function bulk_delete() {
  if ($("#paket tbody tr .check:checked").length == 0) {
    Swal({
      title: "Gagal",
      text: "Tidak ada data yang dipilih",
      type: "error"
    });
  } else {
    $("#bulk").attr("action", base_url + "paket/delete");
    Swal({
      title: "Anda yakin?",
      text: "Data akan dihapus!",
      type: "warning",
      showCancelButton: true,
      confirmButtonColor: "#3085d6",
      cancelButtonColor: "#d33",
      confirmButtonText: "Hapus!"
    }).then(result => {
      if (result.value) {
        $("#bulk").submit();
      }
    });
  }
}

function bulk_edit() {
  if ($("#paket tbody tr .check:checked").length == 0) {
    Swal({
      title: "Gagal",
      text: "Tidak ada data yang dipilih",
      type: "error"
    });
  } else {
    $("#bulk").attr("action", base_url + "paket/edit");
    $("#bulk").submit();
  }
}
