-- phpMyAdmin SQL Dump
-- version 4.9.4
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Jul 01, 2020 at 06:46 AM
-- Server version: 5.7.30-log
-- PHP Version: 7.3.6

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `bersama4_belajar`
--

-- --------------------------------------------------------

--
-- Table structure for table `bab`
--

CREATE TABLE `bab` (
  `id` int(10) NOT NULL,
  `id_mapel` int(10) NOT NULL,
  `nama_bab` varchar(225) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `bab`
--

INSERT INTO `bab` (`id`, `id_mapel`, `nama_bab`) VALUES
(1, 1, 'Bab A'),
(2, 1, 'Bab 1 Verbal'),
(3, 1, 'Bab 1'),
(6, 24, 'BAB II'),
(7, 26, 'BAB I'),
(8, 26, 'BAB II'),
(22, 18, 'BAB 1 VERBAL'),
(24, 23, 'CHAPTER 1 PART OF SPEECH'),
(25, 18, 'BAB 2 BACAAN'),
(26, 18, 'BAB 3 POLA BILANGAN'),
(27, 18, 'BAB 4 BILANGAN'),
(28, 18, 'BAB 5 POLA GAMBAR'),
(29, 18, 'BAB 6 ALJABAR'),
(30, 18, 'BAB 7 EKSPONEN'),
(31, 18, 'BAB 8 ARITMETIKA SOSIAL DAN PERSENTASE'),
(32, 18, 'BAB 9 LOGICAL REASONING'),
(33, 18, 'BAB 10 ANALYTICAL REASONING'),
(34, 18, 'BAB 11 PELUANG'),
(35, 18, 'BAB 12 PERBANDINGAN'),
(36, 18, 'BAB 13 KECEPATAN'),
(37, 18, 'BAB 14 HIMPUNAN'),
(38, 18, 'BAB 15 STATISTIKA'),
(39, 18, 'BAB 16 BANGUN DATAR'),
(40, 18, 'BAB 17 BANGUN RUANG'),
(41, 23, 'CHAPTER 2 COMPLETING PARAGRAPH AND READING'),
(42, 23, 'CHAPTER 3 TENSES I'),
(43, 23, 'CHAPTER 4 TENSES II'),
(44, 23, 'CHAPTER 5 ARTICLE, CONJUNCTION, AND PREPOSITION'),
(45, 23, 'CHAPTER 6 DERIVATIVE, ADJ ORDER, HYPHENATED, PRONOUN, APPOSITIVE '),
(46, 23, 'CHAPTER 7 CONCORDAGREEMENT AND PARALLEL STRUCTURE '),
(47, 23, 'CHAPTER 8 RELATIVE PRONOUNS, CLAUSES'),
(48, 23, 'CHAPTER 9 EXPRESSING PREFERENCE AND MODAL '),
(49, 23, 'CHAPTER 10 PHRASES, PARTICIPLES '),
(50, 23, 'CHAPTER 11 TO INFINITIVE AND GERUND '),
(51, 23, 'CHAPTER 12 PASSIVE VOICE AND CAUSATIVE'),
(52, 23, 'CHAPTER 13 REPORTED SPEECH AND INVERSION '),
(53, 23, 'CHAPTER 14 SUBJUNCTIVE DAN CONDITIONAL SENTENCE '),
(54, 23, 'CHAPTER 15 DEGREES OF COMPARISON, QUESTION TAG, AND ELLIPTICAL STRUCTURE'),
(55, 20, 'BAB 1 - PANCASILA '),
(56, 20, 'BAB 2 - UUD 1945 '),
(57, 20, 'BAB 3 - BHINNEKA TUNGGAL IKA'),
(58, 20, 'BAB 4 - TATA NEGARA & KONSTITUSI '),
(59, 20, 'BAB 5 - GEO POLITIK, HUBUNGAN INTERNASIONAL, DAN HAM DI INDONESIA '),
(60, 20, 'BAB 6 - SEJARAH NKRI : ZAMAN KERAJAAN HINDU BUDHA '),
(61, 20, 'BAB 7 - SEJARAH NKRI : ZAMAN KERAJAAN ISLAM '),
(62, 20, 'BAB 8 - SEJARAH NKRI : PERJUANGAN MELAWAN KOLONIALISME & IMPERIALISME '),
(63, 20, 'SOAL BAB 9 - SEJARAH NKRI : PERJUANGAN MELAWAN PENJAJAH PADA MASA PERGERAKAN NASIONAL (PERIODE 1908-1942) '),
(64, 20, 'BAB 10 - SEJARAH NKRI : PERISTIWA-PERISTIWA MENJELANG DAN PASCA PROKLAMAS'),
(65, 20, 'BAB 11 - SEJARAH NKRI : PERJUANGAN MEMPERTAHANKAN KEMERDEKAAN (PERIODE 1945-1949) '),
(66, 20, 'BAB 12 - SEJARAH NKRI : PEMERINTAHAN MASA AWAL KEMERDEKAAN, ORDE LAMA, ORDE BARU & REFORMASI'),
(67, 20, 'BAB 13 - BAHASA INDONESIA : FONEM, HURUF, & MORFOLOGI'),
(68, 20, 'BAB 14 - BAHASA INDONESIA : KALIMAT & PEMAKAIAN TANDA BACA'),
(69, 21, 'BAB 1 - PELAYANAN PUBLIK '),
(70, 21, 'BAB 2 - JEJARING KERJA'),
(71, 21, 'BAB 3 - SOSIAL BUDAYA '),
(72, 21, 'BAB 4 - TEKNOLOGI INFORMASI DAN KOMUNIKASI '),
(73, 21, 'BAB 5 - PROFESIONALISME '),
(74, 20, 'BAB 9 SEJARAH NNKRI: PERJUANGAN MELAWAN PENJAJAH PADA MASA PERGERAKAN NASIONAL (PERIODE 1908-1942)');

-- --------------------------------------------------------

--
-- Table structure for table `dosen`
--

CREATE TABLE `dosen` (
  `id_dosen` int(11) NOT NULL,
  `nip` char(12) NOT NULL,
  `nama_dosen` varchar(50) NOT NULL,
  `email` varchar(254) NOT NULL,
  `matkul_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `dosen`
--

INSERT INTO `dosen` (`id_dosen`, `nip`, `nama_dosen`, `email`, `matkul_id`) VALUES
(4, '1411764101', 'D3 PKN STAN', 'd3pknstan@bersamabelajar.com', 5),
(5, '1411764104', 'DIV PKN STAN', 'divpknstan@bersamabelajar.com', 6),
(6, '1411764102', 'CPNS', 'cpns@bersamabelajar.com', 7),
(8, '0000111111', 'Pungki', 'pungkimuh@gmail.com', 6),
(14, '00000003', 'Yuli', 'yuli@gmail.com', 21),
(15, '00000005', 'ika', 'ika@gmail.com', 16),
(20, '10000001', 'Muh Pungki Nur Setiawan (1)', 'pungkimuh1@gmail.com', 18),
(21, '10000002', 'Muh Pungki Nur Setiawan (2)', 'pungkimuh2@gmail.com', 23),
(22, '10000005', 'Mpns sb', 'muhpungkinur@gmail.com', 23),
(23, '10000003', 'Muh Pungki Nur Setiawan (3)', 'Pungkimuh3@gmail.com', 20),
(24, '10000004', 'Muh Pungki Nur Setiawan (4)', 'pungkimuh4@gmail.com', 21);

--
-- Triggers `dosen`
--
DELIMITER $$
CREATE TRIGGER `edit_user_dosen` BEFORE UPDATE ON `dosen` FOR EACH ROW UPDATE `users` SET `email` = NEW.email, `username` = NEW.nip WHERE `users`.`username` = OLD.nip
$$
DELIMITER ;
DELIMITER $$
CREATE TRIGGER `hapus_user_dosen` BEFORE DELETE ON `dosen` FOR EACH ROW DELETE FROM `users` WHERE `users`.`username` = OLD.nip
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `groups`
--

CREATE TABLE `groups` (
  `id` mediumint(8) UNSIGNED NOT NULL,
  `name` varchar(20) NOT NULL,
  `description` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `groups`
--

INSERT INTO `groups` (`id`, `name`, `description`) VALUES
(1, 'admin', 'Administrator'),
(2, 'dosen', 'Pembuat Soal dan ujian'),
(3, 'mahasiswa', 'Peserta Ujian');

-- --------------------------------------------------------

--
-- Table structure for table `h_kuis`
--

CREATE TABLE `h_kuis` (
  `id` int(11) NOT NULL,
  `kuis_id` int(11) NOT NULL,
  `mahasiswa_id` int(11) NOT NULL,
  `list_soal` longtext NOT NULL,
  `list_jawaban` longtext NOT NULL,
  `jml_benar` int(11) NOT NULL,
  `nilai` decimal(10,2) NOT NULL,
  `nilai_bobot` decimal(10,2) NOT NULL,
  `tgl_mulai` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE CURRENT_TIMESTAMP,
  `tgl_selesai` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `status` enum('Y','N') NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `h_kuis`
--

INSERT INTO `h_kuis` (`id`, `kuis_id`, `mahasiswa_id`, `list_soal`, `list_jawaban`, `jml_benar`, `nilai`, `nilai_bobot`, `tgl_mulai`, `tgl_selesai`, `status`) VALUES
(5, 6, 1, '5,6,7', '5:B:N,6:C:N,7:B:N', 3, 100.00, 100.00, '2019-12-29 09:03:35', '2019-12-29 09:13:20', 'N');

-- --------------------------------------------------------

--
-- Table structure for table `h_latihan`
--

CREATE TABLE `h_latihan` (
  `id` int(11) NOT NULL,
  `latihan_id` int(11) NOT NULL,
  `mahasiswa_id` int(11) NOT NULL,
  `list_soal` longtext NOT NULL,
  `list_jawaban` longtext NOT NULL,
  `jml_benar` int(11) NOT NULL,
  `nilai` decimal(10,2) NOT NULL,
  `nilai_bobot` decimal(10,2) NOT NULL,
  `tgl_mulai` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `tgl_selesai` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `status` enum('Y','N') NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `h_latihan`
--

INSERT INTO `h_latihan` (`id`, `latihan_id`, `mahasiswa_id`, `list_soal`, `list_jawaban`, `jml_benar`, `nilai`, `nilai_bobot`, `tgl_mulai`, `tgl_selesai`, `status`) VALUES
(3, 2, 1, '9,11,8', '8:B:N,11:B:N,9:D:N', 3, 100.00, 100.00, '2019-12-31 23:43:35', '2019-12-31 23:53:22', 'N'),
(4, 3, 5, '23,26,22', '23:E:N,25:D:N,22:D:N', 0, 0.00, 100.00, '2020-03-25 13:32:23', '2020-03-25 13:41:29', 'N'),
(5, 4, 5, '24,25,26', '24:B:N,25:D:N,26:C:N', 0, 0.00, 100.00, '2020-03-26 05:38:18', '2020-03-26 05:48:12', 'N'),
(6, 5, 5, '21,22,23', '21:C:N,22:A:N,23:C:N', 1, 33.00, 100.00, '2020-03-28 01:20:50', '2020-03-28 01:29:44', 'N');

-- --------------------------------------------------------

--
-- Table structure for table `h_ujian`
--

CREATE TABLE `h_ujian` (
  `id` int(11) NOT NULL,
  `ujian_id` int(11) NOT NULL,
  `mahasiswa_id` int(11) NOT NULL,
  `list_soal` longtext NOT NULL,
  `list_jawaban` longtext NOT NULL,
  `jml_benar` int(11) NOT NULL,
  `nilai` decimal(10,2) NOT NULL,
  `nilai_bobot` decimal(10,2) NOT NULL,
  `tgl_mulai` datetime NOT NULL,
  `tgl_selesai` datetime NOT NULL,
  `status` enum('Y','N') NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `h_ujian`
--

INSERT INTO `h_ujian` (`id`, `ujian_id`, `mahasiswa_id`, `list_soal`, `list_jawaban`, `jml_benar`, `nilai`, `nilai_bobot`, `tgl_mulai`, `tgl_selesai`, `status`) VALUES
(1, 1, 1, '3,6', '2:D:N,3:E:N', 0, 0.00, 0.00, '2019-12-07 22:08:09', '2019-12-07 22:18:09', 'Y'),
(2, 1, 1, '3,6', '3::N,6::N', 0, 0.00, 0.00, '2019-12-07 22:08:09', '2019-12-07 22:18:09', 'Y'),
(3, 2, 1, '4,2', '4:A:N,3:C:N', 1, 50.00, 100.00, '2019-12-28 10:58:55', '2019-12-28 11:08:55', 'N'),
(4, 3, 18, '10,11', '10::N,11::N', 0, 0.00, 0.00, '2020-04-13 01:01:13', '2020-04-13 01:21:13', 'Y'),
(6, 5, 20, '15,16', '15::N,16::N', 0, 0.00, 0.00, '2020-05-24 03:57:53', '2020-05-24 03:58:53', 'Y'),
(7, 6, 21, '17,18,19,20,21', '17:A:N,18:A:N,19:C:N,20:E:N,21:D:N', 2, 40.00, 100.00, '2020-05-10 10:36:46', '2020-05-10 10:37:46', 'N'),
(8, 5, 20, '15,16', '15:B:N,16:D:N', 0, 0.00, 200.00, '2020-05-24 03:57:53', '2020-05-24 03:58:53', 'N'),
(9, 7, 21, '24,25,26,27,28', '24:A:N,25:A:N,26:A:N,27:A:N,28:A:N', 2, 40.00, 100.00, '2020-05-31 02:59:56', '2020-05-31 03:04:56', 'N'),
(10, 10, 23, '36,37,38,39,40', '36::N,37::N,38::N,39::N,40::N', 0, 0.00, 0.00, '2020-06-10 11:28:43', '2020-06-10 11:33:43', 'Y'),
(11, 10, 23, '36,37,38,39,40', '36::N,37::N,38::N,39::N,40::N', 0, 0.00, 0.00, '2020-06-10 11:28:43', '2020-06-10 11:33:43', 'Y'),
(12, 10, 23, '36,37,38,39,40', '36:B:N,37:D:N,38:A:N,39:C:N,40:A:N', 0, 12.00, 240.00, '2020-06-10 11:28:43', '2020-06-10 11:33:43', 'N'),
(13, 13, 35, '536,564,591,526,579,522,524,571,557,513,531,556,558,509,551,521,565,544,555,503,599,577,493,585,505,548,512,553,581,569,600,568,534,515,561,507,535,501,543,497,496,528,559,506,494,583,582,518,575,539,586,516,576,592,594', '536::N,564::N,591::N,526::N,579::N,522::N,524::N,571::N,557::N,513::N,531::N,556::N,558::N,509::N,551::N,521::N,565::N,544::N,555::N,503::N,599::N,577::N,493::N,585::N,505::N,548::N,512::N,553::N,581::N,569::N,600::N,568::N,534::N,515::N,561::N,507::N,535::N,501::N,543::N,497::N,496::N,528::N,559::N,506::N,494::N,583::N,582::N,518::N,575::N,539::N,586::N,516::N,576::N,592::N,594::N', 0, 0.00, 0.00, '2020-06-16 23:28:49', '2020-06-17 00:08:49', 'Y'),
(14, 13, 35, '536,564,591,526,579,522,524,571,557,513,531,556,558,509,551,521,565,544,555,503,599,577,493,585,505,548,512,553,581,569,600,568,534,515,561,507,535,501,543,497,496,528,559,506,494,583,582,518,575,539,586,516,576,592,594', '536::N,564::N,591::N,526::N,579::N,522::N,524::N,571::N,557::N,513::N,531::N,556::N,558::N,509::N,551::N,521::N,565::N,544::N,555::N,503::N,599::N,577::N,493::N,585::N,505::N,548::N,512::N,553::N,581::N,569::N,600::N,568::N,534::N,515::N,561::N,507::N,535::N,501::N,543::N,497::N,496::N,528::N,559::N,506::N,494::N,583::N,582::N,518::N,575::N,539::N,586::N,516::N,576::N,592::N,594::N', 0, 0.00, 0.00, '2020-06-16 23:28:49', '2020-06-17 00:08:49', 'Y'),
(15, 14, 28, '142,42,114,122,151,84,111,121,102,88,93,127,58,46,163,67,115,98,89,138,118,72,49,55,135,80,158,107,99,70,47,170,53,129,95,57,52,61,94,77,155,132,82,146,145,78,85,71,81,54,149,119,106,161,92,66,51,74,150,87,160,120,68,96,168', '150:E:N,163:B:N,99:A:N,108:C:N,158:B:N,121:D:N,122:B:Y,58:B:N,69:A:N,70:A:N,47:B:N,120:C:N,166:A:N,143:D:Y,157:B:N,126:B:N,53:D:N,110:D:N,119:C:N,44:A:N,162:C:N,57:B:N,82:E:N,97:E:N,71:D:N,123:B:N,148:C:N,96:B:N,156:E:Y,86:A:N,78:D:N,102:C:Y,85:E:Y,94:E:N,138:D:N,114:C:N,76:A:N,129:A:N,139:E:N,87:A:N,109:A:N,67:D:N,137:B:N,84:D:N,98:A:Y,92:D:N,65:A:N,104:A:N,100:D:N,77:D:N,54:C:N,136:E:N,80:C:N,48:C:N,45:E:N,43:A:N,73:B:N,74:D:N,146:E:N,134:A:N,115:A:N,118:D:N,140:A:N,113:D:N,55:D:N', 49, 180.00, 276.00, '2020-06-16 22:11:07', '2020-06-16 22:46:07', 'N'),
(16, 13, 35, '536,564,591,526,579,522,524,571,557,513,531,556,558,509,551,521,565,544,555,503,599,577,493,585,505,548,512,553,581,569,600,568,534,515,561,507,535,501,543,497,496,528,559,506,494,583,582,518,575,539,586,516,576,592,594', '533:B:N,562:D:N,501:C:N,576::N,496:A:N,593:A:N,530:A:N,594:B:N,506:D:N,499:B:N,502:A:N,571:A:N,602::N,595::N,564::N,550::N,504::N,548::N,587:A:N,510:C:N,565:C:N,573:D:N,601:A:N,494:D:N,497::N,551:D:N,546:C:N,591:A:N,544:D:N,560:C:N,598:A:N,583:C:N,529:B:N,582:C:N,508:A:N,600:A:N,572:B:N,513:B:N,505:C:N,495:D:N,553:C:N,542:D:N,517:B:N,580::N,589:D:N,586:C:N,556:D:N,599:A:N,522:C:N,575:D:N,555:C:N,524::N,543::N,584::N,590::N', 13, 23.00, 41.00, '2020-06-16 23:28:49', '2020-06-17 00:08:49', 'N'),
(17, 15, 28, '202,227,181,178,207,240,222,258,208,239,241,244,198,199,212,249,197,184,201,223,261,253,229,215,230,182,252,246,210,200,257,209,245,254,242,179,193,217,263,189,204,183,265,262,221,216,243,192,267,188', '181::N,186::N,185::N,242::N,193::N,254::N,198::N,245::N,196::N,229::N,258::N,255::N,211::N,243::N,246::N,252::N,216::N,212::N,240::N,221::N,217::N,191::N,260::N,204::N,199::N,206::N,215::N,230::N,253::N,207::N,224::N,190::N,200::N,235::N,192::N,256::N,265::N,227::N,205::N,264::N,232::N,239::N,210::N,231::N,259::N,226::N,219::N,236::N,195::N,220::N', 0, 0.00, 0.00, '2020-06-17 15:35:03', '2020-06-17 16:20:03', 'N'),
(18, 11, 28, '318,319,320,321,322,323,324,325,326,327,328,329,330,331,332,333,334,335,336,337,338,339,340,341,342,343,344,345,346,347,348,349,350,351,352,353,354,355,356,357,358,359,360,361,362,363,364,365,366,367,368,369,370,371,372,373,374,375,376,377', '318::N,319::N,320::N,321::N,322::N,323::N,324::N,325::N,326::N,327::N,328::N,329::N,330::N,331::N,332::N,333::N,334::N,335::N,336::N,337::N,338::N,339::N,340::N,341::N,342::N,343::N,344::N,345::N,346::N,347::N,348::N,349::N,350::N,351::N,352::N,353::N,354::N,355::N,356::N,357::N,358::N,359::N,360::N,361::N,362::N,363::N,364::N,365::N,366::N,367::N,368::N,369::N,370::N,371::N,372::N,373::N,374::N,375::N,376::N,377::N', 0, -2.00, -4.00, '2020-06-20 02:06:56', '2020-06-20 02:51:56', 'N'),
(19, 12, 28, '404,407,409,411,412,413,414,431,432,433,434,435,436,437,438,439,440,441,442,443,444,445,446,447,448,449,450,451,452,453,454,455,456,457,458,459,460,461,462,463,464,465,466,467,468,469,470,471,472,473,474,475,476,477,478,479,480,481,482,483,484,485,486,487,488', '404:A:N,407:D:N,409:B:N,411:A:N,412:C:N,413:B:N,414:C:N,431:A:N,432:C:N,433:C:N,434:C:N,435:B:N,436:C:N,437:B:N,438:D:N,439:B:N,440:A:N,441:A:N,442:A:N,443:C:N,444:A:N,445:B:N,446:A:N,447:B:N,448:C:N,449:C:N,450:D:N,451:C:N,452:D:N,453:B:N,454:D:N,455:B:N,456::N,457::N,458::N,459::N,460::N,461::N,462::N,463::N,464:B:N,465:A:N,466:A:N,467:C:N,468:A:N,469::N,470:E:N,471:B:N,472:C:N,473:B:N,474:B:N,475:B:N,476:E:N,477:C:N,478::N,479:B:N,480:A:N,481::N,482:D:N,483:D:N,484:D:N,485:D:N,486:A:N,487:C:N,488:C:N', 37, 131.00, 201.00, '2020-06-20 02:10:32', '2020-06-20 03:00:32', 'N'),
(20, 0, 26, '0', '0', 0, 0.00, 0.00, '2020-06-28 14:28:26', '1970-01-01 07:00:00', 'Y');

-- --------------------------------------------------------

--
-- Table structure for table `jenis_soal`
--

CREATE TABLE `jenis_soal` (
  `id` int(10) UNSIGNED NOT NULL,
  `jenis_soal` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `bobot_benar` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `bobot_salah` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `bobot_kosong` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `waktu_pengerjaan` double(8,2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `jenis_soal`
--

INSERT INTO `jenis_soal` (`id`, `jenis_soal`, `bobot_benar`, `bobot_salah`, `bobot_kosong`, `waktu_pengerjaan`) VALUES
(4, 'TRYOUT', '1', '0', '0', 90.00),
(5, 'KUIS', '1', '0', '0', 90.00),
(6, 'LATIHAN', '1', '1', '0', 90.00);

-- --------------------------------------------------------

--
-- Table structure for table `jurusan`
--

CREATE TABLE `jurusan` (
  `id_jurusan` int(11) NOT NULL,
  `nama_jurusan` varchar(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `jurusan`
--

INSERT INTO `jurusan` (`id_jurusan`, `nama_jurusan`) VALUES
(12, 'TES KEMAMPUAN AKADEMIS (TPA)'),
(13, 'TES BAHASA INGGRIS (TBI)'),
(14, 'TES SUBSTANSI AKADEMIK (TSA)'),
(15, 'TES WAWASAN KEBANGSAAN (TWK)'),
(16, 'TES KARAKTERISTIK PRIBADI (TKP');

-- --------------------------------------------------------

--
-- Table structure for table `jurusan_matkul`
--

CREATE TABLE `jurusan_matkul` (
  `id` int(11) NOT NULL,
  `matkul_id` int(11) NOT NULL,
  `jurusan_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `kabupaten`
--

CREATE TABLE `kabupaten` (
  `id` int(11) NOT NULL,
  `nama` varchar(100) NOT NULL,
  `id_provinsi` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `kabupaten`
--

INSERT INTO `kabupaten` (`id`, `nama`, `id_provinsi`) VALUES
(74, 'KAB. ACEH BARAT', 3),
(75, 'KAB. ACEH BARAT DAYA', 3),
(76, 'KAB. ACEH BESAR', 3),
(77, 'KAB. ACEH JAYA', 3),
(78, 'KAB. ACEH SELATAN', 3),
(79, 'KAB. ACEH SINGKIL', 3),
(80, 'KAB. ACEH TAMIANG', 3),
(81, 'KAB. ACEH TENGAH', 3),
(82, 'KAB. ACEH TENGGARA', 3),
(83, 'KAB. ACEH TIMUR', 3),
(84, 'KAB. ACEH UTARA', 3),
(85, 'KAB. BENER MERIAH', 3),
(86, 'KAB. BIREUEN', 3),
(87, 'KAB. GAYO LUES', 3),
(88, 'KAB. NAGAN RAYA', 3),
(89, 'KAB. PIDIE', 3),
(90, 'KAB. PIDIE JAYA', 3),
(91, 'KAB. SIMEULUE', 3),
(92, 'KOTA BANDA ACEH', 3),
(93, 'KOTA LANGSA', 3),
(94, 'KOTA LHOKSEUMAWE', 3),
(95, 'KOTA SABANG', 3),
(96, 'KOTA SUBULUSSALAM', 3),
(97, 'KAB. BADUNG', 4),
(98, 'KAB. BANGLI', 4),
(99, 'KAB. BULELENG', 4),
(100, 'KAB. GIANYAR', 4),
(101, 'KAB. JEMBRANA', 4),
(102, 'KAB. KARANGASEM', 4),
(103, 'KAB. KLUNGKUNG', 4),
(104, 'KAB. TABANAN', 4),
(105, 'KOTA DENPASAR', 4),
(106, 'KAB. LEBAK', 5),
(107, 'KAB. PANDEGLANG', 5),
(108, 'KAB. SERANG', 5),
(109, 'KAB. TANGERANG', 5),
(110, 'KOTA CILEGON', 5),
(111, 'KOTA SERANG', 5),
(112, 'KOTA TANGERANG', 5),
(113, 'KOTA TANGERANG SELATAN', 5),
(114, 'KAB. BENGKULU SELATAN', 6),
(115, 'KAB. BENGKULU TENGAH', 6),
(116, 'KAB. BENGKULU UTARA', 6),
(117, 'KAB. KAUR', 6),
(118, 'KAB. KEPAHIANG', 6),
(119, 'KAB. LEBONG', 6),
(120, 'KAB. MUKO MUKO', 6),
(121, 'KAB. REJANG LEBONG', 6),
(122, 'KAB. SELUMA', 6),
(123, 'KOTA BENGKULU', 6),
(124, 'KAB. BANTUL', 7),
(125, 'KAB. GUNUNGKIDUL', 7),
(126, 'KAB. KULON PROGO', 7),
(127, 'KAB. SLEMAN', 7),
(128, 'KOTA YOGYAKARTA', 7),
(129, 'KAB. ADM. KEP. SERIBU', 8),
(130, 'KOTA ADM. JAKARTA BARAT', 8),
(131, 'KOTA ADM. JAKARTA PUSAT', 8),
(132, 'KOTA ADM. JAKARTA SELATAN', 8),
(133, 'KOTA ADM. JAKARTA TIMUR', 8),
(134, 'KOTA ADM. JAKARTA UTARA', 8),
(135, 'KAB. BOALEMO', 9),
(136, 'KAB. BONE BOLANGO', 9),
(137, 'KAB. GORONTALO', 9),
(138, 'KAB. GORONTALO UTARA', 9),
(139, 'KAB. PAHUWATO', 9),
(140, 'KOTA GORONTALO', 9),
(141, 'KAB. BATANGHARI', 10),
(142, 'KAB. BUNGO', 10),
(143, 'KAB. SAROLANGUN', 10),
(144, 'KAB. TANJUNG JABUNG BARAT', 10),
(145, 'KAB. TANJUNG JABUNG TIMUR', 10),
(146, 'KAB. TEBO', 10),
(147, 'KAB.KERINCI', 10),
(148, 'KAB.MERANGIN', 10),
(149, 'KAB.MUARO JAMBI', 10),
(150, 'KOTA JAMBI', 10),
(151, 'KOTA SUNGAI PENUH', 10),
(152, 'KAB. BANDUNG', 11),
(153, 'KAB. BANDUNG BARAT', 11),
(154, 'KAB. BEKASI', 11),
(155, 'KAB. BOGOR', 11),
(156, 'KAB. CIAMIS', 11),
(157, 'KAB. CIANJUR', 11),
(158, 'KAB. CIREBON', 11),
(159, 'KAB. GARUT', 11),
(160, 'KAB. INDRAMAYU', 11),
(161, 'KAB. KARAWANG', 11),
(162, 'KAB. KUNINGAN', 11),
(163, 'KAB. MAJALENGKA', 11),
(164, 'KAB. PANGANDARAN', 11),
(165, 'KAB. PURWAKARTA', 11),
(166, 'KAB. SUBANG', 11),
(167, 'KAB. SUKABUMI', 11),
(168, 'KAB. SUMEDANG', 11),
(169, 'KAB. TASIKMALAYA', 11),
(170, 'KOTA BANDUNG', 11),
(171, 'KOTA BANJAR', 11),
(172, 'KOTA BEKASI', 11),
(173, 'KOTA BOGOR', 11),
(174, 'KOTA CIMAHI', 11),
(175, 'KOTA CIREBON', 11),
(176, 'KOTA DEPOK', 11),
(177, 'KOTA SUKABUMI', 11),
(178, 'KOTA TASIKMALAYA', 11),
(179, 'KAB. BANJARNEGARA', 12),
(180, 'KAB. BANYUMAS', 12),
(181, 'KAB. BATANG', 12),
(182, 'KAB. BLORA', 12),
(183, 'KAB. BOYOLALI', 12),
(184, 'KAB. BREBES', 12),
(185, 'KAB. CILACAP', 12),
(186, 'KAB. DEMAK', 12),
(187, 'KAB. GROBOGAN', 12),
(188, 'KAB. JEPARA', 12),
(189, 'KAB. KARANGANYAR', 12),
(190, 'KAB. KEBUMEN', 12),
(191, 'KAB. KENDAL', 12),
(192, 'KAB. KLATEN', 12),
(193, 'KAB. KUDUS', 12),
(194, 'KAB. MAGELANG', 12),
(195, 'KAB. PATI', 12),
(196, 'KAB. PEKALONGAN', 12),
(197, 'KAB. PEMALANG', 12),
(198, 'KAB. PURBALINGGA', 12),
(199, 'KAB. PURWOREJO', 12),
(200, 'KAB. REMBANG', 12),
(201, 'KAB. SEMARANG', 12),
(202, 'KAB. SRAGEN', 12),
(203, 'KAB. SUKOHARJO', 12),
(204, 'KAB. TEGAL', 12),
(205, 'KAB. TEMANGGUNG', 12),
(206, 'KAB. WONOGIRI', 12),
(207, 'KAB. WONOSOBO', 12),
(208, 'KOTA MAGELANG', 12),
(209, 'KOTA PEKALONGAN', 12),
(210, 'KOTA SALATIGA', 12),
(211, 'KOTA SEMARANG', 12),
(212, 'KOTA SURAKARTA', 12),
(213, 'KOTA TEGAL', 12),
(214, 'KAB. BANGKALAN', 13),
(215, 'KAB. BANYUWANGI', 13),
(216, 'KAB. BLITAR', 13),
(217, 'KAB. BOJONEGORO', 13),
(218, 'KAB. BONDOWOSO', 13),
(219, 'KAB. GRESIK', 13),
(220, 'KAB. JEMBER', 13),
(221, 'KAB. JOMBANG', 13),
(222, 'KAB. KEDIRI', 13),
(223, 'KAB. LAMONGAN', 13),
(224, 'KAB. LUMAJANG', 13),
(225, 'KAB. MADIUN', 13),
(226, 'KAB. MAGETAN', 13),
(227, 'KAB. MALANG', 13),
(228, 'KAB. MOJOKERTO', 13),
(229, 'KAB. NGANJUK', 13),
(230, 'KAB. NGAWI', 13),
(231, 'KAB. PACITAN', 13),
(232, 'KAB. PAMEKASAN', 13),
(233, 'KAB. PASURUAN', 13),
(234, 'KAB. PONOROGO', 13),
(235, 'KAB. PROBOLINGGO', 13),
(236, 'KAB. SAMPANG', 13),
(237, 'KAB. SIDOARJO', 13),
(238, 'KAB. SITUBONDO', 13),
(239, 'KAB. SUMENEP', 13),
(240, 'KAB. TRENGGALEK', 13),
(241, 'KAB. TUBAN', 13),
(242, 'KAB. TULUNGAGUNG', 13),
(243, 'KOTA BATU', 13),
(244, 'KOTA BLITAR', 13),
(245, 'KOTA KEDIRI', 13),
(246, 'KOTA MADIUN', 13),
(247, 'KOTA MALANG', 13),
(248, 'KOTA MOJOKERTO', 13),
(249, 'KOTA PASURUAN', 13),
(250, 'KOTA PROBOLINGGO', 13),
(251, 'KOTA SURABAYA', 13),
(252, 'KAB. BENGKAYANG', 14),
(253, 'KAB. KAPUAS HULU', 14),
(254, 'KAB. KAYONG UTARA', 14),
(255, 'KAB. KETAPANG', 14),
(256, 'KAB. KUBU RAYA', 14),
(257, 'KAB. LANDAK', 14),
(258, 'KAB. MELAWI', 14),
(259, 'KAB. MEMPAWAH', 14),
(260, 'KAB. SAMBAS', 14),
(261, 'KAB. SANGGAU', 14),
(262, 'KAB. SEKADAU', 14),
(263, 'KAB. SINTANG', 14),
(264, 'KOTA PONTIANAK', 14),
(265, 'KOTA SINGKAWANG', 14),
(266, 'KAB. BALANGAN', 15),
(267, 'KAB. BANJAR', 15),
(268, 'KAB. BARITO KUALA', 15),
(269, 'KAB. HULU SUNGAI SELATAN', 15),
(270, 'KAB. HULU SUNGAI TENGAH', 15),
(271, 'KAB. HULU SUNGAI UTARA', 15),
(272, 'KAB. KOTABARU', 15),
(273, 'KAB. TABALONG', 15),
(274, 'KAB. TANAH BUMBU', 15),
(275, 'KAB. TANAH LAUT', 15),
(276, 'KAB. TAPIN', 15),
(277, 'KOTA BANJARBARU', 15),
(278, 'KOTA BANJARMASIN', 15),
(279, 'KAB. BARITO SELATAN', 16),
(280, 'KAB. BARITO TIMUR', 16),
(281, 'KAB. BARITO UTARA', 16),
(282, 'KAB. GUNUNG MAS', 16),
(283, 'KAB. KAPUAS', 16),
(284, 'KAB. KATINGAN', 16),
(285, 'KAB. KOTAWARINGIN BARAT', 16),
(286, 'KAB. KOTAWARINGIN TIMUR', 16),
(287, 'KAB. LAMANDAU', 16),
(288, 'KAB. MURUNG RAYA', 16),
(289, 'KAB. PULANG PISAU', 16),
(290, 'KAB. SERUYAN', 16),
(291, 'KAB. SUKAMARA', 16),
(292, 'KOTA PALANGKARAYA', 16),
(293, 'KAB. BERAU', 17),
(294, 'KAB. KUTAI BARAT', 17),
(295, 'KAB. KUTAI KARTANEGARA', 17),
(296, 'KAB. KUTAI TIMUR', 17),
(297, 'KAB. MAHAKAM ULU', 17),
(298, 'KAB. PASER', 17),
(299, 'KAB. PENAJAM PASER UTARA', 17),
(300, 'KOTA BALIKPAPAN', 17),
(301, 'KOTA BONTANG', 17),
(302, 'KOTA SAMARINDA', 17),
(303, 'KAB. BULUNGAN', 18),
(304, 'KAB. MALINAU', 18),
(305, 'KAB. NUNUKAN', 18),
(306, 'KAB. TANA TIDUNG', 18),
(307, 'KOTA TARAKAN', 18),
(308, 'KAB. BANGKA', 19),
(309, 'KAB. BANGKA BARAT', 19),
(310, 'KAB. BANGKA SELATAN', 19),
(311, 'KAB. BANGKA TENGAH', 19),
(312, 'KAB. BELITUNG', 19),
(313, 'KAB. BELITUNG TIMUR', 19),
(314, 'KOTA PANGKAL PINANG', 19),
(315, 'KAB. BINTAN', 20),
(316, 'KAB. KARIMUN', 20),
(317, 'KAB. KEPULAUAN ANAMBAS', 20),
(318, 'KAB. LINGGA', 20),
(319, 'KAB. NATUNA', 20),
(320, 'KOTA BATAM', 20),
(321, 'KOTA TANJUNG PINANG', 20),
(322, 'KAB. LAMPUNG BARAT', 21),
(323, 'KAB. LAMPUNG SELATAN', 21),
(324, 'KAB. LAMPUNG TENGAH', 21),
(325, 'KAB. LAMPUNG TIMUR', 21),
(326, 'KAB. LAMPUNG UTARA', 21),
(327, 'KAB. MESUJI', 21),
(328, 'KAB. PESAWARAN', 21),
(329, 'KAB. PESISIR BARAT', 21),
(330, 'KAB. PRINGSEWU', 21),
(331, 'KAB. TANGGAMUS', 21),
(332, 'KAB. TULANG BAWANG', 21),
(333, 'KAB. TULANG BAWANG BARAT', 21),
(334, 'KAB. WAY KANAN', 21),
(335, 'KOTA BANDAR LAMPUNG', 21),
(336, 'KOTA METRO', 21),
(337, 'KAB. BURU', 22),
(338, 'KAB. BURU SELATAN', 22),
(339, 'KAB. KEPULAUAN ARU', 22),
(340, 'KAB. MALUKU BARAT DAYA', 22),
(341, 'KAB. MALUKU TENGAH', 22),
(342, 'KAB. MALUKU TENGGARA', 22),
(343, 'KAB. MALUKU TENGGARA BARAT', 22),
(344, 'KAB. SERAM BAGIAN BARAT', 22),
(345, 'KAB. SERAM BAGIAN TIMUR', 22),
(346, 'KOTA AMBON', 22),
(347, 'KOTA TUAL', 22),
(348, 'KAB. HALMAHERA BARAT', 23),
(349, 'KAB. HALMAHERA SELATAN', 23),
(350, 'KAB. HALMAHERA TENGAH', 23),
(351, 'KAB. HALMAHERA TIMUR', 23),
(352, 'KAB. HALMAHERA UTARA', 23),
(353, 'KAB. KEPULAUAN SULA', 23),
(354, 'KAB. PULAU MOROTAI', 23),
(355, 'KAB. PULAU TALIABU', 23),
(356, 'KOTA TERNATE', 23),
(357, 'KOTA TIDORE KEPULAUAN', 23),
(358, 'KAB. BIMA', 24),
(359, 'KAB. DOMPU', 24),
(360, 'KAB. LOMBOK BARAT', 24),
(361, 'KAB. LOMBOK TENGAH', 24),
(362, 'KAB. LOMBOK TIMUR', 24),
(363, 'KAB. LOMBOK UTARA', 24),
(364, 'KAB. SUMBAWA', 24),
(365, 'KAB. SUMBAWA BARAT', 24),
(366, 'KOTA BIMA', 24),
(367, 'KOTA MATARAM', 24),
(368, 'KAB TIMOR TENGAH SELATAN', 25),
(369, 'KAB. ALOR', 25),
(370, 'KAB. BELU', 25),
(371, 'KAB. ENDE', 25),
(372, 'KAB. FLORES TIMUR', 25),
(373, 'KAB. KUPANG', 25),
(374, 'KAB. LEMBATA', 25),
(375, 'KAB. MALAKA', 25),
(376, 'KAB. MANGGARAI', 25),
(377, 'KAB. MANGGARAI BARAT', 25),
(378, 'KAB. MANGGARAI TIMUR', 25),
(379, 'KAB. NAGEKEO', 25),
(380, 'KAB. NGADA', 25),
(381, 'KAB. ROTE NDAO', 25),
(382, 'KAB. SABU RAIJUA', 25),
(383, 'KAB. SIKKA', 25),
(384, 'KAB. SUMBA BARAT', 25),
(385, 'KAB. SUMBA BARAT DAYA', 25),
(386, 'KAB. SUMBA TENGAH', 25),
(387, 'KAB. SUMBA TIMUR', 25),
(388, 'KAB. TIMOR TENGAH UTARA', 25),
(389, 'KOTA KUPANG', 25),
(390, 'KAB PEGUNUNGAN BINTANG', 26),
(391, 'KAB. ASMAT', 26),
(392, 'KAB. BIAK NUMFOR', 26),
(393, 'KAB. BOVEN DIGOEL', 26),
(394, 'KAB. DEIYAI', 26),
(395, 'KAB. DOGIYAI', 26),
(396, 'KAB. INTAN JAYA', 26),
(397, 'KAB. JAYAPURA', 26),
(398, 'KAB. KEEROM', 26),
(399, 'KAB. LANNY JAYA', 26),
(400, 'KAB. MAMBERAMO RAYA', 26),
(401, 'KAB. MAMBERAMO TENGAH', 26),
(402, 'KAB. MAPPI', 26),
(403, 'KAB. MERAUKE', 26),
(404, 'KAB. MIMIKA', 26),
(405, 'KAB. NDUGA', 26),
(406, 'KAB. PANIAI', 26),
(407, 'KAB. PUNCAK', 26),
(408, 'KAB. PUNCAK JAYA', 26),
(409, 'KAB. SUPIORI', 26),
(410, 'KAB. TOLIKARA', 26),
(411, 'KAB. YAHUKIMO', 26),
(412, 'KAB. YALIMO', 26),
(413, 'KOTA JAYAPURA', 26),
(414, 'KAB. JAYAWIJAYA', 27),
(415, 'KAB. KEPULAUAN YAPEN', 27),
(416, 'KAB. NABIRE', 27),
(417, 'KAB. SARMI', 27),
(418, 'KAB. WAROPEN', 27),
(419, 'KAB. FAK FAK', 28),
(420, 'KAB. KAIMANA', 28),
(421, 'KAB. MANOKWARI', 28),
(422, 'KAB. MANOKWARI SELATAN', 28),
(423, 'KAB. MAYBRAT', 28),
(424, 'KAB. PEGUNUNGAN ARFAK', 28),
(425, 'KAB. RAJA AMPAT', 28),
(426, 'KAB. SORONG', 28),
(427, 'KAB. SORONG SELATAN', 28),
(428, 'KAB. TAMBRAUW', 28),
(429, 'KAB. TELUK BINTUNI', 28),
(430, 'KAB. TELUK WONDAMA', 28),
(431, 'KOTA SORONG', 28),
(432, 'KAB. BENGKALIS', 29),
(433, 'KAB. INDRAGIRI HILIR', 29),
(434, 'KAB. INDRAGIRI HULU', 29),
(435, 'KAB. KAMPAR', 29),
(436, 'KAB. KEPULAUAN MERANTI', 29),
(437, 'KAB. KUANTAN SINGINGI', 29),
(438, 'KAB.PELALAWAN', 29),
(439, 'KAB.ROKAN HILIR', 29),
(440, 'KAB.ROKAN HULU', 29),
(441, 'KAB.SIAK', 29),
(442, 'KOTA DUMAI', 29),
(443, 'KOTA PEKANBARU', 29),
(444, 'KAB. MAJENE', 30),
(445, 'KAB. MAMASA', 30),
(446, 'KAB. MAMUJU', 30),
(447, 'KAB. MAMUJU TENGAH', 30),
(448, 'KAB. MAMUJU UTARA', 30),
(449, 'KAB. POLEWALI MANDAR', 30),
(450, 'KAB. BANTAENG', 31),
(451, 'KAB. BARRU', 31),
(452, 'KAB. BONE', 31),
(453, 'KAB. BULUKUMBA', 31),
(454, 'KAB. ENREKANG', 31),
(455, 'KAB. GOWA', 31),
(456, 'KAB. JENEPONTO', 31),
(457, 'KAB. KEPULAUAN SELAYAR', 31),
(458, 'KAB. LUWU', 31),
(459, 'KAB. LUWU TIMUR', 31),
(460, 'KAB. LUWU UTARA', 31),
(461, 'KAB. MAROS', 31),
(462, 'KAB. PANGKAJENE KEPULAUAN', 31),
(463, 'KAB. PINRANG', 31),
(464, 'KAB. SIDENRENG RAPPANG', 31),
(465, 'KAB. SINJAI', 31),
(466, 'KAB. SOPPENG', 31),
(467, 'KAB. TAKALAR', 31),
(468, 'KAB. TANA TORAJA', 31),
(469, 'KAB. TORAJA UTARA', 31),
(470, 'KAB. WAJO', 31),
(471, 'KOTA MAKASSAR', 31),
(472, 'KOTA PALOPO', 31),
(473, 'KOTA PARE PARE', 31),
(474, 'KAB. BANGGAI', 32),
(475, 'KAB. BANGGAI KEPULAUAN', 32),
(476, 'KAB. BANGGAI LAUT', 32),
(477, 'KAB. BUOL', 32),
(478, 'KAB. DONGGALA', 32),
(479, 'KAB. MOROWALI', 32),
(480, 'KAB. MOROWALI UTARA', 32),
(481, 'KAB. PARIGI MOUTONG', 32),
(482, 'KAB. POSO', 32),
(483, 'KAB. SIGI', 32),
(484, 'KAB. TOJO UNA UNA', 32),
(485, 'KAB. TOLI TOLI', 32),
(486, 'KOTA PALU', 32),
(487, 'KAB. BOMBANA', 33),
(488, 'KAB. BUTON', 33),
(489, 'KAB. BUTON SELATAN', 33),
(490, 'KAB. BUTON TENGAH', 33),
(491, 'KAB. BUTON UTARA', 33),
(492, 'KAB. KOLAKA', 33),
(493, 'KAB. KOLAKA TIMUR', 33),
(494, 'KAB. KOLAKA UTARA', 33),
(495, 'KAB. KONAWE', 33),
(496, 'KAB. KONAWE KEPULAUAN', 33),
(497, 'KAB. KONAWE SELATAN', 33),
(498, 'KAB. KONAWE UTARA', 33),
(499, 'KAB. MUNA', 33),
(500, 'KAB. MUNA BARAT', 33),
(501, 'KAB. WAKATOBI', 33),
(502, 'KOTA BAU BAU', 33),
(503, 'KOTA KENDARI', 33),
(504, 'KAB. BOLAANG MONGONDOW', 34),
(505, 'KAB. BOLAANG MONGONDOW SELATAN', 34),
(506, 'KAB. BOLAANG MONGONDOW TIMUR', 34),
(507, 'KAB. BOLAANG MONGONDOW UTARA', 34),
(508, 'KAB. KEP. SIAU TAGULANDANG BIARO', 34),
(509, 'KAB. KEPULAUAN SANGIHE', 34),
(510, 'KAB. KEPULAUAN TALAUD', 34),
(511, 'KAB. MINAHASA', 34),
(512, 'KAB. MINAHASA SELATAN', 34),
(513, 'KAB. MINAHASA TENGGARA', 34),
(514, 'KAB. MINAHASA UTARA', 34),
(515, 'KOTA BITUNG', 34),
(516, 'KOTA KOTAMOBAGU', 34),
(517, 'KOTA MANADO', 34),
(518, 'KOTA TOMOHON', 34),
(519, 'KAB. AGAM', 35),
(520, 'KAB. DHARMASRAYA', 35),
(521, 'KAB. KEPULAUAN MENTAWAI', 35),
(522, 'KAB. LIMA PULUH KOTA', 35),
(523, 'KAB. PADANG PARIAMAN', 35),
(524, 'KAB. PASAMAN', 35),
(525, 'KAB. PASAMAN BARAT', 35),
(526, 'KAB. PESISIR SELATAN', 35),
(527, 'KAB. SIJUNJUNG', 35),
(528, 'KAB. SOLOK', 35),
(529, 'KAB. SOLOK SELATAN', 35),
(530, 'KAB. TANAH DATAR', 35),
(531, 'KOTA BUKITTINGGI', 35),
(532, 'KOTA PADANG', 35),
(533, 'KOTA PADANG PANJANG', 35),
(534, 'KOTA PARIAMAN', 35),
(535, 'KOTA PAYAKUMBUH', 35),
(536, 'KOTA SAWAHLUNTO', 35),
(537, 'KOTA SOLOK', 35),
(538, 'KAB. BANYUASIN', 36),
(539, 'KAB. EMPAT LAWANG', 36),
(540, 'KAB. LAHAT', 36),
(541, 'KAB. MUARA ENIM', 36),
(542, 'KAB. MUSI BANYUASIN', 36),
(543, 'KAB. MUSI RAWAS', 36),
(544, 'KAB. MUSI RAWAS UTARA', 36),
(545, 'KAB. OGAN ILIR', 36),
(546, 'KAB. OGAN KOMERING ILIR', 36),
(547, 'KAB. OGAN KOMERING ULU', 36),
(548, 'KAB. OGAN KOMERING ULU SELATAN', 36),
(549, 'KAB. OGAN KOMERING ULU TIMUR', 36),
(550, 'KAB. PENUKAL ABAB LEMATANG ILIR', 36),
(551, 'KOTA LUBUK LINGGAU', 36),
(552, 'KOTA PAGAR ALAM', 36),
(553, 'KOTA PALEMBANG', 36),
(554, 'KOTA PRABUMULIH', 36),
(555, 'KAB. ASAHAN', 37),
(556, 'KAB. BATU BARA', 37),
(557, 'KAB. DAIRI', 37),
(558, 'KAB. DELI SERDANG', 37),
(559, 'KAB. HUMBANG HASUNDUTAN', 37),
(560, 'KAB. KARO', 37),
(561, 'KAB. LABUHANBATU', 37),
(562, 'KAB. LABUHANBATU SELATAN', 37),
(563, 'KAB. LABUHANBATU UTARA', 37),
(564, 'KAB. LANGKAT', 37),
(565, 'KAB. MANDAILING NATAL', 37),
(566, 'KAB. NIAS', 37),
(567, 'KAB. NIAS BARAT', 37),
(568, 'KAB. NIAS SELATAN', 37),
(569, 'KAB. NIAS UTARA', 37),
(570, 'KAB. PADANG LAWAS', 37),
(571, 'KAB. PADANG LAWAS UTARA', 37),
(572, 'KAB. PAKPAK BHARAT', 37),
(573, 'KAB. SAMOSIR', 37),
(574, 'KAB. SERDANG BEDAGAI', 37),
(575, 'KAB. SIMALUNGUN', 37),
(576, 'KAB. TAPANULI SELATAN', 37),
(577, 'KAB. TAPANULI TENGAH', 37),
(578, 'KAB. TAPANULI UTARA', 37),
(579, 'KAB. TOBA SAMOSIR', 37),
(580, 'KOTA BINJAI', 37),
(581, 'KOTA GUNUNGSITOLI', 37),
(582, 'KOTA MEDAN', 37),
(583, 'KOTA PADANGSIDIMPUAN', 37),
(584, 'KOTA PEMATANGSIANTAR', 37),
(585, 'KOTA SIBOLGA', 37),
(586, 'KOTA TANJUNG BALAI', 37),
(587, 'KOTA TEBING TINGGI', 37);

-- --------------------------------------------------------

--
-- Table structure for table `kelas`
--

CREATE TABLE `kelas` (
  `id_kelas` int(11) NOT NULL,
  `nama_kelas` varchar(30) NOT NULL,
  `jurusan_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `kelas`
--

INSERT INTO `kelas` (`id_kelas`, `nama_kelas`, `jurusan_id`) VALUES
(17, 'TPA BAB I VERBAL', 6),
(23, 'VERBAL', 12),
(24, 'BACAAN', 12),
(25, 'A', 24),
(26, 'BACAAN', 13),
(27, 'VERBAL', 13);

-- --------------------------------------------------------

--
-- Table structure for table `kelas_dosen`
--

CREATE TABLE `kelas_dosen` (
  `id` int(11) NOT NULL,
  `kelas_id` int(11) NOT NULL,
  `dosen_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `kelas_dosen`
--

INSERT INTO `kelas_dosen` (`id`, `kelas_id`, `dosen_id`) VALUES
(1, 1, 2),
(2, 24, 8),
(3, 6, 4),
(4, 17, 11),
(5, 15, 11),
(6, 16, 11),
(7, 14, 14),
(8, 14, 15),
(17, 18, 23),
(18, 18, 24),
(19, 14, 20),
(20, 18, 20),
(21, 14, 21),
(22, 18, 21);

-- --------------------------------------------------------

--
-- Table structure for table `login_attempts`
--

CREATE TABLE `login_attempts` (
  `id` int(11) UNSIGNED NOT NULL,
  `ip_address` varchar(45) NOT NULL,
  `login` varchar(100) NOT NULL,
  `time` int(11) UNSIGNED DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `mahasiswa`
--

CREATE TABLE `mahasiswa` (
  `id_mahasiswa` int(11) NOT NULL,
  `alamat` text NOT NULL,
  `email` varchar(100) NOT NULL,
  `id_kabupaten` int(11) NOT NULL,
  `id_provinsi` int(11) NOT NULL,
  `nim` varchar(20) NOT NULL,
  `jurusan_id` int(11) NOT NULL,
  `nama` varchar(100) NOT NULL,
  `jenis_kelamin` varchar(11) NOT NULL,
  `kelas_id` int(11) NOT NULL,
  `nickname` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `mahasiswa`
--

INSERT INTO `mahasiswa` (`id_mahasiswa`, `alamat`, `email`, `id_kabupaten`, `id_provinsi`, `nim`, `jurusan_id`, `nama`, `jenis_kelamin`, `kelas_id`, `nickname`) VALUES
(25, '', 'nursilmialfi@gmail.com', 204, 0, '20200001', 0, 'Alfi Nur Silmi Ubaedi', 'P', 18, 'Alfi Nur Silmi Ubaedi'),
(26, '', 'berylcholif.8b@gmail.com', 233, 0, '20200002', 0, 'Beryl Cholif', 'L', 18, 'BerylCholif'),
(27, '', 'churotulismi@gmail.com', 175, 0, '20200003', 0, 'Churotul Ismi', 'P', 18, 'churotulismi'),
(28, '', 'fadel2989@gmail.com', 471, 0, '20200004', 0, 'FADEL MUHAMMAD R', 'L', 18, 'fadel_muhr'),
(29, '', 'mr0809805@gmail.com', 127, 0, '20200006', 0, 'Muhammad Ridwan', 'L', 18, 'ridwan61_'),
(30, '', 'nadasalma735@gmail.com', 210, 0, '20200005', 0, 'SALMA NORA RENADA', 'P', 18, 'norarenada'),
(31, '', 'yusnitanitaa@gmail.com', 176, 0, '20200007', 0, 'Yusnita Rahmadiani', 'P', 18, 'yusnitarmdn'),
(32, '', 'muhpungki.pajak@gmail.com', 0, 0, '11111111', 0, 'Atya Uzumaki', 'L', 18, 'Orangesky'),
(33, '', 'penagihan419@gmail.com', 107, 0, 'Astriatom35', 0, 'Rizky Jaya Ramadhan', 'L', 15, 'Astriatom35'),
(34, '', 'orangesky@gmail.com', 0, 0, '22222222', 0, 'Orange', 'L', 18, 'sky'),
(35, '', '333@gmail.com', 0, 0, '33333333', 0, 'sku', 'L', 18, 'Sky'),
(36, '', '444@gmail.com', 0, 0, '44444444', 0, 'NIta', 'P', 14, 'Shine in');

-- --------------------------------------------------------

--
-- Table structure for table `materi`
--

CREATE TABLE `materi` (
  `id` int(10) NOT NULL,
  `id_bab` int(10) UNSIGNED DEFAULT NULL,
  `link_youtube` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `file_pdf` longtext COLLATE utf8mb4_unicode_ci,
  `isi_materi` longtext COLLATE utf8mb4_unicode_ci,
  `status` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `materi`
--

INSERT INTO `materi` (`id`, `id_bab`, `link_youtube`, `file_pdf`, `isi_materi`, `status`) VALUES
(1, 5, '', '4850-1-33668-1-10-20190114.pdf', '<p>adalah</p>', NULL),
(2, 6, '', 'kelurahan_palembang.pdf', '<p>isi</p>', NULL),
(3, 7, 'yt.com', '4850-1-33668-1-10-201901141.pdf', '<p>materi</p>', NULL),
(4, 10, 'sma', '4850-1-33668-1-10-201901142.pdf', '<p>materi ipa sma</p>', NULL),
(5, 9, 'cpns ips', '4850-1-33668-1-10-201901143.pdf', '<p>materi cpns ips</p>', NULL),
(6, 11, 'cpns bab 2', '4850-1-33668-1-10-201901144.pdf', '<p>materi cpns bab 2</p>', NULL),
(8, 13, 'https://www.youtube.com/watch?v=wtavRlejv_o', 'RESPON_API_GOJEK1.pdf', '<p>materi</p><p data-f-id=\"pbf\" style=\"text-align: center; font-size: 14px; margin-top: 30px; opacity: 0.65; font-family: sans-serif;\">Powered by <a href=\"https://www.froala.com/wysiwyg-editor?pb=1\" title=\"Froala Editor\">Froala Editor</a></p>', NULL),
(13, 20, 'aaaa', 'document.pdf', '<p><strong>materi</strong></p><p><br></p><p><em>materi</em></p><p><br></p><p><u>materi</u></p><p data-f-id=\"pbf\" style=\"text-align: center; font-size: 14px; margin-top: 30px; opacity: 0.65; font-family: sans-serif;\">Powered by <a href=\"https://www.froala.com/wysiwyg-editor?pb=1\" title=\"Froala Editor\">Froala Editor</a></p>', NULL),
(14, 18, '', 'SOP_dan_Tutorial_Appointment_Apps1.pdf', '<p>bbbbb</p><p data-f-id=\"pbf\" style=\"text-align: center; font-size: 14px; margin-top: 30px; opacity: 0.65; font-family: sans-serif;\">Powered by <a href=\"https://www.froala.com/wysiwyg-editor?pb=1\" title=\"Froala Editor\">Froala Editor</a></p>', NULL),
(15, 21, 'https://youtu.be/T3bxbVGWy5k?list=RDT3bxbVGWy5k', 'Soal_Tryout_14_Bersama_Belajar_docx.pdf', '<p>KERJAKAN</p>', 1),
(18, 23, '', 'Soal_Tryout_14_Bersama_Belajar_docx3.pdf', '<p>FFF</p>', 1),
(20, 22, 'https://drive.google.com/file/d/1FoZ-CM_qxMW3t8L0YdL6XYv3h8IHLjeI/view?usp=sharing', 'PELAJARI_MATERI.pdf', '<p>Silahkan dipelajari dan silahkan didiskusidan dan/atau ditanyakan yang kurang dipahami&nbsp;</p>', 1),
(21, 25, 'https://drive.google.com/file/d/1FoZ-CM_qxMW3t8L0YdL6XYv3h8IHLjeI/view?usp=sharing', 'PELAJARI_MATERI1.pdf', '<p class=\"MsoNormal\"><span style=\"font-size: 10.5pt; line-height: 107%; font-family: \'Verdana\',sans-serif; color: black; background: white;\">Silahkan dipelajari dan silahkan didiskusidan dan/atau ditanyakan yang kurang dipahami&nbsp;</span></p>', 1),
(22, 26, 'https://drive.google.com/file/d/1FoZ-CM_qxMW3t8L0YdL6XYv3h8IHLjeI/view?usp=sharing', 'PELAJARI_MATERI2.pdf', '<p class=\"MsoNormal\"><span style=\"font-size: 10.5pt; line-height: 107%; font-family: \'Verdana\',sans-serif; color: black; background: white;\">Silahkan dipelajari dan silahkan didiskusidan dan/atau ditanyakan yang kurang dipahami&nbsp;</span></p>', 1),
(23, 28, 'https://drive.google.com/file/d/10fpu5DqO2-9HtdxgEeyX2Hz2aofoX4ws/view?usp=sharing', 'PELAJARI_MATERI3.pdf', '<p class=\"MsoNormal\"><span style=\"font-size: 10.5pt; line-height: 107%; font-family: \'Verdana\',sans-serif; color: black; background: white;\">Silahkan dipelajari dan silahkan didiskusidan dan/atau ditanyakan yang kurang dipahami&nbsp;</span></p>', 1),
(24, 29, 'https://drive.google.com/file/d/1VIqZHPLvW9s7mvt2Vd5JF1ybQ0l-eSl-/view?usp=sharing', 'PELAJARI_MATERI4.pdf', '<p class=\"MsoNormal\"><span style=\"font-size: 10.5pt; line-height: 107%; font-family: \'Verdana\',sans-serif; color: black; background: white;\">Silahkan dipelajari dan silahkan didiskusidan dan/atau ditanyakan yang kurang dipahami&nbsp;</span></p>', 1),
(25, 30, 'https://drive.google.com/file/d/1VIqZHPLvW9s7mvt2Vd5JF1ybQ0l-eSl-/view?usp=sharing', 'PELAJARI_MATERI5.pdf', '<p class=\"MsoNormal\"><span style=\"font-size: 10.5pt; line-height: 107%; font-family: \'Verdana\',sans-serif; color: black; background: white;\">Silahkan dipelajari dan silahkan didiskusidan dan/atau ditanyakan yang kurang dipahami&nbsp;</span></p>', 1),
(26, 69, 'https://drive.google.com/file/d/1f6ZJxjq2haHLsSJHqjwxTXDFZzN8UWss/view?usp=sharing', 'PELAJARI_MATERI6.pdf', '<p class=\"MsoNormal\"><span style=\"font-size: 10.5pt; line-height: 107%; font-family: \'Verdana\',sans-serif; color: black; background: white;\">Silahkan dipelajari dan silahkan didiskusidan dan/atau ditanyakan yang kurang dipahami&nbsp;</span></p>', 1),
(27, 70, 'https://drive.google.com/open?id=1f6ZJxjq2haHLsSJHqjwxTXDFZzN8UWss', 'PELAJARI_MATERI7.pdf', '<p class=\"MsoNormal\"><span style=\"font-size: 10.5pt; line-height: 107%; font-family: \'Verdana\',sans-serif; color: black; background: white;\">Silahkan dipelajari dan silahkan didiskusidan dan/atau ditanyakan yang kurang dipahami&nbsp;</span></p>', 1),
(28, 57, 'https://drive.google.com/open?id=1f6ZJxjq2haHLsSJHqjwxTXDFZzN8UWss', 'PELAJARI_MATERI8.pdf', '<p class=\"MsoNormal\"><span style=\"font-size: 10.5pt; line-height: 107%; font-family: \'Verdana\',sans-serif; color: black; background: white;\">Silahkan dipelajari dan silahkan didiskusidan dan/atau ditanyakan yang kurang dipahami&nbsp;</span></p>', 1),
(29, 55, 'https://drive.google.com/open?id=1s_2xG2EioooxaoXHz5UO6fssoVJwPY7A', 'PELAJARI_MATERI9.pdf', '<p class=\"MsoNormal\"><span style=\"font-size: 10.5pt; line-height: 107%; font-family: \'Verdana\',sans-serif; color: black; background: white;\">Silahkan dipelajari dan silahkan didiskusidan dan/atau ditanyakan yang kurang dipahami</span></p>', NULL),
(30, 56, 'https://drive.google.com/file/d/1s_2xG2EioooxaoXHz5UO6fssoVJwPY7A/view?usp=sharing', 'PELAJARI_MATERI10.pdf', '<p class=\"MsoNormal\"><span style=\"font-size: 10.5pt; line-height: 107%; font-family: \'Verdana\',sans-serif; color: black; background: white;\">Silahkan dipelajari dan silahkan didiskusidan dan/atau ditanyakan yang kurang dipahami&nbsp;</span></p>', 1),
(31, 24, 'https://drive.google.com/file/d/1PyOq8dUtGkpBKtWaZTfiD0j2l__k0m5R/view?usp=sharing', 'PELAJARI_MATERI11.pdf', '<p class=\"MsoNormal\"><span style=\"font-size: 10.5pt; line-height: 107%; font-family: \'Verdana\',sans-serif; color: black; background: white;\">Silahkan dipelajari dan silahkan didiskusidan dan/atau ditanyakan yang kurang dipahami&nbsp;</span></p>', 1),
(32, 41, 'ttps://drive.google.com/file/d/1PyOq8dUtGkpBKtWaZTfiD0j2l__k0m5R/view?usp=sharing', 'PELAJARI_MATERI12.pdf', '', 1),
(33, 42, 'https://drive.google.com/file/d/1ZAEej0ZyZsvqcfClq7RPRsIkiNK4QYFr/view?usp=sharing', 'PELAJARI_MATERI13.pdf', '', 1),
(34, 43, 'https://drive.google.com/file/d/12DtsYqUqVvO63Tw0ErwTtmxozyks0_cr/view?usp=sharing', 'PELAJARI_MATERI14.pdf', '', 1),
(35, 44, 'https://drive.google.com/file/d/1KdTlP0C9Iekpnc42lSfRogQ-lDnr2MPT/view?usp=sharing', 'PELAJARI_MATERI15.pdf', '', 1),
(36, 45, 'https://drive.google.com/file/d/1KdTlP0C9Iekpnc42lSfRogQ-lDnr2MPT/view?usp=sharing', 'PELAJARI_MATERI16.pdf', '', 1),
(37, 71, 'https://drive.google.com/open?id=1f6ZJxjq2haHLsSJHqjwxTXDFZzN8UWss', 'PELAJARI_MATERI17.pdf', '', 0),
(38, 27, 'https://drive.google.com/file/d/10fpu5DqO2-9HtdxgEeyX2Hz2aofoX4ws/view?usp=sharing', 'PELAJARI_MATERI18.pdf', '', 1),
(39, 46, 'https://drive.google.com/open?id=1RaLIa_DJ93jPyTkPCW4Z6YWQD3KpyIpe', 'PELAJARI_MATERI19.pdf', '', 1),
(40, 47, 'https://drive.google.com/open?id=1RaLIa_DJ93jPyTkPCW4Z6YWQD3KpyIpe', 'PELAJARI_MATERI20.pdf', '', 1),
(41, 31, 'https://drive.google.com/file/d/1VIqZHPLvW9s7mvt2Vd5JF1ybQ0l-eSl-/view?usp=sharing', 'PELAJARI_MATERI111.pdf', '', 1),
(42, 32, 'https://drive.google.com/file/d/1VIqZHPLvW9s7mvt2Vd5JF1ybQ0l-eSl-/view?usp=sharing', 'PELAJARI_MATERI112.pdf', '', 1),
(43, 33, 'https://drive.google.com/file/d/1VIqZHPLvW9s7mvt2Vd5JF1ybQ0l-eSl-/view?usp=sharing', 'PELAJARI_MATERI113.pdf', '', 1),
(44, 34, 'https://drive.google.com/file/d/1VIqZHPLvW9s7mvt2Vd5JF1ybQ0l-eSl-/view?usp=sharing', 'PELAJARI_MATERI114.pdf', '', 1),
(45, 35, 'https://drive.google.com/file/d/1VIqZHPLvW9s7mvt2Vd5JF1ybQ0l-eSl-/view?usp=sharing', 'PELAJARI_MATERI115.pdf', '', 1),
(46, 36, 'https://drive.google.com/file/d/1VIqZHPLvW9s7mvt2Vd5JF1ybQ0l-eSl-/view?usp=sharing', 'PELAJARI_MATERI116.pdf', '', 1),
(47, 37, 'https://drive.google.com/file/d/1VIqZHPLvW9s7mvt2Vd5JF1ybQ0l-eSl-/view?usp=sharing', 'PELAJARI_MATERI117.pdf', '', 1),
(48, 38, 'https://drive.google.com/file/d/1VIqZHPLvW9s7mvt2Vd5JF1ybQ0l-eSl-/view?usp=sharing', 'PELAJARI_MATERI118.pdf', '', 1),
(49, 39, 'https://drive.google.com/file/d/1VIqZHPLvW9s7mvt2Vd5JF1ybQ0l-eSl-/view?usp=sharing', 'PELAJARI_MATERI119.pdf', '', 1),
(50, 40, 'https://drive.google.com/file/d/1VIqZHPLvW9s7mvt2Vd5JF1ybQ0l-eSl-/view?usp=sharing', 'PELAJARI_MATERI1110.pdf', '', 1),
(51, 48, 'https://drive.google.com/file/d/1VIqZHPLvW9s7mvt2Vd5JF1ybQ0l-eSl-/view?usp=sharing', 'PELAJARI_MATERI1111.pdf', '', 1),
(52, 49, 'https://drive.google.com/file/d/1VIqZHPLvW9s7mvt2Vd5JF1ybQ0l-eSl-/view?usp=sharing', 'PELAJARI_MATERI1112.pdf', '', 1),
(53, 50, 'https://drive.google.com/file/d/1VIqZHPLvW9s7mvt2Vd5JF1ybQ0l-eSl-/view?usp=sharing', 'PELAJARI_MATERI1113.pdf', '', 1),
(54, 51, 'https://drive.google.com/file/d/1VIqZHPLvW9s7mvt2Vd5JF1ybQ0l-eSl-/view?usp=sharing', 'PELAJARI_MATERI1114.pdf', '', 1),
(58, 52, 'https://drive.google.com/file/d/1VIqZHPLvW9s7mvt2Vd5JF1ybQ0l-eSl-/view?usp=sharing', 'PELAJARI_MATERI1118.pdf', '', 1),
(59, 53, 'https://drive.google.com/file/d/1VIqZHPLvW9s7mvt2Vd5JF1ybQ0l-eSl-/view?usp=sharing', 'PELAJARI_MATERI1119.pdf', '', 1),
(60, 54, 'https://drive.google.com/file/d/1VIqZHPLvW9s7mvt2Vd5JF1ybQ0l-eSl-/view?usp=sharing', 'PELAJARI_MATERI1120.pdf', '', 1),
(61, 58, 'https://drive.google.com/file/d/1VIqZHPLvW9s7mvt2Vd5JF1ybQ0l-eSl-/view?usp=sharing', 'PELAJARI_MATERI1121.pdf', '', 1),
(62, 72, 'https://drive.google.com/file/d/1VIqZHPLvW9s7mvt2Vd5JF1ybQ0l-eSl-/view?usp=sharing', 'PELAJARI_MATERI1122.pdf', '', 1),
(63, 59, 'https://drive.google.com/file/d/1VIqZHPLvW9s7mvt2Vd5JF1ybQ0l-eSl-/view?usp=sharing', 'PELAJARI_MATERI1123.pdf', '', 1),
(64, 73, 'https://drive.google.com/file/d/1VIqZHPLvW9s7mvt2Vd5JF1ybQ0l-eSl-/view?usp=sharing', 'PELAJARI_MATERI1124.pdf', '', 1),
(65, 60, 'https://drive.google.com/file/d/1VIqZHPLvW9s7mvt2Vd5JF1ybQ0l-eSl-/view?usp=sharing', 'PELAJARI_MATERI1125.pdf', '', 1),
(66, 61, 'https://drive.google.com/file/d/1VIqZHPLvW9s7mvt2Vd5JF1ybQ0l-eSl-/view?usp=sharing', 'PELAJARI_MATERI1126.pdf', '', 1),
(67, 62, 'https://drive.google.com/file/d/1VIqZHPLvW9s7mvt2Vd5JF1ybQ0l-eSl-/view?usp=sharing', 'PELAJARI_MATERI1127.pdf', '', 1),
(68, 64, 'https://drive.google.com/file/d/1VIqZHPLvW9s7mvt2Vd5JF1ybQ0l-eSl-/view?usp=sharing', 'PELAJARI_MATERI1128.pdf', '', 1),
(69, 65, 'https://drive.google.com/file/d/1VIqZHPLvW9s7mvt2Vd5JF1ybQ0l-eSl-/view?usp=sharing', 'PELAJARI_MATERI1129.pdf', '', 1),
(70, 65, 'https://drive.google.com/file/d/1VIqZHPLvW9s7mvt2Vd5JF1ybQ0l-eSl-/view?usp=sharing', 'PELAJARI_MATERI1130.pdf', '', 1),
(71, 66, 'https://drive.google.com/file/d/1VIqZHPLvW9s7mvt2Vd5JF1ybQ0l-eSl-/view?usp=sharing', 'PELAJARI_MATERI1131.pdf', '', 1),
(72, 67, 'https://drive.google.com/file/d/1VIqZHPLvW9s7mvt2Vd5JF1ybQ0l-eSl-/view?usp=sharing', 'PELAJARI_MATERI1132.pdf', '', 1),
(73, 68, 'https://drive.google.com/file/d/1VIqZHPLvW9s7mvt2Vd5JF1ybQ0l-eSl-/view?usp=sharing', 'PELAJARI_MATERI1133.pdf', '', 1),
(74, 74, 'https://drive.google.com/file/d/1VIqZHPLvW9s7mvt2Vd5JF1ybQ0l-eSl-/view?usp=sharing', 'PELAJARI_MATERI1134.pdf', '', 1);

-- --------------------------------------------------------

--
-- Table structure for table `matkul`
--

CREATE TABLE `matkul` (
  `id_matkul` int(11) NOT NULL,
  `nama_matkul` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `matkul`
--

INSERT INTO `matkul` (`id_matkul`, `nama_matkul`) VALUES
(1, 'IPA'),
(4, 'SMA'),
(5, 'D3 Khusus PKN STAN'),
(6, 'DIV PKN STAN'),
(7, 'CPNS');

-- --------------------------------------------------------

--
-- Table structure for table `m_kuis`
--

CREATE TABLE `m_kuis` (
  `id_kuis` int(11) NOT NULL,
  `dosen_id` int(11) NOT NULL,
  `matkul_id` int(11) NOT NULL,
  `id_bab` int(10) NOT NULL,
  `nama_kuis` varchar(200) NOT NULL,
  `jumlah_soal` int(11) NOT NULL,
  `waktu` int(11) NOT NULL,
  `jenis` enum('acak','urut') NOT NULL,
  `tgl_mulai` datetime NOT NULL,
  `terlambat` datetime NOT NULL,
  `token` varchar(5) NOT NULL,
  `dasar_kriteria` varchar(10) NOT NULL,
  `nilai` int(11) NOT NULL,
  `jml_soal` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `m_kuis`
--

INSERT INTO `m_kuis` (`id_kuis`, `dosen_id`, `matkul_id`, `id_bab`, `nama_kuis`, `jumlah_soal`, `waktu`, `jenis`, `tgl_mulai`, `terlambat`, `token`, `dasar_kriteria`, `nilai`, `jml_soal`) VALUES
(1, 6, 7, 14, 'Soal Kuis bab 1 part of speech', 2, 10, 'acak', '2020-04-03 14:16:52', '2020-04-04 14:16:55', 'LMCAP', '', 0, 0),
(2, 9, 17, 20, 'Soal Kuis bab 1 mtk stan', 1, 10, 'urut', '2020-04-10 09:55:36', '2020-04-11 09:55:39', 'VWMNF', '', 0, 0),
(3, 12, 23, 21, 'kuis 1', 1, 90, 'acak', '2020-04-12 21:21:41', '2020-04-24 21:21:43', 'BMDJQ', '', 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `m_latihan`
--

CREATE TABLE `m_latihan` (
  `id_latihan` int(11) NOT NULL,
  `dosen_id` int(11) NOT NULL,
  `matkul_id` int(11) NOT NULL,
  `id_bab` int(10) NOT NULL,
  `nama_latihan` varchar(200) NOT NULL,
  `jumlah_soal` int(11) NOT NULL,
  `waktu` int(11) NOT NULL,
  `jenis` enum('acak','urut') NOT NULL,
  `tgl_mulai` datetime NOT NULL,
  `terlambat` datetime NOT NULL,
  `token` varchar(5) NOT NULL,
  `dasar_kriteria` varchar(10) NOT NULL,
  `nilai` int(11) NOT NULL,
  `jml_soal` int(11) NOT NULL,
  `status` int(1) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `m_latihan`
--

INSERT INTO `m_latihan` (`id_latihan`, `dosen_id`, `matkul_id`, `id_bab`, `nama_latihan`, `jumlah_soal`, `waktu`, `jenis`, `tgl_mulai`, `terlambat`, `token`, `dasar_kriteria`, `nilai`, `jml_soal`, `status`) VALUES
(4, 6, 7, 9, 'BAB 1 CPNS IPS', 3, 10, 'urut', '2020-03-25 21:00:40', '2020-03-26 21:00:43', 'MDVPQ', '', 0, 0, NULL),
(5, 6, 7, 11, 'BAB 2 CPNS IPS', 3, 10, 'urut', '2020-03-25 21:06:48', '2020-03-26 21:06:51', 'PSMLF', '', 0, 0, NULL),
(6, 6, 7, 14, 'BAB 1 PART OF SPEECH', 1, 10, 'urut', '2020-04-03 15:19:10', '2020-04-04 15:19:11', 'VATQE', '', 0, 0, NULL),
(7, 9, 17, 20, 'BAB 1 MTK STAN', 1, 10, 'urut', '2020-04-10 09:59:20', '2020-04-11 09:59:22', 'HEYBD', '', 0, 0, NULL),
(8, 12, 23, 21, 'latihan 1', 1, 100, 'acak', '2020-04-06 21:26:53', '2020-04-24 21:26:56', 'TCNTK', '', 0, 0, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `m_ujian`
--

CREATE TABLE `m_ujian` (
  `id_ujian` int(11) NOT NULL,
  `dosen_id` int(11) NOT NULL,
  `matkul_id` int(11) NOT NULL,
  `nama_ujian` varchar(200) NOT NULL,
  `jumlah_soal` int(11) NOT NULL,
  `waktu` int(11) NOT NULL,
  `jenis` enum('acak','urut') NOT NULL,
  `tgl_mulai` datetime NOT NULL,
  `terlambat` datetime NOT NULL,
  `token` varchar(5) NOT NULL,
  `dasar_kriteria` varchar(10) NOT NULL,
  `nilai` int(11) NOT NULL,
  `jml_soal` int(11) NOT NULL,
  `id_soal_paket` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `m_ujian`
--

INSERT INTO `m_ujian` (`id_ujian`, `dosen_id`, `matkul_id`, `nama_ujian`, `jumlah_soal`, `waktu`, `jenis`, `tgl_mulai`, `terlambat`, `token`, `dasar_kriteria`, `nilai`, `jml_soal`, `id_soal_paket`) VALUES
(1, 7, 2, 'uji', 2, 90, 'acak', '2020-04-01 14:10:19', '2020-04-02 14:10:22', 'PUKVW', '', 0, 0, 4),
(2, 9, 17, 'ujin to 1', 1, 90, 'acak', '2020-04-10 09:34:42', '2020-04-12 09:34:47', 'FAQPA', '', 0, 0, 11),
(3, 11, 18, 'dd', 2, 20, 'urut', '2020-04-13 00:59:08', '2020-04-15 00:59:10', 'EIRAN', '', 0, 0, 13),
(4, 12, 23, 'tryout 1', 1, 90, 'acak', '2020-04-07 21:43:23', '2020-04-24 21:43:25', 'DDUQX', '', 0, 0, 15),
(5, 14, 21, 'to 16 sma', 2, 1, 'urut', '2020-05-10 09:51:26', '2020-05-25 09:51:31', 'XCDTS', '', 0, 0, 19),
(6, 15, 16, 'UNTUK UJI TPA', 5, 1, 'urut', '2020-05-10 10:35:51', '2020-05-11 10:35:57', 'DZBBW', '', 0, 0, 20),
(7, 15, 16, 'to 20', 5, 5, 'urut', '2020-06-01 02:56:30', '2020-06-03 02:56:32', 'MHAAL', 'soal', 0, 3, 23),
(8, 12, 23, 'ujian tryout tkp', 5, 100, 'acak', '2020-06-05 12:33:00', '2020-06-05 20:33:04', 'WKQCO', 'nilai', 20, 3, 24),
(9, 12, 23, 'ujian tryout baru', 5, 100, 'acak', '2020-06-08 08:41:27', '2020-06-13 08:41:35', 'BSFZH', 'nilai', 10, 0, 24),
(10, 14, 21, 'CONTOH LAGI', 5, 5, 'urut', '2020-06-10 11:10:01', '2020-06-13 11:10:06', 'BCOHM', 'nilai', 7, 0, 27),
(11, 21, 23, 'KUIS TBI CHAPTER 3 TENSES I', 60, 45, 'urut', '2020-06-14 18:00:27', '2020-06-14 18:00:30', 'IHQCO', 'soal', 0, 25, 35),
(12, 21, 23, 'KUIS  TBI CHAPTER 4 TENSES II', 65, 50, 'urut', '2020-06-14 20:28:37', '2020-06-28 20:28:39', 'VVMSJ', 'soal', 0, 30, 36),
(13, 21, 23, 'KUIS TBI CHAPTER 5 ARTICLE CONJ AND PREP', 55, 40, 'acak', '2020-06-14 20:33:05', '2020-06-28 20:33:08', 'GREGV', 'soal', 0, 30, 40),
(14, 20, 18, 'KUIS TPA BAB I VERBAL I', 65, 35, 'acak', '2020-06-14 20:35:56', '2020-07-12 20:35:59', 'EHUYO', 'soal', 0, 40, 31),
(15, 20, 18, 'KUIS TPA BAB 3 POLA BILANGAN', 50, 45, 'acak', '2020-06-14 20:38:21', '2020-07-12 20:38:24', 'FITCG', 'soal', 0, 30, 33);

-- --------------------------------------------------------

--
-- Table structure for table `paket`
--

CREATE TABLE `paket` (
  `id` int(10) NOT NULL,
  `id_mapel` int(10) DEFAULT NULL,
  `nama_paket` varchar(100) NOT NULL,
  `jenis` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `paket`
--

INSERT INTO `paket` (`id`, `id_mapel`, `nama_paket`, `jenis`) VALUES
(29, NULL, 'TWK BAB 1 - PANCASILA - LATIHAN', 'Latihan'),
(30, NULL, 'TWK BAB 1 - PANCASILA - KUIS', 'Kuis'),
(31, NULL, 'TKP BAB 1 - PELAYANAN PUBLIK - LATIHAN', 'Latihan'),
(32, NULL, 'TKP BAB 1 - PELAYANAN PUBLIK - KUIS', 'Kuis'),
(33, NULL, 'TPA/TIU BAB 1 VERBAL - LATIHAN', 'Latihan'),
(34, NULL, 'TPA/TIU BAB 1 VERBAL - KUIS', 'Latihan'),
(35, NULL, 'TKP BAB 2 - JEJARING KERJA - LATIHAN', 'Latihan'),
(36, NULL, 'TKP BAB 2 - JEJARING KERJA - KUIS', 'Kuis'),
(37, NULL, ' TWK BAB 2 - UUD 1945 - LATIHAN', 'Latihan'),
(38, NULL, ' TWK BAB 2 - UUD 1945 - KUIS', 'Kuis'),
(39, NULL, 'TPA/TIU BAB 2 BACAAN - LATIHAN', 'Latihan'),
(40, NULL, 'TPA/TIU BAB 2 BACAAN - KUIS', 'Kuis'),
(41, NULL, 'TWK BAB 3 - BHINNEKA TUNGGAL IKA - LATIHAN', 'Latihan'),
(42, NULL, 'TWK BAB 3 - BHINNEKA TUNGGAL IKA - KUIS', 'Kuis'),
(43, NULL, 'TKP BAB 3 - SOSIAL BUDAYA - LATIHAN', 'Latihan'),
(44, NULL, 'TKP BAB 3 - SOSIAL BUDAYA - KUIS', 'Kuis'),
(45, NULL, 'TPA/TIU BAB 3 POLA BILANGAN - LATIHAN', 'Latihan'),
(46, NULL, 'TPA/TIU BAB 3 POLA BILANGAN - KUIS', 'Kuis'),
(47, NULL, 'TPA/TIU BAB 4 BILANGAN - LATIHAN', 'Latihan'),
(48, NULL, 'TPA/TIU BAB 4 BILANGAN - KUIS', 'Kuis'),
(49, NULL, 'TPA/TIU BAB 5 POLA GAMBAR - LATIHAN', 'Latihan'),
(50, NULL, 'TPA/TIU BAB 5 POLA GAMBAR - KUIS', 'Kuis'),
(51, NULL, 'TPA/TIU BAB 6 ALJABAR - LATIHAN', 'Latihan'),
(52, NULL, 'TPA/TIU BAB 6 ALJABAR - KUIS', 'Kuis'),
(53, NULL, 'TPA/TIU BAB 7 EKSPONEN LATIHAN', 'Latihan'),
(54, NULL, 'TPA/TIU BAB 7 EKSPONEN KUIS', 'Kuis'),
(55, NULL, 'TBI CHAPTER 1 PART OF SPEECH - EXERCISE', 'Latihan'),
(56, NULL, 'TBI CHAPTER 1 PART OF SPEECH - QUIZ', 'Kuis'),
(57, NULL, 'TBI CHAPTER 2 COMPLETING PARAGRAPH AND READING - EXERCISE', 'Latihan'),
(58, NULL, 'TBI CHAPTER 2 COMPLETING PARAGRAPH AND READING - QUIZ', 'Kuis'),
(59, NULL, 'TBI CHAPTER 3 TENSES I - EXERCISE', 'Latihan'),
(60, NULL, 'TBI CHAPTER 3 TENSES I - QUIZ', 'Kuis'),
(61, NULL, 'TBI CHAPTER 4 TENSES II - EXERCISE', 'Latihan'),
(62, NULL, 'TBI CHAPTER 4 TENSES II - QUIZ', 'Kuis'),
(63, NULL, 'TBI CHAPTER 5 ARTICLE, CONJUNCTION, AND PREPOSITION - EXERCISE', 'Latihan'),
(64, NULL, 'TBI CHAPTER 5 ARTICLE, CONJUNCTION, AND PREPOSITION - QUIZ', 'Kuis'),
(65, NULL, 'TBI CHAPTER 6 DERIVATIVE, ADJ ORDER, HYPHENATED, PRONOUN, APPOSITIVE - EXERCISE', 'Latihan'),
(66, NULL, 'TBI CHAPTER 6 DERIVATIVE, ADJ ORDER, HYPHENATED, PRONOUN, APPOSITIVE - QUIZ', 'Kuis'),
(67, NULL, 'TBI CHAPTER 10 PHRASES, PARTICIPLES - KUIS', 'Kuis'),
(68, NULL, 'TBI CHAPTER 10 PHRASES, PARTICIPLES - LATIHAN', 'Latihan'),
(69, NULL, 'TBI CHAPTER 11 TO INFINITIVE AND GERUND - KUIS', 'Kuis'),
(70, NULL, 'TBI CHAPTER 11 TO INFINITIVE AND GERUND - LATIHAN', 'Latihan'),
(71, NULL, 'TBI CHAPTER 12 PASSIVE VOICE AND CAUSATIVE - KUIS', 'Kuis'),
(72, NULL, 'TBI CHAPTER 12 PASSIVE VOICE AND CAUSATIVE - LATIHAN', 'Latihan'),
(73, NULL, 'TBI CHAPTER 13 REPORTED SPEECH AND INVERSION - KUIS', 'Kuis'),
(74, NULL, 'TBI CHAPTER 13 REPORTED SPEECH AND INVERSION - LATIHAN', 'Latihan'),
(75, NULL, 'TBI CHAPTER 14 SUBJUNCTIVE DAN CONDITIONAL SENTENCE - KUIS', 'Kuis'),
(76, NULL, 'TBI CHAPTER 14 SUBJUNCTIVE DAN CONDITIONAL SENTENCE - LATIHAN', 'Latihan'),
(77, NULL, 'TBI CHAPTER 15 DEGREES OF COMPARISON, QUESTION TAG, AND ELLIPTICAL STRUCTURE - KUIS', 'Kuis'),
(78, NULL, 'TBI CHAPTER 15 DEGREES OF COMPARISON, QUESTION TAG, AND ELLIPTICAL STRUCTURE - LATIHAN', 'Latihan'),
(79, NULL, 'TBI CHAPTER 7 CONCORDAGREEMENT AND PARALLEL STRUCTURE - KUIS', 'Kuis'),
(80, NULL, 'TBI CHAPTER 7 CONCORDAGREEMENT AND PARALLEL STRUCTURE - LATIHAN', 'Latihan'),
(81, NULL, 'TBI CHAPTER 8 RELATIVE PRONOUNS, CLAUSES - KUIS', 'Kuis'),
(82, NULL, 'TBI CHAPTER 8 RELATIVE PRONOUNS, CLAUSES - LATIHAN', 'Latihan'),
(83, NULL, 'TBI CHAPTER 9 EXPRESSING PREFERENCE AND MODAL - KUIS', 'Kuis'),
(84, NULL, 'TBI CHAPTER 9 EXPRESSING PREFERENCE AND MODAL - LATIHAN', 'Latihan'),
(85, NULL, 'TKP BAB 4 - TEKNOLOGI INFORMASI DAN KOMUNIKASI - KUIS', 'Kuis'),
(86, NULL, 'TKP BAB 4 - TEKNOLOGI INFORMASI DAN KOMUNIKASI - LATIHAN', 'Latihan'),
(87, NULL, 'TKP BAB 5 - PROFESIONALISME - KUIS', 'Kuis'),
(88, NULL, 'TKP BAB 5 - PROFESIONALISME - LATIHAN', 'Latihan'),
(89, NULL, 'TPA/TIU BAB 10 ANALYTICAL REASONING - KUIS', 'Kuis'),
(90, NULL, 'TPA/TIU BAB 10 ANALYTICAL REASONING - LATIHAN', 'Latihan'),
(91, NULL, 'TPA/TIU BAB 11 PELUANG - KUIS', 'Kuis'),
(92, NULL, 'TPA/TIU BAB 11 PELUANG - LATIHAN', 'Latihan'),
(93, NULL, 'TPA/TIU BAB 12 PERBANDINGAN - KUIS', 'Kuis'),
(94, NULL, 'TPA/TIU BAB 12 PERBANDINGAN - LATIHAN', 'Latihan'),
(95, NULL, 'TPA/TIU BAB 13 KECEPATAN - KUIS', 'Kuis'),
(96, NULL, 'TPA/TIU BAB 13 KECEPATAN - LATIHAN', 'Latihan'),
(97, NULL, 'TPA/TIU BAB 14 HIMPUNAN - KUIS', 'Kuis'),
(98, NULL, 'TPA/TIU BAB 14 HIMPUNAN - LATIHAN', 'Latihan'),
(99, NULL, 'TPA/TIU BAB 15 STATISTIKA - KUIS', 'Kuis'),
(100, NULL, 'TPA/TIU BAB 15 STATISTIKA - LATIHAN', 'Latihan'),
(101, NULL, 'TPA/TIU BAB 16 BANGUN DATAR - KUIS', 'Kuis'),
(102, NULL, 'TPA/TIU BAB 16 BANGUN DATAR - LATIHAN', 'Latihan'),
(103, NULL, 'TPA/TIU BAB 17 BANGUN RUANG - KUIS', 'Kuis'),
(104, NULL, 'TPA/TIU BAB 17 BANGUN RUANG - LATIHAN', 'Latihan'),
(105, NULL, 'TPA/TIU BAB 8 ARITMETIKA SOSIAL DAN PERSENTASE - KUIS', 'Kuis'),
(106, NULL, 'TPA/TIU BAB 8 ARITMETIKA SOSIAL DAN PERSENTASE - LATIHAN', 'Latihan'),
(107, NULL, 'TPA/TIU BAB 9 LOGICAL REASONING - KUIS', 'Kuis'),
(108, NULL, 'TPA/TIU BAB 9 LOGICAL REASONING - LATIHAN', 'Latihan'),
(109, NULL, 'TWK BAB 10 - SEJARAH NKRI : PERISTIWA-PERISTIWA MENJELANG DAN PASCA PROKLAMAS - KUIS', 'Kuis'),
(110, NULL, 'TWK BAB 10 - SEJARAH NKRI : PERISTIWA-PERISTIWA MENJELANG DAN PASCA PROKLAMAS - LATIHAN', 'Latihan'),
(111, NULL, 'TWK BAB 11 - SEJARAH NKRI : PERJUANGAN MEMPERTAHANKAN KEMERDEKAAN (PERIODE 1945-1949) - KUIS', 'Kuis'),
(112, NULL, 'TWK BAB 11 - SEJARAH NKRI : PERJUANGAN MEMPERTAHANKAN KEMERDEKAAN (PERIODE 1945-1949) - LATIHAN', 'Latihan'),
(113, NULL, 'TWK BAB 12 - SEJARAH NKRI : PEMERINTAHAN MASA AWAL KEMERDEKAAN, ORDE LAMA, ORDE BARU & REFORMASI - K', 'Kuis'),
(114, NULL, 'TWK BAB 12 - SEJARAH NKRI : PEMERINTAHAN MASA AWAL KEMERDEKAAN, ORDE LAMA, ORDE BARU & REFORMASI - L', 'Latihan'),
(115, NULL, 'TWK BAB 13 - BAHASA INDONESIA : FONEM, HURUF, & MORFOLOGI - KUIS', 'Kuis'),
(116, NULL, 'TWK BAB 13 - BAHASA INDONESIA : FONEM, HURUF, & MORFOLOGI - LATIHAN', 'Latihan'),
(117, NULL, 'TWK BAB 14 - BAHASA INDONESIA : KALIMAT & PEMAKAIAN TANDA BACA - KUIS', 'Kuis'),
(118, NULL, 'TWK BAB 14 - BAHASA INDONESIA : KALIMAT & PEMAKAIAN TANDA BACA - LATIHAN', 'Latihan'),
(119, NULL, 'TWK BAB 4 - TATA NEGARA & KONSTITUSI - KUIS', 'Kuis'),
(120, NULL, 'TWK BAB 4 - TATA NEGARA & KONSTITUSI - LATIHAN', 'Latihan'),
(121, NULL, 'TWK BAB 5 - GEO POLITIK, HUBUNGAN INTERNASIONAL, DAN HAM DI INDONESIA - KUIS', 'Kuis'),
(122, NULL, 'TWK BAB 5 - GEO POLITIK, HUBUNGAN INTERNASIONAL, DAN HAM DI INDONESIA - LATIHAN', 'Latihan'),
(123, NULL, 'TWK BAB 6 - SEJARAH NKRI : ZAMAN KERAJAAN HINDU BUDHA - KUIS', 'Kuis'),
(124, NULL, 'TWK BAB 6 - SEJARAH NKRI : ZAMAN KERAJAAN HINDU BUDHA - LATIHAN', 'Latihan'),
(125, NULL, 'TWK BAB 7 - SEJARAH NKRI : ZAMAN KERAJAAN ISLAM - KUIS', 'Kuis'),
(126, NULL, 'TWK BAB 7 - SEJARAH NKRI : ZAMAN KERAJAAN ISLAM - LATIHAN', 'Latihan'),
(127, NULL, 'TWK BAB 8 - SEJARAH NKRI : PERJUANGAN MELAWAN KOLONIALISME & IMPERIALISME - KUIS', 'Kuis'),
(128, NULL, 'TWK BAB 8 - SEJARAH NKRI : PERJUANGAN MELAWAN KOLONIALISME & IMPERIALISME - LATIHAN', 'Latihan'),
(129, NULL, 'TWK BAB 9 SEJARAH NNKRI: PERJUANGAN MELAWAN PENJAJAH PADA MASA PERGERAKAN NASIONAL (PERIODE 1908-194', 'Kuis'),
(130, NULL, 'TWK BAB 9 SEJARAH NNKRI: PERJUANGAN MELAWAN PENJAJAH PADA MASA PERGERAKAN NASIONAL (PERIODE 1908-194', 'Latihan');

-- --------------------------------------------------------

--
-- Table structure for table `pendaftaran`
--

CREATE TABLE `pendaftaran` (
  `id` int(11) NOT NULL,
  `id_program` int(11) NOT NULL,
  `nama_lengkap` varchar(150) NOT NULL,
  `username` varchar(100) NOT NULL,
  `password` varchar(225) NOT NULL,
  `email` varchar(100) NOT NULL,
  `nomor_hp` varchar(12) NOT NULL,
  `jenis_kelamin` enum('L','P','','') NOT NULL,
  `kabupaten` int(11) NOT NULL,
  `alamat` varchar(225) NOT NULL,
  `flag_registrasi` enum('Y','N','','') NOT NULL DEFAULT 'N'
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

--
-- Dumping data for table `pendaftaran`
--

INSERT INTO `pendaftaran` (`id`, `id_program`, `nama_lengkap`, `username`, `password`, `email`, `nomor_hp`, `jenis_kelamin`, `kabupaten`, `alamat`, `flag_registrasi`) VALUES
(10, 4, 'Yulianto Mustaqim', 'Ymustaqim19', '19juli1995', 'ymustaqim19@gmail.com', '082123123123', 'L', 1, 'alamat', 'Y'),
(11, 8, 'Tutik Maryana', 'tutik', '11maret1995', 'tutik@gmail.com', '9897987', 'P', 8, 'sdfsdf', 'Y'),
(12, 11, 'SILKY', 'SILKYTF', 'SILKYTF', 'SILKY@GMAIL.COM', '089601881023', 'P', 132, 'FFAF', 'Y'),
(13, 10, 'nana', 'nana', '12345678', 'nana@gmail.com', '098098', 'P', 194, 'sadfsdfa', 'Y'),
(14, 11, 'Viviana Dwi Putri', 'Putriviviana', 'cantik', 'putriviviana.senj4@gmail.com', '085715141515', 'P', 580, 'Jl jambi no 1 Binjai selatan', 'N'),
(15, 11, 'Yusnita Rahmadiani', 'yusnitarmdn', 'bersamanita', 'yusnitanitaa@gmail.com', '082297463870', 'P', 176, 'Wismamas B4/1, cinangka, sawangan, depok', 'Y'),
(16, 11, 'Rizky Jaya Ramadhan', 'Astriatom35', 'Smansa35', 'penagihan419@gmail.com', '081295717295', 'L', 107, 'Jalan mayor widagdo no 6 pandegelang', 'N'),
(17, 11, 'Rizky Jaya Ramadhan', 'Astriatom35', 'Smansa35', 'penagihan419@gmail.com', '081295717295', 'L', 107, 'Jalan mayor widagdo no 6 pandegelang', 'Y'),
(18, 11, 'Rizky Jaya Ramadhan', 'Astriatom35', 'Smansa35', 'penagihan419@gmail.com', '081295717295', 'L', 107, 'Jalan mayor widagdo no 6 pandegelang', 'N'),
(19, 11, 'FADEL MUHAMMAD R', 'fadel_muhr', 'Jubel23of17', 'fadel2989@gmail.com', '082293214181', 'L', 471, 'jalan balana 1 no.43 kota makassar', 'Y'),
(20, 11, 'Beryl Cholif', 'BerylCholif', 'z1TsnhOX3K3pFzoXme', 'berylcholif.8b@gmail.com', '085710158581', 'L', 233, 'Jl. Rembang Industri Raya No.1, Jati, Pandean, Kec. Rembang, Pasuruan, Jawa Timur 67152, Indonesia', 'Y'),
(21, 10, 'Dintarima', 'dintarima', 'upik12345', 'dintarahadewi@gmail.com', '089523079922', 'P', 125, 'Wonosari', 'N'),
(22, 11, 'Radhiq Mulyo Dwi Janarko', 'radx87', 'Isfuriyah99', 'radhiqmulia@gmail.com', '081232335393', 'L', 227, 'Jl. Raya Kebonsari No. 216 Tumpang-Malang', 'N'),
(23, 11, 'Restu Nursanti', 'nursanti', 'januari97', 'restunursanti@gmail.com', '081932618559', 'P', 154, 'Jl. Cikarang Baru Raya No.10, Simpangan, Kec. Cikarang Utara, Bekasi, Jawa Barat 17530', 'N'),
(24, 11, 'Radhiq Mulyo Dwi Janarko', 'radx87', '', 'radhiqmulia@gmail.com', '081232335393', 'L', 0, 'Jl. Raya Kebonsari No. 216 Tumpang-Malang', 'N'),
(25, 11, 'Churotul Ismi', 'churotulismi', 'Ismi1998', 'churotulismi@gmail.com', '085706121520', 'P', 175, 'Rumah Dinas Bea Cukai 2\r\nJl. Sukapura 1 No. 3, Sukapura, Kec. Kejaksan, Kota Cirebon, Jawa Barat 45122', 'Y'),
(26, 10, 'INTAN MELANY SORAYA', 'intanmsoraya', 'ayarose24', 'intanmelani220@gmail.com', '089699508112', 'P', 159, 'Jl. Raya Cipendeuy Pasar 03/01 Kec. Malangbong', 'N'),
(27, 10, 'Muhammad Ridwan', 'ridwan61_', 'conan123', 'mr0809805@gmail.com', '085778075175', 'L', 127, 'nganti, sendangadi, mlati, sleman, yogyakarta', 'Y'),
(28, 10, 'Alfi Nur Silmi Ubaedi', 'Alfi Nur Silmi Ubaedi', 'alfi123', 'nursilmialfi@gmail.com', '+62 812-2997', 'P', 204, 'Desa sesepan , jl gandasuli RT 02 RW 04 , kecamatan balapulang kabupaten tegal', 'Y'),
(29, 11, 'Muhammad Yusril MS', 'Yusril', 'yu512ilMS', 'myusrilelsevt@gmail.com', '081342446006', 'L', 455, 'BTN Aura Blok H4 No. 16, Bontoala, Kecamatan Pallangga, Kabupaten Gowa, Sulawesi Selatan ', 'N'),
(30, 10, 'SALMA NORA RENADA', 'norarenada', 'mahasiswipknstanaamiin', 'nadasalma735@gmail.com', '085640648337', 'P', 210, 'JL. Kumpulrejo 1/6 rt 1 rw 4 gendongan, tingkir, salatiga', 'Y'),
(31, 11, 'Ruth Asima S Pardede', 'Rapard', 'B2020084510', 'ruth.beatifullake.asima4@gmail.com', '081285649510', 'P', 154, 'Griya Asri II Blok D 16 No. 7, Tambun Selatan, Bekasi, Jawa Barat', 'N'),
(32, 10, 'Adinda Aulia Rahmadiana', 'Dindarhmdn', 'dinda2808', 'i.am.actually.mermaid@gmail.com', '085693785536', 'P', 176, 'Komp. Wismamas blok B4/1, rt 06/10, cinangka, sawangan, depok', 'N');

-- --------------------------------------------------------

--
-- Table structure for table `provinsi`
--

CREATE TABLE `provinsi` (
  `id` int(11) NOT NULL,
  `nama` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `provinsi`
--

INSERT INTO `provinsi` (`id`, `nama`) VALUES
(3, 'ACEH'),
(4, 'BALI'),
(5, 'BANTEN'),
(6, 'BENGKULU'),
(7, 'DAERAH ISTIMEWA YOGYAKARTA'),
(8, 'DKI JAKARTA'),
(9, 'GORONTALO'),
(10, 'JAMBI'),
(11, 'JAWA BARAT'),
(12, 'JAWA TENGAH'),
(13, 'JAWA TIMUR'),
(14, 'KALIMANTAN BARAT'),
(15, 'KALIMANTAN SELATAN'),
(16, 'KALIMANTAN TENGAH'),
(17, 'KALIMANTAN TIMUR'),
(18, 'KALIMANTAN UTARA'),
(19, 'KEPULAUAN BANGKA BELITUNG'),
(20, 'KEPULAUAN RIAU'),
(21, 'LAMPUNG'),
(22, 'MALUKU'),
(23, 'MALUKU UTARA'),
(24, 'NUSA TENGGARA BARAT'),
(25, 'NUSA TENGGARA TIMUR'),
(26, 'P A P U A'),
(27, 'PAPUA'),
(28, 'PAPUA BARAT'),
(29, 'RIAU'),
(30, 'SULAWESI BARAT'),
(31, 'SULAWESI SELATAN'),
(32, 'SULAWESI TENGAH'),
(33, 'SULAWESI TENGGARA'),
(34, 'SULAWESI UTARA'),
(35, 'SUMATERA BARAT'),
(36, 'SUMATERA SELATAN'),
(37, 'SUMATERA UTARA');

-- --------------------------------------------------------

--
-- Table structure for table `soal_paket`
--

CREATE TABLE `soal_paket` (
  `id` int(10) NOT NULL,
  `id_dosen` int(10) NOT NULL,
  `id_paket` int(10) NOT NULL,
  `jenis_soal` varchar(20) NOT NULL,
  `status_selesai_input` varchar(1) NOT NULL DEFAULT 'N'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `soal_paket`
--

INSERT INTO `soal_paket` (`id`, `id_dosen`, `id_paket`, `jenis_soal`, `status_selesai_input`) VALUES
(1, 2, 7, '', 'N'),
(2, 2, 8, '', 'N'),
(3, 2, 8, '', 'N'),
(4, 7, 8, '', 'N'),
(5, 7, 8, '', 'N'),
(6, 7, 8, '', 'N'),
(7, 3, 11, '', 'N'),
(8, 3, 8, '', 'N'),
(9, 4, 9, '', 'N'),
(10, 3, 9, '', 'N'),
(11, 9, 14, '', 'N'),
(12, 10, 14, '', 'N'),
(13, 11, 22, '', 'N'),
(14, 11, 18, '', 'N'),
(15, 12, 18, '', 'N'),
(16, 12, 19, 'nontkp', 'N'),
(17, 12, 18, 'tkp', 'N'),
(18, 12, 18, 'tkp', 'N'),
(19, 14, 23, 'tkp', 'N'),
(20, 15, 25, 'nontkp', 'N'),
(21, 12, 18, 'tkp', 'N'),
(22, 15, 23, 'nontkp', 'N'),
(23, 15, 26, 'nontkp', 'N'),
(24, 12, 27, 'tkp', 'N'),
(25, 14, 27, 'tkp', 'N'),
(26, 14, 28, 'tkp', 'N'),
(27, 14, 28, 'tkp', 'N'),
(28, 20, 33, 'nontkp', 'N'),
(29, 18, 29, 'nontkp', 'N'),
(30, 19, 31, 'tkp', 'N'),
(31, 20, 34, 'nontkp', 'N'),
(32, 21, 56, 'nontkp', 'N'),
(33, 20, 46, 'nontkp', 'N'),
(34, 21, 57, 'nontkp', 'N'),
(35, 21, 60, 'nontkp', 'N'),
(36, 21, 62, 'nontkp', 'N'),
(37, 20, 48, 'nontkp', 'N'),
(38, 20, 52, 'nontkp', 'N'),
(39, 20, 50, 'nontkp', 'N'),
(40, 21, 64, 'nontkp', 'N'),
(41, 21, 66, 'nontkp', 'N'),
(42, 20, 54, 'nontkp', 'N'),
(43, 20, 45, 'nontkp', 'N'),
(44, 20, 47, 'nontkp', 'N'),
(45, 20, 49, 'nontkp', 'N'),
(46, 20, 51, 'nontkp', 'N'),
(47, 20, 53, 'nontkp', 'N');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_kelas`
--

CREATE TABLE `tbl_kelas` (
  `id` int(11) NOT NULL,
  `nama` varchar(200) NOT NULL,
  `id_program` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_kelas`
--

INSERT INTO `tbl_kelas` (`id`, `nama`, `id_program`) VALUES
(1, 'Kelas A', 2),
(2, 'Kelas B', 2),
(3, 'Kelas A', 3),
(4, 'Kelas 1', 3),
(5, 'Kelas 1', 4),
(10, 'CPNS A', 8),
(11, 'CPNS B', 8),
(12, 'STAN A', 9),
(13, 'STAN B', 9),
(14, 'A', 10),
(15, 'BATCH DEPOK', 11),
(16, 'BATCH KEMAYORAN', 11),
(17, 'BATCH CEMPAKA PUTIH', 11),
(18, 'A', 13);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_mapel`
--

CREATE TABLE `tbl_mapel` (
  `id` int(11) NOT NULL,
  `nama` varchar(200) NOT NULL,
  `id_program` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_mapel`
--

INSERT INTO `tbl_mapel` (`id`, `nama`, `id_program`) VALUES
(14, 'TIUW', 8),
(15, 'TWK', 8),
(16, 'TPA', 9),
(17, 'MTK', 9),
(18, 'TES POTENSI AKADEMIK (TPA) & TES INTELEJENSI UMUM (TIU)', 13),
(20, 'TES WAWASAN KEBANGSAAN (TWK)', 13),
(21, 'TES KARAKTERISTIK PRIBADI (TKP)', 13),
(23, 'TES BAHASA INGGRIS (TBI)', 13);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_program_belajar`
--

CREATE TABLE `tbl_program_belajar` (
  `id` int(11) NOT NULL,
  `nama` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

--
-- Dumping data for table `tbl_program_belajar`
--

INSERT INTO `tbl_program_belajar` (`id`, `nama`) VALUES
(10, 'SMA-PKN STAN'),
(11, 'TUGAS BELAJAR D3K PKN STAN'),
(12, 'PROGRAM KHUSUS SKD'),
(13, 'COMBINED ONLINE');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_soal_tryout`
--

CREATE TABLE `tbl_soal_tryout` (
  `id_soal` int(11) NOT NULL,
  `dosen_id` int(11) NOT NULL,
  `matkul_id` int(11) NOT NULL,
  `id_soal_paket` int(10) NOT NULL,
  `bobot` int(11) NOT NULL,
  `file` varchar(255) NOT NULL,
  `tipe_file` varchar(50) NOT NULL,
  `soal` longtext NOT NULL,
  `opsi_a` longtext NOT NULL,
  `opsi_b` longtext NOT NULL,
  `opsi_c` longtext NOT NULL,
  `opsi_d` longtext NOT NULL,
  `opsi_e` longtext NOT NULL,
  `bobot_a` int(11) NOT NULL,
  `bobot_b` int(11) NOT NULL,
  `bobot_c` int(11) NOT NULL,
  `bobot_d` int(11) NOT NULL,
  `bobot_e` int(11) NOT NULL,
  `bobot_kosong` int(11) DEFAULT NULL,
  `file_a` varchar(255) NOT NULL,
  `file_b` varchar(255) NOT NULL,
  `file_c` varchar(255) NOT NULL,
  `file_d` varchar(255) NOT NULL,
  `file_e` varchar(255) NOT NULL,
  `jawaban` varchar(5) NOT NULL,
  `pembahasan` longtext,
  `created_on` int(11) NOT NULL,
  `updated_on` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

--
-- Dumping data for table `tbl_soal_tryout`
--

INSERT INTO `tbl_soal_tryout` (`id_soal`, `dosen_id`, `matkul_id`, `id_soal_paket`, `bobot`, `file`, `tipe_file`, `soal`, `opsi_a`, `opsi_b`, `opsi_c`, `opsi_d`, `opsi_e`, `bobot_a`, `bobot_b`, `bobot_c`, `bobot_d`, `bobot_e`, `bobot_kosong`, `file_a`, `file_b`, `file_c`, `file_d`, `file_e`, `jawaban`, `pembahasan`, `created_on`, `updated_on`) VALUES
(6, 7, 2, 4, 1, '', '', '<p>sadf</p><p data-f-id=\"pbf\" xss=removed>Powered by <a href=\"https://www.froala.com/wysiwyg-editor?pb=1\" title=\"Froala Editor\">Froala Editor</a></p>', '<p>sadf</p><p data-f-id=\"pbf\" xss=removed>Powered by <a href=\"https://www.froala.com/wysiwyg-editor?pb=1\" title=\"Froala Editor\">Froala Editor</a></p>', '<p>sadf</p><p data-f-id=\"pbf\" xss=removed>Powered by <a href=\"https://www.froala.com/wysiwyg-editor?pb=1\" title=\"Froala Editor\">Froala Editor</a></p>', '<p>sdf</p><p data-f-id=\"pbf\" xss=removed>Powered by <a href=\"https://www.froala.com/wysiwyg-editor?pb=1\" title=\"Froala Editor\">Froala Editor</a></p>', '<p>sadf</p><p data-f-id=\"pbf\" xss=removed>Powered by <a href=\"https://www.froala.com/wysiwyg-editor?pb=1\" title=\"Froala Editor\">Froala Editor</a></p>', '<p>sdaf</p><p data-f-id=\"pbf\" xss=removed>Powered by <a href=\"https://www.froala.com/wysiwyg-editor?pb=1\" title=\"Froala Editor\">Froala Editor</a></p>', 0, 0, 0, 0, 0, NULL, '', '', '', '', '', 'E', NULL, 1585724540, 1585724540),
(7, 7, 2, 4, 1, '', '', '<p>sadf</p><p data-f-id=\"pbf\" xss=removed>Powered by <a href=\"https://www.froala.com/wysiwyg-editor?pb=1\" title=\"Froala Editor\">Froala Editor</a></p>', '<p>a</p><p data-f-id=\"pbf\" xss=removed>Powered by <a href=\"https://www.froala.com/wysiwyg-editor?pb=1\" title=\"Froala Editor\">Froala Editor</a></p>', '<p>b</p><p data-f-id=\"pbf\" xss=removed>Powered by <a href=\"https://www.froala.com/wysiwyg-editor?pb=1\" title=\"Froala Editor\">Froala Editor</a></p>', '<p>c</p><p data-f-id=\"pbf\" xss=removed>Powered by <a href=\"https://www.froala.com/wysiwyg-editor?pb=1\" title=\"Froala Editor\">Froala Editor</a></p>', '<p>d</p><p data-f-id=\"pbf\" xss=removed>Powered by <a href=\"https://www.froala.com/wysiwyg-editor?pb=1\" title=\"Froala Editor\">Froala Editor</a></p>', '<p>e</p><p data-f-id=\"pbf\" xss=removed>Powered by <a href=\"https://www.froala.com/wysiwyg-editor?pb=1\" title=\"Froala Editor\">Froala Editor</a></p>', 0, 0, 0, 0, 0, NULL, '', '', '', '', '', 'E', NULL, 1585724564, 1585724564),
(8, 9, 17, 11, 1, '02451c31b7b2489a9e5e108dec7b14ca.jpg', 'image/jpeg', '<p>film apakah ini?</p><p data-f-id=\"pbf\" xss=removed>Powered by <a href=\"https://www.froala.com/wysiwyg-editor?pb=1\" title=\"Froala Editor\">Froala Editor</a></p>', '<p>tipe A</p><p data-f-id=\"pbf\" xss=removed>Powered by <a href=\"https://www.froala.com/wysiwyg-editor?pb=1\" title=\"Froala Editor\">Froala Editor</a></p>', '<p>tipe B</p><p data-f-id=\"pbf\" xss=removed>Powered by <a href=\"https://www.froala.com/wysiwyg-editor?pb=1\" title=\"Froala Editor\">Froala Editor</a></p>', '<p>tipe C</p><p data-f-id=\"pbf\" xss=removed>Powered by <a href=\"https://www.froala.com/wysiwyg-editor?pb=1\" title=\"Froala Editor\">Froala Editor</a></p>', '<p>tipe D</p><p data-f-id=\"pbf\" xss=removed>Powered by <a href=\"https://www.froala.com/wysiwyg-editor?pb=1\" title=\"Froala Editor\">Froala Editor</a></p>', '<p>Tipe E</p><p data-f-id=\"pbf\" xss=removed>Powered by <a href=\"https://www.froala.com/wysiwyg-editor?pb=1\" title=\"Froala Editor\">Froala Editor</a></p>', 0, 0, 0, 0, 0, NULL, 'fc3331c1d959d592f4bdca6bc19aa6ff.png', '2f88171c4922bef9cc270acb93a350b2.png', '455d1842880bfcd9c2d93e9214a64c05.png', '3e84dc20d7ffdc23e977fb94ceed62e4.png', '3b5187dcfca2f79a664462ef816bf9a3.png', 'A', NULL, 1586485720, 1586485720),
(9, 10, 15, 12, 1, '5498d51065230168abd1818f5533a5de.png', 'image/png', '<p>ini halaman apa?</p><p data-f-id=\"pbf\" xss=removed>Powered by <a href=\"https://www.froala.com/wysiwyg-editor?pb=1\" title=\"Froala Editor\">Froala Editor</a></p>', '<p>diagram A</p><p data-f-id=\"pbf\" xss=removed>Powered by <a href=\"https://www.froala.com/wysiwyg-editor?pb=1\" title=\"Froala Editor\">Froala Editor</a></p>', '<p>diagram B</p><p data-f-id=\"pbf\" xss=removed>Powered by <a href=\"https://www.froala.com/wysiwyg-editor?pb=1\" title=\"Froala Editor\">Froala Editor</a></p>', '<p>diagram C</p><p data-f-id=\"pbf\" xss=removed>Powered by <a href=\"https://www.froala.com/wysiwyg-editor?pb=1\" title=\"Froala Editor\">Froala Editor</a></p>', '<p>diagram D</p><p data-f-id=\"pbf\" xss=removed>Powered by <a href=\"https://www.froala.com/wysiwyg-editor?pb=1\" title=\"Froala Editor\">Froala Editor</a></p>', '<p>diagram E</p><p data-f-id=\"pbf\" xss=removed>Powered by <a href=\"https://www.froala.com/wysiwyg-editor?pb=1\" title=\"Froala Editor\">Froala Editor</a></p>', 0, 0, 0, 0, 0, NULL, 'de5e3a362ad9c4b34dd11500c1cbd6ad.png', 'd47a28ff4ee78250272eddb76a936d1a.png', 'ca7caad0970e61e478a9edeb51f312a9.png', '80310a04e4ae5acba380a7aa1496527b.png', 'b167aaffb203caebf6dc12fab029b382.png', 'D', NULL, 1586485911, 1586485911),
(10, 11, 18, 13, 4, '', '', '<p>kursi untuk...</p>', '<p>duduk</p>', '<p>makan</p>', '<p>berdiri</p>', '<p>teriak</p>', '<p>apaan</p>', 0, 0, 0, 0, 0, NULL, '', '', '', '', '', 'A', NULL, 1586611998, 1586612013),
(11, 11, 18, 13, 4, '', '', '<p>meja</p>', '<p>a</p>', '<p>s</p>', '<p>d</p>', '<p>f</p>', '<p>g</p>', 0, 0, 0, 0, 0, NULL, '', '', '', '', '', 'B', NULL, 1586612040, 1586612040),
(12, 12, 23, 15, 1, '', '', '<p>lhjlh</p>', '<p>kjhkj</p>', '<p>kjhkj</p>', '<p>kjh</p>', '<p>hjg</p>', '<p>jhg</p>', 0, 0, 0, 0, 0, NULL, '', '', '', '', '', 'B', NULL, 1586788828, 1586788828),
(13, 12, 23, 17, 0, '', '', '<p>soal tkp</p>', '<p>a</p>', '<p>b</p>', '<p>c</p>', '<p>d</p>', '<p>e</p>', 1, 2, 4, 3, 5, NULL, '', '', '', '', '', 'A', NULL, 1588817085, 1588817085),
(14, 12, 23, 18, 0, '', '', '<p>wwww</p>', '<p>e</p>', '<p>cc</p>', '<p>ddd</p>', '<p>ddd</p>', '<p>dd</p>', 1, 2, 3, 4, 5, NULL, '', '', '', '', '', 'E', NULL, 1589077607, 1589077607),
(15, 14, 21, 19, 0, '', '', '<p>1</p>', '<p>A</p>', '<p>B</p>', '<p>C</p>', '<p>D</p>', '<p>E</p>', 1, 2, 3, 4, 5, NULL, '', '', '', '', '', 'E', NULL, 1589078449, 1589078449),
(16, 14, 21, 19, 0, '', '', '<p>2</p>', '<p>A</p>', '<p>B</p>', '<p>C</p>', '<p>D</p>', '<p>E</p>', 5, 4, 3, 2, 1, NULL, '', '', '', '', '', 'A', NULL, 1589078522, 1589078522),
(17, 15, 16, 20, 0, '', '', '<p>1</p>', '<p>A</p>', '<p>B</p>', '<p>C</p>', '<p>D</p>', '<p>E</p>', 4, -1, -1, -1, -1, 1, '', '', '', '', '', 'null', NULL, 1589081548, 1590735611),
(18, 15, 16, 20, 0, '', '', '<p>2</p>', '<p>A</p>', '<p>B</p>', '<p>C</p>', '<p>D</p>', '<p>R</p>', -1, 4, -1, -1, -1, -1, '', '', '', '', '', 'null', NULL, 1589081624, 1590735624),
(19, 15, 16, 20, 0, '', '', '<p>3</p>', '<p>A</p>', '<p>B</p>', '<p>C</p>', '<p>D</p>', '<p>E</p>', -1, -1, 4, -1, -1, 0, '', '', '', '', '', 'null', NULL, 1589081660, 1590735631),
(20, 15, 16, 20, 0, '', '', '<p>4</p>', '<p>A</p>', '<p>B</p>', '<p>C</p>', '<p>D</p>', '<p>E</p>', -1, -1, -1, 4, -1, 0, '', '', '', '', '', 'null', NULL, 1589081687, 1590735647),
(21, 15, 16, 20, 0, '', '', '<p>5</p>', '<p>A</p>', '<p>B</p>', '<p>C</p>', '<p>D</p>', '<p>E</p>', -1, -1, -1, -1, 4, 0, '', '', '', '', '', 'null', NULL, 1589081718, 1590735654),
(22, 15, 16, 20, 0, '', '', '<p>6</p>', '<p>2</p>', '<p>3</p>', '<p>4</p>', '<p>t</p>', '<p>3</p>', 0, 0, 0, 0, 0, NULL, '', '', '', '', '', 'null', NULL, 1590735591, 1590735591),
(23, 15, 16, 20, 0, '', '', '<p>7</p>', '<p>3</p>', '<p>r</p>', '<p>r</p>', '<p>r</p>', '<p>r</p>', 0, 0, 0, 0, 0, NULL, '', '', '', '', '', 'null', NULL, 1590735755, 1590735755),
(24, 15, 16, 23, 0, '', '', '<p>1</p>', '<p>a</p>', '<p>s</p>', '<p>d</p>', '<p>f</p>', '<p>g</p>', -1, 4, -1, -1, -1, 0, '', '', '', '', '', 'B', NULL, 1590740401, 1590740401),
(25, 15, 16, 23, 0, '', '', '<p>2</p>', '<p>d</p>', '<p>ff</p>', '<p>ggd</p>', '<p>dfg</p>', '<p>gdfg</p>', -1, 4, -1, -1, -1, 0, '', '', '', '', '', 'B', NULL, 1590740427, 1590740427),
(26, 15, 16, 23, 0, '', '', '<p>3</p>', '<p>w</p>', '<p>s</p>', '<p>s</p>', '<p>s</p>', '<p>s</p>', 4, -1, -1, -1, -1, 0, '', '', '', '', '', 'A', NULL, 1590740458, 1590740458),
(27, 15, 16, 23, 0, '', '', '<p>4</p>', '<p>w</p>', '<p>e</p>', '<p>r</p>', '<p>r</p>', '<p>r</p>', -1, -1, -1, 4, -1, -1, '', '', '', '', '', 'D', NULL, 1590740486, 1590740486),
(28, 15, 16, 23, 0, '', '', '<p>5</p>', '<p>we</p>', '<p>e</p>', '<p>rer</p>', '<p>ere</p>', '<p>ere</p>', 4, -1, -1, -1, -1, 0, '', '', '', '', '', 'A', NULL, 1590740517, 1590740517),
(29, 12, 23, 24, 0, '', '', '<p>soal1</p>', '<p>a</p>', '<p>b</p>', '<p>c</p>', '<p>d</p>', '<p>e</p>', 1, 2, 3, 4, 5, NULL, '', '', '', '', '', 'null', '<p>pembahasan 1</p>', 1591334952, 1591334952),
(30, 12, 23, 24, 0, '', '', '<p>soal 2</p>', '<p>a</p>', '<p>b</p>', '<p>c</p>', '<p>d</p>', '<p>e</p>', 1, 2, 3, 4, 5, NULL, '', '', '', '', '', 'null', '<p>pebahasan soal 2</p>', 1591334990, 1591335006),
(31, 12, 23, 24, 0, '', '', '<p>soal 3</p>', '<p>a</p>', '<p>b</p>', '<p>c</p>', '<p>d</p>', '<p>e</p>', 1, 2, 3, 4, 5, NULL, '', '', '', '', '', 'null', '<p>pembahasan 3</p>', 1591335046, 1591335046),
(32, 12, 23, 24, 0, '', '', '<p>soal 4</p>', '<p>a</p>', '<p>b</p>', '<p>c</p>', '<p>d</p>', '<p>e</p>', 1, 2, 3, 4, 5, NULL, '', '', '', '', '', 'null', '<p>pembahasan 4</p>', 1591335083, 1591335083),
(33, 12, 23, 24, 0, '', '', '<p>soal 5</p>', '<p>a</p>', '<p>b</p>', '<p>c</p>', '<p>d</p>', '<p>e</p>', 1, 2, 3, 4, 5, NULL, '', '', '', '', '', 'null', '<p>pembahasan 5</p>', 1591335118, 1591335157),
(34, 14, 21, 25, 0, '', '', '<p>soal 1</p>', '<p>a bobot 1</p>', '<p>b bobot 2</p>', '<p>c bobot 3</p>', '<p>d bobot  4</p>', '<p>e bobot 5</p>', 1, 2, 3, 4, 5, NULL, '', '', '', '', '', 'null', '<p>pembahasan soal 1</p>', 1591581125, 1591581125),
(35, 14, 21, 25, 0, '', '', '<p>soal 2</p>', '<p>a bobot 5</p>', '<p>b bobot 4</p>', '<p>c bobot 3</p>', '<p>d bobot 2</p>', '<p>e bobot 1</p>', 5, 4, 3, 2, 1, NULL, '', '', '', '', '', 'null', '<p>pembasan soal 2</p>', 1591581191, 1591581191),
(36, 14, 21, 27, 0, '', '', '<p>1</p>', '<p>A</p>', '<p>B</p>', '<p>C</p>', '<p>D</p>', '<p>E</p>', 3, 5, 2, 1, 4, NULL, '', '', '', '', '', 'null', '<p>FGGFFHSFHR</p>', 1591762036, 1591762036),
(37, 14, 21, 27, 0, '', '', '<p>2</p>', '<p>A</p>', '<p>B</p>', '<p>C</p>', '<p>D</p>', '<p>E</p>', 5, 4, 3, 2, 1, NULL, '', '', '', '', '', 'null', '<p>FARGARG</p>', 1591762074, 1591762074),
(38, 14, 21, 27, 0, '', '', '<p>3</p>', '<p>A</p>', '<p>S</p>', '<p>D</p>', '<p>GR</p>', '<p>RR</p>', 1, 2, 3, 4, 5, NULL, '', '', '', '', '', 'null', '<p>FGREGRWGW</p>', 1591762100, 1591762100),
(39, 14, 21, 27, 0, '', '', '<p>4</p>', '<p>GRS</p>', '<p>SRGSR</p>', '<p>SR</p>', '<p>RGS</p>', '<p>GS</p>', 5, 4, 3, 2, 1, NULL, '', '', '', '', '', 'null', '<p>GSRGSSH</p>', 1591762134, 1591762134),
(40, 14, 21, 27, 0, '', '', '<p>5</p>', '<p>DA</p>', '<p>W</p>', '<p>FEFS</p>', '<p>SEF</p>', '<p>EFS</p>', 1, 2, 3, 4, 5, NULL, '', '', '', '', '', 'null', '<p>SFESFFSE</p>', 1591762166, 1591762166),
(41, 20, 18, 31, 0, '', '', '<p>KAKA TUA : MERPATI </p>', '<p>Anjing : Herder</p>', '<p>Gurame : Kakap</p>', '<p>Gajah : Semut</p>', '<p>Singa : Naga</p>', '<p>Elang : Kupu-kupu</p>', -1, 4, -1, -1, -1, 0, '', '', '', '', '', 'B', '', 1591908242, 1591908242),
(42, 20, 18, 31, 0, '', '', '<p>BELAJAR : PANDAI </p>', '<p>Cetak : Kertas</p>', '<p>Berpikir : Arif</p>', '<p>Potret : Kamera</p>', '<p>Litografi : Batu</p>', '<p>Cetak : Tinta</p>', -1, 4, -1, -1, -1, 0, '', '', '', '', '', 'B', '<p>Dengan belajar maka akan menjadi Pandai</p>\r\n<p>Dengan berfikir maka akan menjadi arif</p>', 1591908531, 1591908531),
(43, 20, 18, 31, 0, '', '', '<p>KAMPUNG : SAWAH</p>', '<p>Kampus : Perpus</p>', '<p>Kota : Gedung</p>', '<p>Sawah : Padi</p>', '<p>Bumbu : Dapur</p>', '<p>Reserse : Polisi</p>', -1, 4, -1, -1, -1, 0, '', '', '', '', '', 'B', '', 1591908706, 1591908706),
(44, 20, 18, 31, 0, '', '', '<p>JANJI : BUKTI </p>', '<p>Ucapan : Tindakan</p>', '<p>Maling : Penjara</p>', '<p>Materi : Soal</p>', '<p>Harta : Kendaraan</p>', '<p>Tuan : Tuhan</p>', 4, -1, -1, -1, -1, 0, '', '', '', '', '', 'A', '', 1591908853, 1591908853),
(45, 20, 18, 31, 0, '', '', '<p>SUNGAI : JEMBATAN </p>', '<p>Markah : Jalan</p>', '<p>Rintangan : Godaan</p>', '<p>Janji : Tepati</p>', '<p>Kayu : Terbakar</p>', '<p>Masalah : Jalan Keluar</p>', -1, -1, -1, -1, 4, 0, '', '', '', '', '', 'E', '', 1591908979, 1591908979),
(46, 20, 18, 31, 0, '', '', '<p>MATAHARI : TERANG </p>', '<p>Mendidih : Air</p>', '<p>Membeku : Dingin</p>', '<p>Lampu : Sinar</p>', '<p>Air : Hujan</p>', '<p>Api : Panas</p>', -1, -1, -1, -1, 4, 0, '', '', '', '', '', 'E', '', 1591909350, 1591909350),
(47, 20, 18, 31, 0, '', '', '<p>UMUM : LAZIM </p>', '<p>Kurus : Gemuk</p>', '<p>Langsing: Ramping</p>', '<p>Lapar : Haus</p>', '<p>Garam : Asin</p>', '<p>Cinta : Tinta</p>', -1, 4, -1, -1, -1, 0, '', '', '', '', '', 'B', '', 1591930631, 1591930631),
(48, 20, 18, 31, 0, '', '', '<p>SISWA : BELAJAR </p>', '<p>Santri : Garam</p>', '<p>Ayah : Ibu</p>', '<p>Ilmuwan: Meneliti</p>', '<p>Guru : Murid</p>', '<p>Karyawan: Bekerja</p>', -1, -1, 4, -1, -1, 0, '', '', '', '', '', 'C', '', 1591931122, 1591931122),
(49, 20, 18, 31, 0, '', '', '<p>AIR : ES = UAP : ...... </p>', '<p>Air</p>', '<p>Udara</p>', '<p>Basah</p>', '<p>Mendidih</p>', '<p>Awan</p>', 4, -1, -1, -1, -1, 0, '', '', '', '', '', 'A', '', 1591931179, 1591931179),
(50, 20, 18, 31, 0, '', '', '<p>APOTEKER : OBAT </p>', '<p>Pesawat : Penyakit</p>', '<p>Koki : Masakan</p>', '<p>Montir : Rusak</p>', '<p>Mentor : Dril</p>', '<p>Psikiater: Ide</p>', -1, 4, -1, -1, -1, 0, '', '', '', '', '', 'B', '', 1591931270, 1591931270),
(51, 20, 18, 31, 0, '', '', '<p>PILOT : PESAWAT </p>', '<p>Masinis : Kapal</p>', '<p>Kusir : Kereta</p>', '<p>Nelayan : Kapal</p>', '<p>Motor : Truk</p>', '<p>Supir : Mobil</p>', -1, -1, -1, -1, 4, 0, '', '', '', '', '', 'E', '', 1591931350, 1591931350),
(52, 20, 18, 31, 0, '', '', '<p>GELOMBANG : OMBAK </p>', '<p>Gunung : Bukit</p>', '<p>Berenang: Lari</p>', '<p>Danau : Laut</p>', '<p>Nusa : Pulau</p>', '<p>Nadir : Zenit</p>', 4, -1, -1, -1, -1, 0, '', '', '', '', '', 'A', '', 1591931424, 1591931424),
(53, 20, 18, 31, 0, '', '', '<p>DESIBEL : SUARA </p>', '<p>Are : Jarak</p>', '<p>Warna : Merah</p>', '<p>Suhu : Temperatur</p>', '<p>Volt : Listrik</p>', '<p>Kalori : Berat</p>', -1, -1, -1, 4, -1, 0, '', '', '', '', '', 'D', '', 1591931503, 1591931503),
(54, 20, 18, 31, 0, '', '', '<p>KOSONG : HAMPA</p>', '<p>Ubi : Akar</p>', '<p>Ribut : Serak</p>', '<p>Penuh : Sesak</p>', '<p>Siang : Malam</p>', '<p>Cair : Encer</p>', -1, -1, -1, -1, 4, 0, '', '', '', '', '', 'E', '', 1591931596, 1591931596),
(55, 20, 18, 31, 0, '', '', '<p>SUAP : POLITIK </p>', '<p>Menteri : Presiden</p>', '<p>Cabai : Pedas</p>', '<p>Generik : Paten</p>', '<p>Contek : Ujian</p>', '<p>Pemilu : Legislatif</p>', -1, -1, -1, 4, -1, 0, '', '', '', '', '', 'D', '', 1591931667, 1591931667),
(56, 20, 18, 31, 0, '', '', '<p>SARUNG TANGAN : PETINJU = MIKROSKOP : ... </p>', '<p>Analis</p>', '<p>Dokter</p>', '<p>Optalmolog</p>', '<p>Bakteriolog</p>', '<p>Apoteker</p>', -1, -1, -1, 4, -1, 0, '', '', '', '', '', 'D', '', 1591931755, 1591931755),
(57, 20, 18, 31, 0, '', '', '<p>MATAHARI : BUMI = BUMI : ... </p>', '<p>Gravitasi</p>', '<p>Bulan</p>', '<p>Planet</p>', '<p>Matahari</p>', '<p>BIntang</p>', -1, 4, -1, -1, -1, 0, '', '', '', '', '', 'B', '', 1591931814, 1591931814),
(58, 20, 18, 31, 0, '', '', '<p>SERUT : KAYU </p>', '<p>Karyawan : Perusahaan</p>', '<p>Cangkul : Kebun</p>', '<p>Cek : Batal</p>', '<p>Swasta : Industri</p>', '<p>Ladang : Sawah</p>', -1, 4, -1, -1, -1, 0, '', '', '', '', '', 'B', '', 1591931886, 1591931886),
(59, 20, 18, 31, 0, '', '', '<p>AIR : HAUS</p>', '<p>Angin : Panas</p>', '<p>Makanan: Lapar</p>', '<p>Rumput : Kambing</p>', '<p>Gelap : Lampu</p>', '<p>Minyak : Api</p>', -1, 4, -1, -1, -1, 0, '', '', '', '', '', 'B', '', 1591931958, 1591931958),
(60, 20, 18, 31, 0, '', '', '<p>PELUKIS : GAMBAR </p>', '<p>Koki : Restoran</p>', '<p>Penyanyi: Lagu</p>', '<p>Penyair : Puisi</p>', '<p>Komponis: Lagu</p>', '<p>Kartunis : Pena</p>', -1, -1, -1, 4, -1, 0, '', '', '', '', '', 'D', '', 1591932033, 1591932033),
(61, 20, 18, 31, 0, '', '', '<p>PETUNJUK : AFFIRMASI </p>', '<p>Menolak : Melawan</p>', '<p>Didenda : Ditahan</p>', '<p>Setuju : Berkata</p>', '<p>Relasi : Keadaan</p>', '<p>Rejeksi : Konfirmasi</p>', -1, 4, -1, -1, -1, 0, '', '', '', '', '', 'B', '', 1591932177, 1591932177),
(62, 20, 18, 31, 0, '', '', '<p>KAKI : SEPATU </p>', '<p>Cat : Kuas</p>', '<p>Meja : Ruangan</p>', '<p>Telinga : Anting</p>', '<p>Cincin : Jari</p>', '<p>Topi : Kepala</p>', -1, -1, 4, -1, -1, 0, '', '', '', '', '', 'C', '', 1591932363, 1591932363),
(63, 20, 18, 31, 0, '', '', '<p>KORAN : MAKALAH : BULETIN </p>', '<p>Restoran : Hotel : Losmen</p>', '<p>Cat : Kuas : Lukisan</p>', '<p>Sandal : Sepatu : Kaos</p>', '<p>Air : Roti : Singkong</p>', '<p>Bus : Kereta Api : Delman</p>', -1, -1, -1, -1, 4, 0, '', '', '', '', '', 'E', '', 1591932430, 1591932430),
(64, 20, 18, 31, 0, '', '', '<p>BUSUR : GARIS </p>', '<p>Terbenam : Terbit</p>', '<p>Tangkap : Lempar</p>', '<p>Tombak : Busur</p>', '<p>Busur : Panah</p>', '<p>Relatif : Absolut</p>', -1, -1, -1, 4, -1, 0, '', '', '', '', '', 'D', '', 1591932536, 1591932536),
(65, 20, 18, 31, 0, '', '', '<p>TEMBAKAU : ROKOK : ISAP </p>', '<p>Gandum: Roti : Makan</p>', '<p>Kulit : Sepatu : Kaki</p>', '<p>Plastik : Sisir : Rambut</p>', '<p>Beras : Nasi : Jemur</p>', '<p>Teh : Susu : Minum</p>', 4, -1, -1, -1, -1, 0, '', '', '', '', '', 'A', '', 1591932618, 1591932618),
(66, 20, 18, 31, 0, '', '', '<p>MULUT : MONYONG </p>', '<p>Leher : Jenjang</p>', '<p>Mata : Hitam</p>', '<p>Dahi : Muka</p>', '<p>Hidung : Pesek</p>', '<p>Pipi : Merah</p>', -1, -1, -1, 4, -1, 0, '', '', '', '', '', 'D', '', 1591933951, 1591933951),
(67, 20, 18, 31, 0, '', '', '<p>PENGHORMATAN : JASA = INTENSIF : ... </p>', '<p>Piagam</p>', '<p>Pensiun</p>', '<p>Hadiah</p>', '<p>Prestasi</p>', '<p>Lembur</p>', -1, -1, -1, 4, -1, 0, '', '', '', '', '', 'D', '', 1591934011, 1591934011),
(68, 20, 18, 31, 0, '', '', '<p>TUBUH : PAKAIAN </p>', '<p>Kurva : Alam</p>', '<p>Lidi : Sapu</p>', '<p>Meja : Kotak</p>', '<p>Buku : Sampul</p>', '<p>Jalan : Garis</p>', -1, -1, -1, 4, -1, 0, '', '', '', '', '', 'D', '', 1591934094, 1591934094),
(69, 20, 18, 31, 0, '', '', '<p>AIR : MENGUAP </p>', '<p>Es : Mencair</p>', '<p>Panas : Memuai</p>', '<p>Jatuh : Pecah</p>', '<p>Uap : Hujan</p>', '<p>Laut : Mendung</p>', 4, -1, -1, -1, -1, 0, '', '', '', '', '', 'A', '', 1591934183, 1591934183),
(70, 20, 18, 31, 0, '', '', '<p>GUNDUL : RAMBUT </p>', '<p>Bugil : Pakaian</p>', '<p>Keramik : Lantai</p>', '<p>Kepala : Botak</p>', '<p>Mogok : Mobil</p>', '<p>Rambut : Bulu</p>', 4, -1, -1, -1, -1, 0, '', '', '', '', '', 'A', '', 1591934258, 1591934258),
(71, 20, 18, 31, 0, '', '', '<p>SENAPAN : BERBURU</p>', '<p>Kapal : berlabuh</p>', '<p>Kereta : langsir</p>', '<p>Pancing : ikan</p>', '<p>Perangakap : menangkap</p>', '<p>Parang : mengasah</p>', -1, -1, -1, 4, -1, 0, '', '', '', '', '', 'D', '', 1591934339, 1591934339),
(72, 20, 18, 31, 0, '', '', '<p>BATA : TANAH LIAT</p>', '<p>Batu : pasir</p>', '<p>Tembikar : keramik</p>', '<p>Bunga : buah</p>', '<p>Beton : semen</p>', '<p>Kertas :buku</p>', 0, 0, 0, 4, 0, -2, '', '', '', '', '', 'D', '', 1591934425, 1591934425),
(73, 20, 18, 31, 0, '', '', '<p>KENDARAAN : MOBIL </p>', '<p>Gaji : Upah</p>', '<p>Kapal : Perahu</p>', '<p>Binatang: Lawan</p>', '<p>Orang : Pemuda</p>', '<p>Laut : Danau</p>', -1, -1, -1, 4, -1, 0, '', '', '', '', '', 'D', '', 1591934506, 1591934506),
(74, 20, 18, 31, 0, '', '', '<p>GAMBAR : PELUKIS </p>', '<p>Lagu : Penyanyi</p>', '<p>Restoran : Koki</p>', '<p>Pena : Kartunis</p>', '<p>Lagu : Komponis</p>', '<p>Puisi : Penyair</p>', -1, -1, -1, 4, -1, 0, '', '', '', '', '', 'D', '', 1591934576, 1591934576),
(75, 20, 18, 31, 0, '', '', '<p>BANGSA : ETHNOLOGI </p>', '<p>Demonstrasi : Demografi</p>', '<p>Bumi : Geografi</p>', '<p>Planet : Astronomi</p>', '<p>Penyakit : Patologi</p>', '<p>Alam : Biologi</p>', -1, -1, -1, 4, -1, 0, '', '', '', '', '', 'D', '', 1591934836, 1591934836),
(76, 20, 18, 31, 0, '', '', '<p>SEMINAR : SARJANA </p>', '<p>Seleksi : Pegawai</p>', '<p>Koneurvator : Seniman</p>', '<p>Ruang Pengadilan : Saksi</p>', '<p>Rumah Sakit : Pasien</p>', '<p>Akademi : Taruna</p>', 4, -1, -1, -1, -1, 0, '', '', '', '', '', 'A', '', 1591934904, 1591934904),
(77, 20, 18, 31, 0, '', '', '<p>KANTUK : KEPENATAN = ….</p>', '<p>Mimpi : Tidur</p>', '<p>Marah : Kegeraman</p>', '<p>Muka : Ekspresi</p>', '<p>Senyum : Kegembiraan</p>', '<p>Mata : Melihat</p>', -1, -1, -1, 4, -1, 0, '', '', '', '', '', 'D', '', 1591935025, 1591935025),
(78, 20, 18, 31, 0, '', '', '<p>LABA : PENJUALAN = KESUKSESAN : ... </p>', '<p>Toleransi</p>', '<p>Kebenaran</p>', '<p>Pendekar</p>', '<p>Usaha</p>', '<p>Pembelaan</p>', -1, -1, -1, 4, -1, 0, '', '', '', '', '', 'D', '', 1591935102, 1591935102),
(79, 20, 18, 31, 0, '', '', '<p>TIANG : KOKOH </p>', '<p>Dinding : Fondasi</p>', '<p>Teras : Hiasan</p>', '<p>Lantai : Marmer</p>', '<p>Atap : Terlindung</p>', '<p>Genting : Tanah</p>', -1, -1, -1, 4, -1, 0, '', '', '', '', '', 'D', '', 1591935163, 1591935163),
(80, 20, 18, 31, 0, '', '', '<p>MERAH : MAWAR </p>', '<p>Hijau : Jati</p>', '<p>Kuning : Apel</p>', '<p>Putih : Melati</p>', '<p>Hitam : Tebu</p>', '<p>Salmon : Ikan</p>', -1, -1, 4, -1, -1, 0, '', '', '', '', '', 'C', '', 1591935245, 1591935245),
(81, 20, 18, 31, 0, '', '', '<p>TAJAM : TUMPUL </p>', '<p>Muka : Mata</p>', '<p>Palu : Pukul</p>', '<p>Pulsa : Ponsel</p>', '<p>Dekat : Jauh</p>', '<p>Sukar : Sulit</p>', -1, -1, -1, 4, -1, 0, '', '', '', '', '', 'D', '', 1591950648, 1591950648),
(82, 20, 18, 31, 0, '', '', '<p>LAPAR : NASI </p>', '<p>Membaca : Menggambar</p>', '<p>Bosan : Tidur</p>', '<p>Mengantuk : Melamun</p>', '<p>Mual : Obat</p>', '<p>Haus : Air</p>', -1, -1, -1, -1, 4, 0, '', '', '', '', '', 'E', '', 1591950724, 1591950724),
(83, 20, 18, 31, 0, '', '', '<p>KULIT : SISIK </p>', '<p>Atap : Jantung</p>', '<p>Dinding : Cat</p>', '<p>Keramik : Mozaik</p>', '<p>Rumah : Kamar</p>', '<p>Tegel : Lantai</p>', -1, 4, -1, -1, -1, 0, '', '', '', '', '', 'B', '', 1591950789, 1591950789),
(84, 20, 18, 31, 0, '', '', '<p>MARAH : CEMBURU </p>', '<p>Pemerintah : persamaan</p>', '<p>Beras : Gula</p>', '<p>Integrasi : Persatuan</p>', '<p>Tidak toleransi : Fanatik</p>', '<p>Pahit : Manis</p>', -1, -1, -1, 4, -1, 0, '', '', '', '', '', 'D', '', 1591950859, 1591950859),
(85, 20, 18, 31, 0, '', '', '<p>MATA : TELINGA </p>', '<p>Perut : Dada</p>', '<p>Lutut : Siku</p>', '<p>Kaki : Paha</p>', '<p>Lidah : Hidung</p>', '<p>Jari : Tangan</p>', -1, -1, -1, 4, -1, 0, '', '', '', '', '', 'D', '', 1591950924, 1591950924),
(86, 20, 18, 31, 0, '', '', '<p>PEDAS : CABAI = MANIS : ... </p>', '<p>Kecap</p>', '<p>Sakarin</p>', '<p>Teh Botol</p>', '<p>Manisan</p>', '<p>Gadis</p>', -1, 4, -1, -1, -1, 0, '', '', '', '', '', 'B', '', 1591950972, 1591950972),
(87, 20, 18, 31, 0, '', '', '<p>RAMALAN : ASTROLOGI = BANGSA : ...</p>', '<p>Etnologi</p>', '<p>Psikologi</p>', '<p>Demografi</p>', '<p>Antropologi</p>', '<p>Sosiologi</p>', 4, -1, -1, -1, -1, 0, '', '', '', '', '', 'A', '', 1591951036, 1591951036),
(88, 20, 18, 31, 0, '', '', '<p>SUARA : DESIBEL </p>', '<p>Temperatur : Suhu</p>', '<p>Berat : Kalori</p>', '<p>Jarak : Meter</p>', '<p>Merah : Warna</p>', '<p>Listrik : Volt</p>', -1, -1, -1, -1, 4, 0, '', '', '', '', '', 'E', '', 1591951114, 1591951114),
(89, 20, 18, 31, 0, '', '', '<p>LAMPU : GELAP = MAKANAN : ...</p>', '<p>Lapar</p>', '<p>Mulas</p>', '<p>Gizi</p>', '<p>Penuh</p>', '<p>Kenyang</p>', 4, -1, -1, -1, -1, 0, '', '', '', '', '', 'A', '', 1591951179, 1591951179),
(90, 20, 18, 31, 0, '', '', '<p>BUNGA : BUKET </p>', '<p>Pigmen : Mata</p>', '<p>Balon : Udara</p>', '<p>Pintu : Kunci</p>', '<p>Tubuh : Kulit</p>', '<p>Kertas : Buku</p>', -1, -1, -1, -1, 4, 0, '', '', '', '', '', 'E', '', 1591951257, 1591951257),
(91, 20, 18, 31, 0, '', '', '<p>RUMPUT : LAPANGAN = BINTANG : ...</p>', '<p>Antariksa</p>', '<p>Malam</p>', '<p>Angkasa</p>', '<p>Langit</p>', '<p>Nebula</p>', -1, -1, -1, 4, -1, 0, '', '', '', '', '', 'D', '', 1591951321, 1591951321),
(92, 20, 18, 31, 0, '', '', '<p>ULAT : KEPOMPONG : KUPU-KUPU </p>', '<p>Kecil : Sedang : Besar</p>', '<p>Sore : Siang : Pagi</p>', '<p>Ngantuk : Tidur : Mimpi</p>', '<p>Bayi : Kanak-Kanak : Remaja</p>', '<p>Anak : Ayah : Kakek</p>', -1, -1, -1, 4, -1, 0, '', '', '', '', '', 'D', '', 1591951388, 1591951388),
(93, 20, 18, 31, 0, '', '', '<p>TUKANG : GERGAJI : PALU </p>', '<p>Guru : Kapur : Topi</p>', '<p>Penari : Topi : Selendang</p>', '<p>Montir : Obeng : Tang</p>', '<p>Penulis : Kuas : Cat</p>', '<p>Tukang : Obat : Stetoskop</p>', -1, -1, 4, -1, -1, 0, '', '', '', '', '', 'C', '', 1591951451, 1591951451),
(94, 20, 18, 31, 0, '', '', '<p>BURUNG : TERBANG : JATUH</p>', '<p>Makanan : Nasi : Meja</p>', '<p>Rokok : Tembakau : Asap</p>', '<p>Kuas : Cat : Tembok</p>', '<p>Ibu : Memasak : Gosong</p>', '<p>Ikan : Berenang : Tenggelam</p>', -1, -1, -1, -1, 4, 0, '', '', '', '', '', 'E', '', 1591951518, 1591951518),
(95, 20, 18, 31, 0, '', '', '<p>MACAN : BELANG </p>', '<p>Bangun : Tidur</p>', '<p>Posisi : Tempat</p>', '<p>Virus : Bakteri</p>', '<p>Gajah : Gading</p>', '<p>Racun : Maut</p>', -1, -1, -1, 4, -1, 0, '', '', '', '', '', 'D', '', 1591951589, 1591951589),
(96, 20, 18, 31, 0, '', '', '<p>SUSU : GELAS</p>', '<p>Makan : Orang</p>', '<p>Bubur : Piring</p>', '<p>Nasi : Beras</p>', '<p>Bulat : Telor</p>', '<p>Teh : Sendok</p>', -1, 4, -1, -1, -1, 0, '', '', '', '', '', 'B', '', 1591951663, 1591951663),
(97, 20, 18, 31, 0, '', '', '<p>BUNGA : TAMAN </p>', '<p>Pohon : Ranting</p>', '<p>Murid : PR</p>', '<p>Dokter : Pasien</p>', '<p>Sekretaris: Komputer</p>', '<p>Dosen : Universitas</p>', -1, -1, -1, -1, 4, 0, '', '', '', '', '', 'E', '', 1591951724, 1591951724),
(98, 20, 18, 31, 0, '', '', '<p>PADI : PETANI </p>', '<p>Penyair : Puisi</p>', '<p>Dokter : Obat</p>', '<p>Dukun : Jamu</p>', '<p>Sawah : Sapi</p>', '<p>Matahari: Panas</p>', 4, -1, -1, -1, -1, 0, '', '', '', '', '', 'A', '', 1591951792, 1591951792),
(99, 20, 18, 31, 0, '', '', '<p>PRESIDEN : NEGARA </p>', '<p>Ayah : Keluarga</p>', '<p>Kuping : Anting</p>', '<p>Kepala : Rambut</p>', '<p>RT : RW</p>', '<p>Menteri : Instruksi</p>', 4, -1, -1, -1, -1, 0, '', '', '', '', '', 'A', '', 1591951933, 1591951933),
(100, 20, 18, 31, 0, '', '', '<p>TELUK : LAUT </p>', '<p>Semenanjung : Daratan</p>', '<p>Kapal : Pelabuhan</p>', '<p>Karang : Tanjung</p>', '<p>Selat : Pulau</p>', '<p>Sepeda : Pedal</p>', 4, -1, -1, -1, -1, 0, '', '', '', '', '', 'A', '', 1591951996, 1591951996),
(101, 20, 18, 31, 0, '', '', '<p>ADAGIO : ALLEGRO </p>', '<p>Binatang: Singa</p>', '<p>Lambat : Cepat</p>', '<p>Putih : Merah</p>', '<p>Akal : Orang</p>', '<p>Rakus : Babi</p>', -1, 4, -1, -1, -1, 0, '', '', '', '', '', 'B', '', 1591952057, 1591952057),
(102, 20, 18, 31, 0, '', '', '<p>TAPE : RAGI </p>', '<p>Kuman : Penicilin</p>', '<p>Antiseptik : Iodium</p>', '<p>Pasta gigi : Flour</p>', '<p>Minuman : Soda</p>', '<p>Obat : Aspirin</p>', -1, 4, -1, -1, -1, 0, '', '', '', '', '', 'B', '', 1591952122, 1591952122),
(103, 20, 18, 31, 0, '', '', '<p>KOMPOR : API </p>', '<p>Pohon : Buah</p>', '<p>Kipas : Angin</p>', '<p>Jalan : Macet</p>', '<p>Lemari : Es</p>', '<p>Palung : Gua</p>', -1, 4, -1, -1, -1, 0, '', '', '', '', '', 'B', '', 1591952218, 1591952218),
(104, 20, 18, 31, 0, '', '', '<p>BAWANG : SIUNG </p>', '<p>Telur : Butir</p>', '<p>Buku : Lembar</p>', '<p>Kain : Meter</p>', '<p>Pakaian : Kodi</p>', '<p>Kertas : Kilogram</p>', 4, -1, -1, -1, -1, 0, '', '', '', '', '', 'A', '', 1591952283, 1591952283),
(105, 20, 18, 31, 0, '', '', '<p>BELAJAR : BODOH</p>', '<p>Lapar : Kenyang</p>', '<p>Rajin : Malas</p>', '<p>Litografi : Batu</p>', '<p>Berpikir : Arif</p>', '<p>Bekerja : Miskin</p>', -1, -1, -1, -1, 4, 0, '', '', '', '', '', 'E', '', 1591952363, 1591952363),
(106, 20, 18, 31, 0, '', '', '<p>PIANO : ORGAN </p>', '<p>Drum : Gitar</p>', '<p>Kunci : Pedal</p>', '<p>Senar : Pipa</p>', '<p>Nada : Not</p>', '<p>Gitar : Perkusi</p>', -1, -1, 4, -1, -1, 0, '', '', '', '', '', 'C', '', 1591952430, 1591952430),
(107, 20, 18, 31, 0, '', '', '<p>NELAYAN : PERAHU </p>', '<p>Petani : Traktor</p>', '<p>Koki : Oven</p>', '<p>Dosen : Kelas</p>', '<p>Penulis : Pena</p>', '<p>Fotografer : Kamera</p>', 4, -1, -1, -1, -1, 0, '', '', '', '', '', 'A', '', 1591952499, 1591952499),
(108, 20, 18, 31, 0, '', '', '<p>PANTAI : RESOR </p>', '<p>Bukit : Laut</p>', '<p>Hujan : Basah</p>', '<p>Gunung : Vila</p>', '<p>Laut : Pesiar</p>', '<p>Gunung : Bukit</p>', -1, -1, 4, -1, -1, 0, '', '', '', '', '', 'C', '', 1591952558, 1591952558),
(109, 20, 18, 31, 0, '', '', '<p>HUMOR : KOMEDIAN </p>', '<p>Klimaks : Drama</p>', '<p>Memori : Amnesia</p>', '<p>Perceraian : Pernikahan</p>', '<p>Heroisme : Epik</p>', '<p>Sintaksis : Kata</p>', -1, -1, -1, 4, -1, 0, '', '', '', '', '', 'D', '', 1591952618, 1591952618),
(110, 20, 18, 31, 0, '', '', '<p>GEOLOGI : ILMU </p>', '<p>Kimia : Senyawa</p>', '<p>Teori : Praktek</p>', '<p>Biologi : Laboratorium</p>', '<p>Beringin : Pohon</p>', '<p>Astronomi : Galaksi</p>', -1, -1, -1, 4, -1, 0, '', '', '', '', '', 'D', '', 1591952701, 1591952701),
(111, 20, 18, 31, 0, '', '', '<p>BUNGA : RIBA </p>', '<p>Hemat : Kikir</p>', '<p>Akrab : Sengit</p>', '<p>Mawar : Hutang</p>', '<p>Hasrat : Extravaganza</p>', '<p>Khawatir : Bangkrut</p>', 4, -1, -1, -1, -1, 0, '', '', '', '', '', 'A', '', 1591952768, 1591952768),
(112, 20, 18, 31, 0, '', '', '<p>UMUR : TAHUN </p>', '<p>Ilmu : Nilai</p>', '<p>Jam : Detik</p>', '<p>Kecepatan : Jam</p>', '<p>Kertas : Helai</p>', '<p>Air : Minum</p>', -1, -1, -1, 4, -1, 0, '', '', '', '', '', 'D', '', 1591952849, 1591952849),
(113, 20, 18, 31, 0, '', '', '<p>INTENSITAS : FREKUENSI </p>', '<p>Hadiah : Pengabdian</p>', '<p>Penghargaan : Penghormatan</p>', '<p>Jauh : Jarak</p>', '<p>Jumlah : Total</p>', '<p>Hak : Kewajiban</p>', -1, 4, -1, -1, -1, 0, '', '', '', '', '', 'B', '', 1591952919, 1591952919),
(114, 20, 18, 31, 0, '', '', '<p>MOBIL : BENSIN </p>', '<p>Pesawat terbang : Propeler</p>', '<p>Motor : Solar</p>', '<p>Manusia : Makan</p>', '<p>Sapi : Susu</p>', '<p>Penyakit : Virus</p>', -1, -1, 4, -1, -1, 0, '', '', '', '', '', 'C', '', 1591953003, 1591953003),
(115, 20, 18, 31, 0, '', '', '<p>BAIT : PUISI </p>', '<p>Drama : Epos</p>', '<p>Sajak : Prosa</p>', '<p>Loteng : Bangunan</p>', '<p>Hiasan dinding : Lukisan</p>', '<p>Bendera : Lagu kebangsaan</p>', -1, -1, 4, -1, -1, 0, '', '', '', '', '', 'C', '', 1591953090, 1591953090),
(116, 20, 18, 31, 0, '', '', '<p>MENCURI : MENYESAL</p>', '<p>Menyakiti : Menangis</p>', '<p>Mencontek : Menghukum</p>', '<p>Menyakiti : Menangis</p>', '<p>Makan : Lapar</p>', '<p>Menanam : Menyiang</p>', -1, -1, -1, -1, 4, 0, '', '', '', '', '', 'E', '', 1591953167, 1591953167),
(117, 20, 18, 31, 0, '', '', '<p>AIR : MINYAK </p>', '<p>Rajin : Pandai</p>', '<p>Elang : Ayam</p>', '<p>Anjing : Kucing</p>', '<p>Gula : Kopi</p>', '<p>Pintar : Bodoh</p>', -1, -1, 4, -1, -1, 0, '', '', '', '', '', 'C', '', 1591953241, 1591953241),
(118, 20, 18, 31, 0, '', '', '<p>DANA : PENGGELAPAN </p>', '<p>Deposito : Perbankan</p>', '<p>Cek : Akuntansi</p>', '<p>Kuitansi : Saldo</p>', '<p>Karya tulis : Penjiplakan</p>', '<p>Uang : Brankas</p>', -1, -1, -1, 4, -1, 0, '', '', '', '', '', 'D', '', 1591953309, 1591953309),
(119, 20, 18, 31, 0, '', '', '<p>ABRASI : HEMPASAN </p>', '<p>Memasak : Panas</p>', '<p>Rayuan : Pujian</p>', '<p>Reruntuhan : Penghancuran</p>', '<p>Serpihan : Potongan</p>', '<p>Matahari : Hujan</p>', -1, -1, 4, -1, -1, 0, '', '', '', '', '', 'C', '', 1591953373, 1591953373),
(120, 20, 18, 31, 0, '', '', '<p>DOKTOR : DISERTASI </p>', '<p>Kyai : Jamaah</p>', '<p>Buruh : Upah</p>', '<p>Sarjana : Skripsi</p>', '<p>Kuliah : Praktikum</p>', '<p>Menteri : Kepmen</p>', -1, -1, 4, -1, -1, 0, '', '', '', '', '', 'C', '', 1591953473, 1591953473),
(121, 20, 18, 31, 0, '', '', '<p>ACEH : SERAMBI MEKAH </p>', '<p>Madona : Seksi</p>', '<p>Obama : Presiden</p>', '<p>Michael Schumacer : Pembalap</p>', '<p>Michael Jackson : King of pop</p>', '<p>Michael Gorbachev : Pemimpin Soviet</p>', -1, -1, -1, 4, -1, 0, '', '', '', '', '', 'D', '', 1591953540, 1591953540),
(122, 20, 18, 31, 0, '', '', '<p>KARDIOLOGI : JANTUNG </p>', '<p>Biologi : Ilmu</p>', '<p>Farmakologi : Obat-obatan</p>', '<p>Ternologi : Alat</p>', '<p>Akutansi : Perusahaan</p>', '<p>Phatologi : Peta</p>', -1, 4, -1, -1, -1, 0, '', '', '', '', '', 'B', '', 1591953608, 1591953608),
(123, 20, 18, 31, 0, '', '', '<p>DISELESAIKAN : RAGU </p>', '<p>Diumumkan : Pencalonan</p>', '<p>Dikonfirmasi : Curiga</p>', '<p>Memulai : Mengakhiri</p>', '<p>Dimasukkan : Tamu</p>', '<p>Disarankan : Ide</p>', -1, 4, -1, -1, -1, 0, '', '', '', '', '', 'B', '', 1591953791, 1591953791),
(124, 20, 18, 31, 0, '', '', '<p>GEMPA : RICHTER </p>', '<p> Ombak : Knot</p>', '<p>Jarak : Dinamo</p>', '<p>Obat : Dosis</p>', '<p>Suhu : Fahrenheit</p>', '<p>Banjir : Air</p>', -1, -1, -1, 4, -1, 0, '', '', '', '', '', 'D', '', 1591953861, 1591953861),
(125, 20, 18, 31, 0, '', '', '<p>SEPATU : JALAN </p>', '<p>Buku : Baca</p>', '<p>Pensil : Makan</p>', '<p>Sisir : Rambut</p>', '<p>Garpu : Makan</p>', '<p>Sandal : Jepit</p>', -1, -1, -1, 4, -1, 0, '', '', '', '', '', 'D', '', 1591954099, 1591954099),
(126, 20, 18, 31, 0, '', '', '<p>SEKOLAH : SISWA : BELAJAR </p>', '<p>Sekolah : Guru : Rapat</p>', '<p>Laboratorium : Ilmuan : Meneliti</p>', '<p>Rumah : Ayah : Ibu</p>', '<p>Dokter : Pasien : Periksa</p>', '<p>Kantin : Makan : Siswa</p>', -1, 4, -1, -1, -1, 0, '', '', '', '', '', 'B', '', 1591954504, 1591954504),
(127, 20, 18, 31, 0, '', '', '<p>MURID : BUKU : PERPUSTAKAAN </p>', '<p>Anak : Kelereng : Rumah</p>', '<p>Nasabah : Uang : Bank</p>', '<p>Orang tua : Anak : Ibu</p>', '<p>Pembeli : Makanan : Gudang</p>', '<p>Dosen : Mahasiswa : Kuliah</p>', -1, 4, -1, -1, -1, 0, '', '', '', '', '', 'B', '', 1591954581, 1591954581),
(128, 20, 18, 31, 0, '', '', '<p>TURBIN : AIR </p>', '<p>Tekstil : pabrik</p>', '<p>Money : bank</p>', '<p>Telepon : genggam</p>', '<p>Senter : batu baterai</p>', '<p>Sarjana : wisuda</p>', -1, -1, -1, 4, -1, 0, '', '', '', '', '', 'D', '', 1591954641, 1591954641),
(129, 20, 18, 31, 0, '', '', '<p>GANDUM : TEPUNG : KUE </p>', '<p>Padi : Beras : Nasi</p>', '<p>Nasi : Bubur : Bayi</p>', '<p>Air : Beku : Es</p>', '<p>Wortel : Tomat : Sayur</p>', '<p>Jeruk : Mangga : Buah</p>', 4, -1, -1, -1, -1, 0, '', '', '', '', '', 'A', '', 1591954715, 1591954715),
(130, 20, 18, 31, 0, '', '', '<p>FILM : SUTRADARA = ..... : PENYAIR</p>', '<p>Lagu</p>', '<p>Lukisan</p>', '<p>Puisi</p>', '<p>Skripsi</p>', '<p>Disertasi</p>', -1, -1, 4, -1, -1, 0, '', '', '', '', '', 'C', '', 1591954782, 1591954782),
(131, 20, 18, 31, 0, '', '', '<p>SAPI : HERBIVORA : MELAHIRKAN = AYAM : ..... : ..... </p>', '<p>Rumput : Omnivora</p>', '<p>karnivora : Bertelur</p>', '<p>Herbivora : Susu</p>', '<p>Karnivora : Beranak</p>', '<p>Omnivora : Bertelur</p>', -1, -1, -1, -1, 4, 0, '', '', '', '', '', 'E', '', 1591954868, 1591954868),
(132, 20, 18, 31, 0, '', '', '<p>SOSIOLOGI : ILMU = MONOPOLI : .....</p>', '<p>Kekuasaan</p>', '<p>Main</p>', '<p>Strategi</p>', '<p>Perekonomian</p>', '<p>Pertanian</p>', -1, -1, 4, -1, -1, 0, '', '', '', '', '', 'C', '', 1591954918, 1591954918),
(133, 20, 18, 31, 0, '', '', '<p>SISWA : GURU : SEKOLAH = PASIEN : ..... : .....</p>', '<p>Obat : Apotek</p>', '<p>Obat : Dokter</p>', '<p>Dokter : Rumah Sakit</p>', '<p>Rumah Sakit : Obat</p>', '<p>Apotek : Rumah Sakit</p>', -1, -1, 4, -1, -1, 0, '', '', '', '', '', 'C', '', 1591954983, 1591954983),
(134, 20, 18, 31, 0, '', '', '<p>TIDUR : NGANTUK = ..... : .....</p>', '<p>Istirahat : Lelah</p>', '<p>Melati : Bunga</p>', '<p>Kaki : Sepatu</p>', '<p>Minum : Air</p>', '<p>Kampus : Kuliah</p>', 4, -1, -1, -1, -1, 0, '', '', '', '', '', 'A', '', 1591955050, 1591955050),
(135, 20, 18, 31, 0, '', '', '<p>TUNTUNAN : SANTUNAN </p>', '<p>Undian : hadiah</p>', '<p>Permintaan : pemberian</p>', '<p>Persediaan : penawaran</p>', '<p>Rusak : ganti rugi</p>', '<p>Tabungan : pembelanjaan</p>', -1, 4, -1, -1, -1, 0, '', '', '', '', '', 'B', '', 1591955255, 1591955255),
(136, 20, 18, 31, 0, '', '', '<p>DIAMETER : LINGKARAN </p>', '<p>Diagonal : segiempat</p>', '<p>Luas : panjang</p>', '<p>Tinggi : segitiga</p>', '<p>sudut : tegak lurus</p>', '<p>Radius : busur</p>', 4, -1, -1, -1, -1, 0, '', '', '', '', '', 'A', '', 1591955316, 1591955316),
(137, 20, 18, 31, 0, '', '', '<p>KARATE : BELA DIRI = BALET : .....</p>', '<p>Sepatu</p>', '<p>Tari</p>', '<p>Indah</p>', '<p>Panggung</p>', '<p>Gaun</p>', -1, 4, -1, -1, -1, 0, '', '', '', '', '', 'B', '', 1591955408, 1591957085),
(138, 20, 18, 31, 0, '', '', '<p>PEDATI : KUDA = HELIKOPTER : ...... </p>', '<p>Sayap</p>', '<p>Baling-baling</p>', '<p>Pramugari</p>', '<p>Pilot</p>', '<p>Landasan</p>', -1, 4, -1, -1, -1, 0, '', '', '', '', '', 'B', '', 1591955408, 1591957122),
(139, 20, 18, 31, 0, '', '', '<p>ANTISEPTIK : KUMAN = SENAPAN</p>', '<p>Peluru</p>', '<p>Sirkus</p>', '<p>Menerkam</p>', '<p>Rusa</p>', '<p>Binatang</p>', -1, -1, -1, 4, -1, 0, '', '', '', '', '', 'D', '', 1591955440, 1591955440),
(140, 20, 18, 31, 0, '', '', '<p>SAPU : BERSIH = </p>', '<p>Lap : Kering</p>', '<p>Babi : Rakus</p>', '<p>Uang : Bank</p>', '<p>Tubuh : Kulit</p>', '<p>Foto : Bingkai</p>', 4, -1, -1, -1, -1, 0, '', '', '', '', '', 'A', '', 1591955503, 1591955503),
(141, 20, 18, 31, 0, '', '', '<p>MELANGGAR : DIHUKUM = </p>', '<p>Kantuk : Mimpi</p>', '<p>Sakit : Kehujanan</p>', '<p>Bukit : Gunung</p>', '<p>Rajin : Disayang</p>', '<p>Doa : Pendeta</p>', -1, -1, -1, 4, -1, 0, '', '', '', '', '', 'D', '', 1591955606, 1591955606),
(142, 20, 18, 31, 0, '', '', '<p>MASINIS : KERETA = PILOT : .....</p>', '<p>Kerja</p>', '<p>Mobil</p>', '<p>Jalanan</p>', '<p>Pesawat</p>', '<p>Andong</p>', -1, -1, -1, 4, -1, 0, '', '', '', '', '', 'D', '', 1591955674, 1591955674),
(143, 20, 18, 31, 0, '', '', '<p>TANK : TENTARA = ..... : .....</p>', '<p>Militer : Sawah</p>', '<p>Tanah : Subur</p>', '<p>Cangkul : Petani</p>', '<p>Bandara : Pilot</p>', '<p>Traktor : Guru</p>', -1, -1, 4, -1, -1, 9, '', '', '', '', '', 'C', '', 1591955746, 1591955746),
(144, 20, 18, 31, 0, '', '', '<p>JANTUNG : DADA = USUS : .....</p>', '<p>Hati</p>', '<p>Buntu</p>', '<p>Perut</p>', '<p>Halus</p>', '<p>Pankreas</p>', -1, -1, 4, -1, -1, 0, '', '', '', '', '', 'C', '', 1591955793, 1591955793),
(145, 20, 18, 31, 0, '', '', '<p>AIR : CEMAR = PAKAIAN : .....</p>', '<p>Daki</p>', '<p>Jorok</p>', '<p>Cemong</p>', '<p>Kotor</p>', '<p>Kumuh</p>', -1, -1, -1, 4, -1, 0, '', '', '', '', '', 'D', '', 1591955833, 1591955833),
(146, 20, 18, 31, 0, '', '', '<p>RASA : HAMBAR = TENAGA : .....</p>', '<p>Segar</p>', '<p>Masam</p>', '<p>Sehat</p>', '<p>Lemak</p>', '<p>Lemah</p>', -1, -1, -1, -1, 4, 0, '', '', '', '', '', 'E', '', 1591955875, 1591955875),
(147, 20, 18, 31, 0, '', '', '<p>MATA : SIPIT = HIDUNG : .....</p>', '<p>Besar</p>', '<p>Lurus</p>', '<p>Sempit</p>', '<p>Maju</p>', '<p>Pesek</p>', -1, -1, -1, -1, 4, 0, '', '', '', '', '', 'E', '', 1591955917, 1591955917),
(148, 20, 18, 31, 0, '', '', '<p> ULAR : TIKUS = CECAK : .....</p>', '<p>Singa</p>', '<p>Kucing</p>', '<p>Nyamuk</p>', '<p>Semut</p>', '<p>Lintah</p>', -1, -1, 4, -1, -1, 0, '', '', '', '', '', 'C', '', 1591955966, 1591955966),
(149, 20, 18, 31, 0, '', '', '<p>BANK : TELER = RESTORAN : .....</p>', '<p>Pelayan tamu</p>', '<p>Penyaji</p>', '<p>Juru masak</p>', '<p>Waiters</p>', '<p>Jamuan</p>', -1, -1, -1, 4, -1, 0, '', '', '', '', '', 'D', '', 1591956013, 1591956013),
(150, 20, 18, 31, 0, '', '', '<p>BELAJAR : CERDAS = MAKAN : .....</p>', '<p>Lega</p>', '<p>Senang</p>', '<p>Puas</p>', '<p>Lezat</p>', '<p>Kenyang</p>', -1, -1, -1, -1, 4, 0, '', '', '', '', '', 'E', '', 1591956061, 1591956061),
(151, 20, 18, 31, 0, '', '', '<p>ARIES : ZODIAK = BERLIAN : .....</p>', '<p>Batu Mulia</p>', '<p>Logam</p>', '<p>Mahal</p>', '<p>Perhiasan</p>', '<p>Keras</p>', 4, -1, -1, -1, -1, 0, '', '', '', '', '', 'A', '', 1591956109, 1591956109),
(152, 20, 18, 31, 0, '', '', '<p>SAKIT : KLINIK = BELAJAR : .....</p>', '<p>Hutan</p>', '<p>Masjid</p>', '<p>BIoskop</p>', '<p>Kantor</p>', '<p>Sekolah</p>', -1, -1, -1, -1, 4, 0, '', '', '', '', '', 'E', '', 1591956159, 1591956159),
(153, 20, 18, 28, 0, '', '', '<p><span class=\"fontstyle0\">SANGGRALOKA (USM PKN STAN 2017)</span> </p>', '<p><span class=\"fontstyle0\">Tempat bersemedi</span></p>', '<p><span class=\"fontstyle0\">Tempat berwisata</span> </p>', '<p><span class=\"fontstyle0\">Tempat upacara adat</span> </p>', '<p><span class=\"fontstyle0\">Tempat pesta pengantin</span> </p>', '<p><span class=\"fontstyle0\">Tempat pengasingan diri</span> </p>', -1, 4, -1, -1, -1, 0, '', '', '', '', '', 'B', '<p class=\"MsoListParagraph\" xss=removed><span lang=\"IN\" xss=removed>Dari KBBI, Sanggraloka : tempat untuk orang berlibur, beristirahat atau berwisata.</span></p>\r\n<p class=\"MsoListParagraph\" xss=removed><span lang=\"IN\" xss=removed><strong><em>Kunci Jawaban : B</em></strong></span></p>', 1591956484, 1593283689),
(154, 20, 18, 28, 0, '', '', '<p><span class=\"fontstyle0\">TANDON (USM PKN STAN 2017)</span> </p>', '<p>Barang sitaan</p>', '<p>Barang Plastik</p>', '<p>Barang Gadaian</p>', '<p>Barang Pinjaman</p>', '<p>Barang Tanggungan</p>', -1, -1, -1, -1, 4, 0, '', '', '', '', '', 'E', '<p> </p>\r\n<p class=\"MsoListParagraph\" xss=removed> </p>\r\n<p class=\"MsoNormal\" xss=removed><span lang=\"IN\" xss=removed>Tandon : barang tanggungan ; barang persediaan</span></p>\r\n<p> </p>\r\n<p> </p>\r\n<p> </p>', 1591956512, 1593283825),
(155, 20, 18, 31, 0, '', '', '<p>GELANG : TANGAN = TOPI : ....</p>', '<p>Kaki</p>', '<p>Tangan</p>', '<p>Kepala</p>', '<p>Bahu</p>', '<p>Punggung</p>', -1, -1, 4, -1, -1, 0, '', '', '', '', '', 'C', '', 1591957161, 1591957161),
(156, 20, 18, 31, 0, '', '', '<p>PUPIL : MATA = KANCING : .....</p>', '<p>Baju</p>', '<p>Tutup</p>', '<p>Kunci</p>', '<p>Sandal</p>', '<p>Benang</p>', 4, -1, -1, -1, -1, 0, '', '', '', '', '', 'A', '', 1591957215, 1591957215),
(157, 20, 18, 31, 0, '', '', '<p>BURUNG : SANGKAR = KUDA: .....</p>', '<p>Kolam</p>', '<p>Kandang</p>', '<p>Akuarium</p>', '<p>Rumah</p>', '<p>Istana</p>', -1, 4, -1, -1, -1, 0, '', '', '', '', '', 'B', '', 1591957280, 1591957280),
(158, 20, 18, 31, 0, '', '', '<p>PENERBIT : BUKU = PERGURUAN TINGGI : .....</p>', '<p>Mahasiswa</p>', '<p>Sarjana</p>', '<p>Pendidikan</p>', '<p>Penyunting</p>', '<p>Publikasi</p>', -1, 4, -1, -1, -1, 0, '', '', '', '', '', 'B', '', 1591957328, 1591957328),
(159, 20, 18, 31, 0, '', '', '<p>DANGDUT : MUSIK = ..... : .....</p>', '<p>Teater : Acara</p>', '<p>Sandiwara : Plot</p>', '<p>Drama : Panggung</p>', '<p> Film : Skenario</p>', '<p> Farmalogi : Ilmu</p>', -1, -1, -1, -1, 4, 0, '', '', '', '', '', 'E', '', 1591957390, 1591957390),
(160, 20, 18, 31, 0, '', '', '<p>PELANGI : BIANGLALA = ..... : .....</p>', '<p>Matahari : Terik</p>', '<p>Mobil : Mewah</p>', '<p>Sarana : Prasarana</p>', '<p>Makan : Minum</p>', '<p>Penjara : Bui</p>', -1, -1, -1, -1, 4, 0, '', '', '', '', '', 'E', '', 1591957457, 1591957457),
(161, 20, 18, 31, 0, '', '', '<p>SUHU : KELVIN = ..... : .....</p>', '<p>Gempa : Skala</p>', '<p>Matahari : Panas</p>', '<p>Gempa : Ritcher</p>', '<p>Listrik : Padam</p>', '<p>Air : Dingin</p>', -1, -1, 4, -1, -1, 0, '', '', '', '', '', 'C', '', 1591957531, 1591957531),
(162, 20, 18, 31, 0, '', '', '<p>RADIO : INFORMASI = ..... : .....</p>', '<p>Sarjana : Toga</p>', '<p>Televisi : Antena</p>', '<p>Telepon : Komunikasi</p>', '<p>Baju : Cuci</p>', '<p>Makan : Minum</p>', -1, -1, 4, -1, -1, 0, '', '', '', '', '', 'C', '', 1591957614, 1591957614),
(163, 20, 18, 31, 0, '', '', '<p>MELATI : PUTIH = ..... : .....</p>', '<p>Makan : Minum</p>', '<p>Langit : Biru</p>', '<p>Mawar : Duri</p>', '<p>Susu : Stroberi</p>', '<p>Burung : Sayap</p>', -1, 4, -1, -1, -1, 0, '', '', '', '', '', 'B', '', 1591957692, 1591957692),
(164, 20, 18, 31, 0, '', '', '<p>BANGSA : ETHNOLOGI = ..... : .....</p>', '<p>Bumi : Demografi</p>', '<p>Penyakit : Patologi</p>', '<p>Alam : Astronomi</p>', '<p>Hewan : Gastronomi</p>', '<p>Tumbuhan : Eksobiologi</p>', -1, 4, -1, -1, -1, 0, '', '', '', '', '', 'B', '', 1591957790, 1591957790),
(165, 20, 18, 31, 0, '', '', '<p>MENGEREM : BERHENTI = BIUS : .....</p>', '<p>Mobil</p>', '<p>Oat</p>', '<p>Kecelakaan</p>', '<p>Tidur</p>', '<p>Sadar</p>', -1, -1, -1, 4, -1, 0, '', '', '', '', '', 'D', '', 1591957828, 1591957828),
(166, 20, 18, 31, 0, '', '', '<p>INDRA : MATA = PERNAPASAN : .....</p>', '<p>Hidung</p>', '<p>Kuku</p>', '<p>Insang</p>', '<p>Telinga</p>', '<p>Tulang</p>', -1, -1, 4, -1, -1, 0, '', '', '', '', '', 'C', '', 1591957879, 1591957879),
(167, 20, 18, 31, 0, '', '', '<p>PANGGUNG : AKTOR = RING : .....</p>', '<p>Pelayan</p>', '<p>Petinju</p>', '<p>Pengarang</p>', '<p>Guru</p>', '<p>Penyanyi</p>', -1, 4, -1, -1, -1, 0, '', '', '', '', '', 'B', '', 1591957919, 1591957919),
(168, 20, 18, 31, 0, '', '', '<p>BUSUR : ANAK PANAH = SENAPAN : .....</p>', '<p>Bahaya</p>', '<p>Peluru</p>', '<p>Senjata</p>', '<p>Tembak</p>', '<p>Tentara</p>', -1, 4, -1, -1, -1, 0, '', '', '', '', '', 'B', '', 1591957971, 1591957971),
(169, 20, 18, 31, 0, '', '', '<p>MOBIL : JALAN = PERAHU : .....</p>', '<p>Orang</p>', '<p>Ombak</p>', '<p>Nelayan</p>', '<p>Angin</p>', '<p>Laut</p>', -1, -1, -1, -1, 4, 0, '', '', '', '', '', 'E', '', 1591958033, 1591958033),
(170, 20, 18, 31, 0, '', '', '<p>DOMBA : CUKUR : BULU = .....</p>', '<p>Sapi : Kawin : Susu</p>', '<p>Ayam : Potong : Daging</p>', '<p>Kuda : Penunggang : Sadel</p>', '<p>Kambing : Embik : Tanduk</p>', '<p>Kerbau : Tanduk : Kepala</p>', -1, 4, -1, -1, -1, 0, '', '', '', '', '', 'B', '', 1591958096, 1591958096),
(171, 20, 18, 31, 0, '', '', '<p>BELALANG : KATAK : ULAR = .....</p>', '<p>Rumput : Sapi : Manusia</p>', '<p>Ikan : Gurita : Udang</p>', '<p>Susu : Sapi : Manusia</p>', '<p>Bunga : Mawar : Melati</p>', '<p>Cacing : Ayam : Bebek</p>', 4, -1, -1, -1, -1, 0, '', '', '', '', '', 'A', '', 1591958190, 1591958190),
(172, 20, 18, 31, 0, '', '', '<p>JEPANG : YEN : KIMONO = .....</p>', '<p>Meksiko : Gulden : Sabero</p>', '<p>India : Rupee : Sari</p>', '<p>Malaysia : Ringgit : Topi</p>', '<p>Thailand : Bath : Ankara</p>', '<p>Amerika Serikat : Lima : Dolar</p>', -1, 4, -1, -1, -1, 0, '', '', '', '', '', 'B', '', 1591958253, 1591958253),
(173, 21, 23, 32, 0, '', '', '<p>1. I bought an (expensive) book at the mall.</p>', '<p>Adjective</p>', '<p>Verb</p>', '<p>Noun</p>', '<p>Conjunction</p>', '<p>-</p>', 4, -1, -1, -1, -1, 0, '', '', '', '', '', 'A', '<p>Expensinve is an adjecticve. </p>', 1591979296, 1591979296),
(174, 21, 23, 32, 0, '', '', '<p>2. My father is going to (swim).</p>', '<p>Adjective</p>', '<p>verb</p>', '<p>preposition</p>', '<p>adverb</p>', '<p>-</p>', -1, 4, -1, -1, -1, 0, '', '', '', '', '', 'B', '', 1591979360, 1591979360),
(175, 21, 23, 32, 0, '', '', '<p>3. John puts their shoes (between) Anna and Linda.</p>', '<p>Adjective</p>', '<p>verb</p>', '<p>conjunction</p>', '<p>preposition</p>', '', -1, -1, -1, 4, -1, 0, '', '', '', '', '', 'D', '', 1591979476, 1591979476),
(176, 21, 23, 32, 0, '', '', '<p>4. If we shoot the ball (well), we will win the match.</p>', '<p>adjeective</p>', '<p>verb</p>', '<p>prepositon</p>', '<p>adverb</p>', '<p>-</p>', -1, -1, -1, 4, -1, 0, '', '', '', '', '', 'D', '', 1591979521, 1591979521),
(177, 21, 23, 32, 0, '', '', '<p>5. Both Chris and John refused to sign the petition. Nobody could force (them) to do it</p>', '<p>adjective</p>', '<p>verb</p>', '<p>noun</p>', '<p>pronoun</p>', '<p>-</p>', -1, -1, -1, 4, -1, 0, '', '', '', '', '', 'D', '', 1591979589, 1591979589),
(178, 20, 18, 33, 0, '', '', '<p>231,   ....   ,453,   564</p>', '<p>321</p>', '<p>342</p>', '<p>421</p>', '<p>241</p>', '<p>325</p>', -1, 4, -1, -1, -1, 0, '', '', '', '', '', 'B', '', 1591991312, 1592108323),
(179, 20, 18, 33, 0, '', '', '<p>4, 8, ..., ..., 64, 128</p>', '<p>28, 24</p>', '<p>16, 32</p>', '<p>226, 14</p>', '<p>13, 29</p>', '<p>22, 14</p>', -1, 4, -1, -1, -1, 0, '', '', '', '', '', 'B', '', 1591991511, 1592108358),
(180, 20, 18, 33, 0, '', '', '<p>3, 7, 15, ..., ..., 127, 255</p>', '<p>31, 63</p>', '<p>34, 42</p>', '<p>25, 16</p>', '<p>25, 25</p>', '<p>38, 14</p>', 4, -1, -1, -1, -1, 0, '', '', '', '', '', 'A', '', 1591991575, 1592108398),
(181, 20, 18, 33, 0, '', '', '<p>1, 1, 2, 3, ..., ..., 13, 21, 34</p>', '<p>4, 5</p>', '<p>5, 19</p>', '<p>5, 8</p>', '<p>2, 10</p>', '<p>4, 12</p>', -1, -1, 4, -1, -1, 0, '', '', '', '', '', 'C', '', 1591991621, 1592108433),
(182, 20, 18, 33, 0, '', '', '<p>..., 2, 5, 6, 7, 10, 9, 14</p>', '<p>6</p>', '<p>5</p>', '<p>4</p>', '<p>3</p>', '<p>2</p>', -1, -1, -1, 4, -1, 0, '', '', '', '', '', 'D', '', 1591991651, 1592108472),
(183, 20, 18, 33, 0, '', '', '<p>42, 13, 19, 49, 19, 19, 56, 25, 19, ..., ...</p>', '<p>18, 24</p>', '<p>62, 31</p>', '<p>63, 31</p>', '<p>66, 34</p>', '<p>30, 22</p>', -1, -1, 4, -1, -1, 0, '', '', '', '', '', 'C', '', 1591991701, 1592108507),
(184, 20, 18, 33, 0, '', '', '<p>3, 7, 15, 31, 63, ..., ...</p>', '<p>127, 255</p>', '<p>148, 310</p>', '<p>153, 312</p>', '<p>158, 352</p>', '<p>158, 328</p>', 4, -1, -1, -1, -1, 0, '', '', '', '', '', 'A', '', 1591991756, 1592108566),
(185, 20, 18, 33, 0, '', '', '<p>1, 1, 2, 3, 5, 8, ..., ...</p>', '<p>12,17</p>', '<p>13,21</p>', '<p>14, 21</p>', '<p>15, 28</p>', '<p>17, 21</p>', -1, 4, -1, -1, -1, 0, '', '', '', '', '', 'B', '', 1591991800, 1592108586),
(186, 20, 18, 33, 0, '', '', '<p>5, 8, 16, 19, 38, 41, ..., ...</p>', '<p>43, 45</p>', '<p>44, 47</p>', '<p>48, 70</p>', '<p>82, 85</p>', '<p>44, 88</p>', -1, -1, -1, 4, -1, 0, '', '', '', '', '', 'D', '', 1591991863, 1592108610),
(187, 20, 18, 33, 0, '', '', '<p>2, 3, 6, 7, 14, 15, ..., ...</p>', '<p>14, 6</p>', '<p>28, 29</p>', '<p>9, 3</p>', '<p>30, 31</p>', '<p>16, 32</p>', -1, -1, -1, 4, -1, 0, '', '', '', '', '', 'D', '', 1591991920, 1592108632),
(188, 20, 18, 33, 0, '', '', '<p>11. 5, 6, 7, 8, 9, 10, 14, ..., ...</p>', '<p>15, 19</p>', '<p>16, 24</p>', '<p>14, 18</p>', '<p>38, 39</p>', '<p>17, 20</p>', 4, -1, -1, -1, -1, 0, '', '', '', '', '', 'A', '', 1591991985, 1591991985),
(189, 20, 18, 33, 0, '', '', '<p>12. 1, 2, 3, 9, 10, 11, ..., ...</p>', '<p>12, 13</p>', '<p>20, 17</p>', '<p>33, 36</p>', '<p>121, 122</p>', '<p>17, 16</p>', -1, -1, -1, 4, -1, 0, '', '', '', '', '', 'D', '', 1591992119, 1591992119);
INSERT INTO `tbl_soal_tryout` (`id_soal`, `dosen_id`, `matkul_id`, `id_soal_paket`, `bobot`, `file`, `tipe_file`, `soal`, `opsi_a`, `opsi_b`, `opsi_c`, `opsi_d`, `opsi_e`, `bobot_a`, `bobot_b`, `bobot_c`, `bobot_d`, `bobot_e`, `bobot_kosong`, `file_a`, `file_b`, `file_c`, `file_d`, `file_e`, `jawaban`, `pembahasan`, `created_on`, `updated_on`) VALUES
(190, 20, 18, 33, 0, '', '', '<p>13. 1/9, 1/3, 1, 3, 9, 27, ..., ...</p>', '<p>90, 210</p>', '<p>21, 35</p>', '<p>81, 243</p>', '<p>27, 89</p>', '<p>9, 1</p>', -1, -1, 4, -1, -1, 0, '', '', '', '', '', 'C', '', 1591992172, 1591992172),
(191, 20, 18, 33, 0, '', '', '<p>14. 18, 9, 3, 8, 4, 2, ..., ...</p>', '<p>15, 5</p>', '<p>10, 5</p>', '<p>0, -3</p>', '<p>7, 31/2</p>', '<p>0, 1/2</p>', -1, -1, -1, 4, -1, 0, '', '', '', '', '', 'D', '', 1591992241, 1591992241),
(192, 20, 18, 33, 0, '', '', '<p>15. 10, 30, 32, 16, 48, 50, ..., ...</p>', '<p>18, 16</p>', '<p>98, 60</p>', '<p>58, 48</p>', '<p>25, 75</p>', '<p>32, 64</p>', -1, -1, -1, 4, -1, 0, '', '', '', '', '', 'D', '', 1591992541, 1591992541),
(193, 20, 18, 33, 0, '', '', '<p>16. 15, 10, 5, 20, 15, 10, ..., ...</p>', '<p>5, 10</p>', '<p>40, 35</p>', '<p>5, 15</p>', '<p>20, 25</p>', '<p>100, 50</p>', -1, 4, -1, -1, -1, 0, '', '', '', '', '', 'B', '', 1591992598, 1591992598),
(194, 20, 18, 33, 0, '', '', '<p>17. 2, 4, 6, 9, 11, 13, ..., ...</p>', '<p>9, 18</p>', '<p>22,26</p>', '<p>18,22</p>', '<p>14, 17</p>', '<p>16, 18</p>', -1, -1, -1, -1, 4, 0, '', '', '', '', '', 'E', '', 1591992683, 1591992683),
(195, 20, 18, 33, 0, '', '', '<p>18. 94, 88, 82, 76, 70, 64, ..., ...</p>', '<p>52, 60</p>', '<p>58, 52</p>', '<p>56, 50</p>', '<p>70, 68</p>', '<p>60, 54</p>', -1, 4, -1, -1, -1, 0, '', '', '', '', '', 'B', '', 1591992756, 1591992756),
(196, 20, 18, 33, 0, '', '', '<p>19. 12, 9, 9, 8, 6, 7, ..., ...</p>', '<p>3, 6</p>', '<p>3, 3</p>', '<p>4, 2</p>', '<p>34, 33</p>', '<p>5, 4</p>', 4, -1, -1, -1, -1, 0, '', '', '', '', '', 'A', '', 1591992803, 1591992803),
(197, 20, 18, 33, 0, '', '', '<p>20. 18, 13, 27, 21, 36, 29, 45, ..., ...</p>', '<p>55, 28</p>', '<p>54, 37</p>', '<p>36, 54</p>', '<p>39, 55</p>', '<p>8, 9</p>', -1, -1, 4, -1, -1, 0, '', '', '', '', '', 'C', '', 1591992859, 1591992859),
(198, 20, 18, 33, 0, '', '', '<p>21. G, H, I, M, N, J, K, L, M, N, ..., ...</p>', '<p>M, N</p>', '<p>N, M</p>', '<p>O, P</p>', '<p>P, O</p>', '<p>P, Q</p>', 4, -1, -1, -1, -1, 0, '', '', '', '', '', 'A', '', 1591992912, 1591992912),
(199, 20, 18, 33, 0, '', '', '<p>22. A, B, C, C, D, E, F, F, F, G, H, I, I, I, ..., ...</p>', '<p>I, I</p>', '<p>I, J</p>', '<p>J, J</p>', '<p>J, K</p>', '<p>K, K</p>', -1, 4, -1, -1, -1, 0, '', '', '', '', '', 'B', '', 1591992955, 1591992955),
(200, 20, 18, 33, 0, '', '', '<p>23. A, B, D, B, B, D, C, B, D, D, B, D, ..., ...</p>', '<p>B, M</p>', '<p>E, B</p>', '<p>C, D</p>', '<p>D, E</p>', '<p>Z, F</p>', -1, 4, -1, -1, -1, 0, '', '', '', '', '', 'B', '', 1591993005, 1591993005),
(201, 20, 18, 33, 0, '', '', '<p>24. A, C, C, E, ..., ..., G, I, I, K</p>', '<p>E, G</p>', '<p>G, H</p>', '<p>H, I</p>', '<p>G, J</p>', '<p>M, Z</p>', 4, -1, -1, -1, -1, 0, '', '', '', '', '', 'A', '', 1591993068, 1591993068),
(202, 20, 18, 33, 0, '', '', '<p>25. ..., ..., C, F, E, D, G, H, I, L, K, J, M, N, O</p>', '<p>A, P</p>', '<p>I, O</p>', '<p>A, B</p>', '<p>X, L</p>', '<p>B, M</p>', -1, -1, 4, -1, -1, 0, '', '', '', '', '', 'C', '', 1591993110, 1591993110),
(203, 20, 18, 33, 0, '', '', '<p>26. 81, 64, 72, 56, ..., ..., 54, 40, 45, 32, 36</p>', '<p>66, 38</p>', '<p>61, 42</p>', '<p>32, 39</p>', '<p>63, 48</p>', '<p>30, 32</p>', -1, -1, -1, 4, -1, 0, '', '', '', '', '', 'D', '', 1591993169, 1591993169),
(204, 20, 18, 33, 0, '', '', '<p>27. 1, 3, 7, 15, ..., ...</p>', '<p>12, 35</p>', '<p>13, 22</p>', '<p>31, 63</p>', '<p>12, 13</p>', '<p>32, 12</p>', -1, -1, 4, -1, -1, 0, '', '', '', '', '', 'C', '', 1591993219, 1591993219),
(205, 20, 18, 33, 0, '', '', '<p>28. 9, 9, 9, 6, 9, 3, ..., ...</p>', '<p>11, 11</p>', '<p>13, 17</p>', '<p>9, 0</p>', '<p>19, 10</p>', '<p>13, 17</p>', -1, -1, 4, -1, -1, 0, '', '', '', '', '', 'C', '', 1591993436, 1591993436),
(206, 20, 18, 33, 0, '', '', '<p>29. 12, 13, ..., ..., 22, 27</p>', '<p>19, 12</p>', '<p>15, 18</p>', '<p>22, 24</p>', '<p>22, 18</p>', '<p>21, 18</p>', -1, 4, -1, -1, -1, 0, '', '', '', '', '', 'B', '', 1591993503, 1591993503),
(207, 20, 18, 33, 0, '', '', '<p>30. 2, 10, 4, 8, ..., ..., 8, 4</p>', '<p>13, 10</p>', '<p>18, 12</p>', '<p>14, 12</p>', '<p>12, 14</p>', '<p>6, 6</p>', -1, -1, -1, -1, 4, 0, '', '', '', '', '', 'E', '', 1591993547, 1591993547),
(208, 20, 18, 33, 0, '', '', '<p>31. ..., 34, 35, 39, 40, 44, 45</p>', '<p>17</p>', '<p>13</p>', '<p>30</p>', '<p>41</p>', '<p>33</p>', -1, -1, 4, -1, -1, 0, '', '', '', '', '', 'C', '', 1591993607, 1591993607),
(209, 20, 18, 33, 0, '', '', '<p>32. 11, 4, 22, 9, ..., 14, 44, 19, 55</p>', '<p>28</p>', '<p>20</p>', '<p>24</p>', '<p>33</p>', '<p>25</p>', -1, -1, -1, 4, -1, 0, '', '', '', '', '', 'D', '', 1591993649, 1591993649),
(210, 20, 18, 33, 0, '', '', '<p>33. 2, 3, ..., 12, 24</p>', '<p>18</p>', '<p>13</p>', '<p>6</p>', '<p>16</p>', '<p>15</p>', -1, -1, 4, -1, -1, 0, '', '', '', '', '', 'C', '', 1591993823, 1591993823),
(211, 20, 18, 33, 0, '', '', '<p>34. 5, 8, 16, ..., ..., 41, 82, 85</p>', '<p>28, 27</p>', '<p>19, 38</p>', '<p>24, 81</p>', '<p>23, 15</p>', '<p>34, 42</p>', -1, 4, -1, -1, -1, 0, '', '', '', '', '', 'B', '', 1591993879, 1591993879),
(212, 20, 18, 33, 0, '', '', '<p>35. 2, 3, 6, 7, 14, 15, ..., ...</p>', '<p>22, 32</p>', '<p>21, 36</p>', '<p>39, 30</p>', '<p>30, 31</p>', '<p>30, 32</p>', -1, -1, -1, 4, -1, 0, '', '', '', '', '', 'D', '', 1591993950, 1591993950),
(213, 20, 18, 33, 0, '', '', '<p>36. 92, 88, 84, 80, 76, ..., ...</p>', '<p>72, 68</p>', '<p>68, 64</p>', '<p>72, 64</p>', '<p>68, 72</p>', '<p>72, 62</p>', 4, -1, -1, -1, -1, 0, '', '', '', '', '', 'A', '', 1591994062, 1591994062),
(214, 20, 18, 33, 0, '', '', '<p>37. 3, 7, 16, 35, 74, ..., ...</p>', '<p>109, 48</p>', '<p>148, 312</p>', '<p>153, 312</p>', '<p>153, 328</p>', '<p>158, 328</p>', -1, -1, 4, -1, -1, 0, '', '', '', '', '', 'C', '', 1591994124, 1591994124),
(215, 20, 18, 33, 0, '', '', '<p>38. 2, 3, 5, 7, ..., ...</p>', '<p>9, 11</p>', '<p>9, 12</p>', '<p>11, 13</p>', '<p>13, 17</p>', '<p>8, 9</p>', -1, -1, 4, -1, -1, 0, '', '', '', '', '', 'C', '', 1591994172, 1591994172),
(216, 20, 18, 33, 0, '', '', '<p>39. 4, 7, 4, 7, 10, 7, 10, ..., ...</p>', '<p>7, 16</p>', '<p>7, 14</p>', '<p>7, 17</p>', '<p>10, 18</p>', '<p>10, 16</p>', 4, -1, -1, -1, -1, 0, '', '', '', '', '', 'A', '', 1591994367, 1591994367),
(217, 20, 18, 33, 0, '', '', '<p>40. 1, 2, 4, 5, 25, 26, ..., ...</p>', '<p>27, 54</p>', '<p>51, 52</p>', '<p>27, 28</p>', '<p>254, 255</p>', '<p>676, 677</p>', -1, -1, -1, -1, 4, 0, '', '', '', '', '', 'E', '', 1591994427, 1591994427),
(218, 20, 18, 33, 0, '', '', '<p>41. 1, 2, 6, 24, 120, ...</p>', '<p>144</p>', '<p>180</p>', '<p>240</p>', '<p>480</p>', '<p>720</p>', -1, -1, -1, -1, 4, 0, '', '', '', '', '', 'E', '', 1591994471, 1591994471),
(219, 20, 18, 33, 0, '', '', '<p>42. 1, 2, 4, 8, 16, ..., ...</p>', '<p>24, 32</p>', '<p>8, 4</p>', '<p>32, 66</p>', '<p>8, 2</p>', '<p>32, 64</p>', -1, -1, -1, -1, 4, 0, '', '', '', '', '', 'E', '', 1591994622, 1591994622),
(220, 20, 18, 33, 0, '', '', '<p>43. 12, 13, 15, 18, ..., ...</p>', '<p>19, 20</p>', '<p>22, 27</p>', '<p>20, 25</p>', '<p>22, 26</p>', '<p>21, 25</p>', -1, 4, -1, -1, -1, 0, '', '', '', '', '', 'B', '', 1591994675, 1591994675),
(221, 20, 18, 33, 0, '', '', '<p>44. 2, 10, 4, 8, 6, 6, ..., ...</p>', '<p>8, 10</p>', '<p>8, 2</p>', '<p>4, 2</p>', '<p>10, 12</p>', '<p>8, 4</p>', -1, -1, -1, -1, 4, 0, '', '', '', '', '', 'E', '', 1591994726, 1591994726),
(222, 20, 18, 33, 0, '', '', '<p>45. 81, 64, 72, 56, 63, 48, 54, 40, 45, ..., ...</p>', '<p>31, 36</p>', '<p>32, 36</p>', '<p>32, 37</p>', '<p>30, 35</p>', '<p>36, 32</p>', -1, 4, -1, -1, -1, 0, '', '', '', '', '', 'B', '', 1591994786, 1591994786),
(223, 20, 18, 33, 0, '', '', '<p>46. 1, 3, 7, 15, ..., ...</p>', '<p>20, 26</p>', '<p>30, 60</p>', '<p>31, 63</p>', '<p>31, 62</p>', '<p>22, 29</p>', -1, -1, 4, -1, -1, 0, '', '', '', '', '', 'C', '', 1591995208, 1591995208),
(224, 20, 18, 33, 0, '', '', '<p>47. 9, 9, 9, 6, 9, 3, ..., ...</p>', '<p>9, 9</p>', '<p>3, 9</p>', '<p>3, 6</p>', '<p>9, 0</p>', '<p>9, 10</p>', -1, -1, -1, 4, -1, 0, '', '', '', '', '', 'D', '', 1591995255, 1591995255),
(225, 20, 18, 33, 0, '', '', '<p>48. 4, 4, 8, 4, 16, 4, ..., ...</p>', '<p>32, 4</p>', '<p>12, 44</p>', '<p>4, 8</p>', '<p>16, 20</p>', '<p>9, 3</p>', 4, -1, -1, -1, -1, 0, '', '', '', '', '', 'A', '', 1591995302, 1591995302),
(226, 20, 18, 33, 0, '', '', '<p>49. 18, 10, 20, 12, 24, 16, ..., ...</p>', '<p>8, 16</p>', '<p>20, 28</p>', '<p>32, 24</p>', '<p>9, 3</p>', '<p>28, 24</p>', -1, -1, 4, -1, -1, 0, '', '', '', '', '', 'C', '', 1591995355, 1591995355),
(227, 20, 18, 33, 0, '', '', '<p>50. 0, ½, 4 ½, 1 ½, 9, 2 ½, ...,  ...</p>', '<p>13 ½, 3 ½</p>', '<p>12, 2 ½</p>', '<p>3 ½, 12 ½</p>', '<p>14½, 4 ½</p>', '<p>12 ¼, 3 ¼</p>', 2, 4, 4, 4, 4, 0, '', '', '', '', '', 'A', '', 1591995469, 1591995469),
(228, 20, 18, 33, 0, '', '', '<p>51. 54, 27, 9, 5, 30, 15, 5, …</p>', '<p>-7</p>', '<p>1</p>', '<p>7</p>', '<p>9</p>', '<p>30</p>', -1, 4, -1, -1, -1, 0, '', '', '', '', '', 'B', '', 1591995520, 1591995520),
(229, 20, 18, 33, 0, '', '', '<p>52. 4, 4 20, 4, 3, 3, 15, …</p>', '<p>1</p>', '<p>3</p>', '<p>5</p>', '<p>10</p>', '<p>25</p>', -1, 4, -1, -1, -1, 0, '', '', '', '', '', 'B', '', 1591995558, 1591995558),
(230, 20, 18, 33, 0, '', '', '<p>53. 4, 9, …, 39, 79, 159, 319, 639, 1279</p>', '<p>17</p>', '<p>19</p>', '<p>20</p>', '<p>29</p>', '<p>30</p>', -1, 4, -1, -1, -1, 0, '', '', '', '', '', 'B', '', 1591995599, 1591995599),
(231, 20, 18, 33, 0, '', '', '<p>54. 3, 4, 6, …, …, 16, 24, 32, 48</p>', '<p>4, 10</p>', '<p>8, 14</p>', '<p>8, 12</p>', '<p>9, 13</p>', '<p>9, 18</p>', -1, -1, 4, -1, -1, 0, '', '', '', '', '', 'C', '', 1591995653, 1591995653),
(232, 20, 18, 33, 0, '', '', '<p>55. 4, 6, …, …, 12, 16, 16, 24, 32</p>', '<p>5, 17</p>', '<p>7, 8</p>', '<p>8, 9</p>', '<p>8, 8</p>', '<p>9, 9</p>', -1, -1, -1, 4, -1, 0, '', '', '', '', '', 'D', '', 1591995701, 1591995701),
(233, 20, 18, 33, 0, '', '', '<p>56. 0.80, 1.20, 1.60, 2.00, 2.40, …, 3.20, 3.60, 4.00</p>', '<p>2.60</p>', '<p>2.80</p>', '<p>3.10</p>', '<p>3.20</p>', '<p>3.50</p>', -1, 4, -1, -1, -1, 0, '', '', '', '', '', 'B', '', 1591995766, 1591995766),
(234, 20, 18, 33, 0, '', '', '<p>57. 22, 18, 26, 22, 30, 26, 34, …</p>', '<p>0</p>', '<p>22</p>', '<p>30</p>', '<p>38</p>', '<p>42</p>', -1, -1, 4, -1, -1, 0, '', '', '', '', '', 'C', '', 1591995803, 1591995803),
(235, 20, 18, 33, 0, '', '', '<p>58. 34, 51, 38, 46, 42, 41, …, …, 50</p>', '<p>3, 39</p>', '<p>35, 37</p>', '<p>37, 48</p>', '<p>45, 47</p>', '<p>46, 36</p>', -1, -1, -1, -1, 4, 0, '', '', '', '', '', 'E', '', 1591995864, 1591995864),
(236, 20, 18, 33, 0, '', '', '<p>59. 3, 8, 18, …, 78, 4, 9, …, 39</p>', '<p>30, 33</p>', '<p>36, 34</p>', '<p>38, 19</p>', '<p>40, 28</p>', '<p>56, 37</p>', -1, -1, 4, -1, -1, 0, '', '', '', '', '', 'C', '', 1591995930, 1591995930),
(237, 20, 18, 33, 0, '', '', '<p>60. 104, 97, 100, 50, 43, 46, 23, …</p>', '<p>13</p>', '<p>16</p>', '<p>18</p>', '<p>26</p>', '<p>30</p>', -1, 4, -1, -1, -1, 0, '', '', '', '', '', 'B', '', 1591996080, 1591996080),
(238, 20, 18, 33, 0, '', '', '<p>61. 6, 36, 30, 5, 30, 24, 4, …</p>', '<p>-2</p>', '<p>10</p>', '<p>16</p>', '<p>24</p>', '<p>48</p>', -1, -1, -1, 4, -1, 0, '', '', '', '', '', 'D', '', 1591996119, 1591996119),
(239, 20, 18, 33, 0, '', '', '<p>62. e, h, a, j, m, b … …</p>', '<p>p, s</p>', '<p>p, t</p>', '<p>o, q</p>', '<p>o, r</p>', '<p>o, s</p>', -1, -1, -1, 4, -1, 0, '', '', '', '', '', 'D', '', 1591996185, 1591996185),
(240, 20, 18, 33, 0, '', '', '<p>63. z v r …</p>', '<p>m</p>', '<p>o</p>', '<p>t</p>', '<p>l</p>', '<p>n</p>', -1, -1, -1, -1, 4, 0, '', '', '', '', '', 'E', '', 1591996219, 1591996219),
(241, 20, 18, 33, 0, '', '', '<p>64. z, a, h, i, j, z, a, k, l, m, z, a, …, …, …</p>', '<p>n, o, p</p>', '<p>o, p, q</p>', '<p>r, s, t</p>', '<p>o, p, n</p>', '<p>p, o, y</p>', 4, -1, -1, -1, -1, 0, '', '', '', '', '', 'A', '', 1591996315, 1591996315),
(242, 20, 18, 33, 0, '', '', '<p>65. m, m, p, p, s, s, v, …</p>', '<p>w</p>', '<p>v</p>', '<p>x</p>', '<p>t</p>', '<p>u</p>', -1, 4, -1, -1, -1, 0, '', '', '', '', '', 'B', '', 1591996411, 1591996411),
(243, 20, 18, 33, 0, '', '', '<p>66. a, c, e, y, f, h, j, y, k, m, …, …</p>', '<p>o</p>', '<p>p</p>', '<p>q</p>', '<p>n</p>', '<p>l</p>', 4, -1, -1, -1, -1, 0, '', '', '', '', '', 'A', '', 1591996472, 1591996472),
(244, 20, 18, 33, 0, '', '', '<p>67. a, c, f, j, o, …</p>', '<p>p</p>', '<p>t</p>', '<p>v</p>', '<p>u</p>', '<p>z</p>', -1, -1, -1, 4, -1, 0, '', '', '', '', '', 'D', '', 1591996515, 1591996515),
(245, 20, 18, 33, 0, '', '', '<p>c, i, o, ….</p>', '<p>t</p>', '<p>v</p>', '<p>u</p>', '<p>w</p>', '<p>h</p>', -1, -1, 4, -1, -1, 0, '', '', '', '', '', 'C', '', 1591996562, 1592255976),
(246, 20, 18, 33, 0, '', '', '<p>h, m, i, n, j, …, …</p>', '<p>l, p</p>', '<p>m, k</p>', '<p>o, k</p>', '<p>z, a</p>', '<p>p, r</p>', -1, -1, 4, -1, -1, 0, '', '', '', '', '', 'C', '', 1591996612, 1592108279),
(247, 20, 18, 33, 0, '', '', '<p>A, B, C, B, C, D, C, D, E, D, E, F, …. …. ….</p>', '<p>E, F, G</p>', '<p>D, E, F</p>', '<p>D, G, H</p>', '<p>F, G, H</p>', '<p>E, G, F</p>', 4, -1, -1, -1, -1, 0, '', '', '', '', '', 'A', '', 1591996724, 1592108229),
(248, 20, 18, 33, 0, '', '', '<p>C, F, I, L, O, ..., ...</p>', '<p>T, V</p>', '<p>R, U</p>', '<p>R, S</p>', '<p>Q, T</p>', '<p>P, S</p>', -1, 4, -1, -1, -1, 0, '', '', '', '', '', 'B', '', 1591996787, 1591996787),
(249, 20, 18, 33, 0, '', '', '<p>B, A, C ,A, D, A, E, A, F, A, G, A, …, …</p>', '<p>A, J</p>', '<p>A,H</p>', '<p>H, A</p>', '<p>I, A</p>', '<p>J, A</p>', -1, -1, 4, -1, -1, 0, '', '', '', '', '', 'C', '', 1591996884, 1592108191),
(250, 20, 18, 33, 0, '', '', '<p>aba abe abi … …</p>', '<p>abm, abq</p>', '<p>abu, abo</p>', '<p>abe, abz</p>', '<p>abj, abg</p>', '<p>abd, abx</p>', 4, -1, -1, -1, -1, 0, '', '', '', '', '', 'A', '', 1591996964, 1592108138),
(251, 20, 18, 33, 0, '', '', '<p>A, G, ..., S, ...</p>', '<p>H, N</p>', '<p>J, O</p>', '<p>M, Y</p>', '<p>N, Q</p>', '<p>O, R</p>', -1, -1, 4, -1, -1, 0, '', '', '', '', '', 'C', '', 1591997029, 1591997029),
(252, 20, 18, 33, 0, '', '', '<p>Q, S, W, S, E, ..., S, ..., S, Y</p>', '<p>R, W</p>', '<p>R, J</p>', '<p>R, T</p>', '<p>R, X</p>', '<p>R, M</p>', -1, -1, 4, -1, -1, 0, '', '', '', '', '', 'C', '', 1592012863, 1592108088),
(253, 20, 18, 33, 0, '', '', '<p>Lagu yang dipesan pendengar. Lagu yang dipesan pendengar A akan diputar menjelang akhir acara, lagu pendengar B akan diputar lebih dahulu dari lagu yang dipesan A tetapi bukan sebagai lagu pembuka. Lagu yang dipesan pendengar D dan E diputar berurutan diantara lagu pendengar B dan C. Pesanan lagu siapakah yang diputar paling awal?</p>', '<p>A</p>', '<p>B</p>', '<p>C</p>', '<p>D</p>', '<p>E</p>', -1, -1, 4, -1, -1, 0, '', '', '', '', '', 'C', '', 1592012919, 1592012919),
(254, 20, 18, 33, 0, '', '', '<p>77. Lima orang pedagang asongan menghitung hasil penjualan dalam satu hari. Pedagang III lebih banyak menjual dari pedagang IV, tetapi tidak melebihi pedagang I. Penjualan pedagang II tidak melebihi pedagang V dan melebihi pedagang I. Pedagang mana yang hasil penjualannya paling banyak?</p>', '<p>I</p>', '<p>II</p>', '<p>III</p>', '<p>IV</p>', '<p>V</p>', -1, -1, -1, -1, 4, 0, '', '', '', '', '', 'E', '', 1592012968, 1592012968),
(255, 20, 18, 33, 0, '', '', '<p>Siska harus kursus bahasa mandarin setiap kamis. Sedangkan Julia kursus bahasa Inggris tiga kali seminggu setiap selasa, rabu dan kamis. Sementara Weni harus kursus bahasa Jepang pada hari yang sama dengan Julia kecuali hari Rabu. Linda kursus komputer setiap hari Rabu. Siapakah yang pergi kursus pada hari yang sama setiap Minggu?</p>', '<p>Linda, Siska, Weni</p>', '<p>Julia, Linda, dan Weni</p>', '<p>Linda dan Siska</p>', '<p>Weni dan Linda</p>', '<p>Siska dan Weni</p>', -1, -1, -1, -1, 4, 0, '', '', '', '', '', 'E', '', 1592013064, 1592108053),
(256, 20, 18, 33, 0, '', '', '<p>Intan sekarang berusia 12 tahun. Sedangkan umur Anto dua kali lebih tua daripada umur Badu. Umur Badu tiga tahun lebih tua daripada umur Nida. Jika umur Intan lima tahun lebih tua daripada umur Sari yang setahun lebih muda daripada Anto, maka urutan umur mereka dari yang termuda ke yang tertua adalah....</p>', '<p>Nida, Badu, Anto, Sari, Intan</p>', '<p>Badu, Nida, Sari, Intan, Anto</p>', '<p>Intan, Badu, Sari, Anto, Intan</p>', '<p>Nida, Badu, Sari, Anto, Intan</p>', '<p>Intan, Nida, Sari, Badu, Anto</p>', -1, -1, -1, 4, -1, 0, '', '', '', '', '', 'D', '', 1592013135, 1592107962),
(257, 20, 18, 33, 0, '', '', '<p>Farhan menyenangi buku-buku fiksi. Meta penggemar ensiklopedia. Mayang membeli segala macam novel dan beberapa kamus. Irvan penggemar biografi, sedang Sonya menyukai buku-buku non fiksi. Jika dibuat kelompok membaca dengan anggota 3 orang, susunan yang paling sesuai adalah....</p>', '<p>Farhan, Meta, Sonya</p>', '<p>Farhan, Mayang, Sonya</p>', '<p>Farhan, Irvan, Mayang</p>', '<p>Meta, Irvan, Sonya</p>', '<p>Farhan, Meta, Mayang</p>', -1, -1, -1, 4, -1, 0, '', '', '', '', '', 'D', '', 1592013237, 1592108005),
(258, 20, 18, 33, 0, '', '', '<p>Hasil produksi kerajinan seorang pengusaha setiap bulannya meningkat mengikuti aturan barisan geometri. Produksi pada bulan pertama sebanyak 150 unit kerajinan dan pada bulan keempat sebanyak 4.050 kerajinan. Hasil produksi selama 5 bulan adalah ⋯ unit kerajinan.</p>', '<p>17.850</p>', '<p>18.150</p>', '<p>18.250</p>', '<p>18.850</p>', '<p>19.350</p>', -1, 4, -1, -1, -1, 0, '', '', '', '', '', 'B', '<p><span xss=removed>Diketahui: a=150 dan U4=4.050</span><br xss=removed><span xss=removed>Rasio barisan geometri ini dapat ditentukan dengan melakukan perbandingan antarsuku sebagai berikut.</span><br xss=removed><span xss=removed>U4/U1=4.050/150</span><br xss=removed><span xss=removed>ar<sup>3</sup>/a=27</span><br xss=removed><span xss=removed>r<sup>3</sup>=27</span><br xss=removed><span xss=removed>r<sup>3</sup>=√27=3</span><br xss=removed><span xss=removed>Dengan demikian,</span><br xss=removed><span xss=removed>Sn=a(r<sup>n</sup>−1)r−1</span></p>\r\n<p><span xss=removed>S<sub>5</sub>=150(3<sup>5</sup>–1)/(3–1)</span></p>\r\n<p><span xss=removed>=150(243–1)/2</span></p>\r\n<p><span xss=removed>=75x242</span></p>\r\n<p><span xss=removed>=18.150</span><br xss=removed><span xss=removed>Jadi, hasil produksi selama 5 bulan adalah 18.150 unit kerajinan. </span></p>', 1592013347, 1592107878),
(259, 20, 18, 33, 0, '', '', '<p>Seutas tali dipotong menjadi 4 bagian, masing-masing membentuk barisan geometri. Jika potongan tali terpendek adalah 2 cm dan potongan tali terpanjang adalah 54 cm, panjang tali semula adalah ⋯ cm. </p>', '<p>60</p>', '<p>70</p>', '<p>80</p>', '<p>90</p>', '<p>100</p>', -1, -1, 4, -1, -1, 0, '', '', '', '', '', 'C', '<p><span xss=removed>Panjangnya setiap potongan tali merupakan suku-suku dalam barisan geometri, dengan U1=a=2 dan U4=54. Dalam hal ini, akan dicari S4=U1+U2+U3+U4</span><br xss=removed><span xss=removed>Langkah pertama adalah menentukan rasionya.</span><br xss=removed><span xss=removed>U4=ar354=2r327=r3r=3√27=3</span><br xss=removed><span xss=removed>Jadi, rasio barisannya adalah 3. Untuk itu, didapat</span><br xss=removed><span xss=removed>U2=ar=2⋅3=6</span><br xss=removed><span xss=removed>dan</span><br xss=removed><span xss=removed>U3=ar2=2⋅32=18</span><br xss=removed><span xss=removed>Dengan demikian,</span><br xss=removed><span xss=removed>S4=2+6+18+54=80</span><br xss=removed><span xss=removed>Jadi, panjang tali semula (sebelum dipotong) adalah 80 cm</span></p>', 1592013539, 1592107724),
(260, 20, 18, 33, 0, '', '', '<p>Pesawat terbang melaju dengan kecepatan 300 km/menit pada menit pertama. Kecepatan pada menit berikutnya 1 1/2 (satu setengah) kali dari kecepatan sebelumnya. Panjang lintasan seluruhnya dalam 4 menit pertama adalah ⋯</p>', '<p>2.437,50 km</p>', '<p>2.438,00 km</p>', '<p>2.438,50 km</p>', '<p>2.439,00 km</p>', '<p>2.439,50 km</p>', 4, -1, -1, -1, -1, 0, '', '', '', '', '', 'A', '<p><span xss=removed>Kecepatan pesawat tiap menitnya membentuk barisan geometri.</span><br xss=removed><span xss=removed>Diketahui: a=300 dan r=1 1/2=3/2.</span><br xss=removed><span xss=removed>Ditanya: S4=2.437,50 km</span></p>', 1592013640, 1592107688),
(261, 20, 18, 33, 0, '', '', '<p>Dua orang anak sedang melakukan percobaan matematika dengan menjatuhkan sebuah bola dari lantai 2 rumah mereka. Ketinggian bola dijatuhkan adalah 9 meter dari atas tanah. Dari pengamatan, diketahui bahwa pantulan bola mencapai 89 dari tinggi pantulan sebelumnya. Ketinggian bola setelah pantulan ke-5 yang paling mendekati adalah ⋯ m. </p>', '<p>4</p>', '<p>4,25</p>', '<p>4,5</p>', '<p>4,75</p>', '<p>5</p>', -1, -1, -1, -1, 4, 0, '', '', '', '', '', 'E', '<p><span xss=removed>U1 = 9m</span><br xss=removed><span xss=removed>r=8/9</span><br xss=removed><span xss=removed>n sebelum memantul adalah 1</span><br xss=removed><span xss=removed>setelah memantul sekali adalah 2</span><br xss=removed><span xss=removed>setelah pantulan ke 5 berarti u6</span><br xss=removed><br xss=removed><span xss=removed>u6=a.r^5=4,994 , mendekati 5</span></p>', 1592013727, 1592107652),
(262, 20, 18, 33, 0, '', '', '<p>Bakteri A berkembang biak menjadi dua kali lipat setiap lima menit. Setelah 15 menit, banyak bakteri ada 400. Banyak bakteri setelah 30 menit adalah ...</p>', '<p>800</p>', '<p>1.200</p>', '<p>1.600</p>', '<p>3.200</p>', '<p>6.400</p>', -1, -1, -1, 4, -1, 0, '', '', '', '', '', 'D', '<p><span xss=removed>Misalkan U1 menyatakan banyaknya bakteri mula-mula (0 menit), U2 saat 5 menit, U3 saat 10 menit, dan seterusnya.</span><br xss=removed><span xss=removed>Diketahui: U4=ar3=400 dan r=2</span><br xss=removed><span xss=removed>Ditanya: U7</span><br xss=removed><span xss=removed>Dengan demikian, didapat</span><br xss=removed><span xss=removed>Un=arn−1U7=ar6=(ar3)r3=(400)(2)3=400(8)=3.200</span><br xss=removed><span xss=removed>Banyak bakteri setelah 30 menit adalah 3.200</span></p>', 1592013804, 1592107601),
(263, 20, 18, 33, 0, '', '', '<p>Keuntungan sebuah percetakan setiap bulannya bertambah menjadi dua kali lipat dari keuntungan bulan sebelumnya. Jika keuntungan bulan pertama Rp600.000,00, maka keuntungan percetakan tersebut pada bulan keenam adalah ⋯</p>', '<p>Rp17.000.000,00</p>', '<p>Rp19.200.000,00</p>', '<p>Rp19.850.000,00</p>', '<p>Rp20.200.000,00</p>', '<p>Rp20.450.000,00</p>', -1, 4, -1, -1, -1, 0, '', '', '', '', '', 'B', '<p><span xss=removed>Kasus di atas adalah masalah kontekstual terkait barisan geometri dengan a=600.000 dan r=2. Dalam hal ini, akan dicari nilai dari U6.</span><br xss=removed><span xss=removed>Un=arn−1U6=600.000⋅26−1=600.000⋅25=600.000⋅32=19.200.000</span><br xss=removed><span xss=removed>Jadi, keuntungan percetakan tersebut pada bulan keenam adalah Rp19.200.000,00</span></p>', 1592013869, 1592107556),
(264, 20, 18, 33, 0, '', '', '<p>Sebuah bola jatuh dari ketinggian 10 m dan memantul kembali dengan ketinggian 3/4 kali tinggi sebelumnya, begitu seterusnya hingga bola berhenti. Jumlah seluruh lintasan bola adalah … meter</p>', '<p>65</p>', '<p>70</p>', '<p>75</p>', '<p>77</p>', '<p>80</p>', -1, 4, -1, -1, -1, 0, '', '', '', '', '', 'B', '<p><span xss=removed>Memakai deret geometri tak hingga, karena memntul jadi dikalikan dua., namun dikurangi a karena dijatuhkan (a nya cuma turun)</span><br xss=removed><br xss=removed><span xss=removed>(2a/(1-r))-a</span><br xss=removed><span xss=removed>(2.10/(1-3/4))</span><br xss=removed><span xss=removed>=80-10</span><br xss=removed><span xss=removed>=70</span></p>', 1592013930, 1592107474),
(265, 20, 18, 33, 0, '', '', '<p>Seutas tali dipotong menjadi 7 bagian dan panjang masing–masing potongan membentuk barisan geometri. Jika panjang potongan tali terpendek sama dengan 6cm dan potongan tali terpanjang sama dengan 384cm, panjang keseluruhan tali tersebut adalah … cm.</p>', '<p>378</p>', '<p>390</p>', '<p>570</p>', '<p>762</p>', '<p>1.530</p>', -1, -1, -1, 4, -1, 0, '', '', '', '', '', 'D', '<p><span xss=removed>u1 = a = 6</span><br xss=removed><br xss=removed><span xss=removed>u7 = ar6 = 384</span><br xss=removed><br xss=removed><span xss=removed>6.r6 = 384</span><br xss=removed><br xss=removed><span xss=removed>r6 = 64 => r = 2</span><br xss=removed><br xss=removed><span xss=removed>Sn = {a(r^n-1)}/{r-1}</span><br xss=removed><br xss=removed><span xss=removed>S7 = {6(2^7-1)}/{2-1}</span><br xss=removed><br xss=removed><span xss=removed>= {6(128-1)}/{2-1} = 762</span></p>', 1592013997, 1592107435),
(266, 20, 18, 33, 0, '', '', '<p>Pertambahan penduduk suatu kota tiap tahun mengikuti aturan barisan geometri. Pada tahun 1996 pertambahannya sebanyak 6 orang, tahun 1998 sebanyak 54 orang. Pertambahan penduduk pada tahun 2001 adalah … orang.</p>', '<p>324</p>', '<p>486</p>', '<p>648</p>', '<p>1.458</p>', '<p>4.374</p>', -1, -1, -1, 4, -1, 0, '', '', '', '', '', 'D', '<p><span xss=removed>tahun 1996 => u1 = a = 6</span><br xss=removed><br xss=removed><span xss=removed>tahun 1998 => u3 = ar2 = 54</span><br xss=removed><br xss=removed><span xss=removed>6.r2 = 54</span><br xss=removed><br xss=removed><span xss=removed>r2 = 9 => r = 3</span><br xss=removed><br xss=removed><span xss=removed>tahun 2001 => u6 = ar5</span><br xss=removed><br xss=removed><span xss=removed>6.(3)5 = 1.458</span></p>', 1592014041, 1592107312),
(267, 20, 18, 33, 0, '', '', '<p>Produksi pupuk organik menghasilkan 100 ton pupuk pada bulan pertama, setiap bulannya menaikan produksinya secara tetap 5 ton. Jumlah pupuk yang diproduksi selama 1 tahun adalah  .....</p>', '<p>1.200 ton</p>', '<p>1.260 ton</p>', '<p>1.500 ton</p>', '<p>1.530 ton</p>', '<p>1.560</p>', -1, -1, -1, 4, -1, 0, '', '', '', '', '', 'D', '<p>Diketahui:<br>Produksi bulan pertama (a) = 100 ton<br>Kenaikan produksi (b) = 5 ton<br>Ditanyakan:<br>Jumlah produksi selama 1 tahun (S₁₂)<br><br>Penyelesaian:<br>Sn = n/2 (2a + (n - 1)b)<br>S₁₂ = 12/2 (2(100) + (12 - 1).5)<br>S₁₂ = 6(200 + 55)<br>S₁₂ = 6(255)<br>S₁₂ = 1.530</p>', 1592014104, 1592107245),
(268, 21, 23, 32, 0, '', '', '<p>6. What did he (tell) you about me?</p>', '<p>Adjective</p>', '<p>Verb</p>', '<p>Noun</p>', '<p>Pronoun</p>', '', -1, 4, -1, -1, -1, 0, '', '', '', '', '', 'B', '', 1592014258, 1592014258),
(269, 21, 23, 32, 0, '', '', '<p>7. I will go to one of beautiful (islands) in Indonesia this month.</p>', '<p>Adjective</p>', '<p>Verb</p>', '<p>Preposition</p>', '<p>Noun</p>', '', -1, -1, -1, 4, -1, 0, '', '', '', '', '', 'D', '', 1592014304, 1592014304),
(270, 21, 23, 32, 0, '', '', '<p>8. I haven’t finished my project (because) I am very busy.</p>', '<p>Conjunction</p>', '<p>Verb</p>', '<p>Preposition</p>', '<p>Adverb</p>', '', 4, -1, -1, -1, -1, 0, '', '', '', '', '', 'A', '', 1592014365, 1592014365),
(271, 21, 23, 32, 0, '', '', '<p>9. Tara cooked (chicken) soup, but that was not delicious.</p>', '<p>Adjective</p>', '<p>Verb</p>', '<p>Noun</p>', '<p>Adverb</p>', '', -1, -1, 4, -1, -1, 0, '', '', '', '', '', 'C', '', 1592014411, 1592014411),
(272, 21, 23, 32, 0, '', '', '<p>10. Frank doesn’t like wasting her money (whereas) he has a lot of money</p>', '<p>Conjunction</p>', '<p>Verb</p>', '<p>Preposition</p>', '<p>Adverb</p>', '', 4, -1, -1, -1, -1, 0, '', '', '', '', '', 'A', '', 1592014553, 1592014553),
(273, 21, 23, 32, 0, '', '', '<p>11. Robby entered the classroom (quietly) because he was late</p>', '<p>Conjuction</p>', '<p>Verb</p>', '<p>Preposition</p>', '<p>Adverb</p>', '', -1, -1, -1, 4, -1, 0, '', '', '', '', '', 'D', '', 1592014597, 1592014597),
(274, 21, 23, 32, 0, '', '', '<p>12. Everyday, he rides his (yellow) bicycle to go to his school which is full of smart people.</p>', '<p>Adjective</p>', '<p>Verb</p>', '<p>Noun</p>', '<p>Adverb</p>', '', 4, -1, -1, -1, -1, 0, '', '', '', '', '', 'A', '', 1592014655, 1592014655),
(275, 21, 23, 32, 0, '', '', '<p>13. She drives very (carefully) because she has a traumatic car accident.</p>', '<p>Adjective</p>', '<p>Verb</p>', '<p>Prepositoin</p>', '<p>Adverb</p>', '', -1, -1, -1, 4, -1, 0, '', '', '', '', '', 'D', '', 1592014704, 1592014704),
(276, 21, 23, 32, 0, '', '', '<p>14. (We) got a room with very beautiful view here but it is very expensinve.</p>', '<p>Adjective</p>', '<p>Pronoun</p>', '<p>Preposition</p>', '<p>Adverb</p>', '', -1, 4, -1, -1, -1, 0, '', '', '', '', '', 'B', '', 1592014745, 1592014745),
(277, 21, 23, 32, 0, '', '', '<p>15. Parents are (often) neglected by their children when the children are very busy</p>', '<p>Adjective</p>', '<p>Conjunction</p>', '<p>Interjection</p>', '<p>Adverb</p>', '', -1, -1, -1, 4, -1, 0, '', '', '', '', '', 'D', '', 1592014791, 1592014791),
(278, 21, 23, 32, 0, '', '', '<p>16. Coffee can be used to keep awake. (However), overconsumption is not good for our health.</p>', '<p>Adjective</p>', '<p>Conjunction</p>', '<p>Interjection</p>', '<p>Adverb</p>', '', -1, 4, -1, -1, -1, 0, '', '', '', '', '', 'B', '', 1592014832, 1592014832),
(279, 21, 23, 32, 0, '', '', '<p>17. (Although) it seems hard, we should keep trying to ahieve our goals.</p>', '<p>Adjective</p>', '<p>Conjunction</p>', '<p>Interjection</p>', '<p>Adverb</p>', '', -1, 4, -1, -1, -1, 0, '', '', '', '', '', 'B', '', 1592014872, 1592014872),
(280, 21, 23, 32, 0, '', '', '<p>18. Beyonce held a concert last week in Jakarta. Although (she) felt exhausted due to long flight, she was still very excited to visit Jakarta for the first time.</p>', '<p>Adjective</p>', '<p>Verb</p>', '<p>Noun</p>', '<p>Pronoun</p>', '', -1, -1, -1, 4, -1, 0, '', '', '', '', '', 'D', '', 1592014916, 1592014916),
(281, 21, 23, 32, 0, '', '', '<p>19. You should (try) the new dish in the restaurant near my office.</p>', '<p>Adjective</p>', '<p>Verb</p>', '<p>Preposition</p>', '<p>Adverb</p>', '', -1, 4, -1, -1, -1, 0, '', '', '', '', '', 'B', '', 1592014957, 1592014957),
(282, 21, 23, 32, 0, '', '', '<p>20. She was the best students (in) this university.</p>', '<p>Adjective</p>', '<p>Verb</p>', '<p>Preposition</p>', '<p>Adverb</p>', '', -1, -1, 4, -1, -1, 0, '', '', '', '', '', 'C', '', 1592015015, 1592015015),
(283, 21, 23, 32, 0, '', '', '<p>21. Stars are the most <span xss=removed>wide</span> recognized <span xss=removed>astronomical</span> objects, and <span xss=removed>represent</span> the most fundamental <span xss=removed>building</span> blocks of galaxies</p>', '<p>Wide</p>', '<p>Astronomical</p>', '<p>Represent</p>', '<p>Building</p>', '', 4, -1, -1, -1, -1, 0, '', '', '', '', '', 'A', '', 1592015253, 1592015253),
(284, 21, 23, 32, 0, '', '', '<p>22. The age, <span xss=removed>distribution</span>, and composition of the stars in a galaxy <span xss=removed>trace</span> the <span xss=removed>historical</span>, dynamics, and evolution of that <span xss=removed>galaxy</span>.</p>', '<p>Distribution</p>', '<p>Trace</p>', '<p>Historical</p>', '<p>Galaxy</p>', '', -1, -1, 4, -1, -1, 0, '', '', '', '', '', 'C', '', 1592015349, 1592015349),
(285, 21, 23, 32, 0, '', '', '<p>23. Moreover, stars are <span xss=removed>responsibility</span> for the manufacture and distribution of <span xss=removed>heavy elements</span> such as carbon, nitrogen, and oxygen, and <span xss=removed>their</span> characteristics are <span xss=removed>intimately tied</span> to the characteristics of the planetary systems that may coalesce about them.</p>', '<p>responsibility</p>', '<p>heavy elements</p>', '<p>their</p>', '<p>intimately tied</p>', '', 4, -1, -1, -1, -1, 0, '', '', '', '', '', 'A', '', 1592015482, 1592015482),
(286, 21, 23, 32, 0, '', '', '<p>24. <span xss=removed>Consequently</span>, the <span xss=removed>study</span> of the birth, life, and <span xss=removed>death</span> of stars is <span xss=removed>center</span> to the field of astronomy.</p>', '<p>Consequently</p>', '<p>study</p>', '<p>Death</p>', '<p>Center</p>', '', -1, -1, -1, 4, -1, 0, '', '', '', '', '', 'D', '', 1592015568, 1592015568),
(287, 21, 23, 32, 0, '', '', '<p>25. <span xss=removed>In general</span>, the larger a star, the <span xss=removed>shorter</span> its life, although all but the most <span xss=removed>massive</span> stars <span xss=removed>life</span> for billions of years</p>', '<p>In general</p>', '<p>Shorter</p>', '<p>Massive</p>', '<p>Life</p>', '', -1, -1, -1, 4, -1, 0, '', '', '', '', '', 'D', '', 1592015731, 1592015731),
(288, 20, 18, 32, 0, '', '', '<p>26. When a star <span xss=removed>has</span> fused all <span xss=removed>the hydrogen</span> <span xss=removed>at</span> its core, nuclear reactions <span xss=removed>cease</span>.</p>', '<p>Has</p>', '<p>The hydrogen</p>', '<p>At</p>', '<p>Cease</p>', '', -1, -1, 4, -1, -1, 0, '', '', '', '', '', 'C', '', 1592015814, 1592015942),
(289, 21, 23, 32, 0, '', '', '<p>27. <span xss=removed>Deprived</span> of the energy <span xss=removed>production</span> needed to support it, the core <span xss=removed>begining</span> to collapse into itself and <span xss=removed>becomes</span> much hotter.</p>', '<p>Deprived</p>', '<p>Production</p>', '<p>Begining</p>', '<p>Becomes</p>', '', -1, -1, 4, -1, -1, 0, '', '', '', '', '', 'C', '', 1592017101, 1592017101),
(290, 21, 23, 32, 0, '', '', '<p>28. <span xss=removed>Hydrogen is</span> still available <span xss=removed>outside</span> the core, so hydrogen <span xss=removed>fuse</span> continues in a <span xss=removed>shell</span> surrounding the core.</p>', '<p>Hydrogen is</p>', '<p>Outside</p>', '<p>Fuse</p>', '<p>Shell</p>', '', -1, -1, 4, -1, -1, 0, '', '', '', '', '', 'C', '', 1592017180, 1592017180),
(291, 21, 23, 32, 0, '', '', '<p>29. The <span xss=removed>increasingly</span> hot core also <span xss=removed>pushes</span> the outer layers of the star outward, causing <span xss=removed>they</span> to expand and cool, <span xss=removed>transforming</span> the star into a red giant.</p>', '<p>Increasingly</p>', '<p>Pushes</p>', '<p>They</p>', '<p>Transforming</p>', '', -1, -1, 4, -1, -1, 0, '', '', '', '', '', 'C', '', 1592017253, 1592017253),
(292, 21, 23, 32, 0, '', '', '<p>30. <span xss=removed>For average</span> stars like the Sun, the process of <span xss=removed>eject</span> its outer layers <span xss=removed>continues</span> until the stellar core <span xss=removed>is exposed</span>.</p>', '<p>For average</p>', '<p>Eject</p>', '<p>Continues</p>', '<p>Is exposed</p>', '', -1, 4, -1, -1, -1, 0, '', '', '', '', '', 'B', '', 1592017325, 1592017325),
(293, 21, 23, 32, 0, '', '', '<p>31. If a white dwarf <span xss=removed>forms</span> in a binary or multiple star system, it may <span xss=removed>experiencing</span>  a more <span xss=removed>eventful</span> <span xss=removed>demise</span> as a nova.</p>', '<p>Form</p>', '<p>Experiencing</p>', '<p>Eventful</p>', '<p>Demise</p>', '', -1, 4, -1, -1, -1, 0, '', '', '', '', '', 'B', '', 1592017396, 1592017396),
(294, 21, 23, 32, 0, '', '', '<p>32. If a white dwarf is close enough to a <span xss=removed>companion</span> star, its gravity may drag matter - <span xss=removed>most</span> hydrogen - from the outer layers of that star <span xss=removed>onto</span> itself, building up <span xss=removed>its</span> surface layer.</p>', '<p>Companion</p>', '<p>Most</p>', '<p>Onto</p>', '<p>Its</p>', '', -1, 4, -1, -1, -1, 0, '', '', '', '', '', 'B', '', 1592017469, 1592017469),
(295, 21, 23, 32, 0, '', '', '<p>33. When enough <span xss=removed>hydrogens</span> has accumulated on the surface, a <span xss=removed>burst</span> of nuclear <span xss=removed>fusion</span> occurs, causing the white dwarf to brighten <span xss=removed>substantially</span> and expel the remaining material.</p>', '<p>Hydrogens</p>', '<p>Burst</p>', '<p>Fusion</p>', '<p>Substantially</p>', '', 4, -1, -1, -1, -1, 0, '', '', '', '', '', 'A', '', 1592017558, 1592017558),
(296, 21, 23, 32, 0, '', '', '<p>34. David Robert Joseph Beckham is an <span xss=removed>English</span> <span xss=removed>former</span> professional footballer, <span xss=removed>the currently</span> president of Inter Miami CF and <span xss=removed>co-owner</span> of Salford City.</p>', '<p>English</p>', '<p>Former</p>', '<p>The currently</p>', '<p>Co-owner</p>', '', -1, -1, 4, -1, -1, 0, '', '', '', '', '', 'C', '', 1592017660, 1592017660),
(297, 21, 23, 32, 0, '', '', '<p>35. He played <span xss=removed>for</span> Manchester United, Preston North End, Real Madrid, Milan, LA Galaxy, Paris Saint-Germain and the England <span xss=removed>national team</span>, for which he held the <span xss=removed>appears</span> record for an <span xss=removed>outfield</span> player until 2016</p>', '<p>For</p>', '<p>National team</p>', '<p>Appears</p>', '<p>Outfield player</p>', '', -1, -1, 4, -1, -1, 0, '', '', '', '', '', 'C', '', 1592017854, 1592017854),
(298, 21, 23, 32, 0, '', '', '<p>36. Known <span xss=removed>of</span> his range of passing, crossing <span xss=removed>ability</span> and <span xss=removed>bending</span> free-kicks as a right winger, Beckham has been <span xss=removed>hailed</span> as one of the greatest midfielders of all time.</p>', '<p>Of</p>', '<p>Ability</p>', '<p>Bending</p>', '<p>Hailed</p>', '', 4, -1, -1, -1, -1, 0, '', '', '', '', '', 'A', '', 1592017945, 1592017945),
(299, 21, 23, 32, 0, '', '', '<p>37. Beckham has <span xss=removed>consistent</span> ranked among the highest earners in football, and in 2013 was <span xss=removed>listed</span> as the <span xss=removed>highest-paid</span> player in the world, having earned over $50 million in the <span xss=removed>previous</span> 12 months.</p>', '<p>Consistent</p>', '<p>Listed</p>', '<p>Highest-paid</p>', '<p>Previous</p>', '', 4, -1, -1, -1, -1, 0, '', '', '', '', '', 'A', '', 1592018412, 1592018412),
(300, 21, 23, 32, 0, '', '', '<p>38. He has been <span xss=removed>marriaged</span> to Victoria Beckham <span xss=removed>since</span> 1999 and they <span xss=removed>have</span> four <span xss=removed>children</span>.</p>', '<p>Marriaged</p>', '<p>Since</p>', '<p>Have</p>', '<p>Children</p>', '', 4, -1, -1, -1, -1, 0, '', '', '', '', '', 'A', '', 1592018508, 1592018508),
(301, 21, 23, 32, 0, '', '', '<p>39. His parents were <span xss=removed>fanatical</span> Manchester United supporters who <span xss=removed>frequent</span> travelled to Old Trafford <span xss=removed>from</span> London to <span xss=removed>attend</span> the team\'s home matches. </p>', '<p>Fanatical</p>', '<p>Frequent</p>', '<p>From</p>', '<p>Attend</p>', '', -1, 4, -1, -1, -1, 0, '', '', '', '', '', 'B', '', 1592018583, 1592018583),
(302, 21, 23, 32, 0, '', '', '<p>40. Beckham <span xss=removed>returned</span> to Manchester and made his Premier League <span xss=removed>debut</span> for Manchester United <span xss=removed>in</span> 2 April 1995, in a <span xss=removed>goalless</span> draw against Leeds United.</p>', '<p>Returned</p>', '<p>Debut</p>', '<p>In</p>', '<p>Goalless</p>', '', -1, -1, 4, -1, -1, 0, '', '', '', '', '', 'C', '', 1592018712, 1592018712),
(303, 21, 23, 32, 0, '', '', '<p>41. The new service was expected to be a success; __________ very few customers upgraded their accounts.</p>', '<p>Although</p>', '<p>Yet</p>', '<p>Just</p>', '<p>Moreover</p>', '', -1, 4, -1, -1, -1, 0, '', '', '', '', '', 'B', '', 1592018764, 1592018764),
(304, 21, 23, 32, 0, '', '', '<p>42. Strong exports ___________ in driving first-quarter growth, rising 35 percent from a year earlier.</p>', '<p>Played a big role</p>', '<p>Instrumental</p>', '<p>Playd a hand</p>', '<p>Effectively</p>', '', 4, -1, -1, -1, -1, 0, '', '', '', '', '', 'A', '', 1592018817, 1592018817),
(305, 21, 23, 32, 0, '', '', '<p>43. The software developers __________ investigated the latest problem.</p>', '<p>Are just</p>', '<p>Still</p>', '<p>Have Al ready</p>', '<p>Have yet</p>', '', -1, -1, 4, -1, -1, 0, '', '', '', '', '', 'C', '', 1592018884, 1592018884),
(306, 21, 23, 32, 0, '', '', '<p>44. The company expects to see ___________ breakeven and a 15 cent a share loss in the second quarter.</p>', '<p>More than</p>', '<p>Approxmately</p>', '<p>More or less</p>', '<p>Somewhere between</p>', '', -1, -1, -1, 4, -1, 0, '', '', '', '', '', 'D', '', 1592019009, 1592019009),
(307, 21, 23, 32, 0, '', '', '<p>45. First quarter revenue _________ $45.1 billion from $44.7 billion a year earlier.</p>', '<p>Increased</p>', '<p>Rose to</p>', '<p>Declined from</p>', '<p>Expaded</p>', '', -1, 4, -1, -1, -1, 0, '', '', '', '', '', 'B', '', 1592019193, 1592019193),
(308, 21, 23, 32, 0, '6b33827078ba309eabf3cbfd385cef1c.jpg', 'image/jpeg', '<p>46. The purpose of the text is to…</p>', '<p>compare the perfumes from different countries</p>', '<p>describe the history of perfume making</p>', '<p>describe the problems faced by perfumers</p>', '<p>explain the different uses of perfume over time</p>', '', -1, 4, -1, -1, -1, 0, '', '', '', '', '', 'B', '', 1592029178, 1592029178),
(309, 21, 23, 32, 0, '08d5fe65db2a312b319d3bceaa4d8662.jpg', 'image/jpeg', '<p>47. Which of the following is NOT true about perfume making in Islamic countries?</p>', '<p>They created perfume by soaking flower petals in oil.</p>', '<p>They dominated perfume making after the fall of the Roman Empire.</p>', '<p>They took raw materials for their perfumes from India.</p>', '<p>They created a technique which required fewer plant materials.</p>', '', 4, -1, -1, -1, -1, 0, '', '', '', '', '', 'A', '', 1592029347, 1592029347),
(310, 21, 23, 32, 0, '0341981a0f4b38fbda5ae9067a457412.jpg', 'image/jpeg', '<p>48.  Why does the writer include this sentence in paragraph 2? \"During the Black Death, the bubonic plague was thought to have resulted from a bad odour which could be averted by inhaling pleasant fragrances such as cinnamon.\"</p>', '<p>To explain why washing was not popular during the Black Death</p>', '<p>To show how improper use of perfume caused widespread disease</p>', '<p>To illustrate how perfumes used to be ingested to treat disease</p>', '<p>To give an example of how fragrances were used for health purposes</p>', '', -1, -1, -1, 4, -1, 0, '', '', '', '', '', 'D', '', 1592029482, 1592029482),
(311, 21, 23, 32, 0, '946cfa822c7df5856973a7781791355b.jpg', 'image/jpeg', '<p>49. Why did the perfume industry develop in Paris?</p>', '<p>Because it was an important trade route</p>', '<p>Because of the rise in the glove-making industry</p>', '<p>Because of the introduction of new trade laws</p>', '<p>Because of a new fashion in scented gloves</p>', '', -1, -1, 4, -1, -1, 0, '', '', '', '', '', 'C', '', 1592029972, 1592029972),
(312, 20, 18, 32, 0, '5e2a4ceedab30ea5faae70aa028675c4.jpg', 'image/jpeg', '<p>What does putrid mean (paragraph 3)? </p>', '<p>Bad-smelling</p>', '<p>Rare</p>', '<p>Prestigious</p>', '<p>Numerous</p>', '', 4, -1, -1, -1, -1, 0, '', '', '', '', '', 'A', '', 1592030086, 1592065909),
(313, 20, 18, 32, 0, 'bbcbde4447fccbd1a87d9820a224f2e0.jpg', 'image/jpeg', '<p>Which of the following people most influenced the decline of perfumes as medicine?</p>', '<p>Louis XIV</p>', '<p>Louis XV</p>', '<p>Rimmel</p>', '<p>Napoleon</p>', '', -1, -1, -1, 4, -1, 0, '', '', '', '', '', 'D', '', 1592030187, 1592065851),
(314, 20, 18, 32, 0, '6eaa9b570c938f53123a9e622edd2cc7.jpg', 'image/jpeg', '<p>In paragraph 4, it is implied that…</p>', '<p>Master glove and perfume makers created a new perfume each week.</p>', '<p>Mercers, spicers and other traders began to call themselves masters.</p>', '<p>The Royal Court only bought perfume from masters.</p>', '<p>Cosmetics were still only popular within the Royal Courts.</p>', '', -1, -1, 4, -1, -1, 0, '', '', '', '', '', 'C', '', 1592030289, 1592065818),
(315, 20, 18, 32, 0, '220afc1ff4ca88de07e9ff440bf40544.jpg', 'image/jpeg', '<p>How did the French Revolution affect the Parisian perfume industry?</p>', '<p>The industry declined then rose again.</p>', '<p>The industry collapsed and took a long time to recover.</p>', '<p>The industry was greatly boosted.</p>', '<p>The industry lost most of its overseas customers.</p>', '', 4, -1, -1, -1, -1, 0, '', '', '', '', '', 'A', '', 1592030371, 1592065794),
(316, 20, 18, 32, 0, '', '', '<p>London came to lead the perfume industry because…</p>', '<p>the French Revolution meant that there were fewer customers in France.</p>', '<p>Napoleon’s new laws affected the profitability of perfume-making.</p>', '<p>the production of perfume ceased during the Napoleonic wars.</p>', '<p>the French were unable to export perfumes for a period of time.</p>', '', -1, -1, -1, 4, -1, 0, '', '', '', '', '', 'D', '', 1592030457, 1592065774),
(317, 20, 18, 32, 0, '024e8a4918b034a497865d1b8b6e12c1.jpg', 'image/jpeg', '<p>Which of the following is NOT true of Rimmel?</p>', '<p>He was one of the first people to utilise trademarks.</p>', '<p>He created attractive packaging for his products.</p>', '<p>His products were more expensive than other brands.</p>', '<p>He transported his goods to potential customers by train.</p>', '', 4, -1, -1, -1, -1, 0, '', '', '', '', '', 'A', '', 1592030802, 1592065609),
(318, 21, 23, 35, 0, '', '', '<p>Most of us……….24 SKS this semester.</p>', '<p>to take</p>', '<p>are  taking</p>', '<p>took</p>', '<p>be taking</p>', '<p>-</p>', -1, 4, -1, -1, -1, 0, '', '', '', '', '', 'B', '<p>are taking menandakan continous. mengapa continous? karena dianggapnya this year sebagai around the time</p>', 1592033793, 1592033793),
(319, 21, 23, 35, 0, '', '', '<p>What are they doing right now?</p>', '<p>They are doing swimming.</p>', '<p>They swim right now.</p>', '<p>They are swimming.</p>', '<p>They usually go swimming.</p>', '<p>-</p>', -1, -1, 4, -1, -1, 0, '', '', '', '', '', 'C', '<p>sangat jelas now menunjukkan continous</p>', 1592033868, 1592033868),
(320, 21, 23, 35, 0, '', '', '<p>Mega can’t come to the conference right now because she………. her little baby.</p>', '<p>taking care of</p>', '<p>is taking care of</p>', '<p>is takes care of</p>', '<p>be taking care of</p>', '<p>-</p>', -1, 4, -1, -1, -1, 0, '', '', '', '', '', 'B', '<p>sangat jelas berbentuk continous</p>', 1592034004, 1592034004),
(321, 21, 23, 35, 0, '', '', '<p>It is still raining now outside. Therefore, the riders ………. their rain coat.</p>', '<p>are wearing</p>', '<p>will wearing</p>', '<p>is wearing</p>', '<p>have wearing</p>', '', 4, -1, -1, -1, -1, 0, '', '', '', '', '', 'A', '<p>subjeknya adalah riders jadi predikatnya harus menyesuaikan</p>', 1592034076, 1592034076),
(322, 21, 23, 35, 0, '', '', '<p>What time does your brother get up?</p>', '<p>He get up at 5 am.</p>', '<p>He always gets up at 5 am.</p>', '<p>He usually got up at 5 am.</p>', '<p>She always gets up at 5 am.</p>', '', -1, 4, -1, -1, -1, 0, '', '', '', '', '', 'B', '<p>sangat jelas, does dalam pertanyaan menunjukkan present, subjek nya he jadi gets</p>', 1592034168, 1592034168),
(323, 21, 23, 35, 0, '', '', '<p>Father ………. a car but he……….it very often.</p>', '<p>does not have</p>', '<p>has- does not drive</p>', '<p>had-does not drive</p>', '<p>has – is not driving</p>', '', -1, 4, -1, -1, -1, 0, '', '', '', '', '', 'B', '<p>perhatikan saja kesesuaian dan makna kata</p>', 1592034233, 1592034233),
(324, 21, 23, 35, 0, '', '', '<p>The baby …………. for three hours by now.</p>', '<p>has sleeping</p>', '<p>has been sleeping</p>', '<p>has sleep</p>', '<p>has been slept</p>', '<p>-</p>', -1, 4, -1, -1, -1, 0, '', '', '', '', '', 'B', '<p>The baby …………. for three hours by now.</p>\r\n<p> </p>\r\n<p>for three hours menunjukkan durasi sehingga perfect, by now menunjukkan continous</p>', 1592034331, 1592034331),
(325, 21, 23, 35, 0, '', '', '<p> ………. You and Ahmed at the library last night?</p>', '<p>Were</p>', '<p>Did</p>', '<p>Are</p>', '<p>Do</p>', '<p>-</p>', 4, -1, -1, -1, -1, 0, '', '', '', '', '', 'A', '<p>yang ditanya akan menjawab: yes, we were. so pertanyaanya ya were you....</p>', 1592034810, 1592034810),
(326, 21, 23, 35, 0, '', '', '<p>My older sister receives a lot of flowers ……….</p>', '<p>now</p>', '<p>tomorrow</p>', '<p>every Sunday</p>', '<p>yesterday</p>', '', -1, -1, 4, -1, -1, 0, '', '', '', '', '', 'C', '<p>receives merupakan ciri present simple.</p>\r\n<p>present simple digunakan salah satunya untuk rutinitas present :)</p>', 1592035191, 1592035191),
(327, 21, 23, 35, 0, '', '', '<p>X: Where did you buy your new book last night? Y: ……….</p>', '<p>I sold it in Gramedia.</p>', '<p>I borrowed it from my friend.</p>', '<p>I bought it in Fajar Agung.</p>', '<p>I took it in Ramayana.</p>', '', -1, -1, 4, -1, -1, 0, '', '', '', '', '', 'C', '<p>jelas kan ya, karena yang ditanyakan buy</p>', 1592035369, 1592035369),
(328, 21, 23, 35, 0, '', '', '<p>Carmen and I ……… the lunch yet. So, we are very hungry now.</p>', '<p>haven’t eat</p>', '<p>haven’t eating</p>', '<p>have eaten</p>', '<p>haven’t eaten</p>', '', -1, -1, -1, 4, -1, 0, '', '', '', '', '', 'D', '<p>ada kata yet.</p>\r\n<p>have+v3+yet=belum</p>', 1592035445, 1592035445),
(329, 21, 23, 35, 0, '', '', '<p>X: Why didn’t you answer my phone last night?</p>\r\n<p>Y: Sorry, I ………. out to meet my lecturer, and I left my mobile phone at home.</p>', '<p>go</p>', '<p>am going</p>', '<p>went</p>', '<p>have gone</p>', '', -1, -1, 4, -1, -1, 0, '', '', '', '', '', 'C', '<p>karena last night jawabannya pasti past tense. yang past tense hanya went saja </p>', 1592035592, 1592035592),
(330, 21, 23, 35, 0, '', '', '<p>Sam ………. a very terrible accident on the avenue yesterday.</p>', '<p>to see</p>', '<p>saw</p>', '<p>being saw</p>', '<p>is seen</p>', '', -1, 4, -1, -1, -1, 0, '', '', '', '', '', 'B', '<p>ada kata yesterday yang menunjukkan past tense</p>', 1592035692, 1592035692),
(331, 21, 23, 35, 0, '', '', '<p>What did you do two hours ago?  </p>', '<p>I watched TV</p>', '<p>I will study English</p>', '<p>I am reading a book</p>', '<p>I have breakfast</p>', '', 4, -1, -1, -1, -1, 0, '', '', '', '', '', 'A', '<p>aux. verb pertanyaan adalah did, jadi jawabannya pun harus dalam bentuk simple past tense</p>', 1592035783, 1592035783);
INSERT INTO `tbl_soal_tryout` (`id_soal`, `dosen_id`, `matkul_id`, `id_soal_paket`, `bobot`, `file`, `tipe_file`, `soal`, `opsi_a`, `opsi_b`, `opsi_c`, `opsi_d`, `opsi_e`, `bobot_a`, `bobot_b`, `bobot_c`, `bobot_d`, `bobot_e`, `bobot_kosong`, `file_a`, `file_b`, `file_c`, `file_d`, `file_e`, `jawaban`, `pembahasan`, `created_on`, `updated_on`) VALUES
(332, 21, 23, 35, 0, '', '', '<p>My uncle ………. me a modern laptop last new year.</p>', '<p>bought</p>', '<p>was bought</p>', '<p>have bought</p>', '<p>to bought</p>', '', 4, -1, -1, -1, -1, 0, '', '', '', '', '', 'A', '<p>ada penunjuk waktu last year dan lihatlah artinya.</p>', 1592035854, 1592035854),
(333, 21, 23, 35, 0, '', '', '<p>Desi, Mitha, and Nina ………. here for ten years.</p>', '<p>been</p>', '<p>being have</p>', '<p>have been</p>', '<p>be have been</p>', '', -1, -1, 4, -1, -1, 0, '', '', '', '', '', 'C', '<p>for ten years menunjukkan durasi, ciri dari perfect</p>', 1592035941, 1592035941),
(334, 21, 23, 35, 0, '', '', '<p>They have known each other since ……….</p>', '<p>two days</p>', '<p>tomorrow</p>', '<p>2003</p>', '<p>three years</p>', '', -1, -1, 4, -1, -1, 0, '', '', '', '', '', 'C', '<p>ada kata since jadi yang dibutuhkan adalah waktu sebagai acuan mulainya kapan</p>', 1592036273, 1592036273),
(335, 21, 23, 35, 0, '', '', '<p>Ms. Jenifer ……….  a lot of novels since she was a teenager.</p>', '<p>has being read</p>', '<p>has read</p>', '<p>has been read</p>', '<p>has readed</p>', '', -1, 4, -1, -1, -1, 0, '', '', '', '', '', 'B', '<p>v1 read</p>\r\n<p>v2 read</p>\r\n<p>v3 read</p>\r\n<p> </p>\r\n<p>hanya beda pengucapan yak</p>', 1592036334, 1592036334),
(336, 21, 23, 35, 0, '', '', '<p>X: Are you and your sister going to ………. the movie tonight?                                                         </p>\r\n<p>Y: No, we are not. We are very busy this evening.</p>', '<p>come to</p>', '<p>see</p>', '<p>play</p>', '<p>theater</p>', '', -1, 4, -1, -1, -1, 0, '', '', '', '', '', 'B', '<p>goint to+ v1, artinya sama dengan will hanya memiliki tingkat kepastian yang lebih tinggi</p>', 1592036421, 1592036421),
(337, 21, 23, 35, 0, '', '', '<p>A: Hello, my name is Alice Wong </p>\r\n<p>B: Hi, I’m Susan Crane. </p>\r\n<p>A: Sorry, ………. ?</p>\r\n<p>B: It’s C-R-A-N-E</p>', '<p>How do you spell your last name</p>', '<p>What’s your last name</p>', '<p>How do you spell your first name</p>', '<p>What’s your spelling</p>', '', 4, -1, -1, -1, -1, 0, '', '', '', '', '', 'A', '<p>sudah jelas yak, cara menanyakan pengejaan ya seperti itu </p>', 1592036505, 1592036505),
(338, 21, 23, 35, 0, '', '', '<p>A: Are you a doctor? </p>\r\n<p>B: No, I ……….  a dentist.</p>', '<p>am not</p>', '<p>don’t</p>', '<p>am</p>', '<p>was</p>', '', -1, -1, 4, -1, -1, 0, '', '', '', '', '', 'C', '<p>sudah sangat jelas</p>', 1592036621, 1592036621),
(339, 21, 23, 35, 0, '', '', '<p> ………. your brother and sister ………. four languages?</p>', '<p>Did-speaks</p>', '<p>Does-speak</p>', '<p>Do-speaks</p>', '<p>Do-speak</p>', '', -1, -1, -1, 4, -1, 0, '', '', '', '', '', 'D', '<p>brother and sister jamak, jadi aux. verb nya do</p>', 1592036698, 1592036698),
(340, 21, 23, 35, 0, '', '', '<p>Dina …….. her little brother to buy her some foods at the moment.</p>', '<p>askes</p>', '<p>is asking</p>', '<p>asked</p>', '<p>asks</p>', '', -1, 4, -1, -1, -1, 0, '', '', '', '', '', 'B', '<p>at the moment menunjukkan continous</p>', 1592036750, 1592036750),
(341, 21, 23, 35, 0, '', '', '<p>What ………….. Let’s go to the party.</p>', '<p>are you waiting for?</p>', '<p>you are waiting for?</p>', '<p>will you waiting for?</p>', '<p>do you waiting for?</p>', '', 4, -1, -1, -1, -1, 0, '', '', '', '', '', 'A', '<p>ketika pertanyaan, maka aux. verbnya di depan (inverse), seperti di soal</p>\r\n<p> </p>\r\n<p>pada saat what sebagai relative pronoun, maka tidak boleh diinverse. contoh:</p>\r\n<p> </p>\r\n<p>you always hate what I am doing.</p>\r\n<p>bukan what am I doing</p>', 1592036860, 1592036860),
(342, 21, 23, 35, 0, '', '', '<p>Jim and I …………. lunch at the cafeteria two hours ago.</p>', '<p>eat</p>', '<p>eating</p>', '<p>ate</p>', '<p>eats</p>', '', -1, -1, 4, -1, -1, 0, '', '', '', '', '', 'C', '<p>two hours ago menunjukkan past tense</p>', 1592036921, 1592036921),
(343, 21, 23, 35, 0, '', '', '<p>Nadia isn’t in class today because she …………. flu.</p>', '<p>is getting</p>', '<p>got</p>', '<p>has got</p>', '<p>gotten</p>', '', 4, -1, -1, -1, -1, 0, '', '', '', '', '', 'A', '<p>menunjukkan sekitar hari ini dia itu sedang flu</p>', 1592036992, 1592036992),
(344, 21, 23, 35, 0, '', '', '<p>The class ……. at 09.00 but it begins at 08.00 am in the morning.</p>', '<p>isn’t begin</p>', '<p>doesn’t begins</p>', '<p>doesn’t begin</p>', '<p>don’t begin</p>', '', -1, -1, 4, -1, -1, 0, '', '', '', '', '', 'C', '<p>subjeknya the class (singular) dan begins menunjukkan present</p>', 1592037060, 1592037060),
(345, 21, 23, 35, 0, '', '', '<p>Sometimes I worry about my grades at college but Sonya never …………. about her grades.</p>', '<p>worry</p>', '<p>worries</p>', '<p>worrying</p>', '<p>is worries</p>', '', -1, 4, -1, -1, -1, -2, '', '', '', '', '', 'B', '<p>subjeknya Sonya, singular dan present</p>', 1592037295, 1592037295),
(346, 21, 23, 35, 0, '', '', '<p>It rains a lot in this city, but it …………. right now</p>', '<p>isn’t rain</p>', '<p>isn’t rains</p>', '<p>don\'t rain</p>', '<p>isn’t raining</p>', '', -1, -1, -1, 4, -1, 0, '', '', '', '', '', 'D', '<p>subjeknya it dan ada keterangan now, ada conjunction but yang menunjukkan kontra jadi harus negatif</p>', 1592037370, 1592037370),
(347, 21, 23, 35, 0, '', '', '<p>Nita cooks her own dinner every evening. Right now she …………. rice and beans.</p>', '<p>cooking</p>', '<p>is cook</p>', '<p>cooks</p>', '<p>is cooking</p>', '', -1, -1, -1, 4, -1, 0, '', '', '', '', '', 'D', '<p>ada akta right now dan subjeknya she, jadi present continous</p>', 1592037502, 1592037502),
(348, 21, 23, 35, 0, '019680142679eb5d8d73564a9b338832.jpg', 'image/jpeg', '<p>The phrase “taken to heart” in line 1 is closest in meaning to which of the following?</p>', '<p>Taken Seriously</p>', '<p>Criticized</p>', '<p>Memorized</p>', '<p>Taken Offence</p>', '', 4, -1, -1, -1, -1, 0, '', '', '', '', '', 'A', '<p><span xss=removed>Frasa taken to heart adalah bentuk ungkapan idiomatik dan memiliki arti “melakukan dengan sepenuh hati/serius”.</span></p>', 1592037880, 1592039207),
(349, 21, 23, 35, 0, '4d7d2dd0894b7b9ce826c8196bcde6bd.jpg', 'image/jpeg', '<p>In what way did Wright’s public buildings differ from most of those built by earlier architects?</p>', '<p>They were built on a larger scale.</p>', '<p>Their materials came from the southern United States.</p>', '<p>They looked more like private homes.</p>', '<p>Their designs were based on how they would be used.</p>', '', -1, -1, -1, 4, -1, 0, '', '', '', '', '', 'D', '<p><span xss=removed>Pada kalimat “…that form should follow function…” menunjukkan bahwa desain seharusnya berdasarkan pada fungsinya, untuk apa nantinya bangunan itu digunakan dan jawaban yang demikian ditemukan di pilihan D.</span></p>', 1592037925, 1592039238),
(350, 21, 23, 35, 0, 'ce7af8b6371985e04ba8e19d1b796727.jpg', 'image/jpeg', '<p>The author mentions the Unity Temple because, it</p>', '<p>was Wright’s ﬁrst building</p>', '<p>inﬂuenced the architecture of subsequent churches</p>', '<p>demonstrated traditional ecclesiastical architecture</p>', '<p>was the largest church Wright ever designed</p>', '', -1, 4, -1, -1, -1, 0, '', '', '', '', '', 'B', '<p><span xss=removed>Pada kalimat “…the ﬁrst of those churches that did so much to revolutionize ecclesiastical architecture in the United States” menyatakan bahwa bangunan itu sangat mempengaruhi desain bangunan-bangunan selanjutnya. Jawaban yang mengandung ide serupa ada pada pilihan B.</span></p>', 1592037984, 1592039255),
(351, 21, 23, 35, 0, '82ea99c0711eca996422a9b0b7a22d0b.jpg', 'image/jpeg', '<p>The passage mentions that all of the following structures were built by Wright EXCEPT</p>', '<p>Factories</p>', '<p>public buildings</p>', '<p>offices</p>', '<p>southern plantations</p>', '', -1, -1, -1, 4, -1, 0, '', '', '', '', '', 'D', '<p class=\"MsoNormal\" xss=removed><span xss=removed>Pada kalimat terakhir jelas bahwa oﬃces, factories dan public building, termasuk churches adalah karyanya. Sedangkan South Plantations bukanlah hasil karyanya karena sudah didesain oleh orang terdahulu seperti yang disebutkan dalam bacaan.</span></p>', 1592038051, 1592039276),
(352, 21, 23, 35, 0, '', '', '<p>Which of the following statements best reﬂects one of Frank Lloyd Wright’s architectural principles?</p>', '<p>Beautiful design is more important than utility.</p>', '<p>Ecclesiastical architecture should be derived from traditional designs.</p>', '<p>A building should ﬁt into its surroundings.</p>', '<p>The architecture of public buildings does not need to be revolutionary</p>', '', -1, -1, 4, -1, -1, 0, '', '', '', '', '', 'C', '<p><span xss=removed>Kalimat “…as parts of an organic whole that included the land, the community, and the society” menyatakan bahwa bangunan seharusnya dianggap sebagai bagian dari keseluruhan lingkungan dan masyarakat sekitarnya sehingga dengan kata lain bangunan tersebut harus sesuai dengan lingkungannya.</span></p>', 1592038093, 1592039305),
(353, 21, 23, 35, 0, 'a3d339feae55f1fb489b60b849d53682.jpg', 'image/jpeg', '<p><span xss=removed>What does the passage mainly discuss?</span></p>', '<p><span xss=removed>Where major glaciers are located</span></p>', '<p><span xss=removed>How glaciers shape the land</span></p>', '<p><span xss=removed>How glaciers are formed</span></p>', '<p><span xss=removed>The different kinds of glaciers</span></p>', '', -1, -1, -1, 4, -1, 0, '', '', '', '', '', 'D', '<p><span xss=removed>Ide pokok teks di atas dapat ditemukan pada paragraf pertama kalimat pertama yang menjelaskan dua jenis gletser yang berbeda dan selanjutnya masing-masing dijelaskan lebih lanjut pada paragraf berikutnya.</span></p>', 1592038475, 1592038475),
(354, 21, 23, 35, 0, 'd0f2e5a8f8eafc355d430de7889150c4.jpg', 'image/jpeg', '<p><span xss=removed>The word “massive” in line 3 is closest in meaning to</span></p>', '<p><span xss=removed>huge</span></p>', '<p><span xss=removed>strange</span></p>', '<p><span xss=removed>cold</span></p>', '<p xss=removed><span xss=removed>recent</span></p>', '', 4, -1, -1, -1, -1, 0, '', '', '', '', '', 'A', '<p xss=removed><span xss=removed>Padanan kata massive adalah huge (sangat besar).</span></p>', 1592038546, 1592038546),
(355, 21, 23, 35, 0, '2d4c477aaec8d3bf149c2cbcc67b1440.jpg', 'image/jpeg', '<p><span xss=removed>It can be inferred that ice sheets are so named for which of the following reasons?</span></p>', '<p><span xss=removed>They are conﬁned to mountain valleys.</span></p>', '<p><span xss=removed>They cover large areas of land.</span></p>', '<p><span xss=removed>They are thicker in some areas than in others.</span></p>', '<p><span xss=removed>They have a characteristic circular shape.</span></p>', '', -1, 4, -1, -1, -1, 0, '', '', '', '', '', 'B', '<p><span xss=removed>Frasa whole continents pada kalimat those massive blankets that cover whole continents appropriately called ice sheet menunjukkan bahwa daratan yang tertutup gletser mencakup wilayah yang sangat luas, hampir keseluruhan daratan.</span></p>', 1592038598, 1592038598),
(356, 21, 23, 35, 0, 'c621681e787f1b4e4024183dd0e6f0b9.jpg', 'image/jpeg', '<p><span xss=removed>According to the passage, ice shelves can be found</span></p>', '<p><span xss=removed>covering an entire continent</span></p>', '<p><span xss=removed>covering an entire continent</span></p>', '<p><span xss=removed>spreading into the ocean</span></p>', '<p><span xss=removed>ﬁlling deep valleys</span></p>', '', -1, -1, 4, -1, -1, 0, '', '', '', '', '', 'C', '<p><span xss=removed>Jawaban dapat ditemukan pada kalimat terakhir paragraf kedua bahwa jika ice sheet tersebar (spread out) di lautan akan terbentuk ice shelves.</span></p>', 1592038654, 1592038654),
(357, 21, 23, 35, 0, '635dfaaf6b65608267ae5a509ae7bc33.jpg', 'image/jpeg', '<p><span xss=removed>According to the passage, where was the Cordilleran Ice Sheet thickest?</span></p>', '<p><span xss=removed>Alaska</span></p>', '<p><span xss=removed>Greenland</span></p>', '<p><span xss=removed>Alberta</span></p>', '<p><span xss=removed>Antarctica</span></p>', '', -1, -1, 4, -1, -1, 0, '', '', '', '', '', 'C', '<p><span xss=removed>Jawaban dapat ditemukan pada baris ke-8: it was about 3 kilometers deep at its thickest point in northern Alberta.</span></p>', 1592038709, 1592038709),
(358, 21, 23, 35, 0, '5672d9dc195dcb53535be70f0711d74c.jpg', 'image/jpeg', '<p><span xss=removed>The word “rare” in line 12 is closest in meaning to</span></p>', '<p><span xss=removed>small</span></p>', '<p><span xss=removed> unusual</span></p>', '<p><span xss=removed>valuable</span></p>', '<p><span xss=removed>widespread</span></p>', '', -1, 4, -1, -1, -1, 0, '', '', '', '', '', 'B', '<p><span xss=removed>Rare berarti jarang atau tidak biasa ditemukan sehingga jawaban yang paling mendekati arti dari rare adalah unusual.</span></p>', 1592038761, 1592038761),
(359, 21, 23, 35, 0, 'd54292213232aac8da596b07f5472088.jpg', 'image/jpeg', '<p><span xss=removed>According to the passage (paragraph 5), ice ﬁelds resemble ice caps in which of the following ways?</span></p>', '<p><span xss=removed>Their shape</span></p>', '<p><span xss=removed>Their ﬂow</span></p>', '<p><span xss=removed>Their texture</span></p>', '<p><span xss=removed>Their location</span></p>', '', -1, 4, -1, -1, -1, 0, '', '', '', '', '', 'B', '<p><span xss=removed>Kalimat Mountain glaciers are typically identiﬁed by the landform that controls their ﬂow dan dijelaskan lebih lanjut pada kalimat One form of mountain glacier that resembles an ice cap in that it ﬂows outward in several directions is called an ice ﬁeld menunjukkan bahwa pembentukan ice caps dipengaruhi oleh alirannya (their ﬂow).</span></p>', 1592038843, 1592038843),
(360, 21, 23, 35, 0, '456ee2679e51ed2332246f9ce9e461c1.jpg', 'image/jpeg', '<p><span xss=removed>The word “it” in line 16 refers to</span></p>', '<p><span xss=removed>glacier</span></p>', '<p><span xss=removed>cap</span></p>', '<p><span xss=removed>difference</span></p>', '<p><span xss=removed>terrain</span></p>', '', 4, -1, -1, -1, -1, 0, '', '', '', '', '', 'A', '<p><span xss=removed>It pada kalimat di atas merujuk pada mountain glacier atau gletser.</span></p>', 1592038906, 1592038906),
(361, 21, 23, 35, 0, '558c918da2e53adb3523f8f79eab2827.jpg', 'image/jpeg', '<p><span xss=removed>The word “subtle” in line 17 is closest in meaning to</span></p>', '<p><span xss=removed>slight</span></p>', '<p><span xss=removed>common</span></p>', '<p><span xss=removed>important</span></p>', '<p><span xss=removed>measurable</span></p>', '', 4, -1, -1, -1, -1, 0, '', '', '', '', '', 'A', '<p xss=removed><span xss=removed>Padanan kata subtle adalah slight yang keduanya memiliki arti “tipis, halus, atau sedikit”.</span></p>', 1592038968, 1592038968),
(362, 21, 23, 35, 0, '01322e419ea3047925fd465062d153ea.jpg', 'image/jpeg', '<p xss=removed><span xss=removed>Padanan kata subtle adalah slight yang keduanya memiliki arti “tipis, halus, atau sedikit”.</span></p>', '<p><span xss=removed>cirque glaciers</span></p>', '<p><span xss=removed>ice caps</span></p>', '<p><span xss=removed>valley glaciers</span></p>', '<p><span xss=removed>ice ﬁelds</span></p>', '', -1, 4, -1, -1, -1, 0, '', '', '', '', '', 'B', '<p xss=removed><span xss=removed>Jenis alpine/mountain glacier adalah ice ﬁelds, cirque glaciers dan valley glaciers yang semuanya dipengaruhi oleh aliran dan tidak memiliki domelike cap seperti ice caps.</span></p>', 1592039042, 1592039042),
(363, 21, 23, 35, 0, '4eccc6e2136e3e2658595cf4e558b13f.jpg', 'image/jpeg', '<p><span xss=removed>Which of the following types of glaciers does the author use to illustrate the two basic types of glaciers mentioned in line 1?</span></p>', '<p><span xss=removed>Ice ﬁelds and cirques</span></p>', '<p><span xss=removed>Cirques and alpine glaciers<br>&lt;!-- [if !supportLineBreakNewLine]--&gt;<br>&lt;!--[endif]--&gt;</span></p>', '<p><span xss=removed>Ice sheets and ice shelves</span></p>', '<p xss=removed><span xss=removed>Ice sheets and mountain glaciers</span></p>', '', -1, -1, -1, 4, -1, 0, '', '', '', '', '', 'D', '<p xss=removed><span xss=removed>Ada dua jenis gletser yaitu ice sheet yang mengalir keluar ke segala arah dan mountain glacier yang arah alirannya terbatas hanya di aliran tertentu.</span></p>', 1592039108, 1592039108),
(364, 21, 23, 35, 0, '', '', '<p>Earth\'s Moon <span xss=removed>is</span> an astronomical body that <span xss=removed>orbit</span> the planet and <span xss=removed>acts</span> as <span xss=removed>its</span> only permanent natural satellite.</p>', '<p>is</p>', '<p>orbit</p>', '<p>acts</p>', '<p>its</p>', '', -1, 4, -1, -1, -1, 0, '', '', '', '', '', 'B', '<p>Subject-nya adalah earth’s moon, tunggal. Seharusnya orbits</p>', 1592039435, 1592065765),
(365, 21, 23, 35, 0, '', '', '<p>It is the <span xss=removed>fifth-largest</span> satellite in the Solar System, and the largest <span xss=removed>among</span> planetary <span xss=removed>satellite</span> relative to the size of the planet that it <span xss=removed>orbits</span> (its primary)</p>', '<p>Fifth-largest</p>', '<p>Among</p>', '<p>Satellite</p>', '<p>Orbits</p>', '', -1, -1, 4, -1, -1, 0, '', '', '', '', '', 'C', '<p>Satellite. Perhatikan ada kata among, yang berarti diantara hal-hal (yang jamak), seharusnya satellites</p>', 1592039589, 1592065728),
(366, 21, 23, 35, 0, '', '', '<p>New research of moon rocks, although <span xss=removed>not rejecting</span> the Theia <span xss=removed>hypothesis</span>, <span xss=removed>suggest</span> that the moon may be older than <span xss=removed>previously</span> thought.</p>', '<p>Not rejecting</p>', '<p>Hypothesis</p>', '<p>Suggest</p>', '<p>Previously</p>', '', -1, -1, 4, -1, -1, 0, '', '', '', '', '', 'C', '<p>Kalimat ini kalimat present dengan subject new research of the moon rocks, di mana inti dari subjek adalah new research (sebelum preposisi, tunggal), sehingga predikatnya seharusnya suggests</p>', 1592039739, 1592065690),
(367, 21, 23, 35, 0, '', '', '<p>The Moon <span xss=removed>was first reached</span> in September 1959 by the Soviet Union\'s Luna 2, an <span xss=removed>unman</span> spacecraft, <span xss=removed>followed</span> by the first successful soft landing <span xss=removed>by</span> Luna 9 in 1966. </p>', '<p>as first reached</p>', '<p>Unman</p>', '<p>Followed</p>', '<p>By</p>', '', -1, 4, -1, -1, -1, 0, '', '', '', '', '', 'B', '<p>Seharusnya unmanned yang berarti: not having or needing a crew or staff.</p>', 1592040596, 1592065633),
(368, 21, 23, 35, 0, '', '', '<p>Both the Moon\'s natural <span xss=removed>prominence</span> in the earthly sky and <span xss=removed>its</span> regular cycle of phases as seen from Earth <span xss=removed>has</span> provided cultural references and influences for human societies and cultures since <span xss=removed>time immemorial</span>.</p>', '<p>Rominence</p>', '<p>Its</p>', '<p>Has</p>', '<p>Time immemorial</p>', '', -1, -1, 4, -1, -1, 0, '', '', '', '', '', 'C', '<p>Subject kalimat di atas adalah both...sky and... phases (jamak, seharusnya have)</p>', 1592040669, 1592065576),
(369, 21, 23, 35, 0, '', '', '<p>The Moon <span xss=removed>formed</span> 4.51 billion years ago, <span xss=removed>some</span> 60 <span xss=removed>million years</span> after <span xss=removed>the original</span> of the Solar System.</p>', '<p>Formed</p>', '<p>Some</p>', '<p>Million  years</p>', '<p>The original</p>', '', -1, -1, -1, 4, -1, 0, '', '', '', '', '', 'D', '<p>Setelah terciptanya solar sistem, seharusnya the origin</p>', 1592041001, 1592065499),
(370, 21, 23, 35, 0, '', '', '<p>The dark and <span xss=removed>relatively</span> featureless lunar plains, clearly <span xss=removed>seen</span> with the naked eye, <span xss=removed>were</span> called maria (Latin for \"seas\"; singular mare), as they <span xss=removed>were once</span> believed to be filled with water, they are now known to be vast solidified pools of ancient basaltic lava .</p>', '<p>Relatively</p>', '<p>Seen</p>', '<p>were</p>', '<p>Were once</p>', '', -1, -1, 4, -1, -1, 0, '', '', '', '', '', 'C', '<p>Seharusnya Are called, menyebutkan fakta umum menggunakan simple present tense.</p>', 1592041051, 1592065452),
(371, 21, 23, 35, 0, '', '', '<p>Although similar <span xss=removed>with</span> terrestrial basalts, <span xss=removed>lunar</span> basalts <span xss=removed>have</span> more iron and no minerals <span xss=removed>altered</span> by water.</p>', '<p>With</p>', '<p>Lunar</p>', '<p>Have</p>', '<p>Altered</p>', '', 4, -1, -1, -1, -1, 0, '', '', '', '', '', 'A', '<p>Kata similar yang berarti mirip dengan, diikuti preposisi to bukan with</p>', 1592041094, 1592065232),
(372, 21, 23, 35, 0, '', '', '<p><span xss=removed>Almost</span> all maria are <span xss=removed>on</span> the near side of the Moon, and <span xss=removed>covered</span> 31% of the surface of the near side, <span xss=removed>compared</span> with 2% of the far side.</p>', '<p>Almost</p>', '<p>On</p>', '<p>Covered</p>', '<p>Compared</p>', '', -1, -1, 4, -1, -1, 0, '', '', '', '', '', 'C', '<p>Dari kalimat, Almost all maria are on the near side of the Moon, kita tahu bahwa kalimat ini merupakan present tense. Seharusnya cover</p>', 1592041142, 1592065193),
(373, 21, 23, 35, 0, '', '', '<p>In 2006, <span xss=removed>a study</span> of Ina, a tiny depression in Lacus Felicitatis, <span xss=removed>found</span>  jagged, <span xss=removed>relatively</span> dust-free features that, <span xss=removed>because</span> the lack of erosion by infalling debris, appeared to be only 2 million years old</p>', '<p>A study</p>', '<p>Found</p>', '<p>Relatively</p>', '<p>Because</p>', '', 4, 4, 4, 4, 4, 0, '', '', '', '', '', 'D', '<p>Perhatikan, the lack of erosion by ihfalling debris, merupakan frase dan bukan klausa (ciri klausa ada predikat tapi belum memiliki makna penuh sebagai kalimat). Frase atau kata benda seharusnya didahului because of, klausa didahului because of</p>', 1592041201, 1592065142),
(374, 21, 23, 35, 0, '', '', '<p>The lighter-colored regions of the Moon <span xss=removed>is</span> called terrae, or more <span xss=removed>commonly</span> highlands, <span xss=removed>because</span> they are higher than <span xss=removed>most</span> maria.</p>', '<p>Is</p>', '<p>Commonly</p>', '<p>Because</p>', '<p>Most</p>', '', 4, -1, -1, -1, -1, 0, '', '', '', '', '', 'A', '<p>The lighter-colored regions of the moon merupakan subjek, apabila ada preposisi maka inti dari subjek merupakan kata sebelum preposisi, yaitu regions (jamak). Seharusnya are</p>', 1592041246, 1592065099),
(375, 21, 23, 35, 0, '', '', '<p>They have been <span xss=removed>radiometricated</span> dated to having formed 4.4 billion years ago, and may <span xss=removed>represent</span> plagioclase <span xss=removed>cumulates</span> of the lunar <span xss=removed>magma</span> ocean.</p>', '<p>Radiometricated</p>', '<p>Represent</p>', '<p>Cumulates</p>', '<p>Magma</p>', '', 4, -1, -1, -1, -1, 0, '', '', '', '', '', 'A', '<p>Dalam sebuah tenses perfect, have +adverb+v3, verb tidak bisa menjelaskan verb. Seharusnyaa radiometrically, yang berarti telah ditanggalkan (diukur tanggalnya) secara radiometri terbentuk 4,4.....</p>', 1592041290, 1592065042),
(376, 21, 23, 35, 0, '', '', '<p>The concentration of maria on the Near Side likely reflects the <span xss=removed>substantially</span> thicker crust of the highlands of the Far Side, <span xss=removed>which</span> may <span xss=removed>has formed</span> in a <span xss=removed>slow-velocity</span> impact of a second moon of Earth a few tens of millions of years after their formation.</p>', '<p>Substantially</p>', '<p>Which</p>', '<p>Has formed</p>', '<p>Slow-velocity</p>', '', -1, -1, 4, -1, -1, 0, '', '', '', '', '', 'C', '<p>Terdapat modal may, setelahnya harus infinitif. Seharusnya may have formed</p>', 1592041338, 1592064997),
(377, 21, 23, 35, 0, '', '', '<p>Most of the Moon\'s mare <span xss=removed>basalt</span> erupted during the <span xss=removed>Imbrian</span> period, 3.0–3.5 billion years ago, <span xss=removed>although</span> some <span xss=removed>radiometrically</span> dated samples are as old as 4.2 billion years.</p>', '<p>Basalt</p>', '<p>Imbrian</p>', '<p>Although</p>', '<p>Radiometrically</p>', '', 4, -1, -1, -1, -1, 0, '', '', '', '', '', 'A', '<p>Perhatikan terdapat kata most of, yang seharusnya selalu diikuti object of preposition berbentuk jamak (kebanyakan dari banyk). Sama halnya each of..., (satu dari banyak), sealu diikuti jamak. Seharusnya basalts</p>', 1592064856, 1592064952),
(378, 20, 18, 37, 0, '', '', '<p>Di bawah ini adalah jumlah dua bilangan prima ganjil, kecuali....</p>', '<p>5</p>', '<p>10</p>', '<p>30</p>', '<p>40</p>', '<p>50</p>', 4, -1, -1, -1, -1, 0, '', '', '', '', '', 'A', '<p><span xss=removed>5= 2+3, 2 genap bukan ganjil</span><br xss=removed><span xss=removed>10=5+7</span><br xss=removed><span xss=removed>30=23+7</span><br xss=removed><span xss=removed>40=37+3</span><br xss=removed><span xss=removed>50=47+3</span></p>', 1592066176, 1592076644),
(379, 20, 18, 37, 0, '', '', '<p>Berapakah 66,67</p>', '<p>63,56</p>', '<p>635,58</p>', '<p>642,5</p>', '<p>64,2</p>', '<p>645,2</p>', -1, -1, 4, -1, -1, 0, '', '', '', '', '', 'C', '<p><span xss=removed>2/3 x 963,7 =642,5</span></p>', 1592066228, 1592076604),
(380, 20, 18, 37, 0, '', '', '<p>Jika p = 4 q = 3 r = (p+2q)/pq berapakah (p+q)/r?</p>', '<p>8 <sup>4</sup>/<sub>5</sub></p>', '<p>8 2/<sub>5</sub></p>', '<p><sup>70</sup>/<sub>12</sub></p>', '<p><sup>35</sup>/<sub>6</sub></p>', '<p>5 <sup>10</sup>/<sub>12</sub></p>', -1, 4, -1, -1, -1, 0, '', '', '', '', '', 'B', '<p><span xss=removed>r= (p+2q)/pq</span><br xss=removed><span xss=removed>= (4+6)/12 = 10/12</span><br xss=removed><br xss=removed><br xss=removed><span xss=removed>(p+q)/r = 7/(10/12)</span><br xss=removed><span xss=removed>= 7 x 12/10</span><br xss=removed><span xss=removed>= 42/5</span><br xss=removed><span xss=removed>= 8 2/5</span></p>', 1592066280, 1592076571),
(381, 20, 18, 37, 0, '', '', '<p>jika w.x.y.z adalah 4 bilangan mana diantara yang berikut ini tidak sama dengan w(x+y+z)?</p>', '<p>wx+wy+wz</p>', '<p>(x+y+z)w</p>', '<p>wx+(y+z)w</p>', '<p>wx+x+y+z</p>', '<p>w(x+z)+wy</p>', -1, -1, -1, 4, -1, 0, '', '', '', '', '', 'D', '', 1592066331, 1592074581),
(382, 20, 18, 37, 0, '', '', '<p>Mana bilangan ini yang terkecil?</p>', '<p>1/5</p>', '<p>√5</p>', '<p>1/√5</p>', '<p>√(5/5)</p>', '<p>1/(5√5)</p>', -1, -1, -1, -1, 4, 0, '', '', '', '', '', 'E', '', 1592066387, 1592074872),
(383, 20, 18, 37, 0, '', '', '<p>jika bilangan dibawah ini disusun manakah bilangan yang berada di tengah?</p>', '<p>3<sup>8</sup>/3<sup>6</sup></p>', '<p>3<sup>3</sup> – 1</p>', '<p>3<sup>0</sup> – 1</p>', '<p>2<sup>3</sup></p>', '<p>3<sup>2</sup> x 2<sup>3</sup></p>', 4, -1, -1, -1, -1, 0, '', '', '', '', '', 'A', '<p><span xss=removed>0, 8, 9, 26, 72</span></p>', 1592066469, 1592076486),
(384, 20, 18, 37, 0, '', '', '<p>Jika p= n x 1 x 1 dan q = n + 1+ 1 (n = bilangan positif) maka </p>', '<p>x > y</p>', '<p>x < y>', '<p>x = y</p>', '<p>x dan y tidak bias ditentukan</p>', '<p>x – 2y = 0</p>', -1, 4, -1, -1, -1, 0, '', '', '', '', '', 'B', '', 1592066513, 1592075492),
(385, 20, 18, 37, 0, '', '', '<p>Jika x berat total p kotak yang masing masing beratnya q kg dan y adalah berat total q kotak yang masing masing beratnya p kg maka?</p>', '<p>x > y</p>', '<p>x < y>', '<p>x = y</p>', '<p>x dan y tidak bisa di tentukan</p>', '<p>2x > 2y</p>', -1, -1, 4, -1, -1, 0, '', '', '', '', '', 'C', '', 1592066578, 1592075578),
(386, 20, 18, 37, 0, '', '', '<p>selisih dua bilangan positif adalah 5 jumlah kuadratnya sama dengan 1500 kurangnya dari kuadrat jumlah kedua bilangan itu. Jumlah kedua bilangan itu adalah..</p>', '<p>45</p>', '<p>50</p>', '<p>55</p>', '<p>60</p>', '<p>65</p>', -1, -1, 4, -1, -1, 0, '', '', '', '', '', 'C', '<p><span xss=removed>a^2+b^2 =(a+b)^2 – 1500</span><br xss=removed><span xss=removed>a^2 + b^2 + 1500 = (a+b)^2</span><br xss=removed><span xss=removed>a^2 +b^2 + 1500 = a^2 +b^2 + 2ab</span><br xss=removed><span xss=removed>1500 = 2ab</span><br xss=removed><span xss=removed>Ab = 750</span><br xss=removed><br xss=removed><span xss=removed>a-b = 5</span><br xss=removed><span xss=removed>a = 5+b</span><br xss=removed><span xss=removed>ab = 750</span><br xss=removed><span xss=removed>(5+b)b = 750</span><br xss=removed><span xss=removed>5b + b^2- 750 = 0</span><br xss=removed><span xss=removed>(b+ 30) (b-25)</span><br xss=removed><span xss=removed>B= - 30</span><br xss=removed><span xss=removed>B= 25</span><br xss=removed><span xss=removed>Jumlah dua bilangan positif 55</span></p>', 1592066632, 1592076393),
(387, 20, 18, 37, 0, '', '', '<p>Sisa Pembagian (7<sup>21</sup> x 2<sup>32</sup>) : 10</p>', '<p>4</p>', '<p>5</p>', '<p>6</p>', '<p>2</p>', '<p>8</p>', -1, -1, -1, 4, -1, 0, '', '', '', '', '', 'D', '<p><span xss=removed>7^1 = 7</span><br xss=removed><span xss=removed>7^2 = 49</span><br xss=removed><span xss=removed>7^3 = ..3</span><br xss=removed><span xss=removed>7^4 = ..1</span><br xss=removed><span xss=removed>7^5= ..7</span><br xss=removed><span xss=removed>Berulang setiap 4x --&gt; 21/4 = sisa 1 --&gt; 7^1 = 7</span><br xss=removed><span xss=removed>2^1 = 2</span><br xss=removed><span xss=removed>2^2 = 4</span><br xss=removed><span xss=removed>2^3 = 8</span><br xss=removed><span xss=removed>2^4= ..6</span><br xss=removed><span xss=removed>2^5= .. 2 berulang setiap 4x --&gt; maka sisa 6</span><br xss=removed><br xss=removed><span xss=removed>Jadi 7 x 6 maka sisa nya 42 maka sisa 2</span></p>', 1592066697, 1592076348),
(388, 20, 18, 37, 0, '', '', '<p>Diketahui rata rata tiga bilangan bulat positif adalah 13 dan jumlah dua bilangan terbesarnya adalah 35. Jikaselisih dua bilangan terkecil adalah 9 makahasil kali ketiga bilangan itu adalah.</p>', '<p>2197</p>', '<p>1716</p>', '<p>1144</p>', '<p>1056</p>', '<p>88</p>', -1, -1, 4, -1, -1, 0, '', '', '', '', '', 'C', '<p><span xss=removed>(a+b+c)/3 = 13</span><br xss=removed><span xss=removed>A+b+c = 39</span><br xss=removed><span xss=removed>A+ 35 = 39</span><br xss=removed><span xss=removed>A= 4</span><br xss=removed><br xss=removed><span xss=removed>B+c = 35</span><br xss=removed><span xss=removed>b-a = 9</span><br xss=removed><span xss=removed>b-4 = 9</span><br xss=removed><span xss=removed>b= 13</span><br xss=removed><br xss=removed><span xss=removed>13+c = 35</span><br xss=removed><span xss=removed>C = 22</span><br xss=removed><br xss=removed><span xss=removed>Axbxc = 4 x 13 x 22</span><br xss=removed><span xss=removed>1144</span></p>', 1592066745, 1592076271),
(389, 20, 18, 37, 0, '', '', '<p>Jika 1/6 bagian bilangan pertama ditambah dengan bilangan kedua maka kedua bilangan menjadi sama besar . apabila selisih awal kedua bilangan adalah 2 maka hasil kali kedua bilangan tersebut adalah..</p>', '<p>6</p>', '<p>9</p>', '<p>12</p>', '<p>18</p>', '<p>24</p>', -1, -1, -1, -1, 4, 0, '', '', '', '', '', 'E', '<p><span xss=removed>1/6a + b = a- 1/6a</span><br xss=removed><span xss=removed>b = 5/6 – 1/6a</span><br xss=removed><span xss=removed>b =4/6a</span><br xss=removed><br xss=removed><span xss=removed>a-b = 2</span><br xss=removed><span xss=removed>a- 4/6a =2</span><br xss=removed><span xss=removed>2/6a = 2</span><br xss=removed><span xss=removed>a = 6</span><br xss=removed><br xss=removed><span xss=removed>b = 4/6a</span><br xss=removed><span xss=removed>b= 4</span><br xss=removed><br xss=removed><span xss=removed>axb = 24</span></p>', 1592066794, 1592076224),
(390, 20, 18, 37, 0, '', '', '<p>Selisih antara bilangan terbesar dan terkecil dari bilangan yang terbentuk dari 3 digit angka yang dapat dibuat sehingga habis dibagi 7 adalah..</p>', '<p>890</p>', '<p>889</p>', '<p>888</p>', '<p>887</p>', '<p>886</p>', -1, 4, -1, -1, -1, 0, '', '', '', '', '', 'B', '<p><span xss=removed>1, 2, 3, 4, 5, 6, 7, 8, 9</span><br xss=removed><span xss=removed>Bilangan 3 digit terbesar yaitu 99.. cari bilangan 3 digit yang habis dibagi 7</span><br xss=removed><span xss=removed>994 jadi bilangan 3 digit terbesar</span><br xss=removed><br xss=removed><span xss=removed>Bilangan 3 digit terkecil yaitu 10.. cari yang habis dibagi 7</span><br xss=removed><span xss=removed>105 jadi bilangan 3 digit terkecil</span><br xss=removed><br xss=removed><span xss=removed>994 – 105 = 889</span></p>', 1592066830, 1592076167),
(391, 20, 18, 37, 0, '', '', '<p>Jika x= 1/16 dan y = 16 % maka</p>', '<p>x > y</p>', '<p>x < y>', '<p>x = y</p>', '<p>x dan ya tidak bisa ditentukan</p>', '<p>2x > y</p>', -1, 4, -1, -1, -1, 0, '', '', '', '', '', 'B', '', 1592066878, 1592076797),
(392, 20, 18, 37, 0, '', '', '<p>Suatu bilangan habis dibagi 15 dan habis dibagi 18 maka bilangan tersebut juga akan habis dibagi ..</p>', '<p>8</p>', '<p>9</p>', '<p>12</p>', '<p>20</p>', '<p>30</p>', -1, -1, -1, -1, 4, 0, '', '', '', '', '', 'E', '<p><span xss=removed>faktor prima dari 15 = 3 x 5</span><br xss=removed><span xss=removed>faktor prima dari 18 = 2 x 3^2</span><br xss=removed><span xss=removed>faktor prima dari kedua bilangan tersebut yaitu 2 x 3 x 5 = 30</span></p>', 1592066933, 1592076874),
(393, 20, 18, 37, 0, '', '', '<p>Bilangan yang mendekati hasil bagi 899 dengan 29 adalah..</p>', '<p>30</p>', '<p>25</p>', '<p>28</p>', '<p>24</p>', '<p>26</p>', 4, -1, -1, -1, -1, 0, '', '', '', '', '', 'A', '<p><span xss=removed>899/29 = 900/30</span></p>', 1592066981, 1592076955),
(394, 20, 18, 37, 0, '', '', '<p>Pak dodi mempunyai 56 permen , 40 kue dan 24 biskuit. Makanan tersebut akan dibagikan kepada beberapa anak dilingkungannya, jika setiap anak mendapat bagian yang sma baik jumlah maupun jenisnya maka jumlah anak yang mendapat makanan tersebut paling banyak … orang</p>', '<p>15</p>', '<p>12</p>', '<p>8</p>', '<p>4</p>', '<p>5</p>', -1, -1, 4, -1, -1, 0, '', '', '', '', '', 'C', '<p><span xss=removed>FPB 56 = 2^3 x 7</span><br xss=removed><span xss=removed>40 = 2^3 x 5</span><br xss=removed><span xss=removed>24 = 2^3 x 3</span><br xss=removed><span xss=removed>FPB 2^3 = 8</span></p>', 1592067314, 1592077055),
(395, 20, 18, 37, 0, '', '', '<p>Angga dan Bayu berlatih bela diri di sanggar yang sama. Angga berlatih setiap 6 hari sekali dan bayu 8 hari sekali. Jika pada tanggal 15 Januari 2017 mereka berlatih bersama sama maka mereka akan berlatih bersama sama lagi pada tanggal..</p>', '<p>6 februari 2017</p>', '<p>7 februari 2017</p>', '<p>8 februari 2017</p>', '<p>29 januari 2017</p>', '<p>30 januari 2017</p>', -1, -1, 4, -1, -1, 0, '', '', '', '', '', 'C', '<p><span xss=removed>KPK 6 = 2x 3</span><br xss=removed><span xss=removed>8 = 2^3</span><br xss=removed><span xss=removed>KPK 2^3 x 3= 24 jadi mereka akan bertemu setiap 24 hari,</span><br xss=removed><span xss=removed>Tanggal 15 + 24 = 39 januari sampai tanggal 31 maka tanggal 8 februari 2017</span></p>', 1592067372, 1592077165),
(396, 20, 18, 37, 0, '', '', '<p>Dari sebuah bilangan empat angka, digit ke-1 dan ke3 nya genap. Digit ke-4 satu lebihnya dari digit ke-2. Jika jumlah keempat digitnya 23, maka bilangan terbesar yang memenuhi kriteria tersebut adalah .... </p>', '<p>8546</p>', '<p>8627</p>', '<p>7628</p>', '<p>7826</p>', '<p>8384</p>', -1, 4, -1, -1, -1, 0, '', '', '', '', '', 'B', '', 1592067429, 1592077272),
(397, 20, 18, 37, 0, '', '', '<p>Jumlah peserta suatu seminar sama dengan banyak bilangan asli dari 1 sampai 100 yang bukan kelipatan 3 atau 4. Jumlah peserta seminar tersebut adalah .... </p>', '<p>25</p>', '<p>40</p>', '<p>50</p>', '<p>65</p>', '<p>80</p>', -1, -1, 4, -1, -1, 0, '', '', '', '', '', 'C', '<p><span xss=removed>Bilangan kelipatan 3 dari 1 sampai 100 ada sebanyak 33 (didapat dari 100 3 = ', 1592067507, 1592077434),
(398, 20, 18, 37, 0, '', '', '<p>Jika x = 2 + y dan x + z = 2y, maka</p>', '<p>x < z>', '<p>x > z</p>', '<p>x = z/2</p>', '<p>x = z - 4</p>', '<p>Hubungan x dan z tidak dapat ditentukan</p>', -1, 4, -1, -1, -1, 0, '', '', '', '', '', 'B', '<p><span xss=removed>x = 2 + y sama artinya dengan y = x − 2</span><br xss=removed><span xss=removed>x + z = 2y &lt;=> x + z = 2(x − 2)</span><br xss=removed><span xss=removed>x + z = 2x − 4</span><br xss=removed><span xss=removed>x = z + 4</span><br xss=removed><span xss=removed>Karena x = z + 4, berarti x > z</span></p>', 1592067564, 1592079126),
(399, 20, 18, 37, 0, '', '', '<p>Jika x =Banyak bilangan cacah antara -1000 dan 100 dan y =Banyak bilangan bulat dari -50 sampai 50, maka .... </p>', '<p>x < y>', '<p>x > y</p>', '<p>x = y + 1</p>', '<p>x = y</p>', '<p>Hubungan x dan y tidak dapat ditentukan</p>', 4, -1, -1, -1, -1, 0, '', '', '', '', '', 'A', '<p><span xss=removed>Bilangan cacah antara -1000 dan 100 yaitu : 0, 1, 2, 3, ..., 99, ada 100 buah bilangan, jadi x=100 Bilangan bulat antara -50 dan 50 yaitu : -50, -49, -48, -47, ..., -2, -1, 0, 1, 2, ..., 47, 48, 49, 50 ada 101 bilangan, jadi y=101 Sehingga x < y></p>', 1592067678, 1592079291),
(400, 20, 18, 37, 0, '', '', '<p>Rata-rata empat bilangan adalah 16. Jika diketahui rata-rata bilangan kedua dan ketiga adalah 12, maka hasil penjumlahan bilangan pertama dan terakhir adalah ....</p>', '<p>55</p>', '<p>43</p>', '<p>40</p>', '<p>35</p>', '<p>27</p>', -1, -1, 4, -1, -1, 0, '', '', '', '', '', 'C', '<p><span xss=removed>Misal, keempat bilangan tersebut adalah a, b, c, dan d. Diketahui rata-rata bilangan kedua dan ketiga adalah 12, maka: (b+c)/2 = 12 ↔ b+c = 24</span><br xss=removed><span xss=removed>Rata-rata empat bilangan adalah 16, maka:</span><br xss=removed><span xss=removed>(a +b +c + d)/4 = 16</span><br xss=removed><span xss=removed>a + b + c + d= 64</span><br xss=removed><span xss=removed>a + 24+ d = 64</span><br xss=removed><span xss=removed>a +d = 40</span></p>', 1592067724, 1592103570),
(401, 20, 18, 37, 0, '', '', '<p><sup>1</sup>/<sub>2</sub> - <sup>1</sup>/<sub>3</sub> x <sup>2</sup>/<sub>3</sub> + [(<sup>2</sup>/<sub>3</sub> - <sup>1</sup>/<sub>2</sub>) : <sup>1</sup>/<sub>6</sub>)]</p>', '<p><sup>5</sup>/<sub>18</sub></p>', '<p><sup>9</sup>/<sub>10</sub></p>', '<p>1</p>', '<p><sup>10</sup>/<sub>9</sub></p>', '<p><sup>23</sup>/<sub>18</sub></p>', -1, -1, -1, -1, 4, 0, '', '', '', '', '', 'E', '', 1592067771, 1592104051),
(402, 20, 18, 37, 0, '', '', '<p>Berapakah nilai akar pangkat tiga dari 97.336?</p>', '<p>16</p>', '<p>26</p>', '<p>36</p>', '<p>46</p>', '<p>56</p>', -1, -1, -1, 4, -1, 0, '', '', '', '', '', 'D', '<p><span xss=removed>97.336 --&gt; ambil 3 angak terakhir 336 sisa 97 (dari angka 1-9 yang pangkat tiga nya mendekati angka 97 adalah 4, karna kalau 53 = 125 maka hasilnya pasti 46</span></p>', 1592067821, 1592104130),
(403, 20, 18, 37, 0, '', '', '<p>JIka x = -(8<sup>8</sup>) dan y = (-8<sup>8</sup>), maka</p>', '<p>x > y</p>', '<p>x < y>', '<p>x = y</p>', '<p>x dan y tidak dapat ditentukan</p>', '<p>2x = 2y</p>', -1, 4, -1, -1, -1, 0, '', '', '', '', '', 'B', '', 1592067869, 1592104332),
(404, 21, 23, 36, 0, '', '', '<p> I think ………. come at the meeting tomorrow.</p>', '<p>I will</p>', '<p>I will to</p>', '<p>I will be going to</p>', '<p>I will go</p>', '', 4, -1, -1, -1, -1, 0, '', '', '', '', '', 'A', '<p>Tomorro menandakan future, setelah will infinitive</p>', 1592067909, 1592067909),
(405, 20, 18, 37, 0, '', '', '<p>R dan S adalah bilangan bulat positif , bila R dibagi 5 maka sisanya adalah 3 sedangkan bila S dibagi 5 sisanya adalah 1 berapakah sisanya apabila RS dibagi 5?</p>', '<p>1</p>', '<p>2</p>', '<p>3</p>', '<p>4</p>', '<p>5</p>', -1, -1, 4, -1, -1, 0, '', '', '', '', '', 'C', '<p><span xss=removed>misal</span><br xss=removed><span xss=removed>R= bilangan bulat jika dibagi 5 sisa 3 = 8, 13, 18, 23 dst</span><br xss=removed><span xss=removed>S= bilangan bulat jika dibagi 5 sisa 1 = 6, 11, 16, 21 dst</span><br xss=removed><span xss=removed>Misal R= 13 S= 6 maka RS/5 = 78/5 sisa 3</span></p>', 1592067949, 1592104429),
(406, 20, 18, 37, 0, '', '', '<p>Yang manakah pecahan dibawah ini yang lebih besar dari 1/7?</p>', '<p>11/82</p>', '<p>21/150</p>', '<p>14/97</p>', '<p>33/233</p>', '<p>22/158</p>', -1, -1, 4, -1, -1, 0, '', '', '', '', '', 'C', '<div id=\"c2086\" class=\"freebirdFormeditorViewAssessmentFeedbackFeedbackText\" xss=removed>dicek satu satu<br>a. 11 x 7 = 77 < 82><br>b. 21 x 7 = 147 < 150><br>c. 14 x 7 = 98 > 97 maka 14/97 > 1/7<br><br>d.33 x 7 = 231 < 233><br>e. 22 x 7 = 154 < 158>\r\n<p> </p>', 1592067996, 1592104537),
(407, 21, 23, 36, 0, '', '', '<p>Irene   : What will you do on next week?</p>\r\n<p> </p>\r\n<p>Daniel: I ………. my grandmother.</p>', '<p>will visiting</p>', '<p>visited</p>', '<p>am visiting</p>', '<p>will visit</p>', '', -1, -1, -1, 4, -1, 0, '', '', '', '', '', 'D', '<p>next week menandakan future</p>', 1592067997, 1592067997),
(408, 20, 18, 37, 0, '', '', '<p>Diketahui a = <sup>6</sup>/<sub>b</sub>, b = <sup>3</sup>/<sub>c</sub>  ,  c = <sup>2</sup>/<sub>a</sub>  dengan a, b, c  bilangan positif, perbandingan a:b:c adalah</p>', '<p>2 : 3 : 1</p>', '<p>2 : 3 : 6</p>', '<p>3 : 1 : 2</p>', '<p>4 : 3 : 2</p>', '<p>1 : 2 : 3</p>', 4, -1, -1, -1, -1, 0, '', '', '', '', '', 'A', '<p><span xss=removed>ab =6 , bc = 3 , ac = 2</span><br xss=removed><span xss=removed>kalikan semua bilangan maka diperoleh ab x bc x ac = 36 --&gt; abc = 6</span><br xss=removed><span xss=removed>(ab)c = 6 --&gt; c =1</span><br xss=removed><span xss=removed>a(bc) = 6 --&gt; a= 2</span><br xss=removed><span xss=removed>b(ac) = 6 --&gt; b = 3</span><br xss=removed><span xss=removed>jadi a : b : c --&gt; 2 : 3 : 1</span></p>', 1592068041, 1592104750),
(409, 21, 23, 36, 0, '', '', '<p>X : …………. </p>\r\n<p>Y : I’ll see Mr. Pong tomorrow afternoon.</p>', '<p>When you will see Mr. Pong?</p>', '<p>When will you see Mr. Pong?</p>', '<p>When you will be seen Mr. Pong?</p>', '<p>When you be see Mr. Pong?</p>', '', -1, 4, -1, -1, -1, 0, '', '', '', '', '', 'B', '<p>cara membuat pertanyaan future adalah seperti itu</p>', 1592068106, 1592068106),
(410, 20, 18, 37, 0, '', '', '<p>Jika diketahui a dan b adalah bilangan bulat positif yang memenuhi 6<a>b>9 maka..</p>', '<p>a = b</p>', '<p>a < b>', '<p>2a > 2 b</p>', '<p>2a = b</p>', '<p>tidak dapat ditentukan</p>', -1, 4, -1, -1, -1, 0, '', '', '', '', '', 'B', '<p><span xss=removed>bilangan bulat a=7 b = 8</span></p>', 1592068127, 1592104870),
(411, 21, 23, 36, 0, '', '', '<p>A: Will you be here by seven o’clock?</p>\r\n<p>B: It’s hard to say. Maybe I …………. there a little late.</p>', '<p>will be</p>', '<p>am going</p>', '<p>going to be</p>', '<p>will</p>', '', 4, -1, -1, -1, -1, 0, '', '', '', '', '', 'A', '<p>present : I am there</p>\r\n<p>future : I will be there</p>', 1592068201, 1592068201),
(412, 21, 23, 36, 0, '', '', '<p>5. Mia will take a trip and go to the beach ………….</p>', '<p>last vacation</p>', '<p>vacation ago</p>', '<p>next vacation</p>', '<p>next vacation later</p>', '', -1, -1, 4, -1, -1, 0, '', '', '', '', '', 'C', '', 1592068296, 1592068296),
(413, 21, 23, 36, 0, '', '', '<p>I will be in class tomorrow, but I …………. in class the day after tomorrow.</p>', '<p>will be not</p>', '<p>will not be</p>', '<p>will not</p>', '<p>will be</p>', '', -1, 4, -1, -1, -1, 0, '', '', '', '', '', 'B', '<p>terdapat kata but yang menunjukkan kontradiktif</p>', 1592068381, 1592068381),
(414, 21, 23, 36, 0, '', '', '<p>A: Will the students operate the computer in the lab?</p>\r\n<p>B: Yes, they …….</p>', '<p>are</p>', '<p>do</p>', '<p>will</p>', '<p>will be</p>', '', -1, -1, 4, -1, -1, 0, '', '', '', '', '', 'C', '<p>pertanyaan yes/no tinggal disebutkan saja auxiliary verb dalam pertanyaan (will)</p>', 1592068478, 1592068478),
(415, 20, 18, 37, 0, '6959c9cd061ee5d1110efa51b3cca7ca.JPG', 'image/jpeg', '<p>Bu Chandra membagikan 372 rambutan kepada Dhita , Elfan, Fino, dan Ginting. Dhita menerima 18 lebih banyak dari Elfan, 27 lebih banyak dari Fino dan 43 lebih banyak dari Ginting, jadi banyaknya rambutan yang diterima fino adalah..</p>', '<p>115</p>', '<p>97</p>', '<p>88</p>', '<p>72</p>', '<p>71</p>', -1, -1, 4, -1, -1, 0, '', '', '', '', '', 'C', '<p>D=E + 18<br>D= F + 27<br>D = G + 43<br><br>E + 18 = F +27<br>E = F + 9<br><br>G+ 43 = F + 27<br>G = F – 16<br><br>D + E + F + G = 372<br>(F + 27) + ( F + 9 ) + F + (F – 16) = 372<br>4F + 20 = 372<br>F = 88</p>', 1592068652, 1592106412),
(416, 20, 18, 37, 0, '103f237a92b58e478333265d7a6534cd.JPG', 'image/jpeg', '<p>Dua buah lampu masing-masing berwarna merah dan biru menyala bersamaan dengan tempo yang berbeda. Lampu merah menyala tiap 12 detik dan lampu hijau menyala tiap 10 detik. Apabila pada jam 01.00 kedua lampu itu menyala bersamaan maka mereka akan menyala bersamaan lagi pada…. detik</p>', '<p>15</p>', '<p>30</p>', '<p>45</p>', '<p>60</p>', '<p>80</p>', -1, -1, -1, 4, -1, 0, '', '', '', '', '', 'D', '<p>Faktor dari 10 = 2 x 5, factor dari 12 = 2 x 2 x 3 maka KPK = 2x2x3x5 = 60 detik</p>', 1592068722, 1592106617),
(417, 20, 18, 37, 0, '1b4bad193f804e42919093b179451937.JPG', 'image/jpeg', '<p>Ada 2 bilangan positif. Jika bilangan pertama dikalikan 3, maka hasilnya meruapakan bilangan yang lebih besar dari pada bilangan kedua dengan selisih perbedaan 4. Bila kedua bilangan tersebut dikalikan, hasilnya bernilai 160. Salah satu bilangan itu adalah ….. </p>', '<p>5</p>', '<p>1</p>', '<p>20</p>', '<p>25</p>', '<p>30</p>', -1, -1, 4, -1, -1, 0, '', '', '', '', '', 'C', '<p><span xss=removed>3a = b + 4 dan ab = 160</span><br xss=removed><span xss=removed>a = 160/b</span><br xss=removed><span xss=removed>3a = b + 4</span><br xss=removed><span xss=removed>3 x (160/b) = b + 4</span><br xss=removed><span xss=removed>b^2 + 4b – 480 = 0</span><br xss=removed><span xss=removed>b1 = 20 b2 = -24 maka 20 karena bilangan postif</span></p>', 1592068810, 1592106577),
(418, 20, 18, 37, 0, 'eaef22bb71ebe6388e4a864c4f5c03a3.JPG', 'image/jpeg', '<p>Sebuah angka, bila dikalikan 5 dan hasilnya dikurang 7 kemudian hasilnya dibagi 3 akan menghasilkan 1. Berapakah angka tersebut? </p>', '<p>1</p>', '<p>2</p>', '<p>3</p>', '<p>4</p>', '<p>5</p>', -1, 4, -1, -1, -1, 0, '', '', '', '', '', 'B', '<div id=\"c2164\" class=\"freebirdFormeditorViewAssessmentFeedbackFeedbackText\" xss=removed>1= (5x-7)/3<br>3= 5x-7<br>5x= 10<br>X= 2</div>', 1592068872, 1592106705),
(419, 20, 18, 37, 0, '0f20bc8a93ed3d872a63ef26c82820c8.JPG', 'image/jpeg', '<p>Lima adalah berapa persen dari 160</p>', '<p>3,125</p>', '<p>6,25</p>', '<p>0,3125</p>', '<p>6,75</p>', '<p>7,89</p>', 4, -1, -1, -1, -1, 0, '', '', '', '', '', 'A', '<p>5 = 160xa</p>\r\n<p>a = 5/160 = 3,125%</p>', 1592068931, 1592106869),
(420, 20, 18, 37, 0, 'baf1419daa94bcc32f04af3ab71e944d.JPG', 'image/jpeg', '<p>Jika diketahui 1/5 dari suatu bilangan samadengan 30% bilangan kedua dan hasil kali dari kedua bilangan tersebut adalah 6, maka berapakah selisih dari bilangan pertama denganbilangan kedua?</p>', '<p>5</p>', '<p>1</p>', '<p>0</p>', '<p>-1</p>', '<p>-5</p>', -1, 4, -1, -1, -1, 0, '', '', '', '', '', 'B', '', 1592069308, 1592106956),
(421, 20, 18, 37, 0, '79cc16c2a51aaa4361924ef84270b957.JPG', 'image/jpeg', '<p>Bacaan untuk soal 37-41</p>\r\n<p> </p>\r\n<p>Tokoh wanita satu ini sangat terkenal di Indonesia. Dialah Raden Ajeng Kartini Djojo Adhiningrat atau dikenal sebagai R.A Kartini, beliau dikenal sebagai salah satu pahlawan nasional yang dikenal gigih memperjuangkan emansipasi wanita indonesia kala ia hidup. Mengenai Biografi dan Profil R.A Kartini, beliau lahir pada tanggal 21 April tahun 1879 di Kota Jepara, Hari kelahirannya itu kemudian diperingati sebagai Hari Kartini untuk menghormati jasa RA Kartini pada bangsa Indonesia. Nama lengkap Kartini adalah Raden Ajeng Kartini Djojo Adhiningrat. Mengenai sejarah RA Kartini dan kisah hidup Kartini, ia lahir di tengah-tengah keluarga bangsawan oleh sebab itu ia memperoleh gelar R.A (Raden Ajeng) di depan namanya, gelar itu sendiri (Raden Ajeng) dipergunakan oleh Kartini sebelum ia menikah, jika sudah menikah maka gelar kebangsawanan yang dipergunakan adalah R.A (Raden Ayu) menurut tradisi Jawa. Ayahnya bernama R.M. Sosroningrat, putra dari Pangeran Ario Tjondronegoro IV, seorang bangsawan yang menjabat sebagai bupati jepara, beliau ini merupakan kakek dari R.A Kartini. Ayahnya R.M. Sosroningrat merupakan orang yang terpandang sebab posisinya kala itu sebagai bupati Jepara kala Kartini dilahirkan. Ibu kartini yang bernama M.A. Ngasirah, beliau ini merupakan anak seorang kiai atau guru agama di Telukawur, Kota Jepara. Menurut sejarah, Kartini merupakan keturunan dari Sri Sultan Hamengkubuwono VI, bahkan ada yang mengatakan bahwa garis keturunan ayahnya berasal dari kerajaan Majapahit. Ibu R.A Kartini yaitu M.A. Ngasirah sendiri bukan keturunan bangsawan, melainkan hanya rakyat biasa saja, oleh karena itu peraturan kolonial Belanda ketika itu mengharuskan seorang Bupati harus menikah dengan bangsawan juga, hingga akhirnya ayah Kartini kemudian mempersunting seorang wanita bernama Raden Adjeng Woerjan yang merupakan seorang bangsawan keturunan langsung dari Raja Madura ketika itu.</p>\r\n<p> </p>\r\n<p>Ketika Kartini dilahirkan, yang menjabat sebagai bupati Jepara adalah ....</p>', '<p>Ario Tjondronegoro IV</p>', '<p>Raden Adjeng Woerjan</p>', '<p>M.A. Ngasirah</p>', '<p>R.M. Sosroningrat</p>', '<p>Pemerintah Belanda</p>', 4, 4, 4, 4, 4, 0, '', '', '', '', '', 'D', '', 1592069367, 1592111990);
INSERT INTO `tbl_soal_tryout` (`id_soal`, `dosen_id`, `matkul_id`, `id_soal_paket`, `bobot`, `file`, `tipe_file`, `soal`, `opsi_a`, `opsi_b`, `opsi_c`, `opsi_d`, `opsi_e`, `bobot_a`, `bobot_b`, `bobot_c`, `bobot_d`, `bobot_e`, `bobot_kosong`, `file_a`, `file_b`, `file_c`, `file_d`, `file_e`, `jawaban`, `pembahasan`, `created_on`, `updated_on`) VALUES
(422, 20, 18, 37, 0, 'a61635f4cc6dc5a49827dc67a1dfb24f.JPG', 'image/jpeg', '<p>Bacaan untuk soal 37-41</p>\r\n<p> </p>\r\n<p>Tokoh wanita satu ini sangat terkenal di Indonesia. Dialah Raden Ajeng Kartini Djojo Adhiningrat atau dikenal sebagai R.A Kartini, beliau dikenal sebagai salah satu pahlawan nasional yang dikenal gigih memperjuangkan emansipasi wanita indonesia kala ia hidup. Mengenai Biografi dan Profil R.A Kartini, beliau lahir pada tanggal 21 April tahun 1879 di Kota Jepara, Hari kelahirannya itu kemudian diperingati sebagai Hari Kartini untuk menghormati jasa RA Kartini pada bangsa Indonesia. Nama lengkap Kartini adalah Raden Ajeng Kartini Djojo Adhiningrat. Mengenai sejarah RA Kartini dan kisah hidup Kartini, ia lahir di tengah-tengah keluarga bangsawan oleh sebab itu ia memperoleh gelar R.A (Raden Ajeng) di depan namanya, gelar itu sendiri (Raden Ajeng) dipergunakan oleh Kartini sebelum ia menikah, jika sudah menikah maka gelar kebangsawanan yang dipergunakan adalah R.A (Raden Ayu) menurut tradisi Jawa. Ayahnya bernama R.M. Sosroningrat, putra dari Pangeran Ario Tjondronegoro IV, seorang bangsawan yang menjabat sebagai bupati jepara, beliau ini merupakan kakek dari R.A Kartini. Ayahnya R.M. Sosroningrat merupakan orang yang terpandang sebab posisinya kala itu sebagai bupati Jepara kala Kartini dilahirkan. Ibu kartini yang bernama M.A. Ngasirah, beliau ini merupakan anak seorang kiai atau guru agama di Telukawur, Kota Jepara. Menurut sejarah, Kartini merupakan keturunan dari Sri Sultan Hamengkubuwono VI, bahkan ada yang mengatakan bahwa garis keturunan ayahnya berasal dari kerajaan Majapahit. Ibu R.A Kartini yaitu M.A. Ngasirah sendiri bukan keturunan bangsawan, melainkan hanya rakyat biasa saja, oleh karena itu peraturan kolonial Belanda ketika itu mengharuskan seorang Bupati harus menikah dengan bangsawan juga, hingga akhirnya ayah Kartini kemudian mempersunting seorang wanita bernama Raden Adjeng Woerjan yang merupakan seorang bangsawan keturunan langsung dari Raja Madura ketika itu.</p>\r\n<p> </p>\r\n<p>Pernyataan yang benar sesuai dengan teks di atas adalah .... </p>', '<p>R.A. Kartini lahir pada tanggal 21 April tahun 1879 di Blora</p>', '<p>Ayah kandung Kartini adalah Pangeran Ario Tjondronegoro IV</p>', '<p>Menurut sejarah, Kartini merupakan keturunan dari Sri Sultan Hamengkubuwono VI</p>', '<p>Ibu kandung Kartini adalah Raden Adjeng Woerjan</p>', '<p>Ibu kandung Kartini merupakan anak seorang Bangsawan</p>', -1, -1, 4, -1, -1, 0, '', '', '', '', '', 'C', '', 1592069425, 1592112111),
(423, 20, 18, 37, 0, '2d5b39f5f2b059b305a977e1f2ad1d6b.JPG', 'image/jpeg', '<p>Bacaan untuk soal 37-41</p>\r\n<p> </p>\r\n<p>Tokoh wanita satu ini sangat terkenal di Indonesia. Dialah Raden Ajeng Kartini Djojo Adhiningrat atau dikenal sebagai R.A Kartini, beliau dikenal sebagai salah satu pahlawan nasional yang dikenal gigih memperjuangkan emansipasi wanita indonesia kala ia hidup. Mengenai Biografi dan Profil R.A Kartini, beliau lahir pada tanggal 21 April tahun 1879 di Kota Jepara, Hari kelahirannya itu kemudian diperingati sebagai Hari Kartini untuk menghormati jasa RA Kartini pada bangsa Indonesia. Nama lengkap Kartini adalah Raden Ajeng Kartini Djojo Adhiningrat. Mengenai sejarah RA Kartini dan kisah hidup Kartini, ia lahir di tengah-tengah keluarga bangsawan oleh sebab itu ia memperoleh gelar R.A (Raden Ajeng) di depan namanya, gelar itu sendiri (Raden Ajeng) dipergunakan oleh Kartini sebelum ia menikah, jika sudah menikah maka gelar kebangsawanan yang dipergunakan adalah R.A (Raden Ayu) menurut tradisi Jawa. Ayahnya bernama R.M. Sosroningrat, putra dari Pangeran Ario Tjondronegoro IV, seorang bangsawan yang menjabat sebagai bupati jepara, beliau ini merupakan kakek dari R.A Kartini. Ayahnya R.M. Sosroningrat merupakan orang yang terpandang sebab posisinya kala itu sebagai bupati Jepara kala Kartini dilahirkan. Ibu kartini yang bernama M.A. Ngasirah, beliau ini merupakan anak seorang kiai atau guru agama di Telukawur, Kota Jepara. Menurut sejarah, Kartini merupakan keturunan dari Sri Sultan Hamengkubuwono VI, bahkan ada yang mengatakan bahwa garis keturunan ayahnya berasal dari kerajaan Majapahit. Ibu R.A Kartini yaitu M.A. Ngasirah sendiri bukan keturunan bangsawan, melainkan hanya rakyat biasa saja, oleh karena itu peraturan kolonial Belanda ketika itu mengharuskan seorang Bupati harus menikah dengan bangsawan juga, hingga akhirnya ayah Kartini kemudian mempersunting seorang wanita bernama Raden Adjeng Woerjan yang merupakan seorang bangsawan keturunan langsung dari Raja Madura ketika itu.</p>\r\n<p> </p>\r\n<p>Pernyataan yang tidak benar berdasarkan teks di atas adalah ....</p>', '<p>Peraturan kolonial Belanda ketika itu mengharuskan seorang Bupati harus menikah dengan bukan bangsawan</p>', '<p>Ibu kandung Kartini bernama M.A. Ngasirah</p>', '<p>Kartini lahir di tengah-tengah keluarga bangsawan oleh sebab itu ia memperoleh gelar R.A (Raden Ajeng) di depan namanya</p>', '<p>Raden Adjeng Woerjan merupakan seorang bangsawan keturunan langsung dari Raja Madura</p>', '<p>Ayah Kartini bernama R.M. Sosroningrat, putra dari Pangeran Ario Tjondronegoro IV</p>', 4, -1, -1, -1, -1, 0, '', '', '', '', '', 'A', '', 1592069489, 1592112230),
(424, 20, 18, 37, 0, '0cb10a64991eb5a2e1696ce0de0b629d.JPG', 'image/jpeg', '<p>Bacaan untuk soal 37-41</p>\r\n<p> </p>\r\n<p>Tokoh wanita satu ini sangat terkenal di Indonesia. Dialah Raden Ajeng Kartini Djojo Adhiningrat atau dikenal sebagai R.A Kartini, beliau dikenal sebagai salah satu pahlawan nasional yang dikenal gigih memperjuangkan emansipasi wanita indonesia kala ia hidup. Mengenai Biografi dan Profil R.A Kartini, beliau lahir pada tanggal 21 April tahun 1879 di Kota Jepara, Hari kelahirannya itu kemudian diperingati sebagai Hari Kartini untuk menghormati jasa RA Kartini pada bangsa Indonesia. Nama lengkap Kartini adalah Raden Ajeng Kartini Djojo Adhiningrat. Mengenai sejarah RA Kartini dan kisah hidup Kartini, ia lahir di tengah-tengah keluarga bangsawan oleh sebab itu ia memperoleh gelar R.A (Raden Ajeng) di depan namanya, gelar itu sendiri (Raden Ajeng) dipergunakan oleh Kartini sebelum ia menikah, jika sudah menikah maka gelar kebangsawanan yang dipergunakan adalah R.A (Raden Ayu) menurut tradisi Jawa. Ayahnya bernama R.M. Sosroningrat, putra dari Pangeran Ario Tjondronegoro IV, seorang bangsawan yang menjabat sebagai bupati jepara, beliau ini merupakan kakek dari R.A Kartini. Ayahnya R.M. Sosroningrat merupakan orang yang terpandang sebab posisinya kala itu sebagai bupati Jepara kala Kartini dilahirkan. Ibu kartini yang bernama M.A. Ngasirah, beliau ini merupakan anak seorang kiai atau guru agama di Telukawur, Kota Jepara. Menurut sejarah, Kartini merupakan keturunan dari Sri Sultan Hamengkubuwono VI, bahkan ada yang mengatakan bahwa garis keturunan ayahnya berasal dari kerajaan Majapahit. Ibu R.A Kartini yaitu M.A. Ngasirah sendiri bukan keturunan bangsawan, melainkan hanya rakyat biasa saja, oleh karena itu peraturan kolonial Belanda ketika itu mengharuskan seorang Bupati harus menikah dengan bangsawan juga, hingga akhirnya ayah Kartini kemudian mempersunting seorang wanita bernama Raden Adjeng Woerjan yang merupakan seorang bangsawan keturunan langsung dari Raja Madura ketika itu.</p>\r\n<p> </p>\r\n<p>Pertanyaan yang jawabannya tidak ada pada teks di atas adalah ...</p>', '<p>Mengapa ayah Kartini kemudian mempersunting seorang wanita bernama Raden Adjeng Woerjan yang merupakan seorang bangsawan?</p>', '<p>Siapakah kakek dari R.A. Kartini?</p>', '<p>Siapakah nama ayah dari Raden Adjeng Woerjan?</p>', '<p>Pada tanggal berapakah R.A. Kartini dilahirkan?</p>', '<p>Di manakah ayah dari M.A. Ngasirah mengajar ilmu agama?</p>', -1, -1, 4, -1, -1, 0, '', '', '', '', '', 'C', '', 1592069570, 1592112317),
(425, 20, 18, 37, 0, 'c87149b628a28ab4be1ae6e4211f54bc.JPG', 'image/jpeg', '<p>Bacaan untuk soal 37-41</p>\r\n<p> </p>\r\n<p>Tokoh wanita satu ini sangat terkenal di Indonesia. Dialah Raden Ajeng Kartini Djojo Adhiningrat atau dikenal sebagai R.A Kartini, beliau dikenal sebagai salah satu pahlawan nasional yang dikenal gigih memperjuangkan emansipasi wanita indonesia kala ia hidup. Mengenai Biografi dan Profil R.A Kartini, beliau lahir pada tanggal 21 April tahun 1879 di Kota Jepara, Hari kelahirannya itu kemudian diperingati sebagai Hari Kartini untuk menghormati jasa RA Kartini pada bangsa Indonesia. Nama lengkap Kartini adalah Raden Ajeng Kartini Djojo Adhiningrat. Mengenai sejarah RA Kartini dan kisah hidup Kartini, ia lahir di tengah-tengah keluarga bangsawan oleh sebab itu ia memperoleh gelar R.A (Raden Ajeng) di depan namanya, gelar itu sendiri (Raden Ajeng) dipergunakan oleh Kartini sebelum ia menikah, jika sudah menikah maka gelar kebangsawanan yang dipergunakan adalah R.A (Raden Ayu) menurut tradisi Jawa. Ayahnya bernama R.M. Sosroningrat, putra dari Pangeran Ario Tjondronegoro IV, seorang bangsawan yang menjabat sebagai bupati jepara, beliau ini merupakan kakek dari R.A Kartini. Ayahnya R.M. Sosroningrat merupakan orang yang terpandang sebab posisinya kala itu sebagai bupati Jepara kala Kartini dilahirkan. Ibu kartini yang bernama M.A. Ngasirah, beliau ini merupakan anak seorang kiai atau guru agama di Telukawur, Kota Jepara. Menurut sejarah, Kartini merupakan keturunan dari Sri Sultan Hamengkubuwono VI, bahkan ada yang mengatakan bahwa garis keturunan ayahnya berasal dari kerajaan Majapahit. Ibu R.A Kartini yaitu M.A. Ngasirah sendiri bukan keturunan bangsawan, melainkan hanya rakyat biasa saja, oleh karena itu peraturan kolonial Belanda ketika itu mengharuskan seorang Bupati harus menikah dengan bangsawan juga, hingga akhirnya ayah Kartini kemudian mempersunting seorang wanita bernama Raden Adjeng Woerjan yang merupakan seorang bangsawan keturunan langsung dari Raja Madura ketika itu.</p>\r\n<p> </p>\r\n<p>Arti kata emansipasi pada teks di atas adalah ....</p>', '<p>Perubahan yang cukup mendasar dalam suatu bidang</p>', '<p>Perubahan secara drastis untuk perbaikan dalam suatu masyarakat</p>', '<p>Persamaan hak dalam berbagai aspek kehidupan masyarakat</p>', '<p>Meliputi semua bagian atau aspek</p>', '<p>Anjuran yang dikemukakan untuk dipertimbangkan atau untuk diterima</p>', -1, -1, 4, -1, -1, 0, '', '', '', '', '', 'C', '', 1592069654, 1592112392),
(426, 20, 18, 37, 0, 'c2c5c087b7090f6a8aa70783d03a231c.JPG', 'image/jpeg', '<p>Soal No 42 - 45</p>\r\n<p> </p>\r\n<p>Di sebuah laboratorium, Profesor X akan membuat suatu zat kimia Y dari mencampur cairan A, B, C, D, E, dan F. Setiap cairan tidak dapat dimasukkan tabung reaksi bersamaan, melainkan harus satu per satu, jika tidak, maka akan terjadi ledakan. Selain itu, zat kimia Y dapat dibuat hanya jika urutan memasukkan cairan tersebut tepat (bisa lebih dari satu cara urutan memasukkan). Cairan dasar adalah cairan yang dimasukkan pertama kali sebelum dicampur dengan cairan lain. Berikut ini ketentuan urutan yang tepat saat memasukkan cairan-cairan tersebut: </p>\r\n<p>• Cairan C dimasukkan pada, urutan ke-3 sebelum cairan A. </p>\r\n<p>• Cairan D dimasukkan pada, urutan ke-3 setelah cairan B. </p>\r\n<p>• Cairan B dimasukkan sebelum cairan F.</p>\r\n<p> </p>\r\n<p>Jika cairan C dimasukkan sebelum cairan B, maka pernyataan yang mungkin benar adalah .... </p>', '<p>Cairan D dimasukkan sebelum cairan A</p>', '<p>Cairan F dimasukkan setelah cairan D dan A</p>', '<p>Cairan F dimasukkan sebelum cairan D tetapi setelah cairan A</p>', '<p>Baik cairan F atau C tidak dapat menjadi cairan dasar</p>', '<p>Cairan C dan B tidak dapat dimasukkan pada urutan yang berdekatan</p>', -1, 4, -1, -1, -1, 0, '', '', '', '', '', 'B', '<p><span xss=removed>Dari bacaan diketahui :</span><br xss=removed><span xss=removed>C dimasukkan pada, urutan ke-3 sebelum A ( C _ _ A )</span><br xss=removed><span xss=removed>D dimasukkan pada, urutan ke-3 setelah B ( B _ _ D )</span><br xss=removed><span xss=removed>B dimasukkan sebelum F ( B < F><br xss=removed><span xss=removed>Dari soal, C dimasukkan sebelum B ( C < B><br xss=removed><span xss=removed>Opsi A salah karena jika D sebelum A, maka B sebelum C, yaitu BCEDAF atau BCFDAE</span><br xss=removed><span xss=removed>(bertentangan dengan soal bahwa C < B><br xss=removed><span xss=removed>Opsi B mungkin benar yaitu saat CBEADF</span><br xss=removed><span xss=removed>Opsi C salah karena jika F sebelum D tetapi setelah A akan menjadi C__AFB__D (tidak mungkin)</span><br xss=removed><span xss=removed>Opsi D salah karena C bisa menjadi cairan dasar misalnya saat CEBAFD</span><br xss=removed><span xss=removed>Opsi E salah karena bisa saja CBEADF</span></p>', 1592069734, 1592112630),
(427, 20, 18, 37, 0, 'ef1e285dfe65640ccdc6f74beaabc15e.JPG', 'image/jpeg', '<p>Soal No 42 - 45</p>\r\n<p> </p>\r\n<p>Di sebuah laboratorium, Profesor X akan membuat suatu zat kimia Y dari mencampur cairan A, B, C, D, E, dan F. Setiap cairan tidak dapat dimasukkan tabung reaksi bersamaan, melainkan harus satu per satu, jika tidak, maka akan terjadi ledakan. Selain itu, zat kimia Y dapat dibuat hanya jika urutan memasukkan cairan tersebut tepat (bisa lebih dari satu cara urutan memasukkan). Cairan dasar adalah cairan yang dimasukkan pertama kali sebelum dicampur dengan cairan lain. Berikut ini ketentuan urutan yang tepat saat memasukkan cairan-cairan tersebut: </p>\r\n<p>• Cairan C dimasukkan pada, urutan ke-3 sebelum cairan A. </p>\r\n<p>• Cairan D dimasukkan pada, urutan ke-3 setelah cairan B. </p>\r\n<p>• Cairan B dimasukkan sebelum cairan F.</p>\r\n<p> </p>\r\n<p>Jika cairan F dimasukkan pada urutan ke-3, maka yang mungkin dimasukkan pada urutan ke-2 adalah cairan ... </p>', '<p>A atau D</p>', '<p>C atau D</p>', '<p>C atau A</p>', '<p>D atau E</p>', '<p>B atau C</p>', -1, -1, -1, -1, 4, 0, '', '', '', '', '', 'E', '<p><span xss=removed>Jika F pada urutan ke-3 maka kemungkinannya adalah BCFDAE atau CBFADE.</span><br xss=removed><span xss=removed>Jadi yang mungkin dimasukkan pada urutan ke-2 adalah B atau C</span></p>', 1592069786, 1592112737),
(428, 20, 18, 37, 0, '6d9d8f3ab952c4ca6a7ea34dd83c3e03.jpg', 'image/jpeg', '<p>Soal No 42 - 45</p>\r\n<p> </p>\r\n<p>Di sebuah laboratorium, Profesor X akan membuat suatu zat kimia Y dari mencampur cairan A, B, C, D, E, dan F. Setiap cairan tidak dapat dimasukkan tabung reaksi bersamaan, melainkan harus satu per satu, jika tidak, maka akan terjadi ledakan. Selain itu, zat kimia Y dapat dibuat hanya jika urutan memasukkan cairan tersebut tepat (bisa lebih dari satu cara urutan memasukkan). Cairan dasar adalah cairan yang dimasukkan pertama kali sebelum dicampur dengan cairan lain. Berikut ini ketentuan urutan yang tepat saat memasukkan cairan-cairan tersebut: </p>\r\n<p>• Cairan C dimasukkan pada, urutan ke-3 sebelum cairan A. </p>\r\n<p>• Cairan D dimasukkan pada, urutan ke-3 setelah cairan B. </p>\r\n<p>• Cairan B dimasukkan sebelum cairan F.</p>\r\n<p> </p>\r\n<p>Jika cairan A dimasukkan setelah cairan D, maka pernyataan yang mungkin benar adalah .... </p>', '<p>Cairan E dimasukkan pada urutan ke-4</p>', '<p>Cairan F dimasukkan pada urutan ke-4</p>', '<p>Cairan B dimasukkan sebelum cairan C</p>', '<p>Cairan D dimasukkan sebelum cairan C</p>', '<p>Cairan A atau C sama-sama bisa dimasukkan pada urutan ke-3</p>', -1, -1, 4, -1, -1, 0, '', '', '', '', '', 'C', '<p><span xss=removed>Jika A dimasukkan setelah D (D < A><br xss=removed><span xss=removed>BECDFA.</span><br xss=removed><span xss=removed>Jadi Opsi A salah, Opsi B salah, Opsi C benar, Opsi D salah, Opsi E salah (A tidak bisa pada urutan</span><br xss=removed><span xss=removed>ke-3)</span></p>', 1592069881, 1592112862),
(429, 20, 18, 37, 0, '80315e6022634f3f759addd4c2a67211.jpg', 'image/jpeg', '<p>Soal No 42 - 45</p>\r\n<p> </p>\r\n<p>Di sebuah laboratorium, Profesor X akan membuat suatu zat kimia Y dari mencampur cairan A, B, C, D, E, dan F. Setiap cairan tidak dapat dimasukkan tabung reaksi bersamaan, melainkan harus satu per satu, jika tidak, maka akan terjadi ledakan. Selain itu, zat kimia Y dapat dibuat hanya jika urutan memasukkan cairan tersebut tepat (bisa lebih dari satu cara urutan memasukkan). Cairan dasar adalah cairan yang dimasukkan pertama kali sebelum dicampur dengan cairan lain. Berikut ini ketentuan urutan yang tepat saat memasukkan cairan-cairan tersebut: </p>\r\n<p>• Cairan C dimasukkan pada, urutan ke-3 sebelum cairan A. </p>\r\n<p>• Cairan D dimasukkan pada, urutan ke-3 setelah cairan B. </p>\r\n<p>• Cairan B dimasukkan sebelum cairan F.</p>\r\n<p> </p>\r\n<p>Urutan memasukkan cairan-cairan tersebut yang mungkin benar sesuai ketentuan pada soal adalah .... </p>', '<p>B, A, D, E, F, C</p>', '<p>F, B, C, E, D, A</p>', '<p>C, B, F, D, E, A</p>', '<p>C, B, F, A, D, E</p>', '<p>D, B, F, C, E, A</p>', -1, -1, -1, 4, -1, 0, '', '', '', '', '', 'D', '<p><span xss=removed>Opsi A salah karena seharusnya C _ _ A (C sebelum A)</span><br xss=removed><span xss=removed>Opsi B salah karena B seharusnya sebelum F</span><br xss=removed><span xss=removed>Opsi C salah karena seharusnya B _ _ D (D urutan, ke-3 setelah B)</span><br xss=removed><span xss=removed>Opsi D mungkin benar</span><br xss=removed><span xss=removed>Opsi E salah karena seharusnya B _ _ D (D urutan, ke-3 setelah B)</span></p>', 1592069948, 1592112965),
(430, 20, 18, 37, 0, 'f16fcc7b1015181402607c508b9f5f1f.jpg', 'image/jpeg', '<p>Soal no 46 - 50</p>\r\n<p> </p>\r\n<p>Empat mahasiswa universitas x : Pipin, Anta, Hanung, dan Rendy akan memilih salah satu dari 5 klub yaitu PSIS, Arema, Persebaya, Bali United, dan Bhayangkara FC untuk mengikuti kompetisi Playstation 4. Tiap tim hanya boleh dipilih oleh satu mahasiswa. Berikut aturannya </p>\r\n<p>• Pipin dan Hanung tidak memilih PSIS </p>\r\n<p>• Anta hanya akan memilih Persebaya atau Arema </p>\r\n<p>• Rendy hanya boleh memilih PSIS atau Bali United </p>\r\n<p>• Jika Rendy memilih Bali United, Hanung akan memilih Arema </p>\r\n<p>• Bhayangkara FC akan dipilih Pipin, Jika Arema dan Bali United telah dipilih.</p>\r\n<p> </p>\r\n<p>Jika Rendy memilih Bali United, pernyataan berikut yang benar adalah …. </p>', '<p>Pipin memilih Persebaya</p>', '<p>Hanung memilih Persebaya</p>', '<p>Pipin memilih Arema</p>', '<p>Anta memilih Persebaya</p>', '<p>Hanung memilih Bhayangkara FC</p>', -1, -1, -1, 4, -1, 0, '', '', '', '', '', 'D', '<p><span xss=removed>Hanung memilih Arema, anta memilih Persebaya</span></p>', 1592070176, 1592113075),
(431, 21, 23, 36, 0, '', '', '<p>A: Will you come to my house tomorrow?</p>\r\n<p>B: I’m sorry. I ………… come because I ………… do my homework tomorrow.</p>', '<p>won’t, will</p>', '<p>won’t, will be</p>', '<p>will, will</p>', '<p>will, won’t</p>', '', 4, -1, -1, -1, -1, 0, '', '', '', '', '', 'A', '', 1592072397, 1592072397),
(432, 21, 23, 36, 0, '', '', '<p>Look at those black clouds. It ………… rain.</p>', '<p>won\'t</p>', '<p>is</p>', '<p>will</p>', '<p>are</p>', '', -1, -1, 4, -1, -1, 0, '', '', '', '', '', 'C', '<p>sudah mendung jadi nya akan hujan</p>', 1592072502, 1592072502),
(433, 21, 23, 36, 0, '', '', '<p>I … on the train station by this time tomorrow morning.</p>', '<p>will wait</p>', '<p>am waiting</p>', '<p>will be waiting</p>', '<p>am waited</p>', '', -1, -1, 4, -1, -1, 0, '', '', '', '', '', 'C', '<p>by this time tomorrow morning</p>\r\n<p>by this time berarti waktu yang spesifik</p>\r\n<p>tomorrow morning berarti future</p>\r\n<p> </p>\r\n<p>future continous</p>', 1592072641, 1592072641),
(434, 21, 23, 36, 0, '', '', '<p xss=removed>Bobby … on Saturday at 2 p.m.</p>', '<p>will not work</p>', '<p>will be not working</p>', '<p>will not be working</p>', '<p>will work not</p>', '', -1, -1, 4, -1, -1, 0, '', '', '', '', '', 'C', '<p>at 2 p.m menunjukkan waktunya spesifik</p>\r\n<p>dan future</p>\r\n<p>future continous</p>', 1592072731, 1592072731),
(435, 21, 23, 36, 0, '', '', '<p>... the book tonight?</p>', '<p>Will read</p>', '<p>Will you read</p>', '<p>Will you are reading</p>', '<p>Will you be read</p>', '', -1, 4, -1, -1, -1, 0, '', '', '', '', '', 'B', '<p>cara membuat pertanyaan dengan jawaban yes/no:</p>\r\n<p>auxiliary ver+Subject+infinitive</p>', 1592072816, 1592072816),
(436, 21, 23, 36, 0, '', '', '<p>She … when her husband comes home.</p>', '<p>will be sleeping</p>', '<p>will sleeping</p>', '<p>sleeps</p>', '<p>will sleeps</p>', '', 4, -1, -1, -1, -1, 0, '', '', '', '', '', 'A', '<p>when her husband comes home merupakan penanda waktu yang spesifik sehingga bentuknya continous</p>\r\n<p> </p>', 1592072894, 1592072894),
(437, 21, 23, 36, 0, '', '', '<p>People … at home during hurricane season.</p>', '<p>will stays</p>', '<p>will be staying</p>', '<p>will be stays</p>', '<p> is stay</p>', '', -1, 4, -1, -1, -1, 0, '', '', '', '', '', 'B', '<p>during hurricane= selama badai = waktu yang spesifik = continous</p>', 1592072994, 1592072994),
(438, 21, 23, 36, 0, '', '', '<p>By this afternoon, I … the lawn with a push mower.</p>', '<p>will have mowed</p>', '<p>will have been mowed</p>', '<p>will mowed</p>', '<p>will be mowing</p>', '', 4, -1, -1, -1, -1, 0, '', '', '', '', '', 'A', '<p>by  this afternoon= per nanti siang, batasan waktu yang menunjukkan perfect, dan future</p>\r\n<p> </p>\r\n<p>future perfect</p>', 1592073304, 1592073304),
(439, 21, 23, 36, 0, '', '', '<p>You … your GMAT score online next week.</p>', '<p>will have saw</p>', '<p>will have seen</p>', '<p>will have see</p>', '<p>will has seen</p>', '', -1, 4, -1, -1, -1, 0, '', '', '', '', '', 'B', '<p>next week menunjukkan future. bentuknya yang benar hanya yang b</p>', 1592073881, 1592073881),
(440, 21, 23, 36, 0, '', '', '<p>The person … probation by January.</p>', '<p>will has finished</p>', '<p>will finished</p>', '<p>will have finished</p>', '<p>will be finished</p>', '', -1, -1, 4, -1, -1, 0, '', '', '', '', '', 'C', '<p>by January menunjukkan cut off yang menandakan perfect</p>', 1592074272, 1592074272),
(441, 21, 23, 36, 0, '', '', '<p>The manager … the meeting room by twelve.</p>', '<p>will have left</p>', '<p>will left</p>', '<p>will leaves</p>', '<p>leaves</p>', '', 4, -1, -1, -1, -1, 0, '', '', '', '', '', 'A', '<p>by twelve menunjukkan cut off yang merupakan ciri perfect</p>', 1592074354, 1592074354),
(442, 21, 23, 36, 0, '', '', '<p>Your best friend … help by the time you … her to take part in your wedding.</p>', '<p>will have offered, ask</p>', '<p>ask, will have offered</p>', '<p>will offer, have asked</p>', '<p>asked, offered</p>', '', 4, -1, -1, -1, -1, 0, '', '', '', '', '', 'A', '', 1592074406, 1592074406),
(443, 21, 23, 36, 0, '', '', '<p>At this time tomorrow, I … to pay off my debts.</p>', '<p>will have money</p>', '<p>will had money</p>', '<p>will have had money</p>', '<p>have</p>', '', -1, -1, 4, -1, -1, 0, '', '', '', '', '', 'C', '<p>perhatikan strukturnya hanya will have had money yang benar</p>', 1592074492, 1592074492),
(444, 21, 23, 36, 0, '', '', '<p>Before you arrive at the station, a hotel room … for you.</p>', '<p>will have booked</p>', '<p>will have book</p>', '<p>will booked</p>', '<p>will have been booked</p>', '', -1, -1, -1, 4, -1, 0, '', '', '', '', '', 'D', '<p>future perfect passive</p>', 1592074547, 1592074547),
(445, 21, 23, 36, 0, '', '', '<p>All the campers … in their tent before midnight.</p>', '<p>will have sleep</p>', '<p>will have slept</p>', '<p>will sleeps</p>', '<p>sleeps</p>', '', -1, 4, -1, -1, -1, 0, '', '', '', '', '', 'B', '', 1592074602, 1592074602),
(446, 21, 23, 36, 0, '', '', '<p>By this time next month, my father and I … our family business for a year.</p>', '<p>will have been running</p>', '<p>will have ran</p>', '<p>will run</p>', '<p>will ran</p>', '', 4, -1, -1, -1, -1, 0, '', '', '', '', '', 'A', '<p>by this time next month menunjukkan batasan waktu sekaligus waktu yang spesifik= future perfect continous</p>', 1592074754, 1592074754),
(447, 21, 23, 36, 0, '', '', '<p>You can’t meet him at my office at 11. He … to the bank at 10.</p>', '<p>will have gone</p>', '<p>will have been gone</p>', '<p>will have go</p>', '<p>will has gone</p>', '', 4, -1, -1, -1, -1, 0, '', '', '', '', '', 'A', '', 1592074812, 1592074812),
(448, 21, 23, 36, 0, '', '', '<p>I knew that the murder case … by the sheriff.</p>', '<p>Would be investigated</p>', '<p>Would investigate</p>', '<p>Would handled</p>', '<p>Would handle</p>', '', 4, -1, -1, -1, -1, 0, '', '', '', '', '', 'A', '<p>knew menunjukkan past tense tetapi ada future, maka past future</p>', 1592074880, 1592074880),
(449, 21, 23, 36, 0, '', '', '<p>Lia …going to give two beautiful scarfs to her friend last afternoon.</p>', '<p>were</p>', '<p>is</p>', '<p>was</p>', '<p>have</p>', '', -1, -1, 4, -1, -1, 0, '', '', '', '', '', 'C', '', 1592074919, 1592074919),
(450, 21, 23, 36, 0, '', '', '<p>I knew you would …all the things for the meeting.</p>', '<p>Prepared</p>', '<p>Prepares</p>', '<p>Had prepared</p>', '<p>Have prepared</p>', '', -1, -1, -1, 4, -1, 0, '', '', '', '', '', 'D', '', 1592074980, 1592074980),
(451, 21, 23, 36, 0, '', '', '<p>My uncle told me that he …come on time.</p>', '<p>will</p>', '<p>would</p>', '<p>would habe</p>', '<p>would have been</p>', '', -1, 4, -1, -1, -1, 0, '', '', '', '', '', 'B', '<p>ada kata told, sehingga indirect speechya yang directnya future menjadi past future</p>', 1592075078, 1592075078),
(452, 21, 23, 36, 0, '', '', '<p>My uncle told me \"I … come on time.\"</p>', '<p>will</p>', '<p>would</p>', '<p>would have</p>', '<p>would have been</p>', '', 4, -1, -1, -1, -1, 0, '', '', '', '', '', 'A', '<p>kalimat langsung tidak perlu merubah jenis tenses</p>', 1592075132, 1592075132),
(453, 21, 23, 36, 0, '', '', '<p>She promised she would … me a box of banana cake.</p>', '<p>given</p>', '<p>give</p>', '<p>have giving</p>', '<p>giving</p>', '', -1, 4, -1, -1, -1, 0, '', '', '', '', '', 'B', '<p>setelah modal (would) adalah infinitive</p>', 1592075198, 1592075198),
(454, 21, 23, 36, 0, '75d361e918e58df0d303179d514d0176.jpg', 'image/jpeg', '<p>It can be inferred from the ﬁrst paragraph that all of the following may have made and used tools <strong>EXCEPT...</strong></p>', '<p>Australopithecus robustus</p>', '<p>Homo erectus</p>', '<p>Homo habilis</p>', '<p>Australopithecus robustus’ ancestors</p>', '', -1, -1, -1, 4, -1, 0, '', '', '', '', '', 'D', '', 1592076197, 1592076197),
(455, 21, 23, 36, 0, '026e0825fc0b6403ce232ab4a91f7cc6.jpg', 'image/jpeg', '<p>The word “extensive” in line 9 is closest in meaning to...</p>', '<p>numerous</p>', '<p>exposed</p>', '<p>ancient</p>', '<p>valuable</p>', '', 4, -1, -1, -1, -1, 0, '', '', '', '', '', 'A', '', 1592076240, 1592076240),
(456, 21, 23, 36, 0, '86b903f77fe8c2f1bb1613c4bdba2214.jpg', 'image/jpeg', '<p>Which of the following does the author mention as the most important recent discovery made in the Swartkrans cave?</p>', '<p>Tools</p>', '<p>Teeth</p>', '<p>Plant fossils</p>', '<p>Hand bones</p>', '', -1, -1, -1, 4, -1, 0, '', '', '', '', '', 'D', '', 1592076290, 1592076290),
(457, 21, 23, 36, 0, 'ccea773728fbf0099e508d68f25b6fa0.jpg', 'image/jpeg', '<p>What does the third paragraph ‘mainly discuss?</p>', '<p>Features of Australopithecus robustus’ hand</p>', '<p>Purposes for which hominids used tools</p>', '<p>Methods used to determine the age of fossils</p>', '<p>Signiﬁcant plant fossils found in layers of sediment</p>', '', 4, -1, -1, -1, -1, 0, '', '', '', '', '', 'A', '', 1592076347, 1592076347),
(458, 21, 23, 36, 0, '', '', '<p>It can be inferred from the description in the last paragraph that Australopithecus robustus was so named because of the species’...</p>', '<p>ancestors</p>', '<p>thumb</p>', '<p>build</p>', '<p>diet</p>', '', -1, -1, 4, -1, -1, 0, '', '', '', '', '', 'C', '', 1592076386, 1592076386),
(459, 21, 23, 36, 0, '7f5db6e5f504bd5fe7be0c2c06a73b20.jpg', 'image/jpeg', '<p>The word “supplant” in line 22 is closest in meaning to...</p>', '<p>exploit</p>', '<p>displace</p>', '<p>understand</p>', '<p>imitate</p>', '', -1, 4, -1, -1, -1, 0, '', '', '', '', '', 'B', '', 1592076437, 1592076437),
(460, 21, 23, 36, 0, '80a3240da8627f9cbd72ec4532698927.jpg', 'image/jpeg', '<p>The word “them” in line 23 refers to...</p>', '<p>tools</p>', '<p>Homo habilis</p>', '<p>Australopithecus robustus</p>', '<p>experts</p>', '', -1, 4, -1, -1, -1, 0, '', '', '', '', '', 'B', '', 1592079824, 1592079824),
(461, 21, 23, 36, 0, '', '', '<p>What does the author suggest is unclear about Australopithecus robustus?</p>', '<p>Whether they used tools</p>', '<p>What they most likely ate</p>', '<p>Whether they are closely related to humans</p>', '<p>Why they became extinct</p>', '', -1, -1, -1, 4, -1, 0, '', '', '', '', '', 'D', '', 1592079864, 1592079864),
(462, 21, 23, 36, 0, '387385b811d8a16404af8b2ec7f0b140.jpg', 'image/jpeg', '<p>The phrase “reliance on” in line 24 is closest in meaning to...</p>', '<p>impact on</p>', '<p>dependence on</p>', '<p>tolerance of</p>', '<p>discovery of</p>', '', -1, 4, -1, -1, -1, 0, '', '', '', '', '', 'B', '', 1592080102, 1592080102),
(463, 21, 23, 36, 0, '', '', '<p>Where in the passage does the author mention the materials from which tools were made?</p>', '<p>Lines 7-9</p>', '<p>Lines 12-13</p>', '<p>Lines 15-17</p>', '<p>Lines 21-23</p>', '', -1, -1, 4, -1, -1, 0, '', '', '', '', '', 'C', '', 1592080157, 1592080157),
(464, 21, 23, 36, 0, '', '', '<p class=\"MsoListParagraph\">Canadian Bianca Andreeescu <u>stunned</u> the tennis world by <u>beated  </u>23-time  grand slam champion Serena Williams <u>to</u> clinch her maiden <u>major</u> <u>title</u></p>', '<p>stunned</p>', '<p>beated</p>', '<p>to</p>', '<p>major</p>', '<p>title</p>', -1, 4, -1, -1, -1, 0, '', '', '', '', '', 'B', '<p>seharusnya beating, karena preposisi diikuti gerund</p>', 1592080453, 1592080509),
(465, 21, 23, 36, 0, '', '', '<p class=\"MsoNormal\" xss=removed><span xss=removed>Snow and ice reflect <u>significant </u> <u>more sunlight</u> than open water <u>does</u>, so bigger, frozen surfaces mean <u>more radiation</u> is <u>sent back</u> to space.</span></p>', '<p>significant</p>', '<p class=\"MsoNormal\" xss=removed><span lang=\"IN\" xss=removed>More sunlight</span></p>', '<p class=\"MsoNormal\" xss=removed><span lang=\"IN\" xss=removed>Does</span></p>', '<p class=\"MsoNormal\" xss=removed><span lang=\"IN\" xss=removed>More radiation</span></p>', '<p class=\"MsoNormal\" xss=removed><span lang=\"IN\" xss=removed>Sent back</span></p>', 4, -1, -1, -1, -1, 0, '', '', '', '', '', 'A', '<p>seharusnya sifnificantly</p>', 1592080751, 1592080751),
(466, 21, 23, 36, 0, '', '', '<p class=\"MsoNormal\" xss=removed><u><span xss=removed>The result </span></u><strong xss=removed><span xss=removed><span xss=removed> </span></span></strong><u><span xss=removed>images</span></u><span xss=removed> show commercial and military aircraft, from B-52s to Boeing 747s, <u>in</u> <u>varying</u> states of <u>decay</u></span><u><span lang=\"IN\" xss=removed>.</span></u></p>', '<p class=\"MsoNormal\" xss=removed><span lang=\"IN\" xss=removed>the result</span></p>', '<p class=\"MsoNormal\" xss=removed><span lang=\"IN\" xss=removed>images</span></p>', '<p>in</p>', '<p>varying</p>', '<p>decay</p>', 4, -1, -1, -1, -1, 0, '', '', '', '', '', 'A', '<p>seharusnya the resulting</p>', 1592080879, 1592080879),
(467, 21, 23, 36, 0, '', '', '<p class=\"MsoNormal\" xss=removed><span xss=removed>Scientists have <u>discovered</u> <u>evidence</u> that the impact of <u>a asteroid</u> as</span> <span xss=removed>powerful as 10 billion </span><span xss=removed><a href=\"https://cnn.com/2019/09/01/europe/germany-poland-ww2-forgiveness-grm-intl/index.html\" target=\"_blank\" rel=\"noopener\"><span xss=removed>World War II</span></a><span xss=removed>-era atomic <u>bombs</u> <u>caused</u> the extinction of the </span><a href=\"https://cnn.com/2019/09/05/world/new-duck-billed-dinosaur-scn-trnd/index.html\" target=\"_blank\" rel=\"noopener\"><span xss=removed>dinosaurs</span></a><span xss=removed>.</span></span></p>', '<p>dicovered</p>', '<p>evidence</p>', '<p>a asteroid</p>', '<p>bombs</p>', '<p>caused</p>', -1, -1, 4, -1, -1, 0, '', '', '', '', '', 'C', '<p><span xss=removed>an asteroid</span></p>', 1592080982, 1592080982),
(468, 21, 23, 36, 0, '', '', '<p class=\"MsoNormal\" xss=removed><span xss=removed>Many dinosaurs would <u>had </u>died that day, but <u>others</u> may have perished </span><span xss=removed><span xss=removed> </span></span><u><span xss=removed>from</span></u><span xss=removed> <u>the atmospheric</u> <u>fall-out</u> that followed</span></p>', '<p>had</p>', '<p>others</p>', '<p>from</p>', '<p>the atmospheric</p>', '<p>fall-out</p>', 4, -1, -1, -1, -1, 0, '', '', '', '', '', 'A', '<p><span xss=removed>modal + infinitive (would have)</span></p>', 1592081071, 1592081071),
(469, 21, 23, 36, 0, '', '', '<p class=\"MsoNormal\" xss=removed><span xss=removed><a href=\"https://www.cnn.com/2019/01/16/health/climate-change-health-emergency-study/index.html\" target=\"_blank\" rel=\"noopener\"><span xss=removed>Rise </span></a><span xss=removed> global temperatures could <u>lead to</u> <u>many</u> <u>more </u></span><a href=\"https://apps.who.int/iris/bitstream/handle/10665/134014/9789241507691_eng.pdf;jsessionid=C193901907CE0EB419097514AE74495C?sequence=1\" target=\"_blank\" rel=\"noopener\"><span xss=removed>deaths</span></a><span xss=removed> than the </span></span><span xss=removed><span xss=removed> </span></span><span xss=removed>250,000 a year between 2030 and 2050 the World Health Organization </span><span xss=removed><span xss=removed> </span></span><u><span xss=removed>predicted</span></u><span xss=removed> just five years ago.</span></p>', '<p>rise</p>', '<p>lead</p>', '<p>may</p>', '<p>more deaths</p>', '<p>predicted</p>', 4, -1, -1, -1, -1, 0, '', '', '', '', '', 'A', '<p><span xss=removed>rise kata kerja, ubah dulu menjadi rising</span></p>', 1592081131, 1592081131),
(470, 21, 23, 36, 0, '', '', '<p class=\"MsoNormal\" xss=removed><u><span xss=removed>Understanding</span></u><span xss=removed> how concrete works in space <u>has</u> direct bearing <u>on</u> future</span> <span xss=removed>plans regarding <u>the </u>moon and Mars, <u>accord</u></span><u><span lang=\"IN\" xss=removed>ed to</span></u><u> </u><span xss=removed>NASA</span><span lang=\"IN\" xss=removed>.</span></p>', '<p>understanding</p>', '<p>has</p>', '<p>on</p>', '<p>the</p>', '<p>accorded to</p>', -1, -1, -1, -1, 4, 0, '', '', '', '', '', 'E', '<p><span xss=removed>seharusnya according to</span></p>', 1592081193, 1592081193),
(471, 21, 23, 36, 0, '', '', '<p class=\"MsoNormal\" xss=removed><u><span lang=\"IN\" xss=removed>in</span></u><u> </u><span xss=removed>late July, for example, a <u>suspected</u> meteorite about <u>the same</u> size as a football <u>landing</u> in a rice field in </span><span xss=removed><a href=\"https://edition.cnn.com/2019/07/26/asia/india-meteorite-bihar-intl-hnk/index.html\" target=\"_blank\" rel=\"noopener\"><span xss=removed>eastern India</span></a><span xss=removed>.</span></span></p>', '<p>in</p>', '<p>suspected</p>', '<p>the same</p>', '<p>landing</p>', '<p>eastern India</p>', -1, -1, -1, 4, -1, 0, '', '', '', '', '', 'D', '<p><span xss=removed>seharusnya landed, karena merupakan predikat</span></p>', 1592081265, 1592081265),
(472, 21, 23, 36, 0, '', '', '<p class=\"MsoNormal\" xss=removed><span xss=removed>Zhang started <u>studying</u>. First <u>on</u> the internet and in libraries, then by <u>knocked </u></span><u><span xss=removed><span xss=removed> </span></span></u><span xss=removed>on the<span xss=removed>       </span> door of the Purple Mountain Observatory (PMO) <u>in</u> Nanjing, 300 <u>kilometers</u> east of Shanghai.</span></p>', '<p>Studying</p>', '<p>On</p>', '<p>Knocked</p>', '<p>In</p>', '<p>Kilometers</p>', -1, -1, 4, -1, -1, 0, '', '', '', '', '', 'C', '<p><span xss=removed>setelah preposisi adalah v.ing</span></p>', 1592081328, 1592081328),
(473, 21, 23, 36, 0, '', '', '<p class=\"MsoNormal\" xss=removed><u><span xss=removed>The</span></u><span xss=removed> city has been <u>rocking</u> by sometimes violent anti-government <u>protests</u> in the </span><span xss=removed><span xss=removed> </span></span><span xss=removed>past few months, leading to <u>a decline</u> in visitor <u>numbers</u>.</span></p>', '<p>the city</p>', '<p>rocking</p>', '<p>protests</p>', '<p>a decline</p>', '<p>numbers</p>', -1, 4, -1, -1, -1, 0, '', '', '', '', '', 'B', '<p><span xss=removed>ada by..., menunjukkan kalimat pasive. seharusnya rocked</span></p>', 1592081403, 1592081403),
(474, 21, 23, 36, 0, '', '', '<p><span xss=removed>Planting more trees and <u>create</u> more green spaces is the best way to take <u>immediate action</u> against climate change -- recent studies show it to be the most </span><span xss=removed> </span><span xss=removed>effective, cost-efficient and <u>broadly available</u> solution to <u>combat</u> global warming and it <u>can</u> be done now, anywhere</span></p>', '<p>Create</p>', '<p>Immediate action</p>', '<p>Broadly available</p>', '<p>Combat</p>', '<p>Can</p>', 4, -1, -1, -1, -1, 0, '', '', '', '', '', 'A', '<p><span xss=removed>pararrel, ada kata planting and... maka seharusnya creating</span></p>', 1592081469, 1592081469),
(475, 21, 23, 36, 0, '', '', '<p><span xss=removed>The surface <u>conditions</u> on the planet Mars are <u>the more</u> like the Earth’s <u>than</u> are</span> <span xss=removed>those of any <u>other</u> planet in the <u>solar system</u>.</span></p>', '<p>confitions</p>', '<p>the more</p>', '<p>than</p>', '<p>other</p>', '<p>solar system</p>', -1, 4, -1, -1, -1, 0, '', '', '', '', '', 'B', '<p><span xss=removed>harusnya more saja tidak usah menggunakan the</span></p>', 1592081549, 1592081549),
(476, 21, 23, 36, 0, '', '', '<p class=\"MsoNormal\" xss=removed><span xss=removed>In the future, the researchers <u>plan</u> to study how muscles <u>might have</u> </span><span xss=removed><span xss=removed> </span></span><span xss=removed>attached <u>to</u> the bones <u>to better</u> understand the way Cryodrakon took off, <u>fly </u>and walked.</span></p>', '<p>Plan</p>', '<p>Might have</p>', '<p>To</p>', '<p>To better</p>', '<p>Fly</p>', -1, -1, -1, -1, 4, 0, '', '', '', '', '', 'E', '<p>seharusnya bentuk past tense ibar pararel</p>', 1592081694, 1592081694),
(477, 21, 23, 36, 0, '', '', '<p class=\"MsoNormal\" xss=removed><span xss=removed>The midnight sun <u>is</u> a <u>phenomenon</u> in which the Sun <u>visible remains</u> in <u>the</u> sky for twenty-four hours <u>or longer</u>.</span></p>', '<p>is</p>', '<p>phenomenon</p>', '<p>visible remains</p>', '<p>the</p>', '<p>or longer</p>', -1, -1, 4, -1, -1, 0, '', '', '', '', '', 'C', '', 1592081747, 1592081747),
(478, 21, 23, 36, 0, '', '', '<p class=\"MsoNormal\" xss=removed><span lang=\"IN\" xss=removed>A rainbow is a <u>meteorological</u> phenomenon that <u>being</u> caused by reflection, refraction and dispersion of light in water droplets <u>resulting</u> in a spectrum of light <u>appearing</u> in the sky.</span></p>', '<p>However</p>', '<p>normaly</p>', '<p>formed</p>', '<p>centering</p>', '', -1, -1, -1, 4, -1, 0, '', '', '', '', '', 'D', '<p><span xss=removed>Pelangi terpusatkan, bukan memusat. Seharusnya centered.</span></p>', 1592081845, 1592081845),
(479, 21, 23, 36, 0, '', '', '<p class=\"MsoNormal\" xss=removed><span lang=\"IN\" xss=removed>In a primary rainbow, the arc <u>is showing</u> red on the outer part and violet on the inner side. This rainbow <u>is caused</u> by light <u>being refracted</u> when entering a droplet of water, then reflected inside on the back of the droplet and <u>refracted </u>again when leaving it.</span></p>', '<p>Is showing</p>', '<p>Is caused</p>', '<p>Being refracted</p>', '<p>Refracted</p>', '', 4, -1, -1, -1, -1, 0, '', '', '', '', '', 'A', '<p><span xss=removed>Merupakan kalimat yang menunjukkan fakta, bukan contionus.seharusnya shows</span></p>', 1592081922, 1592081922),
(480, 21, 23, 36, 0, '', '', '<p class=\"MsoNormal\" xss=removed><span lang=\"IN\" xss=removed>In a double rainbow, a second arc <u>has been seen</u> outside the primary arc, and has the order of its colours reversed, <u>with</u> red on the inner side of the arc. This is caused by the light <u>being reflected </u>twice on the inside of the droplet before <u>leaving</u> it.</span></p>', '<p>Has been seen</p>', '<p>With</p>', '<p>Being reflected</p>', '<p>Leaving</p>', '', 4, -1, -1, -1, -1, 0, '', '', '', '', '', 'A', '<p><span xss=removed>Merupakan kalimat yang meujukkan sesuatu yang umum, simple present bukan perfect. Seharusnya is seen</span></p>', 1592081985, 1592081985),
(481, 21, 23, 36, 0, '', '', '<p class=\"MsoNormal\"><span lang=\"IN\" xss=removed>A rainbow is not located at a specific distance from the observer, but <u>coming </u>from an optical illusion <u>caused by</u> any water droplets <u>viewed</u> from a certain angle relative to <u>a light</u> source.</span></p>', '<p>Coming</p>', '<p>Caused by</p>', '<p>Viewed</p>', '<p>A light</p>', '', 4, -1, -1, -1, -1, 0, '', '', '', '', '', 'A', '<p><span xss=removed>Terdapat kata but yang meunjukkan adanya kalimat majemuk, sehingga membutuhkan predikat lagi. Seharusnya comes</span></p>', 1592082059, 1592082059),
(482, 21, 23, 36, 0, '', '', '<p class=\"MsoNormal\"><span lang=\"IN\" xss=removed>Thus, a rainbow is not an object and cannot be <u>physical approached</u>. <u>Indeed</u>, it is impossible for an observer to see a rainbow from water droplets <u>at any angle</u> other than the <u>customary </u>one of 42 degrees from the direction opposite the light source.</span></p>', '<p>Physical approached</p>', '<p>Indeed</p>', '<p>At any angle</p>', '<p>Customary</p>', '', 4, -1, -1, -1, -1, 0, '', '', '', '', '', 'A', '<p><span xss=removed>Seharusnya phisically approached</span></p>', 1592082117, 1592082117),
(483, 21, 23, 36, 0, '', '', '<p class=\"MsoNormal\"><span lang=\"IN\" xss=removed>During the inspection <u>it</u> was found that the<u> provision</u> for an average <u>5-day working </u>week <u>has been violated.</u></span></p>', '<p>it</p>', '<p>provision</p>', '<p>5-day working</p>', '<p>Has been violated</p>', '', -1, -1, -1, 4, -1, 0, '', '', '', '', '', 'D', '<p><span xss=removed>Perhatikan urutannya.penemuanyna sudah lampau, maka apa yang ditemukan juga dalam bentuk lampau. Seharusnya had been vioated.</span></p>', 1592082180, 1592082180),
(484, 21, 23, 36, 0, '', '', '<p class=\"MsoNormal\"><span lang=\"IN\" xss=removed>The Company <u>has admitted</u> in the course of the <u>proceedings</u> that the boundaries <u>of the tunnel</u> <u>have been measured</u> incorrectly.</span></p>', '<p>Has admitted</p>', '<p>Proceedings</p>', '<p>Of the tunnel</p>', '<p>Have been measured</p>', '', -1, -1, -1, 4, -1, 0, '', '', '', '', '', 'D', '<p><span xss=removed>“were measured” is in the Past Simple tense ­– i.e. the proceedings are ongoing (we know this because “has admitted” is the Present Perfect, which expresses action that continues up to the present time) and the incorrect measurements were made before the proceedings.</span></p>', 1592082240, 1592082240),
(485, 21, 23, 36, 0, '', '', '<p class=\"MsoNormal\"><span lang=\"IN\" xss=removed>Thousands of years ago, the motion of comets <u>seemed erratic</u> and unpredictable compared to the motion of the Sun, Moon, planets, and stars, and the erratic behaviour of comets <u>had led</u> people <u>to assume</u> that they originated from <u>inside of</u> the Earth’s atmosphere.</span></p>', '<p>Seemed erratic</p>', '<p>Had led</p>', '<p>To assume</p>', '<p>Inside of</p>', '', -1, 4, -1, -1, -1, 0, '', '', '', '', '', 'B', '<p><span xss=removed>Tidak ada pembanding waktu yang menunjukkan kalimat perfect. Seharusnya led saj</span></p>', 1592082304, 1592082304),
(486, 21, 23, 36, 0, '', '', '<p class=\"MsoNormal\"><span lang=\"IN\" xss=removed>In 1705, Edmond Halley l<u>ooked</u> at all of the <u>documenting</u> appearances of comets and tried to<u> derive</u> their orbital parameters <u>using</u> Newtonian physics.</span></p>', '<p>Looked</p>', '<p>Documenting</p>', '<p>To derive</p>', '<p>using</p>', '', -1, 4, -1, -1, -1, 0, '', '', '', '', '', 'B', '<p><span xss=removed>Seharusnya documented, didokumentasikan bukan mendokumentasikan</span></p>', 1592082367, 1592082367),
(487, 21, 23, 36, 0, '', '', '<p class=\"MsoNormal\"><span lang=\"IN\" xss=removed>The link between comets and meteor showers <u>was</u> proven in the late 19th century when the Italian astronomer Giovanni Schiaparelli <u>showed</u> that the <u>Perseid </u>meteor shower, which occurs every August, <u>was </u>caused by the path of the Earth travelling through debris left by the comet Swift-Tuttle.</span></p>', '<p>was</p>', '<p>showed</p>', '<p>preseid</p>', '<p>was</p>', '', -1, -1, -1, 4, -1, 0, '', '', '', '', '', 'D', '<p class=\"MsoListParagraph\"><span lang=\"IN\" xss=removed>Was menjadi kurang tepat karena apa yang dibuktikan itu adalah fakta umum. Seharusnya is</span></p>', 1592082419, 1592082419),
(488, 21, 23, 36, 0, '', '', '<p class=\"MsoNormal\" xss=removed><span lang=\"IN\" xss=removed>Rainbows can be full circles. <u>However</u>, the observer <u>normally </u>sees only an arc <u>formed </u>by illuminated droplets above the ground, and <u>centering</u> on a line from the sun to the observer\'s eye.</span></p>', '<p>however</p>', '<p>normally</p>', '<p>formed</p>', '<p>centering</p>', '', -1, -1, -1, 4, -1, 0, '', '', '', '', '', 'D', '<p class=\"MsoListParagraph\" xss=removed><span lang=\"IN\" xss=removed>Pelangi terpusatkan, bukan memusat. Seharusnya centered.</span></p>', 1592082571, 1592082571),
(489, 21, 23, 37, 0, '', '', '<p>Soal no 46 - 50</p>\r\n<p> </p>\r\n<p>Empat mahasiswa universitas x : Pipin, Anta, Hanung, dan Rendy akan memilih salah satu dari 5 klub yaitu PSIS, Arema, Persebaya, Bali United, dan Bhayangkara FC untuk mengikuti kompetisi Playstation 4. Tiap tim hanya boleh dipilih oleh satu mahasiswa. Berikut aturannya </p>\r\n<p>• Pipin dan Hanung tidak memilih PSIS </p>\r\n<p>• Anta hanya akan memilih Persebaya atau Arema </p>\r\n<p>• Rendy hanya boleh memilih PSIS atau Bali United </p>\r\n<p>• Jika Rendy memilih Bali United, Hanung akan memilih Arema </p>\r\n<p>• Bhayangkara FC akan dipilih Pipin, Jika Arema dan Bali United telah dipilih.</p>\r\n<p> </p>\r\n<p>Bila ada satu mahasiswa lain yaitu Yanuar yang dapat memilih tim apapun selain Arema dan Bhayangkara FC, agar semua dapat memilih tim, maka Pipin harus memilih ...</p>', '<p>Arema</p>', '<p>Bali united</p>', '<p>PSIS</p>', '<p>Persebaya</p>', '<p>Bhayangkara FC</p>', -1, -1, -1, 4, -1, 0, '', '', '', '', '', 'D', '<p><span xss=removed>Karena Arema dan Bali united telah dipilih maka Pipin memilih Bhayangkara</span></p>', 1592113169, 1592113169),
(490, 21, 23, 37, 0, '', '', '<p>Soal no 46 - 50</p>\r\n<p> </p>\r\n<p>Empat mahasiswa universitas x : Pipin, Anta, Hanung, dan Rendy akan memilih salah satu dari 5 klub yaitu PSIS, Arema, Persebaya, Bali United, dan Bhayangkara FC untuk mengikuti kompetisi Playstation 4. Tiap tim hanya boleh dipilih oleh satu mahasiswa. Berikut aturannya </p>\r\n<p>• Pipin dan Hanung tidak memilih PSIS </p>\r\n<p>• Anta hanya akan memilih Persebaya atau Arema </p>\r\n<p>• Rendy hanya boleh memilih PSIS atau Bali United </p>\r\n<p>• Jika Rendy memilih Bali United, Hanung akan memilih Arema </p>\r\n<p>• Bhayangkara FC akan dipilih Pipin, Jika Arema dan Bali United telah dipilih.</p>\r\n<p> </p>\r\n<p>Jika Hanung memilih Persebaya, Pipin akan memilih tim ...</p>', '<p>Arema</p>', '<p>Balini United</p>', '<p>PSIS</p>', '<p>Persebaya</p>', '<p>Bhayangkara FC</p>', -1, 4, -1, -1, -1, 0, '', '', '', '', '', 'B', '<div id=\"c3230\" class=\"freebirdFormeditorViewAssessmentFeedbackFeedbackText\" xss=removed>Anta memilih Arema, Rendy memilih PSIS. Dan Pipin memilih Bali united</div>', 1592113256, 1592113256),
(491, 21, 23, 37, 0, '', '', '<p>Soal no 46 - 50</p>\r\n<p> </p>\r\n<p>Empat mahasiswa universitas x : Pipin, Anta, Hanung, dan Rendy akan memilih salah satu dari 5 klub yaitu PSIS, Arema, Persebaya, Bali United, dan Bhayangkara FC untuk mengikuti kompetisi Playstation 4. Tiap tim hanya boleh dipilih oleh satu mahasiswa. Berikut aturannya </p>\r\n<p>• Pipin dan Hanung tidak memilih PSIS </p>\r\n<p>• Anta hanya akan memilih Persebaya atau Arema </p>\r\n<p>• Rendy hanya boleh memilih PSIS atau Bali United </p>\r\n<p>• Jika Rendy memilih Bali United, Hanung akan memilih Arema </p>\r\n<p>• Bhayangkara FC akan dipilih Pipin, Jika Arema dan Bali United telah dipilih.</p>\r\n<p> </p>\r\n<p>Bila Hanung memilih Arema, pernyataan berikut ini yang salah adalah … </p>', '<p>Rendy memilih Bali United</p>', '<p>Pipin memilih Bhayangkara FC</p>', '<p>Rendy memilih PSIS</p>', '<p>Pipin memilih Persebaya</p>', '<p>Anta memilih Persebaya</p>', -1, -1, 4, -1, -1, 0, '', '', '', '', '', 'C', '<p><span xss=removed>Jika Hanung memilih arema, maka rendy memilih bukan PSIS</span></p>', 1592113326, 1592113326);
INSERT INTO `tbl_soal_tryout` (`id_soal`, `dosen_id`, `matkul_id`, `id_soal_paket`, `bobot`, `file`, `tipe_file`, `soal`, `opsi_a`, `opsi_b`, `opsi_c`, `opsi_d`, `opsi_e`, `bobot_a`, `bobot_b`, `bobot_c`, `bobot_d`, `bobot_e`, `bobot_kosong`, `file_a`, `file_b`, `file_c`, `file_d`, `file_e`, `jawaban`, `pembahasan`, `created_on`, `updated_on`) VALUES
(492, 21, 23, 37, 0, '', '', '<p>Soal no 46 - 50</p>\r\n<p> </p>\r\n<p>Empat mahasiswa universitas x : Pipin, Anta, Hanung, dan Rendy akan memilih salah satu dari 5 klub yaitu PSIS, Arema, Persebaya, Bali United, dan Bhayangkara FC untuk mengikuti kompetisi Playstation 4. Tiap tim hanya boleh dipilih oleh satu mahasiswa. Berikut aturannya </p>\r\n<p>• Pipin dan Hanung tidak memilih PSIS </p>\r\n<p>• Anta hanya akan memilih Persebaya atau Arema </p>\r\n<p>• Rendy hanya boleh memilih PSIS atau Bali United </p>\r\n<p>• Jika Rendy memilih Bali United, Hanung akan memilih Arema </p>\r\n<p>• Bhayangkara FC akan dipilih Pipin, Jika Arema dan Bali United telah dipilih.</p>\r\n<p> </p>\r\n<p>Jika Pipin memilih Arema, maka pernyataan dibawah ini yang salah adalah … </p>', '<p>Pipin memilih Arema</p>', '<p>Anta memilih Persebaya</p>', '<p>Hanung memilih Bhayangkara FC</p>', '<p>Rendy memilih Bali United</p>', '<p>Hanung memilih Bali United</p>', -1, -1, -1, 4, -1, 0, '', '', '', '', '', 'D', '<p><span xss=removed>Jika Pipin memilih Arema, maka Hanung tidak memilih arema sehingga rendy tidak </span><span xss=removed>memilih bali united</span></p>', 1592113424, 1592113424),
(493, 21, 23, 40, 0, '', '', '<p>In his quest for _____ freedom, Sinaga went to.</p>', '<p>without article</p>', '<p>a</p>', '<p>the</p>', '<p>an</p>', '', 4, -1, -1, -1, -1, 0, '', '', '', '', '', 'A', '<p>freedom itu uncountable dan disitu masih indefinite, maka tidak memakai article</p>', 1592118043, 1592118043),
(494, 21, 23, 40, 0, '', '', '<p>Sinaga went to _____ Lake Toba to go fishing.    </p>', '<p>(-)</p>', '<p>a</p>', '<p>the</p>', '<p>an</p>', '', 4, -1, -1, -1, -1, 0, '', '', '', '', '', 'A', '<p>terdapat aturan khusus mengenai penggunaan article pada lake, di mana menggunakan article the apabila berbentuk jamak, jika singular tidak memakai</p>', 1592118859, 1592118859),
(495, 21, 23, 40, 0, '', '', '<p>The water in _____ Toba River doesn\'t taste good.</p>', '<p>(-)</p>', '<p>a</p>', '<p>the</p>', '<p>an</p>', '', -1, -1, 4, -1, -1, 0, '', '', '', '', '', 'C', '<p>nama sungai menggunakan article the</p>', 1592118963, 1592118963),
(496, 21, 23, 40, 0, '', '', '<p>_____ laughter is the best medicine.</p>', '<p>(-)</p>', '<p>a</p>', '<p>the</p>', '<p>an</p>', '', 4, -1, -1, -1, -1, 0, '', '', '', '', '', 'A', '<p><span xss=removed>\"Laughter\" is a mass </span><strong xss=removed>noun</strong><span xss=removed>, referring to the concept of laughing in general (uncaountable)</span></p>\r\n<p><span xss=removed>laugh=countable</span></p>', 1592119137, 1592119137),
(497, 21, 23, 40, 0, '', '', '<p>_____ money is not the most important thing in life.</p>', '<p>-</p>', '<p>a</p>', '<p>the</p>', '<p>an</p>', '', 4, -1, -1, -1, -1, 0, '', '', '', '', '', 'A', '<p>money is uncountable dan disitu masih indefinite, maka tidak memakai article</p>', 1592119200, 1592119200),
(498, 21, 23, 40, 0, '', '', '<p>After we sell the car, we\'ll split up _____ money.</p>', '<p>-</p>', '<p>a</p>', '<p>the</p>', '<p>an</p>', '', -1, -1, 4, -1, -1, 0, '', '', '', '', '', 'C', '<p>uang yang mau dibagi sudah spesifik</p>', 1592119243, 1592119243),
(499, 21, 23, 40, 0, '', '', '<p>Zidane doesn\'t like his job but stays at it because _____ salary is good.</p>', '<p>-</p>', '<p>a</p>', '<p>the</p>', '<p>an</p>', '', -1, -1, 4, -1, -1, 0, '', '', '', '', '', 'C', '<p>sudah spesifik gaji yang mana</p>', 1592119296, 1592119296),
(500, 21, 23, 40, 0, '', '', '<p>The Sony Corporation spends a lot of money on _____ Developing software.</p>', '<p>-</p>', '<p>a</p>', '<p>the</p>', '<p>an</p>', '', 4, -1, -1, -1, -1, 0, '', '', '', '', '', 'A', '<p>gerund dianggap uncountable</p>', 1592119501, 1592119501),
(501, 21, 23, 40, 0, '', '', '<p>Where did you buy _____ flower in the living room?</p>', '<p>-</p>', '<p>a</p>', '<p>the</p>', '<p>an</p>', '', -1, -1, 4, -1, -1, 0, '', '', '', '', '', 'C', '<p>bunganya sudah spesifik yang mana</p>', 1592119555, 1592119555),
(502, 21, 23, 40, 0, '', '', '<p>Beckham needs to buy _____ new hard drive for his computer</p>', '<p>-</p>', '<p>a</p>', '<p>the</p>', '<p>an</p>', '', -1, 4, -1, -1, -1, 0, '', '', '', '', '', 'B', '<p>hard drive countable dan tunggal yang belum definite, maka menggunakan article a</p>', 1592119638, 1592119638),
(503, 21, 23, 40, 0, '', '', '<p>There can be many surprises in _____ life.</p>', '<p>-</p>', '<p>a</p>', '<p>the</p>', '<p>an</p>', '', 4, -1, -1, -1, -1, 0, '', '', '', '', '', 'A', '', 1592119679, 1592119679),
(504, 21, 23, 40, 0, '', '', '<p>Who wrote _____ Harry Potter Novel?</p>', '<p>-</p>', '<p>a</p>', '<p>the</p>', '<p>an</p>', '', -1, -1, 4, -1, -1, 0, '', '', '', '', '', 'C', '<p>judul buku yang sudah spesifik</p>', 1592119736, 1592119736),
(505, 21, 23, 40, 0, '', '', '<p>Can you spare _____ dollar</p>', '<p>-</p>', '<p>a</p>', '<p>the</p>', '<p>an</p>', '', -1, 4, -1, -1, -1, 0, '', '', '', '', '', 'B', '<p>tunggal dan tidak spesifik</p>', 1592119787, 1592119787),
(506, 21, 23, 40, 0, '', '', '<p>What is _____ soup of the day?</p>', '<p>-</p>', '<p>a</p>', '<p>the</p>', '<p>an</p>', '', -1, -1, 4, -1, -1, 0, '', '', '', '', '', 'C', '', 1592119821, 1592119821),
(507, 21, 23, 40, 0, '', '', '<p>Killer whales enjoy eating _____ seals.</p>', '<p>-</p>', '<p>a</p>', '<p>the</p>', '<p>an</p>', '', 4, -1, -1, -1, -1, 0, '', '', '', '', '', 'A', '', 1592119852, 1592119852),
(508, 21, 23, 40, 0, '', '', '<p>____ violin sold for over a million dollars in an auction today brought in a record price. </p>', '<p>-</p>', '<p>a</p>', '<p>the</p>', '<p>an</p>', '', -1, 4, -1, -1, -1, 0, '', '', '', '', '', 'B', '<p>singular dan indefinite</p>', 1592119897, 1592119897),
(509, 21, 23, 40, 0, '', '', '<p>___ graffiti is a form of art.</p>', '<p>-</p>', '<p>a</p>', '<p>the</p>', '<p>an</p>', '', 4, -1, -1, -1, -1, 0, '', '', '', '', '', 'A', '<p>membicarakan grafiti secara umum</p>', 1592119936, 1592119936),
(510, 21, 23, 40, 0, '', '', '<p>Unlike other art, graffiti is ____ \"illegal one\". </p>', '<p>-</p>', '<p>a</p>', '<p>the</p>', '<p>an</p>', '', -1, -1, 4, -1, -1, 0, '', '', '', '', '', 'C', '', 1592119976, 1592119976),
(511, 21, 23, 40, 0, '', '', '<p>___ graffiti artists usually work under the cover of darkness.</p>', '<p xss=removed>-</p>', '<p>a</p>', '<p>the</p>', '<p>an</p>', '', 4, -1, -1, -1, -1, 0, '', '', '', '', '', 'A', '', 1592120013, 1592120013),
(512, 21, 23, 40, 0, '', '', '<p>Most artists use _____ spray-paint.</p>', '<p>-</p>', '<p>a</p>', '<p>the</p>', '<p>an</p>', '', 4, 0, 0, 0, 0, 0, '', '', '', '', '', 'A', '', 1592120047, 1592120047),
(513, 21, 23, 40, 0, '', '', '<p>______ freeway walls of Los Angeles have several amazing works.</p>', '<p>-</p>', '<p>a</p>', '<p>the</p>', '<p>an</p>', '', -1, -1, 4, -1, -1, 0, '', '', '', '', '', 'C', '<p>freeway nya sudah jelas yang mana</p>', 1592120889, 1592120889),
(514, 21, 23, 40, 0, '', '', '<p>..... apple a day keeps the doctor away </p>', '<p>-</p>', '<p>a</p>', '<p>the</p>', '<p>an</p>', '', -1, -1, -1, 4, -1, 0, '', '', '', '', '', 'D', '<p>singular dan indefinite</p>', 1592120928, 1592120928),
(515, 21, 23, 40, 0, '', '', '<p>____ Second Stradivarius, said by experts to have beenmade in 1698 but modified afterward, was purchased by an anonymous European bidder for about $716,000, French auctioneers Drouot said. </p>', '<p>-</p>', '<p>a</p>', '<p>the</p>', '<p>an</p>', '', -1, -1, 4, -1, -1, 0, '', '', '', '', '', 'C', '<p>mengacu pada sebuah biolah yang dibuat tahun  1698 but modified afterward</p>', 1592121028, 1592327069),
(516, 21, 23, 40, 0, '', '', '<p>Auctioneer Jacques Tajan had predicted before the sale that the violin made in 1714 -- which he described as ______ exquisite instrument -- would sell for between $583,000 and $833,333 \"If it sold for a million dollars, this would make me very happy,\" he had said.</p>', '<p>-</p>', '<p>a</p>', '<p>the</p>', '<p>an</p>', '', -1, -1, -1, 4, -1, 0, '', '', '', '', '', 'D', '<p>instrumen = indefinite dan singular</p>', 1592121083, 1592121083),
(517, 21, 23, 40, 0, '', '', '<p> ____ wolf caught in a henhouse devoured the farmer\'s flock of hens</p>', '<p>-</p>', '<p>a</p>', '<p>the</p>', '<p>an</p>', '', -1, -1, 4, -1, -1, 0, '', '', '', '', '', 'C', '', 1592121126, 1592121126),
(518, 21, 23, 40, 0, '', '', '<p>___ eruption of Krakatoa, or Krakatau, in August 1883 was one of the most deadly volcanic eruptions of modern history.</p>', '<p>-</p>', '<p>a</p>', '<p>the</p>', '<p>an</p>', '', -1, -1, 4, -1, -1, 0, '', '', '', '', '', 'C', '', 1592121165, 1592121165),
(519, 21, 23, 40, 0, '', '', '<p>Many died as ___ result of thermal injury from the blasts and many more were victims of the tsunamis that followed the collapse of the volcano into the caldera below sea level.</p>', '<p>-</p>', '<p>a</p>', '<p>the</p>', '<p>an</p>', '', -1, 4, -1, -1, -1, 0, '', '', '', '', '', 'B', '', 1592121198, 1592121198),
(520, 21, 23, 40, 0, '', '', '<p>____  island of Krakatau is in the Sunda Strait between Java and Sumatra.</p>', '<p>-</p>', '<p>a</p>', '<p>the</p>', '<p>an</p>', '', -1, -1, 4, -1, -1, 0, '', '', '', '', '', 'C', '', 1592121231, 1592121231),
(521, 21, 23, 40, 0, '', '', '<p>In May 1883, the captain of the Elizabeth, a German warship, reported seeing  ___ clouds of ash above Krakatau</p>', '<p>-</p>', '<p>a</p>', '<p>the</p>', '<p>an</p>', '', 4, -1, -1, -1, -1, 0, '', '', '', '', '', 'A', '', 1592121262, 1592121262),
(522, 21, 23, 40, 0, '', '', '<p>People on nearby islands held festivals celebrating the natural fireworks that lit the night sky. Celebration would come to ___ tragic halt on Aug. 27.</p>', '<p>-</p>', '<p>a</p>', '<p>the</p>', '<p>an</p>', '', -1, 4, -1, -1, -1, 0, '', '', '', '', '', 'B', '', 1592121290, 1592121290),
(523, 21, 23, 40, 0, '', '', '<p>I visit the Grand Canyon _________ I go to Arizona</p>', '<p>Once</p>', '<p>Whenerver</p>', '<p>Wherever</p>', '<p>unless</p>', '', -1, 4, -1, -1, -1, 0, '', '', '', '', '', 'B', '<p>whenever= setiap kali kesempatan</p>', 1592121346, 1592121346),
(524, 21, 23, 40, 0, '', '', '<p>This is the place _________ we stayed last time we visited</p>', '<p>where</p>', '<p>when</p>', '<p>how</p>', '<p>that</p>', '', 4, -1, -1, -1, -1, 0, '', '', '', '', '', 'A', '', 1592121381, 1592121381),
(525, 21, 23, 40, 0, '', '', '<p>_________ you win first place, you won\'t receive a prize.</p>', '<p>wherever</p>', '<p>if</p>', '<p>unless</p>', '<p>whenever</p>', '', -1, -1, 4, -1, -1, 0, '', '', '', '', '', 'C', '', 1592121427, 1592121427),
(526, 21, 23, 40, 0, '', '', '<p>_______ you need help, just let me know.</p>', '<p>unless</p>', '<p>if</p>', '<p>whether</p>', '<p>while</p>', '', -1, 4, -1, -1, -1, 0, '', '', '', '', '', 'B', '', 1592121474, 1592121474),
(527, 21, 23, 40, 0, '', '', '<p>______ the bicycle was expensive, he decided to buy it.</p>', '<p>although</p>', '<p>because</p>', '<p>as</p>', '<p>if</p>', '', 4, -1, -1, -1, -1, 0, '', '', '', '', '', 'A', '', 1592121515, 1592121515),
(528, 21, 23, 40, 0, '', '', '<p>August 31st is a national holiday, _________ everybody dances in the streets.</p>', '<p>that</p>', '<p>when</p>', '<p>which</p>', '<p>where</p>', '', -1, 4, -1, -1, -1, 0, '', '', '', '', '', 'B', '', 1592121561, 1592121561),
(529, 21, 23, 40, 0, '', '', '<p>The house________ I live is very small.</p>', '<p>that</p>', '<p>which</p>', '<p>where</p>', '<p>that which</p>', '', -1, -1, 4, -1, -1, 0, '', '', '', '', '', 'C', '', 1592121604, 1592121604),
(530, 21, 23, 40, 0, '', '', '<p>The situation was ______ serious.</p>', '<p>very</p>', '<p>very much</p>', '<p>much</p>', '<p>too much</p>', '', 4, -1, -1, -1, -1, 0, '', '', '', '', '', 'A', '', 1592121649, 1592121649),
(531, 21, 23, 40, 0, '', '', '<p>I don’t care ______ expensive it is.</p>', '<p>how</p>', '<p>how much</p>', '<p>how many</p>', '<p>what</p>', '', 4, -1, -1, -1, -1, 0, '', '', '', '', '', 'A', '', 1592121697, 1592121697),
(532, 21, 23, 40, 0, '', '', '<p>Today is _________ colder than yesterday.</p>', '<p>very</p>', '<p>much</p>', '<p>too</p>', '<p>quite</p>', '', -1, 4, -1, -1, -1, 0, '', '', '', '', '', 'B', '', 1592121751, 1592121751),
(533, 21, 23, 40, 0, '', '', '<p>This book is _____ more interesting than his previous one.</p>', '<p>much</p>', '<p>quite</p>', '<p>vey</p>', '<p>too</p>', '', 4, -1, -1, -1, -1, 0, '', '', '', '', '', 'A', '<p>jauh lebih menarik</p>', 1592124022, 1592124022),
(534, 21, 23, 40, 0, '', '', '<p>_____ mathematician are you?</p>', '<p>How much of a</p>', '<p>How much a</p>', '<p>How much</p>', '<p>How many of a</p>', '', 4, -1, -1, -1, -1, 0, '', '', '', '', '', 'A', '<p>seberapa matematikawannya anda?</p>', 1592124092, 1592124092),
(535, 21, 23, 40, 0, '', '', '<p>I think she is ________  fool.</p>', '<p>A bit</p>', '<p>A bit of</p>', '<p>A bit of a</p>', '<p>Bit of a</p>', '', -1, -1, 4, -1, -1, 0, '', '', '', '', '', 'C', '<p>artinya agak-agak a fool</p>', 1592124208, 1592124208),
(536, 21, 23, 40, 0, '', '', '<p>_____ she was tired, she went to work.</p>', '<p>although</p>', '<p>but</p>', '<p>since</p>', '<p>for</p>', '<p>as</p>', 4, -1, -1, -1, -1, 0, '', '', '', '', '', 'A', '', 1592124252, 1592124252),
(537, 21, 23, 40, 0, '', '', '<p>The Vaal River is one of the major rivers in South Africa but the runoff is not constant which means that large dams have to be built ---- store water for use.</p>', '<p>notwithstanding</p>', '<p>regardless of</p>', '<p>so as to</p>', '<p>thanks to</p>', '<p>with the aim of</p>', -1, -1, 4, -1, -1, 0, '', '', '', '', '', 'C', '', 1592124304, 1592124304),
(538, 21, 23, 40, 0, '', '', '<p>Due to ---- a lack of production ---- increasing housing prices, Liverpool is now ranked as one of the least affordable cities countrywide.</p>', '<p>neither / nor</p>', '<p>no sooner / than</p>', '<p>scarcely / before</p>', '<p>both / and</p>', '<p>hardly / when</p>', -1, -1, -1, 4, -1, 0, '', '', '', '', '', 'D', '', 1592124338, 1592124338),
(539, 21, 23, 40, 0, '', '', '<p>Mike has been told he will have to pay the fine ---- his high rank in the military.</p>', '<p>even if</p>', '<p xss=removed>furthermore</p>', '<p>on grounds that</p>', '<p>despite</p>', '<p>on purpose that</p>', -1, -1, -1, 4, -1, 0, '', '', '', '', '', 'D', '', 1592124372, 1592124372),
(540, 21, 23, 40, 0, '', '', '<p>Some people believe vaccines overload our immune system, making it less able to react to other diseases ---- meningitis or AIDS, which are now threatening our health.</p>', '<p>but for</p>', '<p>lest</p>', '<p>with the aim of</p>', '<p>whereas</p>', '<p>such as</p>', -1, -1, -1, -1, 4, 0, '', '', '', '', '', 'E', '', 1592124412, 1592124412),
(541, 21, 23, 40, 0, '', '', '<p>Continued high-blood pressure is dangerous ---- it can increase the risk of heart disease and stroke.</p>', '<p>however</p>', '<p>so that</p>', '<p>as</p>', '<p>no matter although</p>', '<p>for instance</p>', -1, -1, 4, -1, -1, 0, '', '', '', '', '', 'C', '', 1592124450, 1592124450),
(542, 21, 23, 40, 0, '', '', '<p>They like to keep their old houses rather than building the new ones ---- it is very hard and expensive to maintain them.</p>', '<p>because</p>', '<p>even though</p>', '<p>on the contrary</p>', '<p>on account of</p>', '<p>for example</p>', -1, 4, -1, -1, -1, 0, '', '', '', '', '', 'B', '', 1592124484, 1592124484),
(543, 21, 23, 40, 0, '', '', '<p>The inhabitants of our village claim that pedestrians have no choice but to risk their lives crossing the dangerous road as there is ---- a pedestrian bridge ---- a crosswalk.</p>', '<p>not only / but also</p>', '<p>neither / nor</p>', '<p>neither / or</p>', '<p>no sooner / than</p>', '<p>hardly / when</p>', -1, 4, -1, -1, -1, 0, '', '', '', '', '', 'B', '', 1592124562, 1592124562),
(544, 21, 23, 40, 0, '', '', '<p> ---- the Oscar Reward, the Cannes Film Festival is the biggest event which takes place in May in the South of France.</p>', '<p>Except for</p>', '<p>Such as</p>', '<p>Lest</p>', '<p>Unless</p>', '<p>But</p>', 4, -1, -1, -1, -1, 0, '', '', '', '', '', 'A', '', 1592124596, 1592124596),
(545, 21, 23, 40, 0, '', '', '<p>______ vaccination has eliminated naturally occurring polio in North and South America, rare cases continue to occur in developing countries of Africa.</p>', '<p>However</p>', '<p>Although</p>', '<p>As a result of</p>', '<p>Hence</p>', '<p>Moreover</p>', -1, 4, -1, -1, -1, 0, '', '', '', '', '', 'B', '', 1592124633, 1592124633),
(546, 21, 23, 40, 0, '', '', '<p>The economy in China is booming; ____many foreign investors are planning to enter China\'s market.</p>', '<p xss=removed>nonetheless</p>', '<p>even if</p>', '<p>so as to</p>', '<p>in addition to</p>', '<p>that\'s why</p>', -1, -1, -1, -1, 4, 0, '', '', '', '', '', 'E', '', 1592124671, 1592124671),
(547, 21, 23, 40, 0, '', '', '<p>_____ you are a professional, you shouldn\'t attempt to clean or adjust the inner optics of your microscope.</p>', '<p>Providing</p>', '<p>As long as</p>', '<p>But for</p>', '<p>Unless</p>', '<p>Otherwise</p>', -1, -1, -1, 4, -1, 0, '', '', '', '', '', 'D', '', 1592124707, 1592124707),
(548, 21, 23, 40, 0, '', '', '<p>High blood pressure and diabetes are examples of diseases that should be aggressively treated ---- prevent severely disabling and potentially life threatening events such as a stroke.</p>', '<p>because of</p>', '<p>in that</p>', '<p>even though</p>', '<p>in view of</p>', '<p>in order to</p>', -1, -1, -1, -1, 4, 0, '', '', '', '', '', 'E', '', 1592124744, 1592124744),
(549, 21, 23, 40, 0, '', '', '<p>The main reason people smoke is ---- they are addicted to the nicotine in cigarettes.</p>', '<p>so</p>', '<p>because</p>', '<p>due to</p>', '<p>consequently</p>', '<p>regardless of</p>', -1, 4, -1, -1, -1, 0, '', '', '', '', '', 'B', '', 1592124781, 1592124781),
(550, 21, 23, 40, 0, '', '', '<p>Since the 1970s, psychoanalysis has become less popular; ----, some people still prefer this kind of treatment.</p>', '<p>however</p>', '<p>despite</p>', '<p>now that</p>', '<p>likewise</p>', '<p>so that</p>', 4, -1, -1, -1, -1, 0, '', '', '', '', '', 'A', '', 1592124812, 1592124812),
(551, 21, 23, 40, 0, '', '', '<p>You should encourage your child to eat all foods ---- he will get a well-balanced meal.</p>', '<p>nevertheless</p>', '<p>even though</p>', '<p>in case</p>', '<p>so that</p>', '<p>for instance</p>', -1, -1, -1, 4, -1, 0, '', '', '', '', '', 'D', '', 1592124846, 1592124846),
(552, 21, 23, 40, 0, '', '', '<p>____ AIDS has spread worldwide, southern Africa continues to be the worst affected area of the globe.</p>', '<p>Thanks to</p>', '<p>Much as</p>', '<p>In spite of</p>', '<p>Hence</p>', '<p>Therefore</p>', -1, 4, -1, -1, -1, 0, '', '', '', '', '', 'B', '', 1592124890, 1592124890),
(553, 21, 23, 40, 0, '', '', '<p>_______ long you have been smoking, your body begins to repair itself as soon as you quit</p>', '<p>Although</p>', '<p>But</p>', '<p>No matter how</p>', '<p>Therefore</p>', '<p>Because</p>', -1, -1, 4, -1, -1, 0, '', '', '', '', '', 'C', '', 1592124936, 1592124936),
(554, 21, 23, 40, 0, '', '', '<p>In countries, where natural disasters such as floods frequently happen, hospitals should determine which areas are safe and which could be evacuated _______ severe damage.</p>', '<p>in the event of</p>', '<p>much as</p>', '<p>in order that</p>', '<p>otherwise</p>', '<p>with the aim of</p>', 4, -1, -1, -1, -1, 0, '', '', '', '', '', 'A', '', 1592124979, 1592124979),
(555, 21, 23, 40, 0, '', '', '<p>The price of oil and oil products began dropping drastically at the beginning of 1998 ---- many famous, large oil companies are cutting back drastically.</p>', '<p>as a result of</p>', '<p>that\'s why</p>', '<p>for fear that</p>', '<p>in addition to</p>', '<p>so as to</p>', -1, 4, -1, -1, -1, 0, '', '', '', '', '', 'B', '', 1592125027, 1592125027),
(556, 21, 23, 40, 0, '', '', '<p>Canada\'s economy was greatly affected in the 1920s ____ the over-production of goods and by the over expansion of industries.</p>', '<p>as</p>', '<p>because of</p>', '<p>although</p>', '<p>since</p>', '<p>but for</p>', -1, 4, -1, -1, -1, 0, '', '', '', '', '', 'B', '<p>menjelaskan sebab akibat, jangan terkecih walau the over-production of goods and by the over expansion of industries cukup panjang tapi merupakan frase</p>', 1592126161, 1592126161),
(557, 21, 23, 40, 0, '', '', '<p>Sandy doesn\'t even know how to use a dictionary ---- she is trying to learn French.</p>', '<p>due to</p>', '<p>as for</p>', '<p>despite</p>', '<p>though</p>', '<p>now that</p>', -1, -1, -1, 4, -1, 0, '', '', '', '', '', 'D', '', 1592126230, 1592126230),
(558, 21, 23, 40, 0, '', '', '<p>When his mother died, the novelist John Lanchester discovered that ---- the name that he knew her by ---- her date of birth were false.</p>', '<p>both / and</p>', '<p>neither / or</p>', '<p>either / nor</p>', '<p>whether / or</p>', '<p>as / as</p>', 4, -1, -1, -1, -1, 0, '', '', '', '', '', 'A', '<p>both --- and untuk menjelaskan kedua duanya</p>', 1592126293, 1592126293),
(559, 21, 23, 40, 0, '', '', '<p>______ dramatic were electric and electronic inventions ---- they overshadowed advances in media for recording visual images and sound.</p>', '<p>So / that</p>', '<p>So / as</p>', '<p>As / as</p>', '<p>Either / or</p>', '<p>No sooner / than</p>', 4, -1, -1, -1, -1, 0, '', '', '', '', '', 'A', '', 1592126326, 1592126326),
(560, 21, 23, 40, 0, '', '', '<p>In some parts of the world the poor are going without food ---- the rich live In luxury.</p>', '<p>in case</p>', '<p>yet</p>', '<p>in order to</p>', '<p>despite</p>', '<p>otherwise</p>', -1, 4, -1, -1, -1, 0, '', '', '', '', '', 'B', '', 1592126368, 1592126368),
(561, 21, 23, 40, 0, '', '', '<p>______ the brain has been determined to be dead, it is possible to keep the heart and lungs operating by machine.</p>', '<p>Even if</p>', '<p>Even so</p>', '<p>Only if</p>', '<p>In spite of</p>', '<p>In case of</p>', 4, -1, -1, -1, -1, 0, '', '', '', '', '', 'A', '<p>bahkan jika...</p>', 1592126413, 1592126413),
(562, 21, 23, 40, 0, '', '', '<p>_____ similarities in purpose among all tribal dances, differences existed from culture to culture.</p>', '<p>Although</p>', '<p>In spite of</p>', '<p>Because</p>', '<p>Due to</p>', '<p>However</p>', -1, 4, -1, -1, -1, 0, '', '', '', '', '', 'B', '<p>similarities in purpose among all tribal dances itu berupa frase</p>', 1592126707, 1592126707),
(563, 21, 23, 40, 0, '', '', '<p>____ sand dunes are spectacular features of deserts, they\'re not as common or widespread as generally believed.</p>', '<p>Despite</p>', '<p>Although</p>', '<p>Since</p>', '<p>Contrary to</p>', '<p>Likewise</p>', -1, 4, -1, -1, -1, 0, '', '', '', '', '', 'B', '<p>sand dunes are spectacular features of deserts, merupakan clause</p>', 1592126767, 1592126767),
(564, 21, 23, 40, 0, '', '', '<p>The automotive industry is an indicator of a nation\'s economic health in_____ industrialized countries _____ the United States, the United Kingdom, and Japan.</p>', '<p>both / and</p>', '<p>neither / nor</p>', '<p>not only / but also</p>', '<p>such / as</p>', '<p>much / as</p>', -1, -1, -1, 4, -1, 0, '', '', '', '', '', 'D', '', 1592126813, 1592126813),
(565, 21, 23, 40, 0, '', '', '<p>______ next year, all US model building codes will incorporate ground motion hazard maps derived from the geological survey studies.</p>', '<p>As of</p>', '<p>Whereby</p>', '<p>As to</p>', '<p>Since</p>', '<p>By</p>', 4, -1, -1, -1, -1, 0, '', '', '', '', '', 'A', '', 1592126864, 1592126864),
(566, 21, 23, 40, 0, '', '', '<p>Jose is a brilliant student, especially ___ English.</p>', '<p>on</p>', '<p>in</p>', '<p>at</p>', '<p>to</p>', '', -1, 4, -1, -1, -1, 0, '', '', '', '', '', 'B', '', 1592126929, 1592126929),
(567, 21, 23, 40, 0, '', '', '<p>We are going a_____ vacation next weekend.</p>', '<p>on</p>', '<p>in</p>', '<p>at</p>', '<p>yo</p>', '', 4, -1, -1, -1, -1, 0, '', '', '', '', '', 'A', '', 1592126962, 1592126962),
(568, 21, 23, 40, 0, '', '', '<p>Bianca is going  ____ Rome this summer for a vacation. </p>', '<p>on</p>', '<p>in</p>', '<p>at</p>', '<p>to</p>', '', -1, -1, -1, 4, -1, 0, '', '', '', '', '', 'D', '', 1592127228, 1592127228),
(569, 21, 23, 40, 0, '', '', '<p>I was born a ___ Saturday. </p>', '<p>on</p>', '<p>in</p>', '<p>at</p>', '<p>to</p>', '', 4, -1, -1, -1, -1, 0, '', '', '', '', '', 'A', '', 1592127259, 1592127259),
(570, 21, 23, 40, 0, '', '', '<p>The train leaves ____ 5 o\'clock, hurry up!</p>', '<p>on</p>', '<p>in</p>', '<p>at</p>', '<p>to</p>', '', -1, -1, 4, -1, -1, 0, '', '', '', '', '', 'C', '', 1592127288, 1592127288),
(571, 21, 23, 40, 0, '', '', '<p>It never snows here even ____ winter time.</p>', '<p>on</p>', '<p>in</p>', '<p>at</p>', '<p>to</p>', '', -1, 4, -1, -1, -1, 0, '', '', '', '', '', 'B', '', 1592127336, 1592127336),
(572, 21, 23, 40, 0, '', '', '<p>Shreya has been living ____ India.</p>', '<p>on</p>', '<p>in</p>', '<p>at</p>', '<p>to</p>', '', -1, 4, -1, -1, -1, 0, '', '', '', '', '', 'B', '', 1592127364, 1592127364),
(573, 21, 23, 40, 0, '', '', '<p>Blue jacket sounds good __ me.</p>', '<p>on</p>', '<p>in</p>', '<p>at</p>', '<p>to</p>', '', -1, -1, -1, 4, -1, 0, '', '', '', '', '', 'D', '', 1592127395, 1592127395),
(574, 21, 23, 40, 0, '', '', '<p>I don\'t want to go out ____ night, it is too cold.</p>', '<p>on</p>', '<p>in</p>', '<p>at</p>', '<p>to</p>', '', -1, -1, 4, -1, -1, 0, '', '', '', '', '', 'C', '', 1592127432, 1592127432),
(575, 21, 23, 40, 0, '', '', '<p>The wheels ___ the bus go round and round.</p>', '<p>on</p>', '<p>in</p>', '<p>at</p>', '<p>to</p>', '', 4, -1, -1, -1, -1, 0, '', '', '', '', '', 'A', '', 1592127470, 1592127470),
(576, 21, 23, 40, 0, '', '', '<p>They live ---- Atlantic Avenue.</p>', '<p>at</p>', '<p>in</p>', '<p>on</p>', '<p>to</p>', '', -1, -1, 4, -1, -1, 0, '', '', '', '', '', 'C', '', 1592127503, 1592127503),
(577, 21, 23, 40, 0, '', '', '<p>Tokyo is the most crowded city ---- the world.</p>', '<p>on</p>', '<p>at</p>', '<p>in</p>', '<p>over</p>', '', -1, -1, 4, -1, -1, 0, '', '', '', '', '', 'C', '', 1592127535, 1592127535),
(578, 21, 23, 40, 0, '', '', '<p>Don\'t walk ---- the street! Walk here ---- the sidewalk.</p>', '<p>in / on</p>', '<p>on / at</p>', '<p>at / on</p>', '<p>in / to</p>', '', 4, -1, -1, -1, -1, 0, '', '', '', '', '', 'A', '', 1592127563, 1592127563),
(579, 21, 23, 40, 0, '', '', '<p>I\'m going to meet my friends, ---- Times Square tonight.</p>', '<p>on</p>', '<p>at</p>', '<p>in</p>', '<p>over</p>', '', -1, -1, 4, -1, -1, 0, '', '', '', '', '', 'C', '', 1592127599, 1592127599),
(580, 21, 23, 40, 0, '', '', '<p>He tried to open the tin ---- a knife.</p>', '<p>with</p>', '<p>by</p>', '<p>to</p>', '<p>of</p>', '', 4, -1, -1, -1, -1, 0, '', '', '', '', '', 'A', '', 1592127634, 1592127634),
(581, 21, 23, 40, 0, '', '', '<p>Mike is sitting ---- the desk ---- front of the door.</p>', '<p>at / in</p>', '<p>in / on</p>', '<p>on / on</p>', '<p>at / at</p>', '', 4, -1, -1, -1, -1, 0, '', '', '', '', '', 'A', '', 1592127664, 1592127664),
(582, 21, 23, 40, 0, '', '', '<p>Listen! I think there is someone ---- the front door. </p>', '<p>on</p>', '<p>at</p>', '<p>in</p>', '<p>with</p>', '', -1, 4, -1, -1, -1, 0, '', '', '', '', '', 'B', '', 1592127696, 1592127696),
(583, 21, 23, 40, 0, '', '', '<p>There\'s paper ---- the floor. Please put it ---- the wastebasket.</p>', '<p>at / into</p>', '<p>on / at</p>', '<p>on / in</p>', '<p>over / at</p>', '', -1, -1, 4, -1, -1, 0, '', '', '', '', '', 'C', '', 1592127724, 1592127724),
(584, 21, 23, 40, 0, '', '', '<p>See you ---- Monday morning.</p>', '<p>under</p>', '<p>at</p>', '<p>in</p>', '<p>on</p>', '', -1, -1, -1, 4, -1, 0, '', '', '', '', '', 'D', '', 1592127750, 1592127750),
(585, 21, 23, 40, 0, '', '', '<p>We are giving him a surprise party ---- his birthday.</p>', '<p>in</p>', '<p>at</p>', '<p>with</p>', '<p>on</p>', '', -1, -1, -1, 4, -1, 0, '', '', '', '', '', 'D', '', 1592127786, 1592127786),
(586, 21, 23, 40, 0, '', '', '<p xss=removed>A dictionary has information ---- words.</p>', '<p>to</p>', '<p>about</p>', '<p>in</p>', '<p>at</p>', '', -1, 4, -1, -1, -1, 0, '', '', '', '', '', 'B', '', 1592127821, 1592127821),
(587, 21, 23, 40, 0, '', '', '<p>You\'ll find the poem ---- page 16.</p>', '<p>at</p>', '<p>on</p>', '<p>in</p>', '<p>over</p>', '', -1, 4, -1, -1, -1, 0, '', '', '', '', '', 'B', '', 1592127856, 1592127856),
(588, 21, 23, 40, 0, '', '', '<p>I\'ll call you ___ seven o\'clock.</p>', '<p>in</p>', '<p>on</p>', '<p>at</p>', '<p>of</p>', '', -1, -1, 4, -1, -1, 0, '', '', '', '', '', 'C', '', 1592127897, 1592127897),
(589, 21, 23, 40, 0, '', '', '<p>We\'ll go ---- Caribbeans ---- June.</p>', '<p>- / in</p>', '<p>at / on</p>', '<p>to / on</p>', '<p>to / in</p>', '', -1, -1, -1, 4, -1, 0, '', '', '', '', '', 'D', '', 1592127944, 1592127944),
(590, 21, 23, 40, 0, '', '', '<p>I was born ---- September 9th.</p>', '<p>in</p>', '<p>on</p>', '<p>at</p>', '<p>of</p>', '', -1, 4, -1, -1, -1, 0, '', '', '', '', '', 'B', '', 1592128047, 1592128047),
(591, 21, 23, 40, 0, '975b7829ef1aecff47131ebde5a6a63d.png', 'image/png', '<p>In paragraph 1, why does the writer include information about the Cherokee language?</p>', '<p>To show how simple, traditional cultures can have complicated grammar structures</p>', '<p>To show how English grammar differs from Cherokee grammar</p>', '<p>To prove that complex grammar structures were invented by the Cherokees.</p>', '<p>To demonstrate how difficult it is to learn the Cherokee language</p>', '', 4, -1, -1, -1, -1, 0, '', '', '', '', '', 'A', '', 1592128213, 1592128213),
(592, 21, 23, 40, 0, '2857cd9543a34cfaa678842d9aed309b.png', 'image/png', '<p>What can be inferred about the slaves\' pidgin language?</p>', '<p>It contained complex grammar.</p>', '<p>It was based on many different languages.</p>', '<p>It was difficult to understand, even among slaves.</p>', '<p>It was created by the land-owners.</p>', '', -1, -1, 4, -1, -1, 0, '', '', '', '', '', 'C', '', 1592128272, 1592128272),
(593, 21, 23, 40, 0, '843f0b81768ecb14766e50921e8b593f.png', 'image/png', '<p>All the following sentences about Nicaraguan sign language are true EXCEPT:</p>', '<p>The language has been created since 1979.</p>', '<p>The language is based on speech and lip reading.</p>', '<p>The language incorporates signs which children used at home.</p>', '<p>The language was perfected by younger children.</p>', '', -1, 4, -1, -1, -1, 0, '', '', '', '', '', 'B', '', 1592128333, 1592128333),
(594, 21, 23, 40, 0, '4201893110a412fa347c569d936305e8.png', 'image/png', '<p>\'From scratch\' in paragraph 2 is closest in meaning to:</p>', '<p>from the very beginning</p>', '<p>in simple cultures</p>', '<p>by copying something else</p>', '<p>by using written information</p>', '', 4, -1, -1, -1, -1, 0, '', '', '', '', '', 'A', '', 1592128472, 1592128472),
(595, 21, 23, 40, 0, 'ed9a91e19a33ea347c900fe69a6a38be.png', 'image/png', '<p>\'Make-shift\' in paragraph 3 is closest in meaning to:</p>', '<p>complicated and expressive</p>', '<p>simple and temporary</p>', '<p>extensive and diverse</p>', '<p>private and personal</p>', '', -1, 4, -1, -1, -1, 0, '', '', '', '', '', 'B', '', 1592128523, 1592128523),
(596, 21, 23, 40, 0, '', '', '<p>Which sentence is closest in meaning to the highlighted sentence? \"Grammar is universal and plays a part in every language, no matter how widespread it is.\"</p>', '<p>All languages, whether they are spoken by a few people or a lot of people, contain grammar.</p>', '<p>Some languages include a lot of grammar, whereas other languages contain a little.</p>', '<p>Languages which contain a lot of grammar are more common that languages that contain a little.</p>', '<p>The grammar of all languages is the same, no matter where the languages evolved.</p>', '', 4, -1, -1, -1, -1, 0, '', '', '', '', '', 'A', '', 1592128568, 1592128568),
(597, 21, 23, 40, 0, '', '', '<p>All of the following are features of the new Nicaraguan sign language EXCEPT:</p>', '<p>All children used the same gestures to show meaning.</p>', '<p>The meaning was clearer than the previous sign language.</p>', '<p>The hand movements were smoother and smaller.</p>', '<p>New gestures were created for everyday objects and activities.</p>', '', -1, -1, -1, 4, -1, 0, '', '', '', '', '', 'D', '', 1592128615, 1592128615),
(598, 21, 23, 40, 0, '17889c392b312618d3134ea48a2f01e3.png', 'image/png', '<p>Which idea is presented in the final paragraph?</p>', '<p>English was probably once a creole.</p>', '<p>The English past tense system is inaccurate.</p>', '<p>Linguists have proven that English was created by children.</p>', '<p>Children say English past tenses differently from adults.</p>', '', 4, -1, -1, -1, -1, 0, '', '', '', '', '', 'A', '', 1592128660, 1592128660),
(599, 21, 23, 40, 0, '6f662c17a79d663f38bced254d02695a.png', 'image/png', '<p>Look at the word \'consistent\' in paragraph 4. This word could best be replaced by which of the following?</p>', '<p>natural</p>', '<p>predictable</p>', '<p>imaginable</p>', '<p>uniform</p>', '', -1, -1, -1, 4, -1, 0, '', '', '', '', '', 'D', '', 1592128700, 1592128700),
(600, 21, 23, 40, 0, 'b23eacba9f681f1b7c780797b7da9afa.png', 'image/png', '<p>The opposite of deaf in paragraph 4 sentence 1 is...</p>', '<p>heedful</p>', '<p>Supersonic</p>', '<p>Resposive</p>', '<p>unhearing</p>', '', 4, -1, -1, -1, -1, 0, '', '', '', '', '', 'A', '', 1592128759, 1592128759),
(601, 21, 23, 40, 0, '5baa2b466ab2a4fc967300a6171728c2.png', 'image/png', '<p>the term \"Creole\" in the firs sentence of the last paraghraph means</p>', '<p>a Non-native-born speaker</p>', '<p>The person whose job is learning language</p>', '<p>a term used to mean those who were \"native-born\"</p>', '<p>People whol love language</p>', '', -1, -1, 4, -1, -1, 0, '', '', '', '', '', 'C', '', 1592128804, 1592128804),
(602, 21, 23, 40, 0, '500780673624ba1753f6e123f2874838.png', 'image/png', '<p>the word innate in paragraph 5 has the closest meaning to _____</p>', '<p>natural</p>', '<p>modern</p>', '<p>expert</p>', '<p>sophisticated</p>', '', 4, -1, -1, -1, -1, 0, '', '', '', '', '', 'A', '', 1592128957, 1592128957),
(603, 20, 18, 39, 0, '73d1976d88f87dc753b8634a2c87ded7.jpg', 'image/jpeg', '<p>Gambar manakah yang merupakan pencerminan dari gambar pada soal?</p>', '<p>A</p>', '<p>B</p>', '<p>C</p>', '<p>D</p>', '<p>E</p>', -1, -1, 4, -1, -1, 0, '', '', '', '', '', 'C', '', 1592174069, 1592176390),
(604, 20, 18, 39, 0, '2620d8718a997bf471e34a7a2c7363d3.jpg', 'image/jpeg', '<p>Gambar manakah yang merupakan pencerminan dari gambar pada soal?</p>', '<p>A</p>', '<p>B</p>', '<p>C</p>', '<p>D</p>', '<p>E</p>', 4, -1, -1, -1, -1, 0, '', '', '', '', '', 'A', '', 1592174802, 1592176656),
(605, 20, 18, 39, 0, 'bc84487a1863c73c91cff5f6c1c2e12a.jpg', 'image/jpeg', '<p>Gambar manakah yang merupakan pencerminan dari gambar pada soal?</p>', '<p>A</p>', '<p>B</p>', '<p>C</p>', '<p>D</p>', '<p>E</p>', -1, -1, -1, 4, -1, 0, '', '', '', '', '', 'D', '', 1592176761, 1592176761),
(606, 20, 18, 39, 0, 'a4780a255f90e6a67bffaee00816b9f1.jpg', 'image/jpeg', '<p>Gambar manakah yang merupakan pencerminan dari gambar pada soal?</p>', '<p>A</p>', '<p>B</p>', '<p>C</p>', '<p>D</p>', '<p>E</p>', 4, -1, -1, -1, -1, 0, '', '', '', '', '', 'A', '', 1592176869, 1592176869),
(607, 20, 18, 39, 0, '753f1e0f969fa0ab407032b872efce46.jpg', 'image/jpeg', '<p>Gambar manakah yang merupakan pencerminan dari gambar pada soal?</p>', '<p>A</p>', '<p>B</p>', '<p>C</p>', '<p>D</p>', '<p>E</p>', -1, -1, -1, 4, -1, 0, '', '', '', '', '', 'D', '', 1592176953, 1592176953),
(608, 20, 18, 39, 0, '8e9bf23b9b9840744daa1f205b606c0f.jpg', 'image/jpeg', '<p>Gambar manakah yang merupakan pencerminan dari gambar pada soal?</p>', '<p>A</p>', '<p>B</p>', '<p>C</p>', '<p>D</p>', '<p>E</p>', 4, -1, -1, -1, -1, 0, '', '', '', '', '', 'A', '', 1592177088, 1592177088),
(609, 20, 18, 39, 0, '68757f37c245f64340a28f406cf6364c.jpg', 'image/jpeg', '<p>Gambar manakah yang merupakan pencerminan dari gambar pada soal?</p>', '<p>A</p>', '<p>B</p>', '<p>C</p>', '<p>D</p>', '<p>E</p>', -1, -1, -1, 4, -1, 0, '', '', '', '', '', 'D', '', 1592177138, 1592177138),
(610, 20, 18, 39, 0, '6ee922c808f2bdc6599fe42caa1aa91b.jpg', 'image/jpeg', '<p>Gambar manakah yang merupakan pencerminan dari gambar pada soal?</p>', '<p>A</p>', '<p>B</p>', '<p>C</p>', '<p>D</p>', '<p>E</p>', 4, -1, -1, -1, -1, 0, '', '', '', '', '', 'A', '', 1592177170, 1592177170),
(611, 20, 18, 39, 0, 'd24c3543cd95a136dfe7334a194f7022.jpg', 'image/jpeg', '<p>Gambar manakah yang merupakan pencerminan dari gambar pada soal?</p>', '<p>A</p>', '<p>B</p>', '<p>C</p>', '<p>D</p>', '<p>E</p>', -1, -1, -1, 4, -1, 0, '', '', '', '', '', 'D', '', 1592177213, 1592177213),
(612, 20, 18, 39, 0, 'e9f2f1730d2cb26cea6ca692432f7b95.jpg', 'image/jpeg', '<p>Gambar manakah yang merupakan pencerminan dari gambar pada soal?</p>', '<p>A</p>', '<p>B</p>', '<p>C</p>', '<p>D</p>', '<p>E</p>', 4, -1, -1, -1, -1, 0, '', '', '', '', '', 'A', '', 1592177251, 1592177251),
(613, 20, 18, 39, 0, '438fcbaf8cb91d8d1f0ce725ec9f813d.jpg', 'image/jpeg', '<p>Gambar manakah yang merupakan pencerminan dari gambar pada soal?</p>', '<p>A</p>', '<p>B</p>', '<p>C</p>', '<p>D</p>', '<p>E</p>', -1, 4, -1, -1, -1, 0, '', '', '', '', '', 'B', '', 1592177322, 1592177322),
(614, 20, 18, 39, 0, 'fbfa68cd0cde9ce30dae4d08bd4f11c1.jpg', 'image/jpeg', '<p>Gambar manakah yang merupakan pencerminan dari gambar pada soal?</p>', '<p>A</p>', '<p>B</p>', '<p>C</p>', '<p>D</p>', '<p>E</p>', 4, -1, -1, -1, -1, 0, '', '', '', '', '', 'A', '', 1592177362, 1592177362),
(615, 20, 18, 39, 0, '8ef6201a7e89566dc09f65e4fb5bd688.jpg', 'image/jpeg', '<p>Gambar manakah yang merupakan pencerminan dari gambar pada soal?</p>', '<p>A</p>', '<p>B</p>', '<p>C</p>', '<p>D</p>', '<p>E</p>', -1, 4, -1, -1, -1, 0, '', '', '', '', '', 'B', '', 1592177413, 1592177413),
(616, 20, 18, 39, 0, 'aee27043f85ea0de91db4e0ee84be11c.jpg', 'image/jpeg', '<p>Gambar manakah yang merupakan pencerminan dari gambar pada soal?</p>', '<p>A</p>', '<p>B</p>', '<p>C</p>', '<p>D</p>', '<p>E</p>', -1, -1, -1, -1, 4, 0, '', '', '', '', '', 'E', '', 1592177469, 1592177469),
(617, 20, 18, 39, 0, '02930bdedf2003af1f6542a49dd9f7bb.jpg', 'image/jpeg', '<p>Gambar manakah yang merupakan pencerminan dari gambar pada soal?</p>', '<p>A</p>', '<p>B</p>', '<p>C</p>', '<p>D</p>', '<p>E</p>', -1, -1, 4, -1, -1, 0, '', '', '', '', '', 'C', '', 1592177510, 1592177510),
(618, 20, 18, 39, 0, '49a0a61fafd5addf86a88a29cdc65d21.jpg', 'image/jpeg', '<p>Gambar manakah yang merupakan ROTASI dari gambar pada soal?</p>', '<p>A</p>', '<p>B</p>', '<p>C</p>', '<p>D</p>', '<p>E</p>', 4, -1, -1, -1, -1, 0, '', '', '', '', '', 'A', '', 1592177585, 1592177585),
(619, 20, 18, 39, 0, '1b9e14bf12097adca5457d4717c1878b.jpg', 'image/jpeg', '<p>Gambar manakah yang merupakan ROTASI dari gambar pada soal?</p>', '<p>A</p>', '<p>B</p>', '<p>C</p>', '<p>D</p>', '<p>E</p>', -1, -1, -1, -1, 4, 0, '', '', '', '', '', 'E', '', 1592177629, 1592177629),
(620, 20, 18, 39, 0, '9fa2619c65a2fbcc10066d2eb2e8d169.jpg', 'image/jpeg', '<p>Gambar manakah yang merupakan ROTASI dari gambar pada soal?</p>', '<p>A</p>', '<p>B</p>', '<p>C</p>', '<p>D</p>', '<p>E</p>', -1, 4, -1, -1, -1, 0, '', '', '', '', '', 'B', '', 1592177668, 1592177668),
(621, 20, 18, 39, 0, 'dd02b8581a964db854f8f7acd8c2aac2.jpg', 'image/jpeg', '<p>Gambar manakah yang merupakan ROTASI dari gambar pada soal?</p>', '<p>A</p>', '<p>B</p>', '<p>C</p>', '<p>D</p>', '<p>E</p>', 4, -1, -1, -1, -1, 0, '', '', '', '', '', 'A', '', 1592177764, 1592177764),
(622, 20, 18, 39, 0, '7e7d0f9c7e0777af9eab5aaa733d3315.jpg', 'image/jpeg', '<p>Gambar manakah yang merupakan ROTASI dari gambar pada soal?</p>', '<p>A</p>', '<p>B</p>', '<p>C</p>', '<p>D</p>', '<p>E</p>', -1, 4, -1, -1, -1, 0, '', '', '', '', '', 'B', '', 1592177852, 1592177852),
(623, 20, 18, 39, 0, 'dfbd6ce707ecf27fbec5d1b8bc00f090.jpg', 'image/jpeg', '<p>Gambar manakah yang merupakan ROTASI dari gambar pada soal?</p>', '<p>A</p>', '<p>B</p>', '<p>C</p>', '<p>D</p>', '<p>E</p>', -1, -1, -1, -1, 4, 0, '', '', '', '', '', 'E', '', 1592178002, 1592178002),
(624, 20, 18, 39, 0, '7dcde13c6b1f67007eff8a20267d2348.jpg', 'image/jpeg', '<p>Gambar manakah yang merupakan ROTASI dari gambar pada soal?</p>', '<p>A</p>', '<p>B</p>', '<p>C</p>', '<p>D</p>', '<p>E</p>', -1, -1, 4, -1, -1, 0, '', '', '', '', '', 'C', '', 1592178058, 1592178058),
(625, 20, 18, 39, 0, '536b5253f5cb6b800e7db7ed6401d0c3.jpg', 'image/jpeg', '<p>Gambar manakah yang merupakan ROTASI dari gambar pada soal?</p>', '<p>A</p>', '<p>B</p>', '<p>C</p>', '<p>D</p>', '<p>E</p>', -1, 4, -1, -1, -1, 0, '', '', '', '', '', 'B', '', 1592178140, 1592178140),
(626, 20, 18, 39, 0, '838b02681660dacf76ef67d587ad2412.jpg', 'image/jpeg', '<p>Gambar manakah yang merupakan ROTASI dari gambar pada soal?</p>', '<p>A</p>', '<p>B</p>', '<p>C</p>', '<p>D</p>', '<p>E</p>', -1, -1, -1, 4, -1, 0, '', '', '', '', '', 'D', '', 1592178200, 1592178200),
(627, 20, 18, 39, 0, '35771112d6302bc6d4c10b8664aaddd6.jpg', 'image/jpeg', '<p>Gambar manakah yang merupakan ROTASI dari gambar pada soal?</p>', '<p>A</p>', '<p>B</p>', '<p>C</p>', '<p>D</p>', '<p>E</p>', -1, 4, -1, -1, -1, 0, '', '', '', '', '', 'B', '', 1592178240, 1592178240),
(628, 20, 18, 39, 0, 'a798eef8867c76a2a411aa348f5d64e5.jpg', 'image/jpeg', '<p>Gambar manakah yang merupakan ROTASI dari gambar pada soal?</p>', '<p>A</p>', '<p>B</p>', '<p>C</p>', '<p>D</p>', '<p>E</p>', 4, -1, -1, -1, -1, 0, '', '', '', '', '', 'A', '', 1592178306, 1592178306),
(629, 20, 18, 39, 0, '287f901402279c6034ad188fcbae709e.jpg', 'image/jpeg', '<p>Gambar manakah yang merupakan ROTASI dari gambar pada soal?</p>', '<p>A</p>', '<p>B</p>', '<p>C</p>', '<p>D</p>', '<p>E</p>', 4, -1, -1, -1, -1, 0, '', '', '', '', '', 'A', '', 1592178345, 1592178345),
(630, 20, 18, 39, 0, '176cee981605680d67c6b283dd5f2d76.jpg', 'image/jpeg', '<p>Gambar manakah yang merupakan ROTASI dari gambar pada soal?</p>', '<p>A</p>', '<p>B</p>', '<p>C</p>', '<p>D</p>', '<p>E</p>', -1, -1, -1, -1, 4, 0, '', '', '', '', '', 'E', '', 1592178382, 1592178382),
(631, 20, 18, 39, 0, '3acc388a7b16aa85d1a5d41817b3d821.jpg', 'image/jpeg', '<p>Gambar manakah yang merupakan ROTASI dari gambar pada soal?</p>', '<p>A</p>', '<p>B</p>', '<p>C</p>', '<p>D</p>', '<p>E</p>', -1, -1, -1, -1, 4, 0, '', '', '', '', '', 'E', '', 1592178414, 1592178414),
(632, 20, 18, 39, 0, '30c24cd03a75fd2c7ecdcc6c46e1d302.jpg', 'image/jpeg', '<p>Gambar manakah yang merupakan ROTASI dari gambar pada soal?</p>', '<p>A</p>', '<p>B</p>', '<p>C</p>', '<p>D</p>', '<p>E</p>', -1, -1, -1, 4, -1, 0, '', '', '', '', '', 'D', '', 1592178457, 1592178457),
(633, 20, 18, 39, 0, 'b874f77ff0cb2b0d569bcc6c86cadeba.jpg', 'image/jpeg', '', '<p>A</p>', '<p>B</p>', '<p>C</p>', '<p>D</p>', '<p>E</p>', 4, -1, -1, -1, -1, 0, '', '', '', '', '', 'A', '', 1592178527, 1592178527),
(634, 20, 18, 39, 0, '167709597d7ed0c7f4da4cef1e224442.jpg', 'image/jpeg', '', '<p>A</p>', '<p>B</p>', '<p>C</p>', '<p>D</p>', '<p>E</p>', -1, 4, -1, -1, -1, 0, '', '', '', '', '', 'B', '', 1592178581, 1592178581),
(635, 20, 18, 39, 0, '53b79e73318caa96194807c445ec58e4.jpg', 'image/jpeg', '', '<p>A</p>', '<p>B</p>', '<p>C</p>', '<p>D</p>', '<p>E</p>', 4, -1, -1, -1, -1, 0, '', '', '', '', '', 'A', '', 1592178615, 1592178615),
(636, 20, 18, 39, 0, '233b555642741943a180d8663886b7ee.jpg', 'image/jpeg', '', '<p>A</p>', '<p>B</p>', '<p>C</p>', '<p>D</p>', '<p>E</p>', -1, -1, -1, -1, 4, 0, '', '', '', '', '', 'E', '', 1592178656, 1592178656),
(637, 20, 18, 39, 0, '020c17bcd439f253e30e4d37fe4bd698.jpg', 'image/jpeg', '', '<p>A</p>', '<p>B</p>', '<p>C</p>', '<p>D</p>', '<p>E</p>', -1, -1, 4, -1, -1, 0, '', '', '', '', '', 'C', '', 1592178693, 1592178693),
(638, 20, 18, 39, 0, '2d1b1ec1cf8ac6564634648c83411f7c.jpg', 'image/jpeg', '', '<p>A</p>', '<p>B</p>', '<p>C</p>', '<p>D</p>', '<p>E</p>', -1, -1, 4, -1, -1, 0, '', '', '', '', '', 'C', '', 1592178755, 1592178755),
(639, 20, 18, 39, 0, 'd839bc6bc015add148bd6803b9134d84.jpg', 'image/jpeg', '', '<p>A</p>', '<p>B</p>', '<p>C</p>', '<p>D</p>', '<p>E</p>', -1, -1, -1, -1, 4, 0, '', '', '', '', '', 'E', '', 1592178801, 1592178801),
(640, 20, 18, 39, 0, '07dbd452b89394b3c4d2d8b4ef7fb0f3.jpg', 'image/jpeg', '', '<p>A</p>', '<p>B</p>', '<p>C</p>', '<p>D</p>', '<p>E</p>', -1, -1, -1, 4, -1, 0, '', '', '', '', '', 'D', '', 1592178843, 1592178843),
(641, 20, 18, 39, 0, 'b165b5cc94ac3f3fdd80029d706e10e0.jpg', 'image/jpeg', '', '<p>A</p>', '<p>B</p>', '<p>C</p>', '<p>D</p>', '<p>E</p>', -1, -1, -1, -1, 4, 0, '', '', '', '', '', 'E', '', 1592178894, 1592178894),
(642, 20, 18, 39, 0, '3f2f767e6d1cd437807f5b90d2240b95.jpg', 'image/jpeg', '', '<p>A</p>', '<p>B</p>', '<p>C</p>', '<p>D</p>', '<p>E</p>', -1, 4, -1, -1, -1, 0, '', '', '', '', '', 'B', '', 1592178939, 1592178939),
(643, 20, 18, 39, 0, 'fb42f2adab117475d6eb41a0458d6d25.jpg', 'image/jpeg', '', '<p>A</p>', '<p>B</p>', '<p>C</p>', '<p>D</p>', '<p>E</p>', -1, 4, -1, -1, -1, 0, '', '', '', '', '', 'B', '', 1592178995, 1592178995),
(644, 20, 18, 39, 0, 'ca4bc7a9308a21098123a3053f814522.jpg', 'image/jpeg', '', '<p>A</p>', '<p>B</p>', '<p>C</p>', '<p>D</p>', '<p>E</p>', -1, -1, -1, -1, 4, 0, '', '', '', '', '', 'E', '', 1592179041, 1592179041),
(645, 20, 18, 39, 0, '3e6ca305a51aafa3bd4f985a0d4948cc.jpg', 'image/jpeg', '', '<p>A</p>', '<p>B</p>', '<p>C</p>', '<p>D</p>', '<p>E</p>', -1, -1, 4, -1, -1, 0, '', '', '', '', '', 'C', '', 1592179096, 1592179144),
(646, 20, 18, 39, 0, '8c399d81d007aba287172447a7eb826f.jpg', 'image/jpeg', '', '<p>A</p>', '<p>B</p>', '<p>C</p>', '<p>D</p>', '<p>E</p>', -1, -1, 4, -1, -1, 0, '', '', '', '', '', 'C', '', 1592179190, 1592179190),
(647, 20, 18, 39, 0, 'd4e4e7909eb82e848979dd8aa03e7af6.jpg', 'image/jpeg', '', '<p>A</p>', '<p>B</p>', '<p>C</p>', '<p>D</p>', '<p>E</p>', -1, -1, -1, 4, -1, 0, '', '', '', '', '', 'D', '', 1592179241, 1592179241),
(648, 20, 18, 39, 0, '0be936e89c6e3e20d6046eae920148bf.jpg', 'image/jpeg', '', '<p>A</p>', '<p>B</p>', '<p>C</p>', '<p>D</p>', '<p>E</p>', -1, -1, 4, -1, -1, 0, '', '', '', '', '', 'C', '<p>Gambar ke tiga adalah hasil penggabungan bidang yang besar pada gambar pertama dimasukkan ke dalam bidan yang kecil pada gambar kedua. Sehingga untuk selanjutnya adalah segilima dimasukkan kedalam segitiga</p>', 1592195262, 1592255559),
(649, 20, 18, 39, 0, '467a9e2a1113869c5fb29be2911e873f.jpg', 'image/jpeg', '', '<p>A</p>', '<p>B</p>', '<p>C</p>', '<p>D</p>', '<p>E</p>', -1, -1, 4, -1, -1, 0, '', '', '', '', '', 'C', '<p>Perhatikan bentuk rambut. Setiap baris makan akan terdapat bentuk rambut yang berbeda-beda. Selain itu, perhatikan juga pola corak raster bagian mulut. Setiap barismemiliki 3 macam gambar yang berbeda, sedangkan bagian mata sama</p>', 1592195577, 1592255669),
(650, 20, 18, 39, 0, '3c210edcbaa8b6157a070dc553ace281.jpg', 'image/jpeg', '', '<p>A</p>', '<p>B</p>', '<p>C</p>', '<p>D</p>', '<p>E</p>', -1, -1, -1, 4, -1, 0, '', '', '', '', '', 'D', '<p>Perhatikan gambar yang belum ada dan kombinasi bentuk dan arsiran pada masing-masing gambar</p>', 1592195623, 1592255734),
(651, 20, 18, 39, 0, '0e725a15959d5964a99bc4aa67a69ccd.jpg', 'image/jpeg', '', '<p>A</p>', '<p>B</p>', '<p>C</p>', '<p>D</p>', '<p>E</p>', 4, -1, -1, -1, -1, 0, '', '', '', '', '', 'A', '<p>Polanya gambar pertama jika dikurangi gambar kecua hasilnya adalah gambar ketiga</p>', 1592195668, 1592255783),
(652, 20, 18, 39, 0, '32c149528935b00341896cda0a5b7aac.jpg', 'image/jpeg', '', '<p>A</p>', '<p>B</p>', '<p>C</p>', '<p>D</p>', '<p>E</p>', -1, 4, -1, -1, -1, 0, '', '', '', '', '', 'B', '<p>Polanya gambar tersebu dirotasi 90<sup>o</sup> berlawanana arah jarum jam</p>', 1592195759, 1592255849),
(653, 20, 18, 39, 0, 'ff0ad3d847709745badde503b7f190fc.jpg', 'image/jpeg', '', '<p>A</p>', '<p>B</p>', '<p>C</p>', '<p>D</p>', '<p>E</p>', -1, -1, -1, 4, -1, 0, '', '', '', '', '', 'D', '<p>Polanya adalah penambahan jumlah belah ketupat. untuk baris pertama terdapat pola (1,2,3). Baris kedua terdapat pola (3,4,5) sehingga untuk bari ketiga harusnya berpola (5,6,7)</p>', 1592195985, 1592195985),
(654, 20, 18, 39, 0, '9ab27ad8fb197dfb506f9f7ff3465a36.jpg', 'image/jpeg', '', '<p>A</p>', '<p>B</p>', '<p>C</p>', '<p>D</p>', '<p>E</p>', -1, 4, -1, -1, -1, 0, '', '', '', '', '', 'B', '<p>Polanya jika gambar pertama ditambah dengan gambar kedua maka akan menjadi gambar ketiga</p>', 1592196119, 1592196119),
(655, 20, 18, 39, 0, 'bf582ed3da2d413faff2274cb42857cb.jpg', 'image/jpeg', '', '<p>A</p>', '<p>B</p>', '<p>C</p>', '<p>D</p>', '<p>E</p>', 4, -1, -1, -1, -1, 0, '', '', '', '', '', 'A', '<p>Perhatikan bentuk bangunnya. Bangun-bangun tersebut terdiri dari segienam, persegi, dan trapesium. selain itu, perhatikan jumlah lingkaran kecil yang berada di bawah bangun. jawaban yang sesuai untuk gambar selanjutnya adalah segienam dengan 1 lingkaran kecil di bawahnya</p>', 1592196271, 1592196271),
(656, 20, 18, 39, 0, '44217ca45f481fc24ca8a117e8f240a8.jpg', 'image/jpeg', '', '<p>A</p>', '<p>B</p>', '<p>C</p>', '<p>D</p>', '<p>E</p>', -1, -1, -1, 4, -1, 0, '', '', '', '', '', 'D', '<p>Peratikan corak gambar tersebut. Pada setiap baris, terdapat empat corak, dimana ada dua pola arsiran, satu putih polos, dan yang lainnya hitam</p>', 1592196617, 1592196617),
(657, 20, 18, 39, 0, 'aca9bfa48e38f31409208a43d57d27c2.jpg', 'image/jpeg', '', '<p>A</p>', '<p>B</p>', '<p>C</p>', '<p>D</p>', '<p>E</p>', 4, -1, -1, -1, -1, 0, '', '', '', '', '', 'A', '<p>Setiap baris memuat garis dengan posisi mendatar tegak dan miring. sehingga jawaban untuk garis selanjutnya adalah miring, kemudian perhatikan ujung dan pangkal garis. maka kita akan mendapatkan jawaban A</p>', 1592196760, 1592196760),
(658, 20, 18, 39, 0, '18b154024c0fa99141769d42558be3c4.jpg', 'image/jpeg', '', '<p>A</p>', '<p>B</p>', '<p>C</p>', '<p>D</p>', '<p>E</p>', -1, 4, -1, -1, -1, 0, '', '', '', '', '', 'B', '<p>Perhatikan gerak rotasinya. gambar diputar berlawanana arah jarum jama 90<sup>o</sup>. sehingga jawaban sesuai gambar sebelumnya adalah B</p>', 1592196903, 1592196903);
INSERT INTO `tbl_soal_tryout` (`id_soal`, `dosen_id`, `matkul_id`, `id_soal_paket`, `bobot`, `file`, `tipe_file`, `soal`, `opsi_a`, `opsi_b`, `opsi_c`, `opsi_d`, `opsi_e`, `bobot_a`, `bobot_b`, `bobot_c`, `bobot_d`, `bobot_e`, `bobot_kosong`, `file_a`, `file_b`, `file_c`, `file_d`, `file_e`, `jawaban`, `pembahasan`, `created_on`, `updated_on`) VALUES
(659, 20, 18, 39, 0, 'cd47568a3c49943f2e05cf446e876d3a.jpg', 'image/jpeg', '', '<p>A</p>', '<p>B</p>', '<p>C</p>', '<p>D</p>', '<p>E</p>', 4, -1, -1, -1, -1, 0, '', '', '', '', '', 'A', '<p>Polanya setiap baris selalu ada gambar polos putih. sedangkan gambar yang lainnya berselisih 2 garis</p>', 1592197014, 1592197014),
(660, 20, 18, 39, 0, 'a7f465ca9f65305c0ecf91b64125fd39.jpg', 'image/jpeg', '', '<p>A</p>', '<p>B</p>', '<p>C</p>', '<p>D</p>', '<p>E</p>', -1, -1, -1, -1, 4, 0, '', '', '', '', '', 'E', '<p>Masing-masing baris memiliki jumlah pagar tegak maupun mendatar 1, 2, dan 3 buah. sesuai dengan dua gambar sebelumnya, maka gambar yang ketiga kelanjutannya adalah E</p>', 1592245306, 1592245306),
(661, 20, 18, 39, 0, 'b0500f3b735b0b9d085cd0c07f86ad86.jpg', 'image/jpeg', '', '<p>A</p>', '<p>B</p>', '<p>C</p>', '<p>D</p>', '<p>E</p>', -1, 4, -1, -1, -1, 0, '', '', '', '', '', 'B', '<p>Polanya merupakan rotai gambar. gambar pertama dirotasi 90<sup>o</sup>, kemudian gambar selanjutnya dirotasi lagi 90<sup>o</sup></p>', 1592245541, 1592245541),
(662, 20, 18, 39, 0, 'b45fdb56312ae11c058e6feb389ca271.jpg', 'image/jpeg', '', '<p>A</p>', '<p>B</p>', '<p>C</p>', '<p>D</p>', '<p>E</p>', -1, -1, -1, -1, 4, 0, '', '', '', '', '', 'E', '<p>Perhatikan urutan banyaknya bintang berarsir. Setiap baris, jumlah banyaknya bintang berarsir adalah berurutan. sehingga untuk gambar selanjutnya pada baris ketiga adalah memiliki 5 arsiran bintang</p>', 1592245737, 1592245737),
(663, 20, 18, 39, 0, '23bf173b61ac0b0aa1c8a43459644f47.jpg', 'image/jpeg', '', '<p>A</p>', '<p>B</p>', '<p>C</p>', '<p>D</p>', '<p>E</p>', -1, 4, -1, -1, -1, 0, '', '', '', '', '', 'B', '<p>Polanya lingkaran dan belah ketupat sama-sama bergerak searah jarum jam. sehingga kita akan mendapatkan jawaban B</p>', 1592246158, 1592246158),
(664, 20, 18, 39, 0, '893a1cbddad69fce5414470806c41207.jpg', 'image/jpeg', '', '<p>A</p>', '<p>B</p>', '<p>C</p>', '<p>D</p>', '<p>E</p>', -1, -1, 4, -1, -1, 0, '', '', '', '', '', 'C', '<p>Perhatikan daerah yang diarsir, akan bergerak berlawanan arah jarum jam. dari bangun oertama ke bangun kedua polanya teratur. dari bangun pertama ke bangun kedua melompati satu bagian, selanjutnya 2, 3, dan seterusnya. dimana setiap perputaran maka bagian yang akan dilompati bertambah satu. maka kita akan mendapatkan jawaban C</p>', 1592246379, 1592246379),
(665, 20, 18, 39, 0, '5631a2af359283f49ffc0f5da554a2cc.jpg', 'image/jpeg', '', '<p>A</p>', '<p>B</p>', '<p>C</p>', '<p>D</p>', '<p>E</p>', -1, 4, -1, -1, -1, 0, '', '', '', '', '', 'B', '<p>Polanya daerah yang diarsir bergerak berlawanan arah jarum jam, sedangkan lingkaran bergerak searah jarum jam. Maka kita akan mendapatkan jawaban B</p>', 1592246489, 1592246489),
(666, 20, 18, 39, 0, 'cd8ed9ebc4d00b90cba53725f2e9bcc4.jpg', 'image/jpeg', '', '<p>A</p>', '<p>B</p>', '<p>C</p>', '<p>D</p>', '<p>E</p>', 4, -1, -1, -1, -1, 0, '', '', '', '', '', 'A', '<p>Pada dasarnya gambar diputar 90o searah jarum jam. selain itu, daerah yang diarsir akan mengalami pertukaran setiap gambar berputar. melihat polanya, maka gambar selanjutnya adalah A</p>', 1592246588, 1592246588),
(667, 20, 18, 39, 0, '353b7d3c42384793baa6fc37019373f0.jpg', 'image/jpeg', '', '<p>A</p>', '<p>B</p>', '<p>C</p>', '<p>D</p>', '<p>E</p>', -1, -1, 4, -1, -1, 0, '', '', '', '', '', 'C', '<p>Polanya gambar tersebut ber[utar 90<sup>o</sup>. Dimana setiap berputar gambar akan bertambah satu. Sesuai polanya, jawaban yang tepat adalah C</p>', 1592246700, 1592246700),
(668, 20, 18, 39, 0, '3e3c5af3d513888059863d11d6068331.jpg', 'image/jpeg', '', '<p>A</p>', '<p>B</p>', '<p>C</p>', '<p>D</p>', '<p>E</p>', 4, -1, -1, -1, -1, 0, '', '', '', '', '', 'A', '<p>Gambar kedua merupakan hasil penceriminan dari gambar pertama, maka untuk selanjutnya, yaitu gambar ke empat juga merupakan pencerminan gambar ke tiga. Jawaban : A</p>', 1592246791, 1592246791),
(669, 20, 18, 39, 0, '44b40db11fa7913607c62a95bd3a3e68.jpg', 'image/jpeg', '', '<p>A</p>', '<p>B</p>', '<p>C</p>', '<p>D</p>', '<p>E</p>', 4, -1, -1, -1, -1, 0, '', '', '', '', '', 'A', '<p>Gambar berputar berlawanan arah jarum jam. dimana setiap kali berputar maka bagian yang melengkung akan berkurang satu. perhatikan juga pola selang seling lingkaran kecil diujungnya akan berubah setiap melakukan perputaran. sehingga untuk gambar selanjutnya adalah A</p>', 1592246934, 1592246934),
(670, 20, 18, 39, 0, '6e16c01fe431b67f4187bdcd6c6f4b22.jpg', 'image/jpeg', '', '<p>A</p>', '<p>B</p>', '<p>C</p>', '<p>D</p>', '<p>E</p>', -1, 4, -1, -1, -1, 0, '', '', '', '', '', 'B', '<p>Polanya pada gambar tersebut adalah banyak sudutnya berkurang. Dari 6 menjadi 5, 4, dan untuk selanjutnya maka menjadi 3 titik sudut. Jawaban yang benar adalah B</p>', 1592247268, 1592247268),
(671, 20, 18, 39, 0, '43937c2530bf92f86c0b9f15ffd1b5c0.jpg', 'image/jpeg', '', '<p>A</p>', '<p>B</p>', '<p>C</p>', '<p>D</p>', '<p>E</p>', -1, -1, 4, -1, -1, 0, '', '', '', '', '', 'C', '<p>Pola pada gambar tersebut adalah berputar searah jarum jam. selain itu coba perhatikan selang-seling lingkaran kecil yang berada di dalam bangun sedi-8 tersebut. Setiap berputar maka lingkarannya akan bertambah 1 dan warnanya menyesuaikan. jawaban : C</p>', 1592247420, 1592247420),
(672, 20, 18, 39, 0, '41fac087194d3a453e7414829c4953d9.jpg', 'image/jpeg', '', '<p>A</p>', '<p>B</p>', '<p>C</p>', '<p>D</p>', '<p>E</p>', -1, 4, -1, -1, -1, 0, '', '', '', '', '', 'B', '<p>Gambar anak panah berputar berlawanan arah jarum jam, sedangkan bangun lengkungnya berputar searah jarum jam. Sehingga jawaban B</p>', 1592247605, 1592247605),
(673, 20, 18, 39, 0, '5c4ef0c4a6bacc424b4a23d5c9ecbed6.jpg', 'image/jpeg', '', '<p>A</p>', '<p>B</p>', '<p>C</p>', '<p>D</p>', '<p>E</p>', -1, -1, 4, -1, -1, 0, '', '', '', '', '', 'C', '<p>Pola gambar tersebut sebenarnya hanya berotasi berlawanan arah jarum jam. jadi jawaban C</p>', 1592247724, 1592247724),
(674, 20, 18, 39, 0, '5cbf692947e075abee2e0909167330fc.jpg', 'image/jpeg', '', '<p>A</p>', '<p>B</p>', '<p>C</p>', '<p>D</p>', '<p>E</p>', -1, 4, -1, -1, -1, 0, '', '', '', '', '', 'B', '<p>Perhatikan posisi anak panah pada gambar tersebut. Anak panahnya berputar searah jarum jam setiap 45<sup>o</sup>. sehingga untuk gambar selanjutnya B</p>', 1592247896, 1592247896),
(675, 20, 18, 39, 0, 'bbebb188f5a0c59c50db6d686b6d3292.jpg', 'image/jpeg', '', '<p>A</p>', '<p>B</p>', '<p>C</p>', '<p>D</p>', '<p>E</p>', -1, -1, 4, -1, -1, 0, '', '', '', '', '', 'C', '<p>Gambarnya merupakan hasil rotasi dari gambar oertama, searah jarum jam. sehingga untuk gambar selanjutnya adalah C</p>', 1592248146, 1592248146),
(676, 20, 18, 39, 0, '7e2479566ca03495de4b76c9c1bcdd73.jpg', 'image/jpeg', '', '<p>A</p>', '<p>B</p>', '<p>C</p>', '<p>D</p>', '<p>E</p>', 4, -1, -1, -1, -1, 0, '', '', '', '', '', 'A', '<p>Perhatikan banyaknya engkung yang diarsir. Pada gambar pertama ada satu bagian lengkung yang diarsir, unutk gambar selanjutnya bertambah 2 lengkung, yaitu satu lengkung kanan dan satu lengkung kiri. Maka selanjutnya adalah gambar A</p>', 1592248296, 1592248296),
(677, 20, 18, 39, 0, '6064da442e86755411263afccfda4415.jpg', 'image/jpeg', '', '<p>A</p>', '<p>B</p>', '<p>C</p>', '<p>D</p>', '<p>E</p>', -1, 4, -1, -1, -1, 0, '', '', '', '', '', 'B', '<p>Peratikan belah ketupat dan lingkaran yang ada dalam persegi. Gambar akan bertambah searah jarum jam, dimana setelah gambar belah ketupat, pertambahannya akan lingkaran, begitu untuk gambar selanjutnya maka setelah lingkaran akan bertambah belah ketupat. Selain itu, yang harus diperhatikan adalah warna, dimana setiap ada perubahan warnanya akan berubah juga, menyesuaikan. Jawaban : B</p>', 1592248629, 1592248629),
(678, 20, 18, 39, 0, '7827bad563c8947eade498b85bacb501.jpg', 'image/jpeg', '', '<p>A</p>', '<p>B</p>', '<p>C</p>', '<p>D</p>', '<p>E</p>', -1, -1, -1, 4, -1, 0, '', '', '', '', '', 'D', '<p>Polanya gambar dirotasi 90o berlawanan arah jarum jam kemudian dicerminkan</p>', 1592248732, 1592248732),
(679, 20, 18, 39, 0, '557b360b28b488c2bddd8db2ff44a879.jpg', 'image/jpeg', '', '<p>A</p>', '<p>B</p>', '<p>C</p>', '<p>D</p>', '<p>E</p>', -1, 4, -1, -1, -1, 0, '', '', '', '', '', 'B', '<p>Polanya gambar dirotasi sejauh 90<sup>o</sup> berlawanan arah jarum jam, lalu diurutkan penamaan sudutnya</p>', 1592248856, 1592248856),
(680, 20, 18, 39, 0, '84016fa79518dace5855f9394a4cc8f9.jpg', 'image/jpeg', '', '<p>A</p>', '<p>B</p>', '<p>C</p>', '<p>D</p>', '<p>E</p>', -1, 4, -1, -1, -1, 0, '', '', '', '', '', 'B', '<p>Polanya gambar diputar 90<sup>o</sup> searah jarum jam</p>', 1592248952, 1592248952),
(681, 20, 18, 39, 0, '1634352870b78db0fd2961847725d409.jpg', 'image/jpeg', '', '<p>A</p>', '<p>B</p>', '<p>C</p>', '<p>D</p>', '<p>E</p>', 4, -1, -1, -1, -1, 0, '', '', '', '', '', 'A', '<p>Polanya gambar diputar 90o berlawanan arah jarum jam. Setelah diputar, gambar yang di atas dicerminkan, sedangkan dua gambar dibawahnya saling ditukarkan posisi dan warnanya</p>', 1592249071, 1592249071),
(682, 20, 18, 39, 0, 'e65536a7cdb78550d068191236606b45.jpg', 'image/jpeg', '', '<p>A</p>', '<p>B</p>', '<p>C</p>', '<p>D</p>', '<p>E</p>', -1, 4, -1, -1, -1, 0, '', '', '', '', '', 'B', '<p>Polanya adalah ambil gambar yang ada ditengah yang memiliki warna berbeda</p>', 1592249204, 1592249204),
(683, 20, 18, 39, 0, 'be3e805d0e63bda05b8c46bb17fdefc9.jpg', 'image/jpeg', '', '<p>A</p>', '<p>B</p>', '<p>C</p>', '<p>D</p>', '<p>E</p>', 4, -1, -1, -1, -1, 0, '', '', '', '', '', 'A', '<p>Pola gambar tersebut adalah pertama gambar diputar 90<sup>o</sup> searah jarum jam, kemudian warnanya ditukar, dari abu-abu menjadi putih, dan yang putih menjadi abu-abu</p>', 1592249328, 1592249328),
(684, 20, 18, 39, 0, '026a4afd26da584a4ac332a25ff12fd5.jpg', 'image/jpeg', '', '<p>A</p>', '<p>B</p>', '<p>C</p>', '<p>D</p>', '<p>E</p>', 4, -1, -1, -1, -1, 0, '', '', '', '', '', 'A', '<p>Peratama gambar dicerminkan ke bawah. Gambar lalu digabung dengan hasil pencerminannya kemudian gambar diputar 45o berlawanan arah jarum jam. Setelah itu Gambar asli dan hasil pencerminannya digeser sedikit dalah garis atau sumbu cerminnya. Terakhir, garis tengah dihilangkan.</p>', 1592249585, 1592249585),
(685, 20, 18, 39, 0, '6a876b876d8f3e411ba9c98648ee9964.jpg', 'image/jpeg', '', '<p>A</p>', '<p>B</p>', '<p>C</p>', '<p>D</p>', '<p>E</p>', -1, -1, -1, 4, -1, 0, '', '', '', '', '', 'D', '<p>Pertama  gambar dirotasi 90<sup>o</sup>. gambar asli dan hasil pencerminannya digabung pada titik tengah. Terakhir garis tengah dihilangkan</p>', 1592249739, 1592249739),
(686, 20, 18, 39, 0, '94ad7dd8e63813cfe6f8de32a3c3067e.jpg', 'image/jpeg', '', '<p>A</p>', '<p>B</p>', '<p>C</p>', '<p>D</p>', '<p>E</p>', -1, 4, -1, -1, -1, 0, '', '', '', '', '', 'B', '<p>Huruf L dan I sam-sam terbuat dari garis lurus. Sementaran pasangan huruf O dan S sama-sama terbuat dari garis lengkung</p>', 1592249839, 1592249839),
(687, 20, 18, 39, 0, '465639000d36a96db425c25b19235d57.jpg', 'image/jpeg', '', '<p>A</p>', '<p>B</p>', '<p>C</p>', '<p>D</p>', '<p>E</p>', -1, -1, 4, -1, -1, 0, '', '', '', '', '', 'C', '<p>Gambar diputar 180<sup>o</sup></p>', 1592249901, 1592249901),
(688, 20, 18, 39, 0, '5489b89bb7efc33a7d6cf2973861cd2b.jpg', 'image/jpeg', '', '<p>A</p>', '<p>B</p>', '<p>C</p>', '<p>D</p>', '<p>E</p>', 4, -1, -1, -1, -1, 0, '', '', '', '', '', 'A', '<p>B-D, J-L, R-T, setiap huruf selisih atau loncat 1.</p>\r\n<p>Jawaban : F-H, N-P, V-X (A)</p>', 1592250112, 1592250112),
(689, 20, 18, 39, 0, '1bbcd8eb1ac02647756155ea8eaf7256.jpg', 'image/jpeg', '', '<p>A</p>', '<p>B</p>', '<p>C</p>', '<p>D</p>', '<p>E</p>', 4, -1, -1, -1, -1, 0, '', '', '', '', '', 'A', '<p>Polanya mengenai jumlah sudut. Bangun datar yang memiliki titik sudut berjulah ganjil maka pasangannya akan ganjil (3 dan 5). sedangankan yang berjumlah genap maka pasangannya juga berjumlah genap (4 dan 6(</p>', 1592250227, 1592250227),
(690, 20, 18, 39, 0, '13891658a1318a58834559ce681265c9.jpg', 'image/jpeg', '', '<p>A</p>', '<p>B</p>', '<p>C</p>', '<p>D</p>', '<p>E</p>', -1, -1, -1, 4, -1, 0, '', '', '', '', '', 'D', '<p>Dicerminkan ke bawah</p>', 1592250323, 1592250323),
(691, 20, 18, 39, 0, '9c71305e9563b54358d2a1bcbb45013d.jpg', 'image/jpeg', '', '<p>A</p>', '<p>B</p>', '<p>C</p>', '<p>D</p>', '<p>E</p>', -1, 4, -1, -1, -1, 0, '', '', '', '', '', 'B', '<p>Gambar dicerminkan ke kanan kemudian lingkaran yang berada di atas dipindah ke bawah</p>', 1592250580, 1592250580),
(692, 20, 18, 39, 0, '759167b2f6223484fd3f55da9d4149b6.jpg', 'image/jpeg', '', '<p>A</p>', '<p>B</p>', '<p>C</p>', '<p>D</p>', '<p>E</p>', 4, -1, -1, -1, -1, 0, '', '', '', '', '', 'A', '<p>Gambar kedua didapat dari gambar yang pertama diputar 45<sup>o</sup> searah jarum jam kemudian jumlah bulatan adalahkelanjutan dari gambar sebelumnya</p>', 1592250703, 1592250703),
(693, 20, 18, 39, 0, '8816efcd5f1d052d4133eca0384b2826.jpg', 'image/jpeg', '', '<p>A</p>', '<p>B</p>', '<p>C</p>', '<p>D</p>', '<p>E</p>', 4, -1, -1, -1, -1, 0, '', '', '', '', '', 'A', '', 1592250741, 1592250741),
(694, 20, 18, 39, 0, '283d8f6b19626aef0c00bf2a6a6a8bbd.jpg', 'image/jpeg', '', '<p>A</p>', '<p>B</p>', '<p>C</p>', '<p>D</p>', '<p>E</p>', -1, -1, -1, 4, -1, 0, '', '', '', '', '', 'D', '', 1592250797, 1592250797),
(695, 20, 18, 39, 0, '7b2e634e6922ed60cb742626bb8e118f.jpg', 'image/jpeg', '', '<p>A</p>', '<p>B</p>', '<p>C</p>', '<p>D</p>', '<p>E</p>', -1, 4, -1, -1, -1, 0, '', '', '', '', '', 'B', '', 1592250842, 1592250842),
(696, 20, 18, 39, 0, '120ef32583b41aeb0374b516e54bf893.jpg', 'image/jpeg', '', '<p>A</p>', '<p>B</p>', '<p>C</p>', '<p>D</p>', '<p>E</p>', 4, -1, -1, -1, -1, 0, '', '', '', '', '', 'A', '', 1592250884, 1592250884),
(697, 20, 18, 39, 0, 'eab953f7091fad4d28e17ce87e1ca45f.jpg', 'image/jpeg', '', '<p>A</p>', '<p>B</p>', '<p>C</p>', '<p>D</p>', '<p>E</p>', 4, -1, -1, -1, -1, 0, '', '', '', '', '', 'A', '', 1592250940, 1592250940),
(698, 20, 18, 39, 0, '242747dd5a5193cd66200e231d8a1fb5.jpg', 'image/jpeg', '', '<p>A</p>', '<p>B</p>', '<p>C</p>', '<p>D</p>', '<p>E</p>', -1, -1, 4, -1, -1, 0, '', '', '', '', '', 'C', '', 1592250990, 1592250990),
(699, 20, 18, 39, 0, 'cc3d4a3d2fc6092c62047713df982f35.jpg', 'image/jpeg', '', '<p>A</p>', '<p>B</p>', '<p>C</p>', '<p>D</p>', '<p>E</p>', -1, -1, -1, 4, -1, 0, '', '', '', '', '', 'D', '', 1592251021, 1592251021),
(700, 20, 18, 39, 0, '6bc8b358807ef1c56a39832c405eceed.jpg', 'image/jpeg', '', '<p>A</p>', '<p>B</p>', '<p>C</p>', '<p>D</p>', '<p>E</p>', 4, -1, -1, -1, -1, 0, '', '', '', '', '', 'A', '', 1592251119, 1592251119),
(701, 20, 18, 39, 0, '758a45556374cf8d5e40af50b45bcc78.jpg', 'image/jpeg', '', '<p>A</p>', '<p>B</p>', '<p>C</p>', '<p>D</p>', '<p>E</p>', -1, 4, -1, -1, -1, 0, '', '', '', '', '', 'B', '', 1592251152, 1592251152),
(702, 20, 18, 39, 0, 'aea3c889c64e91ad73690cb4596e7437.jpg', 'image/jpeg', '', '<p>A</p>', '<p>B</p>', '<p>C</p>', '<p>D</p>', '<p>E</p>', 4, -1, -1, -1, -1, 0, '', '', '', '', '', 'A', '', 1592251203, 1592251203),
(703, 20, 18, 39, 0, 'ebe6a0d2727ab80e8dddf01b71f85642.jpg', 'image/jpeg', '', '<p>A</p>', '<p>B</p>', '<p>C</p>', '<p>D</p>', '<p>E</p>', -1, 4, -1, -1, -1, 0, '', '', '', '', '', 'B', '<p>Tentukan sisi yang berada di depan, baru kemudian sisi yang lain, selanjutnya perhatikan sisi yang lainnya untuk menentukan benar atau salah.</p>\r\n<p>-) Pilihan A -> jika a di depan dan b di samping kanan, maka C harusnya di bawah. (Salah) </p>\r\n<p>-) Pilihan B -> jika b di depan dan a di sebelah kiri, maka c di bawah (Benar)</p>\r\n<p>-) karena B benar yang lain salah</p>', 1592252710, 1592252710),
(704, 20, 18, 39, 0, 'd6bfde95b2b171efd2fbb06a556209f1.jpg', 'image/jpeg', '', '<p>A</p>', '<p>B</p>', '<p>C</p>', '<p>D</p>', '<p>E</p>', -1, -1, 4, -1, -1, 0, '', '', '', '', '', 'C', '<p>Perhatikan sisi dengan gambar bangun segitiga berada di depan. selain itu juga perhatikan gambar segitiga ujungnya selalu menghadap persegi.</p>\r\n<p>-) pilihan A salah kerena jika sisi segitiga di depan dan persegi di atas, maka lingkaran di kanan</p>\r\n<p>-) Pilihan B salah karena ujung segitiga tidak menghadap pesegi</p>\r\n<p>-) Pilihan C benar</p>\r\n<p>-) Pilihan D salah karena ujung segitiga menghadap ke lingkaran</p>\r\n<p>-) pilihan E salah karena seharusnya segitiga berada di sisi bawah</p>', 1592253002, 1592253002),
(705, 20, 18, 39, 0, '1fbbf21aa62a9703836acd4880f3f616.jpg', 'image/jpeg', '', '<p>A</p>', '<p>B</p>', '<p>C</p>', '<p>D</p>', '<p>E</p>', -1, -1, -1, 4, -1, 0, '', '', '', '', '', 'D', '<p>Kuncinya gambar segitiga tidak boleh berdampingan dengan lingkaran sehingga pilihan A, B, C, E salah</p>', 1592253084, 1592253084),
(706, 20, 18, 39, 0, '22c0d6f427c434ab79cb9df8ff12f9e4.jpg', 'image/jpeg', '<p>A</p>', '<p>B</p>', '<p>C</p>', '<p>D</p>', '<p>E</p>', '', -1, -1, -1, 4, -1, 0, '', '', '', '', '', 'D', '<p>Perhatikan sisi dengan bintang berasa di sisi depan. Tinggal perhatikan satu sisi lainnya. Benar tifaknya gambar bisa dilihat dari letak sisi yang ketiga. selain itu, juga perhatikan letak kedua segitiga hitam. Segitiga tersebut searah atau tidak berhadapan dan terletak di pojok sisi yang bersesuaian. Gambar yang memungkainkan hanya opsi D. Opsi A, B, E salah karena kedua segitiga saling berhadapan. Opsi C salah karena segitiga letaknya tidak bersesuaian.</p>', 1592253353, 1592253353),
(707, 20, 18, 39, 0, '5a719b109a323f0e8d8b439b633db5b0.jpg', 'image/jpeg', '', '<p>A</p>', '<p>B</p>', '<p>C</p>', '<p>D</p>', '<p>E</p>', -1, 4, -1, -1, -1, 0, '', '', '', '', '', 'B', '<p>Setiap jaring-jaring kubus memiliki motif lingkaran kecil-kecil. Sisi dengan 5 lingakaran ujungnya tidak mungkinbersatu dengan sisi yang memiliki 3 lingkaran. jawaban yang tepat adalah B.</p>\r\n<p>-) Opsi A salah. JIka sisi dengan 3 lingkaean di depan dan sisi dengan 1 lingkaran di kanan, maka seharusnya sisi dengan 2 lingkaran di bawah</p>\r\n<p>-) Opsi C salah karena sisi dengan 4 lingkaran bentuknya tidak sejajar</p>\r\n<p>-) Opsi D salah. seharusnya sisi depan dua lingkaran dan sisi dengan 3 lingkaran searah</p>\r\n<p>-) opsi E jelas salah</p>', 1592253669, 1592253669),
(708, 20, 18, 39, 0, '5a78ff85718fd91f6bdcc3a727d3a3f3.jpg', 'image/jpeg', '', '<p>A</p>', '<p>B</p>', '<p>C</p>', '<p>D</p>', '<p>E</p>', -1, -1, -1, 4, -1, 0, '', '', '', '', '', 'D', '<p>PERHATIKAN SISI DENGAN GAMBAR DUA GARIS. jiKA DIBUAT KUBUS MAKA KEDUA SISI TERSEBUT TIDAK AKAN MUNGKIN POSISI GARISNYA SEARAH SATU sama lain. Karena seharusnya posisi kedua sisi itu berlawanan, dimana salah satu garisnya menuju bintang dan sisi lin garisnya tidak menuju bintang. </p>\r\n<p>-) Opsi A, C, E jelas salah.</p>\r\n<p>-) Opsi B Salah, jika sisi dengan garis menuju bintang ada di sebelah kiri, maka sisi lain yang ada garisnya berada di sebelah bawah</p>', 1592253910, 1592253910),
(709, 20, 18, 39, 0, 'c0eaafa6fd64513bb44b8ee69ab97995.jpg', 'image/jpeg', '', '<p>A</p>', '<p>B</p>', '<p>C</p>', '<p>D</p>', '<p>E</p>', -1, -1, -1, 4, -1, 0, '', '', '', '', '', 'D', '<p>Jika jaring-jaring dilipat maka akan terbentuk bangun prisma segitiga. Dimana, tingginya lebih panjang daripada segitiga. Jawaban yang paling tepat opsi D</p>', 1592254008, 1592254008),
(710, 20, 18, 39, 0, '934fbcafa4d66b22ac538583c6eec7d0.jpg', 'image/jpeg', '', '<p>A</p>', '<p>B</p>', '<p>C</p>', '<p>D</p>', '<p>E</p>', 4, -1, -1, -1, -1, 0, '', '', '', '', '', 'A', '<p>Perhatikan dua garis sejajar. Dimana garis tersebut menghadao ke lingkaran hitam.</p>\r\n<p>-) Opsi B, C, D jelas salah</p>\r\n<p>-) Opsi E salah karena jika dua garis silang berada di depan, dan lingkaran hitam berada di sebelah kiri, seharusnyha dua garis sejajar berasa di atas.</p>', 1592254138, 1592254138),
(711, 20, 18, 39, 0, 'f68cceac9ddea741fa99f546cf117bcb.jpg', 'image/jpeg', '', '<p>A</p>', '<p>B</p>', '<p>C</p>', '<p>D</p>', '<p>E</p>', 4, -1, -1, -1, -1, 0, '', '', '', '', '', 'A', '<p>Cukup jelas. Jawaban yang benar adalah A</p>', 1592254203, 1592254203),
(712, 20, 18, 39, 0, 'a726a6ef1e5a2330d8d6d2bb099e5142.jpg', 'image/jpeg', '', '<p>A</p>', '<p>B</p>', '<p>C</p>', '<p>D</p>', '<p>E</p>', -1, -1, -1, -1, 4, 0, '', '', '', '', '', 'E', '<p>JIKA JARING-JARING KUBUS DIRANGKARI DENGAN GAMBAR BELAH ketupat berada di sisi depan, maka sisi dengan gambar belah ketupat tidak mungkin akan bersebelahan dengan lingkaran.</p>\r\n<p>Opsi A, B, C, D jelas salah</p>', 1592254402, 1592254402),
(713, 20, 18, 39, 0, '2eea442d52301ea8eae33bc041519e86.jpg', 'image/jpeg', '', '<p>A</p>', '<p>B</p>', '<p>C</p>', '<p>D</p>', '<p>E</p>', 4, -1, -1, -1, -1, 0, '', '', '', '', '', 'A', '<p>Tetapkan sisi dengan gambar bintang menjadi sisi depan, kemudian lanjutkan lipatan sembari memutar bangun agar bagian sisinya sama dengan gambar yang dicari. Perhatikan bentuk elips hitam jelas condong menjauhi bintang. </p>\r\n<p>-) Opsi C, D, E jelas salah</p>\r\n<p>-) Opsi B Salah karena jika dirangkai maka lingkaran hitam tidak berada disisi atas. </p>', 1592254980, 1592254980),
(714, 20, 18, 39, 0, 'dffae8b58094cb27e0715223bd95ec91.jpg', 'image/jpeg', '', '<p>A</p>', '<p>B</p>', '<p>C</p>', '<p>D</p>', '<p>E</p>', 4, -1, -1, -1, -1, 0, '', '', '', '', '', 'A', '<p>PERHATIKAN JIKA SISI DENGAN PERSEGI HITAM BERADA DI DEPAN, MAKA GARIS-GARIS pada sis yang lain akan menghadap ke persegi tersebut. JIka jaring-jaring dilipat, maka hanya jawaban A yang sesuai denga bentuk kubus pada soal</p>', 1592255117, 1592255117),
(715, 20, 18, 39, 0, '19c59618c233d85cde09a811ff6d3665.jpg', 'image/jpeg', '', '<p>A</p>', '<p>B</p>', '<p>C</p>', '<p>D</p>', '<p>E</p>', -1, -1, 4, -1, -1, 0, '', '', '', '', '', 'C', '<p>Dari gambar kubus, terlihat bahwa gambar anak panah dan segitiga hitam saling berhadapan. </p>\r\n<p>-) Opsi A, B, D, E jelas salah</p>', 1592255196, 1592255196),
(716, 20, 18, 39, 0, '4e9bd154b1a2ebe06418da299efc9eda.jpg', 'image/jpeg', '', '<p>A</p>', '<p>B</p>', '<p>C</p>', '<p>D</p>', '<p>E</p>', -1, -1, 4, -1, -1, 0, '', '', '', '', '', 'C', '<p>Perhatikan gambar bintang hitam berada di sisi depan. Dan belah ketupat berada di didi kanan. jawaban yang memenuhi hanya opsi C</p>', 1592255297, 1592255297),
(717, 20, 18, 39, 0, '2231ea438e8d3abe510e98c65c862b28.jpg', 'image/jpeg', '', '<p>A</p>', '<p>B</p>', '<p>C</p>', '<p>D</p>', '<p>E</p>', -1, 4, -1, -1, -1, 0, '', '', '', '', '', 'B', '<p>Perhatikan bahwa kedua ujung segitiga tersebut semuanya menuju ke sisi yang berwarna hitam dan kedua sisi tersebut salung berdampingan. </p>\r\n<p>-) Opsi C, D, E jelas salah</p>\r\n<p>-) Opsi A salah kerena kedua sisi yang terdapat gambar segitiga tidak berdampingan atau saling bertolak belakang</p>', 1592255430, 1592255430),
(718, 21, 23, 41, 0, '', '', '<p>Which one of these sentences is grammatically correct.</p>', '<p>Our professor much-loved was sixty-five years old</p>', '<p>Our much-loved professor was sixty five years old.</p>', '<p>Our much-loved professor was sixty-five years old.</p>', '<p>Our professor much-loved was sixty five years old</p>', '', -1, 4, -1, -1, -1, 0, '', '', '', '', '', 'B', '<p>Our professor much-loved was sixty five years old</p>', 1592414869, 1592414869),
(719, 21, 23, 41, 0, '', '', '<p>Tom has just finished writing a _________ article. </p>', '<p>Nine-hundred-word</p>', '<p>Nine-hundreds-word</p>', '<p>Nine-hundred-words</p>', '<p>Nine-hundreds-words</p>', '', 4, -1, -1, -1, -1, 0, '', '', '', '', '', 'A', '<p><span xss=removed>Kata yang menjelaskan kata benda tidak boleh plural</span></p>', 1592414924, 1592414924),
(720, 21, 23, 41, 0, '', '', '<p>Which one of following compound adjective is not correct?</p>', '<p>Man-made</p>', '<p>Post-graduate-degree</p>', '<p>Life-long</p>', '<p>Full-time</p>', '', -1, 4, -1, -1, -1, 0, '', '', '', '', '', 'B', '<p><span xss=removed>kata yang menerangkan kata benda dan kata bendanya tidak digabung, yang tepat post-graduate degree</span></p>', 1592414968, 1592414968),
(721, 21, 23, 41, 0, '', '', '<p>The new lab being built now is to be ____ high</p>', '<p>Five storeys</p>', '<p>Five-storey</p>', '<p>Five-storeys</p>', '<p>Five-storeyed</p>', '', -1, 4, -1, -1, -1, 0, '', '', '', '', '', 'B', '<p>high di sini sebagai noun sehingga five-storey adalah yang paling tepat</p>', 1592415185, 1592415185),
(722, 21, 23, 41, 0, '', '', '<p>When we paid for our groceries, ____________</p>', '<p>the cashier gave us, Lia and I, some discount coupons.</p>', '<p>the cashier gave we, Lia and me, some discount coupons.</p>', '<p>the cashier gave us, Lia and myself, some discount coupons.</p>', '<p>the cashier gave us, Lia and me, some discount coupons.</p>', '', -1, -1, -1, 4, -1, 0, '', '', '', '', '', 'D', '<p><span xss=removed>us (personal pronoun sebagai objek) jika diuraikan menjadi Lia and me (sama-sama personal pronoun sebagai objek)</span></p>', 1592415248, 1592415248),
(723, 21, 23, 41, 0, '', '', '<p>Did you know that ____________</p>', '<p>we, James and I, have been good friends since we met in junior high school.</p>', '<p>we, James and me, have been good friends since we met in junior high school.</p>', '<p>we, James and I, have been good friends since our met in junior high school.</p>', '<p>we, James and I, have been good friends since us met in junior high school.</p>', '', 4, -1, -1, -1, -1, 0, '', '', '', '', '', 'A', '<p><span xss=removed>we (personal pronoun sebagai subjek) jika diuraikan menjadi James and I (sama-sama personal pronoun sebagai subjek)</span></p>', 1592415301, 1592415301),
(724, 21, 23, 41, 0, '', '', '<p>Please be quiet. ____________</p>', '<p>Paula and she have a test tomorrow, so both of they are studying.</p>', '<p>Paula and she have a test tomorrow, so both of them are studying.</p>', '<p>Paula and her have a test tomorrow, so both of they are studying.</p>', '<p>Paula and she have a test tomorrow, so both of hers are studying.</p>', '', -1, 4, -1, -1, -1, 0, '', '', '', '', '', 'B', '<p><span xss=removed>... + verb..., pronoun yang tepat adalah sebagai subjek, yaitu Paula and she</span></p>', 1592415353, 1592415353),
(725, 21, 23, 41, 0, '', '', '<p>Our neighbours ____________</p>', '<p>and us are good friends</p>', '<p>and ourselves are good friends</p>', '<p>and us is good friends</p>', '<p>and we are good friends</p>', '', -1, -1, -1, 4, -1, 0, '', '', '', '', '', 'D', '<p><span xss=removed>... + to be..., pronoun yang tepat adalah sebagai subjek, yaitu our neighbors and we</span></p>', 1592415524, 1592415524),
(726, 21, 23, 41, 0, '', '', '<p>____________ I promise.</p>', '<p>If me see her tonight, I will give her your message.</p>', '<p>If I see her tonight, I will give she your message.</p>', '<p>If I see her tonight, I will give her your message.</p>', '<p>If I see she tonight, I will give her your message.</p>', '', -1, -1, 4, -1, -1, 0, '', '', '', '', '', 'C', '<p><span xss=removed>cukup jelas, I sebagai subjek dan her sebagai objek</span></p>', 1592415583, 1592415583),
(727, 21, 23, 41, 0, '', '', '<p>I hope that ____________</p>', '<p>you and them enjoy the pizza that we, Bruce and I, made.</p>', '<p>you and they enjoy the pizza that us, Bruce and I, made</p>', '<p>you and they enjoy the pizza that we, Bruce and me, made</p>', '<p>you and they enjoy the pizza that we, Bruce and I, made</p>', '', -1, -1, -1, 4, -1, 0, '', '', '', '', '', 'D', '<p><span xss=removed>you and they enjoy ... (S + verb), Bruce and I made (S + verb)</span></p>', 1592415637, 1592415637),
(728, 20, 18, 38, 0, '', '', '<p>Jika diketahui <sup>1</sup>/<sub>a</sub> =2,5 maka <sup>1</sup>/<sub>(2-a)</sub> = ...</p>', '<p>8/5</p>', '<p>7/5</p>', '<p>7/9</p>', '<p>5/9</p>', '<p>5/8</p>', -1, -1, -1, -1, 4, 0, '', '', '', '', '', 'E', '<p><span xss=removed>1/a = 2,5</span><br xss=removed><span xss=removed>a = 1/2,5</span><br xss=removed><span xss=removed>a = 0,4</span><br xss=removed><br xss=removed><span xss=removed>1/(2-0.4)</span><br xss=removed><span xss=removed>1/1.6</span><br xss=removed><span xss=removed>5/8</span></p>', 1592488086, 1592488086),
(729, 20, 18, 38, 0, '', '', '<p>Jika diketahui a = <sup>26</sup>/<sub>99</sub> + <sup>29</sup>/<sub>61</sub> +<sup>14</sup>/<sub>76</sub> + <sup>23</sup>/<sub>87</sub> = ...</p>', '<p>1,1866</p>', '<p>1,0866</p>', '<p>0,9166</p>', '<p>0,8166</p>', '<p>0,5166</p>', 4, -1, -1, -1, -1, 0, '', '', '', '', '', 'A', '<p> </p>\r\n<p>Kita gunakan metode taksiran (setiap pecahan kita ubah ke bentuk yang lebih familiar dan mudah disederhanakan)</p>\r\n<p><sup>26</sup>/<sub>99</sub> + <sup>29</sup>/<sub>61</sub> +<sup>14</sup>/<sub>76</sub> + <sup>23</sup>/<sub>87</sub> </p>\r\n<p><sup>25</sup>/<sub>100</sub> + <sup>30</sup>/<sub>60</sub> + <sup>15</sup>/<sub>75</sub> + <sup>22</sup>/<sub>88</sub> = ...</p>\r\n<p>1/4 + 1/2 + 1/3 + 1/4 = 1 <sup>1</sup>/<sub>3 </sub></p>\r\n<p>Dari opsi yang ada, paling mendekati adalah opsi A<code></code></p>\r\n<p> </p>\r\n<p> </p>', 1592488881, 1592488881),
(730, 20, 18, 38, 0, '', '', '<p>2a + 3b – <sup>2</sup>/<sub>7 </sub>a+ (<sup>-3</sup>/<sub>5 </sub>b)</p>\r\n<p><code></code></p>', '<p>12a – 35b</p>', '<p>12 (a-b)</p>', '<p><sup>6</sup>/<sub>7</sub> (a-b)</p>', '<p><sup>12</sup>/<sub>35</sub> (5a + 7b)</p>', '<p><sup>12</sup>/<sub>35</sub> (a-b)</p>', -1, -1, -1, 4, -1, 0, '', '', '', '', '', 'D', '<p>Lebih cepat dengan cek opsi. ganti nilai a = 7, b = 5</p>\r\n<p>2a + 3b – <sup>2</sup>/<sub>7 </sub>a+ (<sup>-3</sup>/<sub>5 </sub>b) --&gt; 14 + 14 - 2 - 3 = 24</p>\r\n<p>A. 12a – 35b = jelas salah</p>\r\n<p>B. 12 (a-b) = -24 (salah)</p>\r\n<p>C. <sup>6</sup>/<sub>7</sub> (a-b) jelas salah</p>\r\n<p>D.<sup>12</sup>/<sub>35</sub> (5a + 7b) = 24 (Benar)</p>\r\n<p> </p>', 1592490146, 1592490146),
(731, 20, 18, 38, 0, '9f73f9ee0e35b215715797ac7baccc3a.jpg', 'image/jpeg', '', '<p>1,17</p>', '<p>1,07</p>', '<p>0,97</p>', '<p>0,87</p>', '<p>0,77</p>', -1, -1, 4, -1, -1, 0, '', '', '', '', '', 'C', '', 1592490769, 1592492361),
(732, 20, 18, 38, 0, '', '', '<p>4*3 (28)</p>\r\n<p>5*6 (55)</p>\r\n<p>2*A (14)</p>\r\n<p> </p>\r\n<p>Berapakah A?</p>\r\n<p> </p>', '<p>2</p>', '<p>3</p>', '<p>5</p>', '<p>4</p>', '<p>6</p>', -1, -1, 4, -1, -1, 0, '', '', '', '', '', 'C', '<p><span xss=removed>4*3 [28] = (4+3)*4=28</span><br xss=removed><span xss=removed>(5+6)*5=55</span><br xss=removed><span xss=removed>(2+A)*2=14 --&gt; A=5</span></p>', 1592504393, 1592504393),
(733, 20, 18, 38, 0, '', '', '<p>Diketahui p adalah bilangan bulat dan p* = <sup>p-1</sup>/<sub>p</sub>. Jika x = (2+(-1)*)*  dan y = 4*, maka ...</p>', '<p>x + y > 1</p>', '<p>x > y</p>', '<p>x = y</p>', '<p>x < y>', '<p>Hubungan x dan y tidak dapat ditentukan</p>', -1, -1, 4, -1, -1, 0, '', '', '', '', '', 'C', '<p><span xss=removed>itu temen2 dikasih aturan,p* = (p-1)/p </span></p>\r\n<p><span xss=removed>Jika </span><span xss=removed>kerjakan dari dalam</span><br xss=removed><span xss=removed>x=(2+(-1)*)*</span><br xss=removed><span xss=removed>x=(2+(-1-1/-1))*</span><br xss=removed><span xss=removed>x=(2+2)*</span><br xss=removed><span xss=removed>x=(4)*</span><br xss=removed><br xss=removed><span xss=removed>y=4*</span><br xss=removed><br xss=removed><span xss=removed>x=y</span></p>', 1592504679, 1592504679),
(734, 20, 18, 38, 0, '', '', '<p>Diketahui s adalah bilangan bulat positif dan s=r+3. Jika x-2=(r+2)(r+4) dan y=2(r+4)<sup>2</sup>, maka ....</p>', '<p>x < y>', '<p>x>y</p>', '<p>x=y</p>', '<p>x+y>1</p>', '<p>Hubungan x dan y tidak dapat ditentukan</p>', 4, -1, -1, -1, -1, 0, '', '', '', '', '', 'A', '<p><span xss=removed>coba masukan r= -2 dan bandingkan jikan r=0</span></p>\r\n<p><span xss=removed>terjadi ketidak konsistenan hubungan x dan y</span></p>', 1592505284, 1592505284),
(735, 20, 18, 38, 0, '', '', '<p>Selisih dua bilangan adalah 6, dan jika kedua bilangan dijumlahkan hasilnya adalah 32. Berapa selisih dari kuadrat kedua bilangan tersebut?</p>', '<p>198</p>', '<p>204</p>', '<p>216</p>', '<p>288</p>', '<p>192</p>', -1, -1, -1, -1, 4, 0, '', '', '', '', '', 'E', '<p><span xss=removed>x-y=6</span><br xss=removed><span xss=removed>x+y=32</span><br xss=removed><span xss=removed>_________+</span><br xss=removed><span xss=removed>2x =38</span><br xss=removed><span xss=removed>x=19</span><br xss=removed><span xss=removed>y=13</span><br xss=removed><span xss=removed>x<sup>2</sup>-y<sup>2</sup>= 19<sup>2</sup>-13<sup>2</sup></span><br xss=removed><span xss=removed>=361-169=192</span></p>', 1592505656, 1592505656),
(736, 20, 18, 38, 0, '', '', '<p>Jika <sup>1</sup>/<sub>x</sub> – <sup>1</sup>/<sub>y</sub> = <sup>1</sup>/<sub>z</sub> maka z sama dengan …</p>', '<p><sup>xy</sup>/<sub>y-z</sub></p>', '<p><sup>x-y</sup>/<sub>xy</sub></p>', '<p>xy</p>', '<p><sup>xy</sup>/<sub>y-y</sub></p>', '<p>x+y</p>', 4, -1, -1, -1, -1, 0, '', '', '', '', '', 'A', '', 1592505941, 1592505941),
(737, 20, 18, 38, 0, '', '', '<p>Tiga buah bilangan jika dijumlahkan hasilnya 78. Selisih bilangan pertama dan bilangan kedua sama dengan selisih bilangan kedua dan bilangan ketiga. Hasil perkalian ketiga bilangan adalah 17.342. Ketiga bilangan tersebut adalah....</p>', '<p>23, 26, dan 29</p>', '<p>22, 26, 30</p>', '<p>24, 25, 26</p>', '<p>24, 26, 28</p>', '<p>25, 26, 27</p>', 4, -1, -1, -1, -1, 0, '', '', '', '', '', 'A', '<p>a + b +c = 78</p>\r\n<p>b-a = c-b</p>\r\n<p>abc = 17.342</p>\r\n<p>lihat option, hasil kali ketiga bilangan berakiran 2, maka opsi B, C, E sudah pasti salah, tinggal kemungkinan A, dan D. dengan melakukan cek abc = 17.342 maka jawaban benar adalah opsi A</p>', 1592506973, 1592506973),
(738, 20, 18, 38, 0, '', '', '<p>Umur Leo 7 tahun lebih tua dari umur Bollon. Sedangkan jumlah umur mereka adalah 43 tahun. Berapakah umur masing-masing …</p>', '<p>Leo 24 tahun dan Bollon 19 tahun</p>', '<p>Leo 25 tahun dan Bollon 18 tahun</p>', '<p>Leo 25 tahun dan Bolon 17 tahun</p>', '<p>Leo 26 tahun dan Bollon 17 tahun</p>', '<p>Leo 27 tahun dan Bollon 16 tahun</p>', -1, 4, -1, -1, -1, 0, '', '', '', '', '', 'B', '', 1592507295, 1592507295),
(739, 20, 18, 38, 0, '', '', '<p>Tentukan nilai 6x – 2y jika x dan y merupakan penyelesaian dari sistem persamaan 3x + 3y =3 dan 2x – 4y =14 </p>', '<p>-12</p>', '<p>18</p>', '<p>=17</p>', '<p>16</p>', '<p>22</p>', -1, -1, -1, -1, 4, 0, '', '', '', '', '', 'E', '', 1592507347, 1592507347),
(740, 20, 18, 38, 0, '', '', '<p>{(R,S)} adalah himpunan penyelesaian dari sistem persamaan 2r – 3s = 2 dan 5r + 2s =24. maka nilai (s-r) adalah?</p>', '<p>6</p>', '<p>5</p>', '<p>-2</p>', '<p>2</p>', '<p>-6</p>', -1, -1, 4, -1, -1, 0, '', '', '', '', '', 'C', '', 1592507590, 1592507590),
(741, 20, 18, 38, 0, '', '', '<p>Jumlah dua bilangan adalah 72 dan selisihnya 18. Jika dibuat suatu pecahan dengan pembilangnya bilangan yang kecil, maka penyebut pecahan tersebut adalah?</p>', '<p>67</p>', '<p>40</p>', '<p>45</p>', '<p>27</p>', '<p>13</p>', -1, -1, 4, -1, -1, 0, '', '', '', '', '', 'C', '', 1592508852, 1592508852),
(742, 20, 18, 38, 0, '', '', '<p>Diketahui x dan y adalah himpunan penyelesaian dari x+2y=7 dan 5x-3y=9. Maka nilai dari 3x<sup>2</sup>+2y<sup>-1</sup> adalah ...</p>', '<p>31</p>', '<p>35</p>', '<p>18</p>', '<p>22</p>', '<p>28</p>', -1, -1, -1, -1, 4, 0, '', '', '', '', '', 'E', '', 1592509001, 1592509001),
(743, 20, 18, 38, 0, '', '', '<p>Jika 17 < a>', '<p>a>b</p>', '<p>a<b>', '<p>a=b</p>', '<p>a=b-1</p>', '<p>Tidak dapat ditentukan</p>', -1, -1, -1, -1, 4, 0, '', '', '', '', '', 'E', '', 1592509255, 1592509255),
(744, 20, 18, 38, 0, '', '', '<p>Jika K=251x249-250<sup>2</sup>+17 dan L=300<sup>2</sup>-301x299+5, maka ...</p>', '<p>K>L</p>', '<p>l>k</p>', '<p>K=L</p>', '<p>K=L=0</p>', '<p>Tidak dapat ditentukan</p>', 4, -1, -1, -1, -1, 0, '', '', '', '', '', 'A', '', 1592509411, 1592509411),
(745, 20, 18, 38, 0, '', '', '<p>Jika <sup>(x-1)</sup>/<sub>(x+1)</sub>=<sup>4</sup>/<sub>5</sub>, maka x =...</p>', '<p>3</p>', '<p>4</p>', '<p>9</p>', '<p>12</p>', '<p>13</p>', -1, -1, 4, -1, -1, 0, '', '', '', '', '', 'C', '', 1592509519, 1592509519),
(746, 20, 18, 38, 0, '', '', '<p><sup>4-x</sup>/<sub>2+x</sub>=x, berapakan nilai dari x<sup>2</sup>+3x-4 = ...</p>', '<p>-4</p>', '<p>0</p>', '<p>1</p>', '<p>2</p>', '<p>4</p>', -1, 4, -1, -1, -1, 0, '', '', '', '', '', 'B', '', 1592510109, 1592510109),
(747, 20, 18, 38, 0, '', '', '<p>x dan y adalah bilangan asli dengan x>y. JIka x<sup>2</sup>-10x+24=0 dan xy+x+y=34. Maka nilaix.y = ...</p>', '<p>35</p>', '<p>24</p>', '<p>18</p>', '<p>12</p>', '<p>8</p>', -1, 4, -1, -1, -1, 0, '', '', '', '', '', 'B', '', 1592510274, 1592510274),
(748, 20, 18, 38, 0, '', '', '<p>Umur sang ayah saat ini adalah 24 tahun lebih tua dari pada anaknya. Dua tahun yang lalu, umur sang ayah 4 kali lebih tua dari umur anaknya. Berapakah umur anaknya sekarang? (Soal USM STAN 2006)</p>', '<p>10 tahun</p>', '<p>8 tahun</p>', '<p>12 tahun</p>', '<p>9 tahun</p>', '<p>13 tahun</p>', 4, -1, -1, -1, -1, 0, '', '', '', '', '', 'A', '', 1592550654, 1592550654),
(749, 20, 18, 38, 0, '', '', '<p>Seorang pengecat telah mengecat sepertiga tembok berbentuk persegi panjang yang tingginya 10 meter. Apabila ia selesai mengecat bagian tembok lain seluas 75 meter persegi, maka ia akan menyelesaikan tiga perempat dari pekerjaannya. Berapakah panjang tembok tersebut? (Soal USM STAN 2006)</p>', '<p>9 meter</p>', '<p>10 meter</p>', '<p>12 meter</p>', '<p>16 meter</p>', '<p>18 meter</p>', -1, -1, -1, -1, 4, 0, '', '', '', '', '', 'E', '', 1592550757, 1592550757),
(750, 20, 18, 38, 0, '', '', '<p>Pada tahun 2002 usia seorang anak sama dengan seperempat usia ibunya (dalam tahun). Jika pada tahun 2006 usia anak tersebut sepertiga usia ibunya, maka anak tersebut sebenarnya lahir pada tahun? (Soal USM STAN 2008)</p>', '<p>1988</p>', '<p>1990</p>', '<p>1992</p>', '<p>1994</p>', '<p>1996</p>', -1, -1, -1, 4, -1, 0, '', '', '', '', '', 'D', '', 1592550826, 1592550826),
(751, 20, 18, 38, 0, '', '', '<p>Enam orang mahasiswa masing-masing memiliki sejumlah uang. Jika uang yang dimiliki oleh setiap dua orang dari mereka dijumlahkan adalah (dalam ribuan) 215 ; 197 ; 264 ; 236 ; 208 ; 229 ; 252 ; 244 ; 218 ; 241 ; 226 ; 249 ; 258 ; 256 ; 247. Jumlah uang keenam mahasiswa tersebut adalah? (Soal USM STAN 2009)</p>', '<p>590</p>', '<p>688</p>', '<p>708</p>', '<p>780</p>', '<p>788</p>', -1, -1, 4, -1, -1, 0, '', '', '', '', '', 'C', '', 1592550895, 1592550895),
(752, 20, 18, 38, 0, '', '', '<p>Sebuah drum berisi minyak 2/5 bagian. Apabila ke dalam drum dituangkan 2 liter minyak maka drum itu menjadi ½ bagian. Kapasitas drum tersebut adalah …. (Soal USM STAN 2010)</p>', '<p>10</p>', '<p>12</p>', '<p>15</p>', '<p>20</p>', '<p>24</p>', -1, -1, -1, 4, -1, 0, '', '', '', '', '', 'D', '', 1592550933, 1592550933),
(753, 20, 18, 38, 0, '', '', '<p>Jika salah satu akar persamaan x<sup>2</sup>+(a+1)x+(3a+2)=0 adalah 5, maka akar yang lain adalah ... (Soal USM STAN 2007)</p>', '<p>-4</p>', '<p>4</p>', '<p>-2</p>', '<p>2</p>', '<p>1</p>', -1, -1, 4, -1, -1, 0, '', '', '', '', '', 'C', '', 1592551105, 1592551133),
(754, 20, 18, 38, 0, '', '', '<p>-102x + 17y = 136, maka nilai 66x – 11y = (Soal USM STAN 2009)</p>', '<p>-88</p>', '<p>-48</p>', '<p>48</p>', '<p>88</p>', '<p>66</p>', 4, -1, -1, -1, -1, 0, '', '', '', '', '', 'A', '', 1592551239, 1592551239),
(755, 20, 18, 38, 0, '', '', '<p>Jika 0 < x>', '<p>-20</p>', '<p>-12</p>', '<p>0</p>', '<p>25</p>', '<p>12</p>', -1, -1, -1, 4, -1, 0, '', '', '', '', '', 'D', '', 1592551308, 1592551308),
(756, 20, 18, 38, 0, '', '', '<p>2x + 3y = 7 dan 5x – 2y = 8, maka.... (Soal USM STAN 2008)</p>', '<p>x>y</p>', '<p>x<y>', '<p>x=y</p>', '<p>x=1/y</p>', '<p>Tidak dapat ditentukan</p>', 4, -1, -1, -1, -1, 0, '', '', '', '', '', 'A', '', 1592551365, 1592551365),
(757, 20, 18, 38, 0, '', '', '<p>Jika 3x + 5y = 27 dan 2x + 5y = 23, maka x dan y masing-masing adalah ... (Soal USM STAN 2008)</p>', '<p>3 dan 4</p>', '<p>4 dan 3</p>', '<p>4 dan 5</p>', '<p>5 dan 4</p>', '<p>3 dan 5</p>', -1, 4, -1, -1, -1, 0, '', '', '', '', '', 'B', '', 1592551432, 1592551432),
(758, 20, 18, 38, 0, '92b4cef943f69ad5cd192cb063226d76.jpg', 'image/jpeg', '', '<p>10</p>', '<p>8</p>', '<p>6</p>', '<p>4</p>', '<p>2</p>', -1, -1, -1, 4, -1, 0, '', '', '', '', '', 'D', '', 1592551762, 1592551788),
(759, 20, 18, 38, 0, 'ed464dfd2cc3e0f9e3406ef749d0f873.jpg', 'image/jpeg', '', '<p>1</p>', '<p><sup>5</sup>/<sub>6</sub></p>', '<p>0</p>', '<p>-<sup>5</sup>/<sub>6</sub></p>', '<p>-1</p>', 4, -1, -1, -1, -1, 0, '', '', '', '', '', 'A', '', 1592552169, 1592552169),
(760, 20, 18, 38, 0, '17d4435a1f2b3401b47bdaaf6bb7e1e0.jpg', 'image/jpeg', '', '<p>1</p>', '<p>2</p>', '<p>4</p>', '<p>8</p>', '<p>0</p>', -1, 4, -1, -1, -1, 0, '', '', '', '', '', 'B', '', 1592553220, 1592553220),
(761, 20, 18, 38, 0, '40ba1005bef390706c225495910c7c43.jpg', 'image/jpeg', '', '<p>0,67</p>', '<p>0,7</p>', '<p>0,75</p>', '<p>0,77</p>', '<p>0,8</p>', -1, -1, -1, -1, 4, 0, '', '', '', '', '', 'E', '', 1592553314, 1592553314),
(762, 20, 18, 38, 0, '', '', '<p>Pak Anto mendapat giliran ronda setiap 6 hari, Pak BUrhan setiap 9 hari, Pak Candra setiap 12 har dan Pak Dani setiap 18 hari. Jika mereka ronda bersama-sama pada tanggal 1 Januari 2014, pada tanggal berepa mereka berempat akan ronda bersama-sama lagi? (USM PKN STAN 2014)</p>', '<p>8 Februari 2014</p>', '<p>7 Februari 2014</p>', '<p>6 Februari 2014</p>', '<p>5 Februari 2014</p>', '<p>4 Februari 2014 </p>', -1, -1, 4, -1, -1, 0, '', '', '', '', '', 'C', '', 1592553864, 1592553864),
(763, 20, 18, 38, 0, '', '', '<p>16<sup>0,125</sup>-(0,5)<sup>-0,5</sup>= ... (Soal USM PKN STAN 2014)</p>', '<p><span xss=removed>2</span><span xss=removed>Ö2</span></p>', '<p><span xss=removed>Ö2</span></p>', '<p>0</p>', '<p>-<span xss=removed>Ö2</span></p>', '<p>-2<span xss=removed>Ö2</span></p>', -1, -1, 4, -1, -1, 0, '', '', '', '', '', 'C', '', 1592554159, 1592554159),
(764, 20, 18, 38, 0, '', '', '<p>a(b+c)=152</p>\r\n<p>b(a+c)=162</p>\r\n<p>c(a+b)=170</p>\r\n<p>Maka nilai abc adalah ...</p>', '<p>680</p>', '<p>700</p>', '<p>720</p>', '<p>750</p>', '<p>800</p>', -1, -1, 4, -1, -1, 0, '', '', '', '', '', 'C', '', 1592554263, 1592554263),
(765, 20, 18, 38, 0, 'edd57e9856513c4cc5e5868c674448d3.jpg', 'image/jpeg', '', '<p>-2</p>', '<p>-1</p>', '<p>0</p>', '<p>1</p>', '<p>2</p>', -1, -1, -1, 4, -1, 0, '', '', '', '', '', 'D', '', 1592554731, 1592554731),
(766, 20, 18, 38, 0, '', '', '<p>a<sup>b</sup>=8<sup>4</sup>(4<sup>6</sup>-2<sup>11</sup>), jika a dan b adalah bilangan bulat, maka nilai a+b = ...</p>', '<p>21</p>', '<p>22</p>', '<p>23</p>', '<p>25</p>', '<p>24</p>', -1, -1, -1, 4, -1, 0, '', '', '', '', '', 'D', '', 1592555112, 1592555112),
(767, 20, 18, 38, 0, '', '', '<p>Diberikan</p>\r\n<p>x + y = 9xy</p>\r\n<p>x + z = 10xz</p>\r\n<p>y + z = 11yz</p>\r\n<p>dimana x, y, z ≠ 0, maka <sup>1</sup>/<sub>xyz </sub>= ...</p>', '<p>115</p>', '<p>120</p>', '<p>125</p>', '<p>130</p>', '<p>135</p>', -1, 4, -1, -1, -1, 0, '', '', '', '', '', 'B', '', 1592555438, 1592555687),
(768, 20, 18, 38, 0, '4af7d19ef78b73762e4779c04f2a4737.jpg', 'image/jpeg', '', '<p>-2</p>', '<p>-1</p>', '<p>0</p>', '<p>1</p>', '<p>2</p>', -1, 4, -1, -1, -1, 0, '', '', '', '', '', 'B', '', 1592555751, 1592555751),
(769, 20, 18, 38, 0, '916bfb4514143c66d3a4dab4e91c5274.jpg', 'image/jpeg', '', '<p>abc</p>', '<p>a<sup>2</sup>b</p>', '<p>a<sup>2</sup>b<sup>3</sup></p>', '<p>a<sup>3</sup>b<sup>2</sup></p>', '<p>a<sup>5</sup>b</p>', -1, -1, -1, 4, -1, 0, '', '', '', '', '', 'D', '', 1592555984, 1592555984),
(770, 20, 18, 38, 0, '', '', '<p>Misalkan α dan β adalah akar-akar persamaan 2x<sup>2 </sup>- x - 5 = 0. Persamaan kuadrat baru yang akar-akarnya 2α+1 dan 2β+1 adalah ...</p>\r\n<p class=\"MsoNormal\"> </p>\r\n<p class=\"MsoNormal\"> </p>\r\n<p class=\"MsoNormal\"> </p>', '<p>x<sup>2 </sup>- 4x - 21 = 0</p>', '<p>x<sup>2 </sup>- 4x - 19 = 0</p>', '<p>x<sup>2 </sup>- 4x + 20 = 0</p>', '<p>x<sup>2 </sup>- 3x + 20 = 0</p>', '<p>x<sup>2 </sup>- 3x - 8 = 0</p>', -1, -1, -1, -1, 4, 0, '', '', '', '', '', 'E', '', 1592558198, 1592558198),
(771, 20, 18, 38, 0, '', '', '<p>Tempat parkir seluas 500 m<sup>2</sup> hanya mampu menampung 30 bus dan mobil. TIap mobil membutuhkan tempat seluas 5 m<sup>2</sup> dan bus menempati m<sup>2</sup>. Model matematika yang memenuhi persamaan tersebut adalah ...</p>', '<p>x + y ≤ 30, x + 5y ≤ 100, x ≥0, y ≥ 0</p>', '<p>x - y ≤ 30, x + 5y ≤ 100, x ≥0, y ≥ 0</p>', '<p>x + y ≤ 30, x - 5y ≤ 100, x ≥0, y ≥ 0</p>', '<p>x + y ≤ 30, x + 5y ≥ 100, x ≥ 0, y ≥ 0</p>', '<p>x + y ≥ 30, x - 5y ≤ 100, x ≥ 0, y ≥ 0</p>', 4, -1, -1, -1, -1, 0, '', '', '', '', '', 'A', '<p><span xss=removed>Banyak bus = x</span><br xss=removed><br xss=removed><span xss=removed>Banyak mobil = y</span><br xss=removed><br xss=removed><span xss=removed>Maka modelnya adalah</span><br xss=removed><br xss=removed><span xss=removed>* jumlah bus dan mobil tidak melebihi kapasitas yaitu 30 → x + y ≤ 30</span><br xss=removed><br xss=removed><span xss=removed>* jumlah lahan dari luas mobil dan bus tidak lebih dari 500 → 5x + 25y ≤ 500 ≡ x + 5y ≤ 100</span><br xss=removed><br xss=removed><span xss=removed>* jumlah bus dan mobil tidak boleh negatif → x ≥ 0, y ≥ 0</span></p>', 1592558470, 1592558506),
(772, 20, 18, 38, 0, '', '', '<p>Harga tiket kelas I dalam final Piala Presiden 2018 adalah Rp500.000,00. Panitia menyediakan 8 baris untuk kelas I, dengan rincian pada baris pertama terdapat 8 kursi, baris kedua 10 kursi, pada baris ketiga 12 kursi dan seterusnya. Jika kursi terisi semua pada kelas tersebut, maka pendapatan yang diterima dari kelas I adalah....</p>', '<p>Rp 60.000.000,00</p>', '<p>Rp 70.000.000,00</p>', '<p>Rp 80.000.000,00</p>', '<p>Rp 85.000.000,00</p>', '<p>Rp 90.000.000,00</p>', 4, -1, -1, -1, -1, 0, '', '', '', '', '', 'A', '<p><span xss=removed>bisa dikerjakan dengan Sn, tapi manual juga bisa untuk mencari total banyaknya kursi</span><br xss=removed><span xss=removed>total banyak kursi =120</span><br xss=removed><span xss=removed>Maka jumlah pendapatan 120×500.000 = Rp60.000.000,00.</span></p>', 1592562465, 1592562465),
(773, 20, 18, 38, 0, 'de92a874e6df0aa3d92337da09dd7372.jpg', 'image/jpeg', '', '<p><sup>5</sup><span xss=removed>Ö11</span></p>', '<p><sup>5</sup><span xss=removed>Ö12</span></p>', '<p><sup>5</sup><span xss=removed>Ö13</span></p>', '<p><sup>5</sup><span xss=removed>Ö14</span></p>', '<p><sup>5</sup><span xss=removed>Ö15</span></p>', -1, 4, -1, -1, -1, 0, '', '', '', '', '', 'B', '', 1592562684, 1592562684),
(774, 20, 18, 38, 0, '74455005dc4c9bbf7c357097360d9ccf.jpg', 'image/jpeg', '', '<p>9</p>', '<p>18</p>', '<p>27</p>', '<p>81</p>', '<p>243</p>', -1, 4, -1, -1, -1, 0, '', '', '', '', '', 'B', '', 1592562941, 1592562941),
(775, 20, 18, 38, 0, '4cce5c0d47996e2caeb0042f5d96787c.jpg', 'image/jpeg', '', '<p>9</p>', '<p>10</p>', '<p>90</p>', '<p>100</p>', '<p>110</p>', -1, -1, 4, -1, -1, 0, '', '', '', '', '', 'C', '<p><span xss=removed>Kuadratkan kedua ruas sehingga menjadi</span><br xss=removed><br xss=removed><span xss=removed>x+x+x+x+...−−−−−√−−−−−−−−−−√−−−−−−−−−−−−−−−√=100</span><br xss=removed><br xss=removed><span xss=removed>Subtitusi persamaan x+x+x+x+...−−−−−√−−−−−−−−−−√−−−−−−−−−−−−−−−√−−−−−−−−−−−−−−−−−−−−√=10</span><br xss=removed><span xss=removed>ke persamaan</span><br xss=removed><br xss=removed><span xss=removed>x+x+x+x+...−−−−−√−−−−−−−−−−√−−−−−−−−−−−−−−−√=100</span><br xss=removed><span xss=removed>sehingga didapat</span><br xss=removed><br xss=removed><span xss=removed>x+10=100</span><br xss=removed><br xss=removed><span xss=removed>x=90</span></p>', 1592563102, 1592563102),
(776, 20, 18, 38, 0, 'c45c7a78ae379c00d21d07ccf4468696.jpg', 'image/jpeg', '', '<p>1</p>', '<p>2</p>', '<p>3</p>', '<p>4</p>', '<p>5</p>', -1, -1, -1, -1, 4, 0, '', '', '', '', '', 'E', '<div class=\"freebirdFormeditorViewAssessmentFeedbackFeedbackBox itemHideInactive\" xss=removed data-feedback-type=\"YZ04zc\">\r\n<div class=\"freebirdFormeditorViewAssessmentFeedbackFeedbackBoxContent\" role=\"group\">\r\n<div id=\"c1931\" class=\"freebirdFormeditorViewAssessmentFeedbackFeedbackText\" xss=removed>Misalkan 20+20+20+20+...−−−−−−√−−−−−−−−−−−√−−−−−−−−−−−−−−−−−√−−−−−−−−−−−−−−−−−−−−−−−√=x<br><br>Kuadratkan kedua ruas sehingga didapat<br>20+20+20+20+...−−−−−−√−−−−−−−−−−−√−−−−−−−−−−−−−−−−−√=x2<br><br>Subtitusikan persamaan 20+20+20+20+...−−−−−−√−−−−−−−−−−−√−−−−−−−−−−−−−−−−−√−−−−−−−−−−−−−−−−−−−−−−−√=x<br>ke persamaan<br><br>20+20+20+20+...−−−−−−√−−−−−−−−−−−√−−−−−−−−−−−−−−−−−√=x2<br>sehingga didapat<br><br>20+x=x2<br><br>x2−x−20=0<br><br>(x−5)(x+4)=0<br><br>x=−4<br>tidak memenuhi, maka x=5<br>sehingga<br><br>20+20+20+20+...−−−−−−√−−−−−−−−−−−√−−−−−−−−−−−−−−−−−√−−−−−−−−−−−−−−−−−−−−−−−√=5</div>\r\n</div>\r\n</div>', 1592563288, 1592563288),
(777, 20, 18, 38, 0, 'eafd4c08dd3e867fd7ff06d441484006.jpg', 'image/jpeg', '', '<p>3</p>', '<p>4</p>', '<p>5</p>', '<p>6</p>', '<p>7</p>', -1, -1, -1, 4, -1, 0, '', '', '', '', '', 'D', '<p><span xss=removed>Misalkan</span><br xss=removed><span xss=removed>3243243...−−−√−−−−−−√−−−−−−−−−√−−−−−−−−−−−−√−−−−−−−−−−−−−−−√=x</span><br xss=removed><br xss=removed><br xss=removed><span xss=removed>Kuadratkan kedua ruas sehingga</span><br xss=removed><span xss=removed>3243243...−−−√−−−−−−√−−−−−−−−−√−−−−−−−−−−−−√=x2</span><br xss=removed><br xss=removed><br xss=removed><span xss=removed>Kuadratkan lagi kedua ruas sehingga</span><br xss=removed><span xss=removed>9×243243...−−−√−−−−−−√−−−−−−−−−√=x4</span><br xss=removed><br xss=removed><span xss=removed>Subtitusikan persamaan 3243243...−−−√−−−−−−√−−−−−−−−−√−−−−−−−−−−−−√−−−−−−−−−−−−−−−√=x ke persamaan</span><br xss=removed><br xss=removed><span xss=removed>9×243243...−−−√−−−−−−√−−−−−−−−−√=x4 sehingga didapat</span><br xss=removed><br xss=removed><span xss=removed>9×24x=x4</span><br xss=removed><br xss=removed><span xss=removed>216x=x4</span><br xss=removed><span xss=removed>x4−216x=0</span><br xss=removed><span xss=removed>x(x−6)(x2+6x+36)=0</span><br xss=removed><br xss=removed><span xss=removed>x=0 tidak memenuhi, x2+6x+36=0 tidak memiliki solusi real, maka yang memenuhi hanya x−6=0</span><br xss=removed><br xss=removed><span xss=removed>x=6</span></p>', 1592573997, 1592573997);
INSERT INTO `tbl_soal_tryout` (`id_soal`, `dosen_id`, `matkul_id`, `id_soal_paket`, `bobot`, `file`, `tipe_file`, `soal`, `opsi_a`, `opsi_b`, `opsi_c`, `opsi_d`, `opsi_e`, `bobot_a`, `bobot_b`, `bobot_c`, `bobot_d`, `bobot_e`, `bobot_kosong`, `file_a`, `file_b`, `file_c`, `file_d`, `file_e`, `jawaban`, `pembahasan`, `created_on`, `updated_on`) VALUES
(778, 20, 18, 38, 0, '0e74cabadfbc5571c2155f47ad97ada0.jpg', 'image/jpeg', '<p>Di antara beberapa kemungkinan di bawah ini, salah satu pilihan yang tidak Benar adalah ....</p>', '<p>Lulu duduk di antara Roni dan Gagah.</p>', '<p>Lisa dan Dadang dipisahkan oleh Aldo.</p>', '<p>Mamad duduk di antara Tio dan Lisa.</p>', '<p>Dadang duduk di antara Gagah dan Tio.</p>', '', -1, -1, -1, 4, -1, 0, '', '', '', '', '', 'D', '', 1592574462, 1592574462),
(779, 20, 18, 38, 0, '597fe729fe329e40c5b8ceb5ff785eef.jpg', 'image/jpeg', '', '<p>Bila Mamad dipisahkan oleh 2 kursi dengan Dadang, pernyataan di bawah ini yang benar adalah ....</p>', '<p>Mamad dan Roni dipisahkan oleh 2 kursi.</p>', '<p>Mamad dan Aldo duduk berseberangan. `</p>', '<p>Dadang dan Tio duduk berhadapan.</p>', '<p>Lisa duduk di sebelah Gagah.</p>', -1, -1, 4, -1, -1, 0, '', '', '', '', '', 'C', '', 1592574546, 1592574546),
(780, 20, 18, 38, 0, '5292d566417ec6ad0b98830946c702ef.jpg', 'image/jpeg', '<p>Pernyataan di bawah ini yang benar adalah ....</p>', '<p>Dadang duduk di atara Gagah dan Tio.</p>', '<p>Roni duduk dipisahkan 2 kursi dengan Tio.</p>', '<p>Roni dan Dadang duduk tepat berhadapan.</p>', '<p>Tio duduk di seberang Roni.</p>', '', -1, 4, -1, -1, -1, 0, '', '', '', '', '', 'B', '', 1592574808, 1592574808),
(781, 20, 18, 38, 0, 'ad8f876c6915dffb3ef15cdf60d61c25.jpg', 'image/jpeg', '<p>Bila Mamad dan Roni duduk berhadapan, pernyataan di bawah ini yang benar adalah ....</p>', '<p>Gagah duduk berhadapan dengan Dadang.</p>', '<p>Gagah duduk di sebelah Mamad.</p>', '<p>Lisa duduk di sebelah Dadang.</p>', '<p>Roni duduk di antara Lulu dan Dadang</p>', '', -1, -1, -1, 4, -1, 0, '', '', '', '', '', 'D', '', 1592574904, 1592574904),
(782, 20, 18, 38, 0, 'ba59b094a232722ddfb18038a297be62.jpg', 'image/jpeg', '<p>Dani dan Putra tidak pernah mendapat giliran membersihkan kelas bersama-sama,Kecuali....</p>', '<p>Senin</p>', '<p>Selasa</p>', '<p>Rabu</p>', '<p>Kamis</p>', '', 4, -1, -1, -1, -1, 0, '', '', '', '', '', 'A', '', 1592575511, 1592575511),
(783, 20, 18, 38, 0, '', '', '<p>Ita dan Dani mendapat giliran bekerjasama membersihkan kelas pada hari ....</p>', '<p>Selasa dan Kamis.</p>', '<p>Senin dan Selasa.</p>', '<p>Senin dan Kamis.</p>', '<p>Rabu dan Kamis.</p>', '', -1, -1, 4, -1, -1, 0, '', '', '', '', '', 'C', '', 1592575569, 1592575569),
(784, 20, 18, 38, 0, 'b246233b3810f0a32fe3a731dc6af528.jpg', 'image/jpeg', '<p>Siswa yang membersihkan kelas pada hari Selasa adalah ....</p>', '<p>Dani, Lani dan Ita.</p>', '<p>Putra, Ita dan Dani.</p>', '<p>Lani, Ita dan Putra.</p>', '<p>Lani, Ayu dan Dani.</p>', '', -1, -1, -1, 4, -1, 0, '', '', '', '', '', 'D', '', 1592575846, 1592575846),
(785, 20, 18, 38, 0, '750325bf494e4c59141efa71e2857c24.jpg', 'image/jpeg', '<p>Judul yang tepat untuk teks tersebut adalah </p>', '<p>Obat Kanker Liforma Diproduksi diDalam Negeri dengan Harga Terjangkau.</p>', '<p>Bendamustine Obat Kanker yang Ditemukan Kembali</p>', '<p>Pengobatan Bendamustin Harapan Bagi Penderita Kanker</p>', '<p>Khasiat Bendamustine Bagi Penderita Kanker</p>', '', 4, -1, -1, -1, -1, 0, '', '', '', '', '', 'A', '', 1592576090, 1592576090),
(786, 20, 18, 38, 0, '', '', '<p>Bendamustine sebenarnya sudah ditemukan sejak 50 tahun lalu namun aksesnya hanya terbatas di negara…</p>', '<p>Jepang</p>', '<p>Jerman</p>', '<p>Inggris</p>', '<p>Perancis</p>', '', -1, 4, -1, -1, -1, 0, '', '', '', '', '', 'B', '', 1592576137, 1592576137),
(787, 20, 18, 38, 0, 'b145c530851b273d6d18fe7ec01db9b9.jpg', 'image/jpeg', '<p>Pernyataan yang sesuai pada bacaan 1 diatas adalah...</p>', '<p>Pengobatan dengan bendamustine memberi harapan baru pada pasien yang membutuhkan, yaitu pasienlimfoma hodgin</p>', '<p>Sebelum melakukan suntikan intravena atau kemoterapi, Pasien harus melakukan pemeriksaan genetikterlebih dahulu.</p>', '<p>Penggunaan bendamustine mempunyai efek samping yang membahayakan ginjal dan jantung</p>', '<p>Harga bendamustine lebih terjangkau produksi obat ini bisa membantu pasien Indoensia dan mengurangi ketergantungan pada obat impor.</p>', '', -1, -1, -1, 4, -1, 0, '', '', '', '', '', 'D', '', 1592576221, 1592576221),
(788, 20, 18, 42, 0, '163a1b8260b4cc15fbde411ab34622a0.jpg', 'image/jpeg', '', '<p><sup>31</sup>/<sub>18</sub></p>', '<p><sup>32</sup>/<sub>18</sub></p>', '<p><sup>33</sup>/<sub>18</sub></p>', '<p><sup>31</sup>/<sub>9</sub></p>', '<p><sup>33</sup>/<sub>9</sub></p>', 4, -1, -1, -1, -1, 0, '', '', '', '', '', 'A', '<p><span xss=removed>dijabarin, nanti ketemu..</span><br xss=removed><span xss=removed>2^5-1/(akar 2)^5 x (akar2)^3/(2^3:1)</span><br xss=removed><span xss=removed>= 32-1/2(9)</span><br xss=removed><span xss=removed>= 31/18</span></p>', 1592578399, 1592578399),
(789, 20, 18, 42, 0, '6aa160e85d1d9e6f582ff69941bcf70c.jpg', 'image/jpeg', '', '<p>A</p>', '<p>B</p>', '<p>C</p>', '<p>D</p>', '<p>E</p>', 4, -1, -1, -1, -1, 0, '', '', '', '', '', 'A', '<p><span xss=removed>kuadratkan kedua ruas dari persamaan di saol, sehingga:</span><br xss=removed><span xss=removed>a+3 = a+ 2akar a + 1</span><br xss=removed><span xss=removed>2 = 2 akar a</span><br xss=removed><span xss=removed>akar a = 1</span><br xss=removed><span xss=removed>kuadratin kedua ruas, maka</span><br xss=removed><span xss=removed>a= 1</span><br xss=removed><br xss=removed><span xss=removed>maka akar a+ 1 = akar 2</span></p>', 1592604034, 1592604034),
(790, 20, 18, 42, 0, 'e1f63a2da966360008501be7bc28860e.jpg', 'image/jpeg', '', '<p>A</p>', '<p>B</p>', '<p>C</p>', '<p>D</p>', '<p>E</p>', -1, 4, -1, -1, -1, 0, '', '', '', '', '', 'B', '<p><span xss=removed>dijabarin, nanti ketemu:</span><br xss=removed><span xss=removed>-2x + 5 = 2+x/2</span><br xss=removed><span xss=removed>-4x + 10 = 2+x</span><br xss=removed><span xss=removed>-5x = -8</span><br xss=removed><span xss=removed>x = 8/5</span></p>', 1592604303, 1592604303),
(791, 20, 18, 42, 0, '919bd4a787d5dd7f88df9f84a2c38c83.jpg', 'image/jpeg', '', '<p>A</p>', '<p>B</p>', '<p>C</p>', '<p>D</p>', '<p>E</p>', -1, -1, -1, 4, -1, 0, '', '', '', '', '', 'D', '<div id=\"c1674\" class=\"freebirdFormeditorViewAssessmentFeedbackFeedbackText\" xss=removed>dijabarin, samain bilangan dikedua ruas, maka pangkatnya diperoleh:<br>3x-9 = 3 (2014)<br>x-3 = 2014<br>x = 2017</div>\r\n<p> </p>', 1592604653, 1592604653),
(792, 20, 18, 42, 0, '24645b4a1d5e8a7119d6bd11902b0c06.jpg', 'image/jpeg', '', '<p>A</p>', '<p>B</p>', '<p>C</p>', '<p>D</p>', '<p>E</p>', -1, -1, -1, 4, -1, 0, '', '', '', '', '', 'D', '<p><span xss=removed>diajabarin</span><br xss=removed><span xss=removed>a/b = (a<sup>b</sup>)<sup>3</sup></span><br xss=removed><span xss=removed>a/b = (ab)<sup>3</sup> .......... karena ab = a<sup>b</sup></span><br xss=removed><span xss=removed>a = a<sup>3</sup>b<sup>4</sup></span><br xss=removed><span xss=removed>a<sup>-2</sup> = b<sup>4</sup></span><br xss=removed><span xss=removed>a = b^-2</span><br xss=removed><br xss=removed><span xss=removed>terus ganti a jadi b^-2</span><br xss=removed><span xss=removed>trus ketemu b= 1/2</span><br xss=removed><span xss=removed>a = b^-2 = (2^-1)-2 = 4</span></p>', 1592604912, 1592604912),
(793, 20, 18, 42, 0, 'ec3d0fdfdb2b5c184a450568b247b281.jpg', 'image/jpeg', '', '<p>A</p>', '<p>B</p>', '<p>C</p>', '<p>D</p>', '<p>E</p>', -1, -1, -1, 4, -1, 0, '', '', '', '', '', 'D', '<p><span xss=removed>masing2 dikali penyebut, terus ketemu:</span><br xss=removed><span xss=removed>-1+akar 2</span><br xss=removed><span xss=removed>-2 + akar 3</span><br xss=removed><span xss=removed>sehingga diperoleh</span><br xss=removed><span xss=removed>-1+akar2 - akar2 + akar 3 + .... - akar 63 + akar 64</span><br xss=removed><span xss=removed>= -1 + akar 64</span><br xss=removed><span xss=removed>= 7</span></p>', 1592606503, 1592606503),
(794, 20, 18, 42, 0, 'd3da3416399dd3d8434721eb23cd8e9a.jpg', 'image/jpeg', '', '<p>A</p>', '<p>B</p>', '<p>C</p>', '<p>D</p>', '<p>E</p>', -1, -1, -1, -1, 4, 0, '', '', '', '', '', 'E', '<p><span xss=removed>ingat ke persamaan (a-b)(a+b) = a^2 - b^2</span><br xss=removed><span xss=removed>lalu dijabarin....</span><br xss=removed><span xss=removed>diperoleh</span><br xss=removed><span xss=removed>2 (12-10) = 4</span></p>', 1592607372, 1592607372),
(795, 20, 18, 42, 0, '', '', '', '<p>A</p>', '<p>B</p>', '<p>C</p>', '<p>D</p>', '<p>E</p>', 4, -1, -1, -1, -1, 0, 'f61615fb1a72f3e13789a3648c866351.jpg', '', '', '', '', 'A', '<p><span xss=removed>dijabarin ketemu 2^m = akar 3</span><br xss=removed><span xss=removed>jadi</span><br xss=removed><span xss=removed>8^m = 2^2m</span><br xss=removed><span xss=removed>= (2^m)^3</span><br xss=removed><span xss=removed>= akar 3^3</span><br xss=removed><span xss=removed>=3 akar 3</span></p>', 1592607479, 1592607479),
(796, 20, 18, 42, 0, '1df9105cd596539cbcdfadc144ea0ae3.jpg', 'image/jpeg', '', '<p>A</p>', '<p>B</p>', '<p>C</p>', '<p>D</p>', '<p>E</p>', -1, -1, -1, 4, -1, 0, '', '', '', '', '', 'D', '<p><span xss=removed>a^b = 2^20 - 2^19</span><br xss=removed><span xss=removed>= 2^19 (2-1)</span><br xss=removed><span xss=removed>= 2^19</span><br xss=removed><span xss=removed>jadi nilai a = 2 dan bilai b = 19</span><br xss=removed><span xss=removed>a+b= 21</span></p>', 1592607546, 1592607546),
(797, 20, 18, 42, 0, '9e9cfee053fc6daaf9dc6ef054437583.jpg', 'image/jpeg', '', '<p>A</p>', '<p>B</p>', '<p>C</p>', '<p>D</p>', '<p>E</p>', -1, 4, -1, -1, -1, 0, '', '', '', '', '', 'B', '<p><span xss=removed>jabarin aja</span><br xss=removed><span xss=removed>ketemu:</span><br xss=removed><span xss=removed>3x^2 - 9x - 12 = x^2 - 2x - 3</span><br xss=removed><span xss=removed>2x^2 - 7x - 9 = 0</span><br xss=removed><span xss=removed>(2x-9)(x+1)=0</span><br xss=removed><span xss=removed>x = 9/2 atau x = -1</span></p>', 1592607734, 1592607734),
(798, 20, 18, 42, 0, '5882a816875b47f864456c1386950551.jpg', 'image/jpeg', '', '<p>A</p>', '<p>B</p>', '<p>C</p>', '<p>D</p>', '<p>E</p>', 4, -1, -1, -1, -1, 0, '', '', '', '', '', 'A', '<p><span xss=removed>sama kan bilangannya, sehingga dapet pangkatnya:</span><br xss=removed><span xss=removed>2 (-2/3 + x/3) = x+4 ........... kali 3 kedua ruas</span><br xss=removed><span xss=removed>-4 + 2x = 3x +12</span><br xss=removed><span xss=removed>x = -16</span></p>', 1592607806, 1592607806),
(799, 20, 18, 42, 0, 'f9b39e771ca15ee7a01e35d0da308e32.jpg', 'image/jpeg', '', '<p>A</p>', '<p>B</p>', '<p>C</p>', '<p>D</p>', '<p>E</p>', 4, -1, -1, -1, -1, 0, '', '', '', '', '', 'A', '<p><span xss=removed>0,3^2(1/2(x-3)) = 0,3^3x+1</span><br xss=removed><span xss=removed>2( 1/2(x-3)) = 3x + 1</span><br xss=removed><span xss=removed>x – 3 = 3x + 1</span><br xss=removed><span xss=removed>-4 = 2x</span><br xss=removed><span xss=removed>-2 = x</span></p>', 1592607888, 1592607888),
(800, 20, 18, 42, 0, '6aeaf737641e7052471e85201af72a3f.jpg', 'image/jpeg', '', '<p>A</p>', '<p>B</p>', '<p>C</p>', '<p>D</p>', '<p>E</p>', 4, -1, -1, -1, -1, 0, '', '', '', '', '', 'A', '<p><span xss=removed>(x^1/2 + x^-1/2)2 = x + 2. x^1/2. x-1/2 + x^-1</span><br xss=removed><span xss=removed>3^2 = x + 2 + x^-1</span><br xss=removed><span xss=removed>7 = x + x^-1</span></p>', 1592607994, 1592607994),
(801, 20, 18, 42, 0, '69989ccc45172146de8244e4b380bad3.jpg', 'image/jpeg', '', '<p>A</p>', '<p>B</p>', '<p>C</p>', '<p>D</p>', '<p>E</p>', -1, -1, -1, -1, 4, 0, '', '', '', '', '', 'E', '<p><span xss=removed>3x^0,4 – 3^2 (3^-1)0,6 = 0</span><br xss=removed><span xss=removed>3x^0,4 – 3^2.3^-0,6 = 0</span><br xss=removed><span xss=removed>3x^0,4 – 3^1,4 = 0</span><br xss=removed><span xss=removed>3x^0,4 = 3^1,4</span><br xss=removed><span xss=removed>3. x^0,4 = 3.3^0,4</span><br xss=removed><span xss=removed>x^0,4 = 3^0,4</span><br xss=removed><span xss=removed>x = 3</span><br xss=removed><span xss=removed>sehingga 3x – x^2 = 3.3 – 3^2 = 0</span></p>', 1592608076, 1592608076),
(802, 20, 18, 42, 0, 'f22064f4b4ac9a7b668394b88fea6bba.jpg', 'image/jpeg', '', '<p>A</p>', '<p>B</p>', '<p>C</p>', '<p>D</p>', '<p>E</p>', 4, -1, -1, -1, -1, 0, '', '', '', '', '', 'A', '<p><span xss=removed>= 2^n + 2 . 2^-(n – 1) . 6^n – 4 . 6^-(n – 1)</span><br xss=removed><span xss=removed>= 2^n + 2 . 2^-n + 1 . 6^n – 4 . 6^-n + 1</span><br xss=removed><span xss=removed>= 2^3 . 6^-3</span><br xss=removed><span xss=removed>= 2.2.2/6.6.6</span><br xss=removed><span xss=removed>= 1/3.3.3</span><br xss=removed><span xss=removed>= 1/27</span></p>', 1592608224, 1592608224),
(803, 20, 18, 42, 0, '853de254930cacb1830c16dd7a1386b7.jpg', 'image/jpeg', '', '<p>A</p>', '<p>B</p>', '<p>C</p>', '<p>D</p>', '<p>E</p>', -1, -1, -1, -1, 4, 0, '', '', '', '', '', 'E', '<p><span xss=removed>-3(3x – 7) = 1 – x</span><br xss=removed><span xss=removed>-9x + 21 = 1 – x</span><br xss=removed><span xss=removed>8x = 20</span><br xss=removed><span xss=removed>x = 5/2</span></p>', 1592608395, 1592608395),
(804, 20, 18, 42, 0, '605fcf414f2d3ef5baf685b7ac7c01c0.jpg', 'image/jpeg', '', '<p>A</p>', '<p>B</p>', '<p>C</p>', '<p>D</p>', '<p>E</p>', -1, -1, -1, 4, -1, 0, '', '', '', '', '', 'D', '<p><span xss=removed>3^x-1/2 = 3^2x – 4</span><br xss=removed><span xss=removed>x - 1/2 = 2x – 4</span><br xss=removed><span xss=removed>x = 3.5</span></p>', 1592608459, 1592608459),
(805, 20, 18, 42, 0, '0a10a17b5d739c0754802f8216de40bc.jpg', 'image/jpeg', '', '<p>A</p>', '<p>B</p>', '<p>C</p>', '<p>D</p>', '<p>E</p>', -1, -1, 4, -1, -1, 0, '', '', '', '', '', 'C', '', 1592608554, 1592608554),
(806, 20, 18, 42, 0, '3737136ac1acfa572aaf27fd9e4c72fb.jpg', 'image/jpeg', '', '<p>A</p>', '<p>B</p>', '<p>C</p>', '<p>D</p>', '<p>E</p>', 4, -1, -1, -1, -1, 0, '', '', '', '', '', 'A', '', 1592608630, 1592608630),
(807, 20, 18, 42, 0, '07487d0176210d461994375138d92bf2.jpg', 'image/jpeg', '', '<p>A</p>', '<p>B</p>', '<p>C</p>', '<p>D</p>', '<p>E</p>', 4, -1, -1, -1, -1, 0, '', '', '', '', '', 'A', '', 1592608704, 1592608704),
(808, 20, 18, 42, 0, 'd14fc45eca27b71885ac14b67f509fc8.jpg', 'image/jpeg', '', '<p>A</p>', '<p>B</p>', '<p>C</p>', '<p>D</p>', '<p>E</p>', -1, -1, -1, 4, -1, 0, '', '', '', '', '', 'D', '', 1592608849, 1592608849),
(809, 20, 18, 42, 0, 'e8ba5ec697f2923a2e8bf24f524beb09.jpg', 'image/jpeg', '', '<p>A</p>', '<p>B</p>', '<p>C</p>', '<p>D</p>', '<p>E</p>', -1, -1, 4, -1, -1, 0, '', '', '', '', '', 'C', '', 1592608957, 1592608957),
(810, 20, 18, 42, 0, '619f9b9914f1d1cfd608c1e43c9cf666.jpg', 'image/jpeg', '', '<p>A</p>', '<p>B</p>', '<p>C</p>', '<p>D</p>', '<p>E</p>', -1, -1, -1, 4, -1, 0, '', '', '', '', '', 'D', '<p><span xss=removed>D. c < b><br xss=removed><span xss=removed>a = (60+40) 99^99 = 100 x 99^99</span><br xss=removed><span xss=removed>b = 99 x 99^99</span><br xss=removed><span xss=removed>c = 90 x 99^99</span><br xss=removed><span xss=removed>∴ c < b></p>', 1592609010, 1592609010),
(811, 20, 18, 42, 0, 'd40c1cabeaf7b80a7ff2985dbc93c5f4.jpg', 'image/jpeg', '', '<p>A</p>', '<p>B</p>', '<p>C</p>', '<p>D</p>', '<p>E</p>', -1, 4, -1, -1, -1, 0, '', '', '', '', '', 'B', '<p><span xss=removed>Persamaan 9^3 + 9^3 + 9^3 = 9^3(1 +1 + 1) = 9^3(3) = 3^6(3) = 3^7</span><br xss=removed><span xss=removed>Sehingga 3^7 = 3^x</span><br xss=removed><span xss=removed>X= 7</span></p>', 1592609076, 1592609076),
(812, 20, 18, 42, 0, '0630594eaf7b34d8784114eb2c734eee.jpg', 'image/jpeg', '', '<p>A</p>', '<p>B</p>', '<p>C</p>', '<p>D</p>', '<p>E</p>', -1, -1, 4, -1, -1, 0, '', '', '', '', '', 'C', '', 1592609112, 1592609112),
(813, 20, 18, 42, 0, '3693b036f4e68c9a4de3567ca1d2efcc.jpg', 'image/jpeg', '', '<p>A</p>', '<p>B</p>', '<p>C</p>', '<p>D</p>', '<p>E</p>', 4, -1, -1, -1, -1, 0, '', '', '', '', '', 'A', '', 1592609158, 1592609158),
(814, 20, 18, 42, 0, '2f8813f907992a3abfc7a8767e472761.jpg', 'image/jpeg', '', '<p>A</p>', '<p>B</p>', '<p>C</p>', '<p>D</p>', '<p>E</p>', -1, -1, -1, 4, -1, 0, '', '', '', '', '', 'D', '<p>Kalikan dengan sekawan dari penyebutnya</p>', 1592758718, 1592758718),
(815, 20, 18, 42, 0, '78a4e1a9564bcb0461cf8b14274668ff.jpg', 'image/jpeg', '', '<p>A</p>', '<p>B</p>', '<p>C</p>', '<p>D</p>', '<p>E</p>', -1, 4, -1, -1, -1, 0, '', '', '', '', '', 'B', '', 1592759396, 1592759396),
(816, 20, 18, 42, 0, '258b077f6f5380513aaf357c389158d5.jpg', 'image/jpeg', '', '<p>A</p>', '<p>B</p>', '<p>C</p>', '<p>D</p>', '<p>E</p>', -1, -1, -1, -1, 4, 0, '', '', '', '', '', 'E', '<p>(25<sup>0,25</sup>)<sup>n</sup>=125</p>\r\n<p>5<sup>0,5n</sup>=5<sup>3</sup></p>\r\n<p>n=6</p>\r\n<p> </p>\r\n<p>(n-3)(n+2) = 3x8 = 24</p>', 1592761621, 1592761621),
(817, 20, 18, 42, 0, '', '', '<p>Jika 4<sup>x </sup>- 4<sup>x-1</sup> = 6, maka (2x)<sup>x</sup> = ...</p>', '<p>3</p>', '<p>3<span xss=removed>Ö3</span></p>', '<p>9</p>', '<p>9<span xss=removed>Ö3</span></p>', '<p>27</p>', -1, 4, -1, -1, -1, 0, '', '', '', '', '', 'B', '', 1592765254, 1592765254),
(818, 20, 18, 42, 0, '7305a64d87dcfd8970f26c6009108104.jpg', 'image/jpeg', '', '<p>A</p>', '<p>B</p>', '<p>C</p>', '<p>D</p>', '<p>E</p>', -1, -1, 4, -1, -1, 0, '', '', '', '', '', 'C', '<p>Kalikan dengan sekawan dari penyebutnya</p>', 1592765631, 1592765631),
(819, 20, 18, 42, 0, '722ef83f083080154927cdcff4220bfb.jpg', 'image/jpeg', '', '<p>A</p>', '<p>B</p>', '<p>C</p>', '<p>D</p>', '<p>E</p>', -1, 4, -1, -1, -1, 0, '', '', '', '', '', 'B', '', 1592843455, 1592843455),
(820, 20, 18, 42, 0, '4a9b0f0e70df747478dc186f1a815488.jpg', 'image/jpeg', '', '<p>A</p>', '<p>B</p>', '<p>C</p>', '<p>D</p>', '<p>E</p>', -1, -1, -1, 4, -1, 0, '', '', '', '', '', 'D', '', 1592843795, 1592843795),
(821, 20, 18, 42, 0, '520711ce959611103df96b7cd0351212.jpg', 'image/jpeg', '', '<p>A</p>', '<p>CB</p>', '<p>C</p>', '<p>D</p>', '<p>E</p>', -1, 4, -1, -1, -1, 0, '', '', '', '', '', 'B', '', 1592844569, 1592844569),
(822, 20, 18, 42, 0, '', '', '<p>Jika 4<sup>m+1</sup> + 4<sup>m</sup> = 15, maka 8<sup>m</sup> = ...</p>', '<p>3<span xss=removed>Ö3</span></p>', '<p>2<span xss=removed>Ö3</span></p>', '<p><span xss=removed>Ö3</span></p>', '<p>3</p>', '<p>6</p>', 4, -1, -1, -1, -1, 0, '', '', '', '', '', 'A', '', 1592845079, 1592845079),
(823, 20, 18, 42, 0, 'd694d12100dd5036b6186d28bea0ef07.jpg', 'image/jpeg', '', '<p>A</p>', '<p>B</p>', '<p>C</p>', '<p>D</p>', '<p>E</p>', -1, -1, -1, 4, -1, 0, '', '', '', '', '', 'D', '', 1592845635, 1592845635),
(824, 20, 18, 42, 0, '', '', '<p>Akar-akar persamaan 32<sup>x+1</sup> - 28.3<sup>x</sup> + 9 = 0 adalah x<sub>1</sub> dan x<sub>2</sub>. Jika x<sub>1</sub> > x<sub>2</sub>, maka nilai 3x<sub>1</sub> - x<sub>2</sub> = ...</p>', '<p>-5</p>', '<p>-1</p>', '<p>4</p>', '<p>5</p>', '<p>7</p>', -1, -1, -1, -1, 4, 0, '', '', '', '', '', 'E', '<p>Misal 3<sup>x</sup> = p, maka persamaan menjadi 3p<sup>2</sup>-28p+9 = 0</p>\r\n<p>&lt;=> (3p-1)(p-9)=0</p>\r\n<p>&lt;=> p=1/3 atau 9</p>\r\n<p>&lt;=> karena p = 3<sup>x</sup>, maka x<sub>1</sub> =2, x<sub>2</sub> = -1.</p>\r\n<p>sehingga 3x<sub>1</sub>-x<sub>2</sub> = 7</p>', 1592846099, 1592846099),
(825, 20, 18, 42, 0, '49daf8d0ef4fcbc1a36edecb93626808.jpg', 'image/jpeg', '', '<p>A</p>', '<p>B</p>', '<p>C</p>', '<p>D</p>', '<p>E</p>', -1, -1, -1, -1, 4, 0, '', '', '', '', '', 'E', '', 1592846888, 1592846888),
(826, 20, 18, 42, 0, '4d9ad372a4c601de9e4d5af70dd4b1ac.jpg', 'image/jpeg', '', '<p>A</p>', '<p>B</p>', '<p>C</p>', '<p>D</p>', '<p>E</p>', -1, -1, -1, -1, 4, 0, '', '', '', '', '', 'E', '', 1592847125, 1592847125),
(827, 20, 18, 42, 0, 'bd58a37fd04a6440a516c0f82134d966.jpg', 'image/jpeg', '', '<p>A</p>', '<p>B</p>', '<p>C</p>', '<p>D</p>', '<p>E</p>', -1, 4, -1, -1, -1, 0, '', '', '', '', '', 'B', '', 1592847991, 1592847991),
(828, 20, 18, 42, 0, '3a3e4f2459f8f7d67525f9a30d057728.jpg', 'image/jpeg', '<p>Pernyataan yang tidak sesuai dengan bacaan  di atas adalah…</p>', '<p>Pameran lukisan dan gambar bertajuk “Geneviève Couteau: The Orient and Beyond” digelar di Galeri Nasional Indonesia.</p>', '<p>Pameran ini membuktikan adanya perupa lain di luar seniman-seniman ikonik tahun 30-an hingga 50-an seperti antara lain Walter Spies, Rudolf Bonnet, Le Mayeur, dan Blanco.</p>', '<p>Geneviève Couteau yang merupakan seorang penulis dan kolumnis dibesarkan di tengah keluarga trader saham yang ulet</p>', '<p>Pada tahun 1952, Geneviève Couteau tampil sebagai pelukis terkemuka di Paris dan dianugerahi piagam bergengsi Prix Lafont Noir et Blanc.</p>', '', -1, -1, 4, -1, -1, 0, '', '', '', '', '', 'C', '', 1592848440, 1592848440),
(829, 20, 18, 42, 0, '08701dd7f3fd957ca4894cbcf6ff766a.jpg', 'image/jpeg', '<p>Pernyataan yang sesuai dengan Kata “kolumnis” di paragraf pertama pada teks di atas mempunyai makna…</p>', '<p>Orang yang berpikiran secara bebas</p>', '<p>Menganut paham komunis</p>', '<p>Orang yang mencintai keindahan</p>', '<p>Orang yang secara tetap menulis artikel dalam surat kabar atau majalah</p>', '', -1, -1, -1, 4, -1, 0, '', '', '', '', '', 'D', '', 1592849415, 1592849415),
(830, 20, 18, 42, 0, 'f6c6167b4e2f97d1340ad6c8a46f9460.jpg', 'image/jpeg', '<p>Manakah dari pernyataan berikut yang sesuai dengan bacaan diatas?</p>', '<p>Geneviève Couteau menikah dengan dokter hewan bernama Joseph Couteau yang berasal dari kota Leipzig.</p>', '<p>Geneviève Couteau merupakan lulusan terbaik sekolah tinggi Beaux-Arts Nantes pada tahun 1952</p>', '<p>Geneviève Couteau menulis buku yng berjudul “Mémoire du Laos” yang merupakan pengalamannya ketika berkunjung ke Bali.</p>', '<p>Geneviève Couteau wafat pada 17 Desember 2013 dengan meninggalkan banyak karya dan jejak perburuan estetik.</p>', '', -1, -1, -1, 4, -1, 0, '', '', '', '', '', 'D', '', 1592849580, 1592849580),
(831, 20, 18, 42, 0, 'ab838d19c57d654f57d2587e21662e75.jpg', 'image/jpeg', '<p>Alasan yang menjadi pembenaran atas tindakan diatas adalah . . .</p>', '<p>rasa kebencian</p>', '<p>perolehan kebahagiaan</p>', '<p>rasa kesetiakawanan</p>', '<p>rasa kecurigaan</p>', '', -1, -1, 4, -1, -1, 0, '', '', '', '', '', 'C', '', 1592849911, 1592849911),
(832, 20, 18, 42, 0, '061ee0473ea918ea74a8608eacedeed0.jpg', 'image/jpeg', '<p>Tema dari wacana di atas adalah . . .</p>', '<p>Pelampiasan kegembiraan yang berlebihan</p>', '<p>Mengubah kebahagiaan menjadi kemalangan</p>', '<p>Empangisasi tidak menggambarkan citra mahasiswa</p>', '<p>Mahasiswa masih bersifat kekanakk-kanakan</p>', '', 4, -1, -1, -1, -1, 0, '', '', '', '', '', 'A', '', 1592876788, 1592876788),
(833, 20, 18, 42, 0, 'b183f3e1a956ecc1e6a2f74b4c7790e8.jpg', 'image/jpeg', '<p>Kalimat yang bermakna ambigu pada paragraf kedua terdapat pada kalimat . . .</p>', '<p>Pertama</p>', '<p>Kedua</p>', '<p>Ketiga</p>', '<p>Keempat</p>', '', -1, -1, -1, 4, -1, 0, '', '', '', '', '', 'D', '', 1592876845, 1592876845),
(834, 20, 18, 42, 0, '4fd11d1dfce04f2de96e375a2df4cdc1.jpg', 'image/jpeg', '<p>Susunan duduk (dari kursi no. 1 ke no. 6) manakah yang dapat diterima atau memenuhi kondisi tersebut?</p>', '<p>L, M, K,O, N, J</p>', '<p>L, J, M, O, N, K</p>', '<p>K, J, L, O, M, N</p>', '<p>M, K, O, N, J, L</p>', '', -1, -1, 4, -1, -1, 0, '', '', '', '', '', 'C', '', 1592878433, 1592878433),
(835, 20, 18, 42, 0, '3b2a86061f683171a88d79c45c78ee56.jpg', 'image/jpeg', '<p>Susunan duduk (dari kursi no. 1 ke no. 6) manakah yang tidak dapat diterima atau tidak memenuhi kondisi tersebut?</p>', '<p>K, J, O, N, M, L</p>', '<p>K, O, N, J, M, L</p>', '<p>L, O, N, J, K, M</p>', '<p>Benar Semua</p>', '', -1, -1, -1, 4, -1, 0, '', '', '', '', '', 'D', '', 1592878508, 1592878508),
(836, 20, 18, 42, 0, '4eb155d4efea3b14b34b65041846579c.jpg', 'image/jpeg', '<p>Jika L duduk di kursi no. 1 dan K di kursi no. 5, manakah berikut ini yang benar?</p>', '<p>M duduk di kursi no. 3</p>', '<p>N duduk di kursi no. 4</p>', '<p>J duduk di kursi no. 4</p>', '<p>M duduk di kursi no. 6</p>', '', -1, -1, 4, -1, -1, 0, '', '', '', '', '', 'C', '', 1592878607, 1592878607),
(837, 20, 18, 42, 0, 'bf4548829ca9946df9044c456cbe8051.jpg', 'image/jpeg', '<p>Jika M duduk di kursi no. 2 dan O duduk di kursi no. 5, manakah berikut ini yang benar?</p>', '<p>J duduk di kursi no. 3</p>', '<p>K duduk di kursi no. 4</p>', '<p>L duduk di kursi no. 4</p>', '<p>Semua Salah</p>', '', -1, -1, -1, 4, -1, 0, '', '', '', '', '', 'D', '', 1592878674, 1592878674),
(838, 20, 18, 28, 0, '', '', '<p><span class=\"fontstyle0\">ULTRAMARIN (USM PKN STAN 2017)</span></p>', '<p><span class=\"fontstyle0\">Warna hijau daun</span></p>', '<p><span class=\"fontstyle0\">Warna hitam pekat</span></p>', '<p><span class=\"fontstyle0\">Warna kuning cerah</span></p>', '<p><span class=\"fontstyle0\">Warna biru cerah</span></p>', '<p><span class=\"fontstyle0\">Warna merah muda</span> </p>', -1, -1, -1, 4, -1, 0, '', '', '', '', '', 'D', '<p class=\"MsoNormal\" xss=removed><span lang=\"IN\" xss=removed>Ultramarin : warna biru cerah</span></p>', 1593283957, 1593283957),
(839, 20, 18, 28, 0, '', '', '<p><span class=\"fontstyle0\">LEMBANG (USM PKN STAN 2017)</span></p>', '<p><span class=\"fontstyle2\">Tanah datar</span></p>', '<p><span class=\"fontstyle2\">Tanah berbukit</span></p>', '<p><span class=\"fontstyle2\">Tanah rendah</span></p>', '<p><span class=\"fontstyle2\">Tanah subur</span></p>', '<p><span class=\"fontstyle2\">Tanah liat</span> </p>', -1, -1, 4, -1, -1, 0, '', '', '', '', '', 'C', '<p class=\"MsoNormal\" xss=removed><span lang=\"IN\" xss=removed>Lembang : lekuk ; menjadi rendah dan dalam ; lembah, tanah rendah</span></p>', 1593284068, 1593284068),
(840, 20, 18, 28, 0, '', '', '<p><span class=\"fontstyle0\">VASAL (USM PKN STAN 2016)</span></p>', '<p><span class=\"fontstyle2\">Menang telak</span></p>', '<p><span class=\"fontstyle2\">Pokok masalah</span></p>', '<p><span class=\"fontstyle2\">Menyerah tanpa syarat</span></p>', '<p><span class=\"fontstyle2\">Sangat luas</span></p>', '<p><span class=\"fontstyle2\">Daerah taklukan</span> </p>', -1, -1, -1, -1, 4, 0, '', '', '', '', '', 'E', '<p class=\"MsoNormal\" xss=removed><span lang=\"IN\" xss=removed>Vasal : Daerah taklukan</span></p>', 1593284218, 1593284218),
(841, 20, 18, 28, 0, '', '', '<p><span class=\"fontstyle0\">INTERNIS (USM PKN STAN 2016)</span></p>', '<p><span class=\"fontstyle2\">Berhubungan dengan</span></p>', '<p><span class=\"fontstyle2\">Penyakit dalam</span></p>', '<p><span class=\"fontstyle2\">Percampuran antara</span></p>', '<p><span class=\"fontstyle2\">Bersinggungan dengan</span></p>', '<p><span class=\"fontstyle2\">Penyakit syaraf</span> </p>', -1, 4, -1, -1, -1, 0, '', '', '', '', '', 'B', '<p><span lang=\"IN\" xss=removed>Internis : Penyakit dalam</span></p>', 1593284330, 1593284330),
(842, 20, 18, 28, 0, '', '', '<p><span class=\"fontstyle0\">JUNTA (USM PKN STAN 2016)</span></p>', '<p><span class=\"fontstyle2\">Aksi militer</span></p>', '<p><span class=\"fontstyle2\">Dewan militer</span></p>', '<p><span class=\"fontstyle2\">Dewan pemerintahan</span></p>', '<p><span class=\"fontstyle2\">Rezim militer</span></p>', '<p><span class=\"fontstyle2\">Pemerintah militer</span> </p>', -1, -1, 4, -1, -1, 0, '', '', '', '', '', 'C', '<p><span lang=\"IN\" xss=removed>Junta : Dewan pemerintahan</span></p>', 1593284426, 1593284426),
(843, 20, 18, 28, 0, '', '', '<p><span class=\"fontstyle0\">ABNUS (USM PKN STAN 2016)</span></p>', '<p><span class=\"fontstyle2\">Kayu arang</span></p>', '<p><span class=\"fontstyle2\">Kayu jati</span></p>', '<p><span class=\"fontstyle2\">Kayu partikel</span></p>', '<p><span class=\"fontstyle2\">Kayu lapis</span></p>', '<p><span class=\"fontstyle2\">Kayu rimba</span> </p>', 4, -1, -1, -1, -1, 0, '', '', '', '', '', 'A', '<p class=\"MsoNormal\" xss=removed><span lang=\"IN\" xss=removed>Abnus : Kayu arang</span></p>', 1593284536, 1593284536),
(844, 20, 18, 28, 0, '', '', '<p><em><strong>SINONIM</strong></em></p>\r\n<p><span class=\"fontstyle0\">TAMSIL (USM PKN STAN 2017)</span></p>', '<p><span class=\"fontstyle2\">Acuan</span></p>', '<p><span class=\"fontstyle2\">Ramalan</span></p>', '<p><span class=\"fontstyle2\">Panduan</span></p>', '<p><span class=\"fontstyle2\">Kepastian</span></p>', '<p><span class=\"fontstyle2\">Perumpamaan</span> </p>', -1, -1, -1, -1, 4, 0, '', '', '', '', '', 'E', '<p class=\"MsoNormal\" xss=removed><span lang=\"IN\" xss=removed>Tamsil (umpama ; ajaran yang terkandung dalam cerita) = Perumpamaan.</span></p>', 1593284670, 1593284670),
(845, 20, 18, 28, 0, '', '', '<p><em><strong>SINONIM</strong></em></p>\r\n<p><span class=\"fontstyle0\">SEMPAL (USM PKN STAN 2017)</span></p>', '<p><span class=\"fontstyle2\">Menyembul</span></p>', '<p><span class=\"fontstyle2\">Menohok</span></p>', '<p><span class=\"fontstyle2\">Merekah</span></p>', '<p><span class=\"fontstyle2\">Menyelip</span></p>', '<p><span class=\"fontstyle2\">Meletak</span> </p>', 4, -1, -1, -1, -1, 0, '', '', '', '', '', 'A', '<p class=\"MsoNormal\" xss=removed><span lang=\"IN\" xss=removed>Dari KBBI, Sempal (menyempal ) <em>:</em> menyembul atau tersembul ke luar sebagian, spt jantung pisang yg baru akan keluar, pusat yg bujal; menutup dng cara menjejalkan sesuatu; menyumpal.</span></p>', 1593284801, 1593284801),
(846, 20, 18, 28, 0, '', '', '<p><em><strong>SINONIM</strong></em></p>\r\n<p><span class=\"fontstyle0\">KEBAS (USM PKN STAN 2017)</span></p>', '<p><span class=\"fontstyle2\">Kedinginan</span></p>', '<p><span class=\"fontstyle2\">Kepanasan</span></p>', '<p><span class=\"fontstyle2\">Kesakitan</span></p>', '<p><span class=\"fontstyle2\">Kesemutan</span></p>', '<p><span class=\"fontstyle2\">Kecanduan</span> </p>', -1, -1, -1, 4, -1, 0, '', '', '', '', '', 'D', '<p class=\"MsoNormal\" xss=removed><span lang=\"IN\" xss=removed>Kebas (berasa kaku ; kesemutan) = Kesemutan</span></p>', 1593284909, 1593284909),
(847, 20, 18, 28, 0, '', '', '<p><em><strong><span class=\"fontstyle0\">SINONIM</span></strong></em></p>\r\n<p><span class=\"fontstyle0\">MUJARAB (USM PKN STAN 2016)</span></p>', '<p><span class=\"fontstyle2\">Mangkus</span></p>', '<p><span class=\"fontstyle2\">Sangkil</span></p>', '<p><span class=\"fontstyle2\">Guna</span></p>', '<p><span class=\"fontstyle2\">Kuat</span></p>', '<p><span class=\"fontstyle2\">Sakti</span> </p>', 4, -1, -1, -1, -1, 0, '', '', '', '', '', 'A', '<p class=\"MsoNormal\" xss=removed><span lang=\"IN\" xss=removed>Mujarab (mustajab ; manjur) = Mangkus (mustajab ; berhasil guna)</span></p>', 1593285112, 1593285112),
(848, 20, 18, 28, 0, '', '', '<p><em><strong><span class=\"fontstyle0\">SINONIM</span></strong></em></p>\r\n<p><span class=\"fontstyle0\">APARATUS (USM PKN STAN 2016)</span></p>', '<p><span class=\"fontstyle2\">Bahan</span></p>', '<p><span class=\"fontstyle2\">Orang</span></p>', '<p><span class=\"fontstyle2\">Badan</span></p>', '<p><span class=\"fontstyle2\">Alat</span></p>', '<p><span class=\"fontstyle2\">Organisasi</span> </p>', -1, -1, -1, 4, -1, 0, '', '', '', '', '', 'D', '<p><span lang=\"IN\" xss=removed>Aparatus = Alat</span></p>', 1593285710, 1593285710),
(849, 20, 18, 28, 0, '', '', '<p><em><strong>SINONIM</strong></em></p>\r\n<p><span class=\"fontstyle0\">CITRA (USM PKN STAN 2016)</span></p>', '<p><span class=\"fontstyle2\">Wibawa</span></p>', '<p><span class=\"fontstyle2\">Gambaran</span></p>', '<p><span class=\"fontstyle2\">Piala</span></p>', '<p><span class=\"fontstyle2\">Kebanggaan</span></p>', '<p><span class=\"fontstyle2\">Perubahan</span> </p>', -1, -1, -1, 4, -1, 0, '', '', '', '', '', 'D', '<p class=\"MsoNormal\" xss=removed><span lang=\"IN\" xss=removed>Citra (rupa ; gambar ; gambaran) = Gambaran</span></p>', 1593285799, 1593285799),
(850, 20, 18, 28, 0, '', '', '<p><em><strong>SINONIM</strong></em></p>\r\n<p><span class=\"fontstyle0\">FUSI (USM PKN STAN 2016)</span></p>', '<p><span class=\"fontstyle2\">Pencampuran</span></p>', '<p><span class=\"fontstyle2\">Peremajaan</span></p>', '<p><span class=\"fontstyle2\">Pemutakhiran</span></p>', '<p><span class=\"fontstyle2\">Penggabungan</span></p>', '<p><span class=\"fontstyle2\">Perubahan</span></p>', -1, 4, -1, -1, -1, 0, '', '', '', '', '', 'B', '<p class=\"MsoNormal\" xss=removed><span lang=\"IN\" xss=removed>Fusi (gabungan ; peleburan ; koalisi) = Penggabungan</span></p>', 1593285897, 1593285897),
(851, 20, 18, 28, 0, '', '', '<p><em><strong>SINONIM</strong></em></p>\r\n<p><span class=\"fontstyle0\">SREGEP (2016)</span></p>', '<p><span class=\"fontstyle2\">Pintar</span></p>', '<p><span class=\"fontstyle2\">cerdas</span></p>', '<p><span class=\"fontstyle2\">Bodoh</span></p>', '<p><span class=\"fontstyle2\">Rajin</span></p>', '<p><span class=\"fontstyle2\">Malas</span> </p>', -1, -1, -1, 4, -1, 0, '', '', '', '', '', 'D', '<p class=\"MsoNormal\" xss=removed><span lang=\"IN\" xss=removed>Sregep (rajin ; tekun) = rajin</span></p>', 1593286061, 1593286061),
(852, 20, 18, 28, 0, '', '', '<p><em><strong>SINONIM</strong></em></p>\r\n<p><span class=\"fontstyle0\">HABUNG (USM PKN STAN 2016)</span></p>', '<p><span class=\"fontstyle2\">Meja</span></p>', '<p><span class=\"fontstyle2\">Pigura</span></p>', '<p><span class=\"fontstyle2\">Lemari</span></p>', '<p><span class=\"fontstyle2\">Wajan</span></p>', '<p><span class=\"fontstyle2\">Keranjang</span> </p>', -1, -1, -1, -1, 4, 0, '', '', '', '', '', 'E', '<p class=\"MsoNormal\" xss=removed><span lang=\"IN\" xss=removed>Habung (keranjang terbuat dari rotan) = Keranjang</span></p>', 1593286165, 1593286165),
(853, 20, 18, 28, 0, '', '', '<p><em><strong>ANTONIM</strong></em></p>\r\n<p><span class=\"fontstyle0\">JENENG </span><span class=\"fontstyle2\">(USM PKN STAN 2017)</span></p>', '<p><span class=\"fontstyle0\">Rata</span></p>', '<p><span class=\"fontstyle0\">Lurus</span></p>', '<p><span class=\"fontstyle0\">Tegak</span></p>', '<p><span class=\"fontstyle0\">Datar</span></p>', '<p><span class=\"fontstyle0\">Kokoh</span> </p>', -1, 4, -1, -1, -1, 0, '', '', '', '', '', 'B', '<p class=\"MsoNormal\" xss=removed><span lang=\"IN\" xss=removed>Jeneng (miring ; condong) >< Tegak></p>', 1593287680, 1593287680),
(854, 20, 18, 28, 0, '', '', '<p><em><strong>ANTONIM</strong></em></p>\r\n<p><span class=\"fontstyle0\">PRUDENSIAL </span><span class=\"fontstyle2\">(USM PKN STAN 2017)</span></p>', '<p><span class=\"fontstyle0\">Picik</span></p>', '<p><span class=\"fontstyle0\">Jahat</span></p>', '<p><span class=\"fontstyle0\">Culas</span></p>', '<p><span class=\"fontstyle0\">Bodoh</span></p>', '<p><span class=\"fontstyle0\">Curang</span> </p>', -1, -1, -1, -1, 4, 0, '', '', '', '', '', 'E', '<p class=\"MsoNormal\" xss=removed><span lang=\"IN\" xss=removed>Prudensial (bersifat bijaksana) >< curang></p>', 1593287772, 1593287772),
(855, 20, 18, 28, 0, '', '', '<p><em><strong>ANTONIM</strong></em></p>\r\n<p><span class=\"fontstyle0\">ACUH (USM PKN STAN 2017)</span></p>', '<p><span class=\"fontstyle2\">Peduli</span></p>', '<p><span class=\"fontstyle2\">Sabar</span></p>', '<p><span class=\"fontstyle2\">Ingat</span></p>', '<p><span class=\"fontstyle2\">Hirau</span></p>', '<p><span class=\"fontstyle2\">Lalai</span></p>', -1, -1, -1, -1, 4, 0, '', '', '', '', '', 'E', '<p class=\"MsoNormal\" xss=removed><span lang=\"IN\" xss=removed>Acuh (peduli ; mengindahkan) >< Lalai></p>', 1593287858, 1593287858),
(856, 20, 18, 28, 0, '', '', '<p><em><strong><span class=\"fontstyle0\">ANTONIM</span></strong></em></p>\r\n<p><span class=\"fontstyle0\">TERATUR (USM PKN STAN 2017)<br></span></p>', '<p><span class=\"fontstyle2\">Sumbang</span></p>', '<p><span class=\"fontstyle2\">Ambigu</span></p>', '<p><span class=\"fontstyle2\">Urakan<br></span></p>', '<p><span class=\"fontstyle2\">Sumir</span></p>', '<p><span class=\"fontstyle2\">Rancu</span> </p>', -1, -1, -1, -1, 4, 0, '', '', '', '', '', 'E', '<p class=\"MsoNormal\" xss=removed><span lang=\"IN\" xss=removed>Teratur (sudah diatur baik-baik ; berturut turut dengan tetap) >< Rancu></p>', 1593287936, 1593287936),
(857, 20, 18, 28, 0, '', '', '<p><em><strong>ANTONIM</strong></em></p>\r\n<p><span class=\"fontstyle0\">RUJUKAN (USM PKN STAN 2016)</span></p>', '<p><span class=\"fontstyle2\">Referensi</span></p>', '<p><span class=\"fontstyle2\">Himbauan</span></p>', '<p><span class=\"fontstyle2\">Panduan</span></p>', '<p><span class=\"fontstyle2\">Acuan</span></p>', '<p><span class=\"fontstyle2\">Pedoman</span> </p>', -1, 4, -1, -1, -1, 0, '', '', '', '', '', 'B', '<p class=\"MsoNormal\" xss=removed><span lang=\"IN\" xss=removed>Rujukan >< Himbauan></p>', 1593288234, 1593288234),
(858, 20, 18, 28, 0, '', '', '<p><em><strong>ANTONIM</strong></em></p>\r\n<p><span class=\"fontstyle0\">LANCUNG (USM PKN STAN 2016)</span></p>', '<p><span class=\"fontstyle2\">Asli</span></p>', '<p><span class=\"fontstyle2\">Palsu</span></p>', '<p><span class=\"fontstyle2\">Dekat</span></p>', '<p><span class=\"fontstyle2\">Jauh</span></p>', '<p><span class=\"fontstyle2\">Laju</span> </p>', 4, -1, -1, -1, -1, 0, '', '', '', '', '', 'A', '<p class=\"MsoNormal\" xss=removed><span lang=\"IN\" xss=removed>Lancung (palsu ; tidak asli) >< Asli></p>', 1593288322, 1593288322),
(859, 20, 18, 28, 0, '', '', '<p><em><strong>ANTONIM</strong></em></p>\r\n<p><span class=\"fontstyle0\">PROGNOSIS (USM PKN STAN 2016)</span></p>', '<p><span class=\"fontstyle2\">Diagnosis</span></p>', '<p><span class=\"fontstyle2\">Fakta</span></p>', '<p><span class=\"fontstyle2\">Prediksi</span></p>', '<p><span class=\"fontstyle2\">Analisis</span></p>', '<p><span class=\"fontstyle2\">Fana</span> </p>', -1, 4, -1, -1, -1, 0, '', '', '', '', '', 'B', '<p><span lang=\"IN\" xss=removed>Prognosis (ramalan) >< Fakta></p>', 1593288423, 1593288423),
(860, 20, 18, 28, 0, '', '', '<p><em><strong><span class=\"fontstyle0\">ANTONIM</span></strong></em></p>\r\n<p><span class=\"fontstyle0\">INSINUASI (USM PKN STAN 2016)<br></span></p>', '<p><span class=\"fontstyle2\">Cacian</span></p>', '<p><span class=\"fontstyle2\">Harapan</span></p>', '<p><span class=\"fontstyle2\">Sindiran</span></p>', '<p><span class=\"fontstyle2\">Gangguan</span></p>', '<p><span class=\"fontstyle2\">Pujaan</span> </p>', -1, -1, -1, -1, 4, 0, '', '', '', '', '', 'E', '<p class=\"MsoNormal\" xss=removed><span lang=\"IN\" xss=removed>Insinuasi (tuduhan tersembunyi ; tidak terangterangan ; sindiran) >< Pujaan></p>', 1593288545, 1593288545),
(861, 20, 18, 28, 0, '', '', '<p><em><strong>ANTONIM</strong></em></p>\r\n<p><span class=\"fontstyle0\">KONTER (USM PKN STAN 2016)</span></p>', '<p><span class=\"fontstyle2\">Anti</span></p>', '<p><span class=\"fontstyle2\">Asese</span></p>', '<p><span class=\"fontstyle2\">Kebalikan</span></p>', '<p><span class=\"fontstyle2\">Berseberangan</span></p>', '<p><span class=\"fontstyle2\">Melawan</span> </p>', -1, 4, -1, -1, -1, 0, '', '', '', '', '', 'B', '<p class=\"MsoNormal\" xss=removed><span lang=\"IN\" xss=removed>Konter (melawan ; menantang) >< Asese></p>', 1593288657, 1593288657),
(862, 20, 18, 28, 0, '', '', '<p><em><strong>ANALOGI</strong></em></p>\r\n<p><span class=\"fontstyle0\">BATA : TANAH LIAT (USM PKN STAN<br>2017)</span></p>', '<p><span class=\"fontstyle2\">Batu : Pasir</span></p>', '<p><span class=\"fontstyle2\">Bunga : Buah</span></p>', '<p><span class=\"fontstyle2\">Beton : Semen</span></p>', '<p><span class=\"fontstyle2\">Kertas : Buku</span></p>', '<p><span class=\"fontstyle2\">Tembikar : Keramik</span> </p>', -1, -1, 4, -1, -1, 0, '', '', '', '', '', 'C', '<p class=\"MsoNormal\" xss=removed><span lang=\"IN\" xss=removed>Kalimat Penguji : Bata terbuat dari tanah liat. </span></p>\r\n<p class=\"MsoListParagraphCxSpFirst\" xss=removed>&lt;!-- [if !supportLists]--&gt;<span lang=\"IN\" xss=removed><span xss=removed>A.<span xss=removed>    </span></span></span>&lt;!--[endif]--&gt;<span lang=\"IN\" xss=removed>Batu terbuat sari pasir (SALAH)</span></p>\r\n<p class=\"MsoListParagraphCxSpMiddle\" xss=removed>&lt;!-- [if !supportLists]--&gt;<span lang=\"IN\" xss=removed><span xss=removed>B.<span xss=removed>    </span></span></span>&lt;!--[endif]--&gt;<span lang=\"IN\" xss=removed>Bnga terbuat dari buah (SALAH)</span></p>\r\n<p class=\"MsoListParagraphCxSpMiddle\" xss=removed>&lt;!-- [if !supportLists]--&gt;<span lang=\"IN\" xss=removed><span xss=removed>C.<span xss=removed>    </span></span></span>&lt;!--[endif]--&gt;<span lang=\"IN\" xss=removed>Beton terbuat dari semen (BENAR)</span></p>\r\n<p class=\"MsoListParagraphCxSpMiddle\" xss=removed>&lt;!-- [if !supportLists]--&gt;<span lang=\"IN\" xss=removed><span xss=removed>D.<span xss=removed>    </span></span></span>&lt;!--[endif]--&gt;<span lang=\"IN\" xss=removed>Kertas terbuat dari buku (SALAH)</span></p>\r\n<p class=\"MsoListParagraphCxSpLast\" xss=removed>&lt;!-- [if !supportLists]--&gt;<span lang=\"IN\" xss=removed><span xss=removed>E.<span xss=removed>     </span></span></span>&lt;!--[endif]--&gt;<span lang=\"IN\" xss=removed>Temikar terbuat dari keramik (SALAH)</span></p>', 1593288894, 1593288894),
(863, 20, 18, 28, 0, '', '', '<p><em><strong>ANALOGI</strong></em></p>\r\n<p><span class=\"fontstyle0\">KARST : BENTUK PERMUKAAN BUMI<br>(USM PKN STAN 2017)</span></p>', '<p><span class=\"fontstyle2\">A. Pasang surut : Bentuk Permukaan Laut</span></p>', '<p><span class=\"fontstyle2\">Mendung : Bentuk Permukaan Udara</span></p>', '<p><span class=\"fontstyle2\">Hijau : Bentuk Permukaan Daun</span></p>', '<p><span class=\"fontstyle2\">Sisik : Bentuk Permukaan Kulit</span></p>', '<p><span class=\"fontstyle2\">Aspal : Bentuk Permukaan Jalan</span> </p>', -1, -1, -1, 4, -1, 0, '', '', '', '', '', 'D', '<p class=\"MsoNormal\" xss=removed><span lang=\"IN\" xss=removed>Karst menjadi bagian terluar dari bumi yang menjadi saringan sebelum air masuk ke dalam tanah (bumi) = sisik yang menjadi bagian terluar dari kulit.</span></p>', 1593289045, 1593289045),
(864, 20, 18, 28, 0, '', '', '<p><em><strong>ANALOGI</strong></em></p>\r\n<p><span class=\"fontstyle0\">MATA : TELINGA (USM PKN STAN 2017)</span></p>', '<p><span class=\"fontstyle2\">Lutut : Siku</span></p>', '<p><span class=\"fontstyle2\">Jari : Tangan</span></p>', '<p><span class=\"fontstyle2\">Kaki : Paha</span></p>', '<p><span class=\"fontstyle2\">Perut : Dada</span></p>', '<p><span class=\"fontstyle2\">Lidah : Hidung</span> </p>', -1, -1, -1, -1, 4, 0, '', '', '', '', '', 'E', '<p class=\"MsoNormal\" xss=removed><span lang=\"IN\" xss=removed>Mata dan Telinga adalah bagian dari pancaindra. Demikian juga dengan Lidah dan Hidung yang juga merupakan bagian dari pancaindra. Selain itu, Mata dan Telinga terletak sejajar, sama seperti Lidah dan Hidung yang letaknya juga sejajar. Dan sama-sama ada di kepala.</span></p>', 1593289140, 1593289140),
(865, 20, 18, 28, 0, '', '', '<p><em><strong><span class=\"fontstyle0\">ANALOGI</span></strong></em></p>\r\n<p><span class=\"fontstyle0\">JURANG : TEBING (USM PKN STAN 2017)<br></span></p>', '<p><span class=\"fontstyle2\">Kilat : Petir</span></p>', '<p><span class=\"fontstyle2\">Laut : Pantai</span></p>', '<p><span class=\"fontstyle2\">Kerikil : Karang</span></p>', '<p><span class=\"fontstyle2\">Ranting : Pohon</span></p>', '<p><span class=\"fontstyle2\">Mendung : Awan</span> </p>', -1, 4, -1, -1, -1, 0, '', '', '', '', '', 'B', '<p class=\"MsoNormal\" xss=removed><span lang=\"IN\" xss=removed>Tebing adalah tepi jurang. Sama seperti pantai adalah tepi laut</span></p>', 1593289244, 1593289244),
(866, 20, 18, 28, 0, '', '', '<p><em><strong>ANALOGI</strong></em></p>\r\n<p><span class=\"fontstyle0\">TERANG : CAHAYA (USM PKN STAN 2017)</span></p>', '<p><span class=\"fontstyle2\">Basah : Air</span></p>', '<p><span class=\"fontstyle2\">Botol : Air</span></p>', '<p><span class=\"fontstyle2\">Gelap : Lilin</span></p>', '<p><span class=\"fontstyle2\">Makanan : Lapar</span></p>', '<p><span class=\"fontstyle2\">Penutup : Pakaian</span> </p>', 4, -1, -1, -1, -1, 0, '', '', '', '', '', 'A', '<p class=\"MsoNormal\" xss=removed><span lang=\"IN\" xss=removed>Cahaya membuat terang. Air membuat basah.</span></p>', 1593289359, 1593289359),
(867, 20, 18, 28, 0, '', '', '<p><em><strong>ANALOGI</strong></em></p>\r\n<p><span class=\"fontstyle0\">KARBONDIOKSIDA : FOTOSINTESA : OKSIGEN (USM PKN STAN 2017)</span></p>', '<p><span class=\"fontstyle2\">Batu : Keras : Arca</span></p>', '<p><span class=\"fontstyle2\">Murid : Didik : Lulus</span></p>', '<p><span class=\"fontstyle2\">Air Asin : Jemur : Garam</span></p>', '<p><span class=\"fontstyle2\">Pohon Aren : Nira : Gula</span></p>', '<p><span class=\"fontstyle2\">Kulit Kelapa : Sabut : Sapu</span> </p>', -1, -1, 4, -1, -1, 0, '', '', '', '', '', 'C', '<p class=\"MsoNormal\" xss=removed><span lang=\"IN\" xss=removed>Karbon dioksida diubah menjadi oksigen pada proses fotosintesa. Air asin diubah menjadi garam pada proses jemur.</span></p>', 1593289436, 1593289436),
(868, 20, 18, 28, 0, '', '', '<p><em><strong>ANALOGI</strong></em></p>\r\n<p><span class=\"fontstyle0\">LEM : SELOTIP (USM PKN STAN 2017)</span></p>', '<p><span class=\"fontstyle2\">Listrik : Air</span></p>', '<p><span class=\"fontstyle2\">Lilin : Lampu</span></p>', '<p><span class=\"fontstyle2\">Kunci : Pintu</span></p>', '<p><span class=\"fontstyle2\">Jam : Tangan</span></p>', '<p><span class=\"fontstyle2\">Sepeda : Roda</span> </p>', -1, 4, -1, -1, -1, 0, '', '', '', '', '', 'B', '<p><span lang=\"IN\" xss=removed>Lem dan selotip adalah benda subtitusi / pengganti, fungsi keduanya sama, untuk perekat. Sama seperti lilin dan lampu, fungsi sama untuk penerangan.</span></p>', 1593289556, 1593289556),
(869, 20, 18, 28, 0, '', '', '<p><em><strong>ANALOGI</strong></em></p>\r\n<p><span class=\"fontstyle0\">TANGKAI : KELOPAK : BUNGA (USM PKN STAN 2016)</span></p>', '<p><span class=\"fontstyle2\">Tubuh : Tangan : Kepala</span></p>', '<p><span class=\"fontstyle2\">Tanah : Laut : Air</span></p>', '<p><span class=\"fontstyle2\">Pelepah : Tangkai : Daun</span></p>', '<p><span class=\"fontstyle2\">Tahun : Bulan : Hari</span></p>', '<p><span class=\"fontstyle2\">Langit : Tanah : Magma</span> </p>', -1, -1, 4, -1, -1, 0, '', '', '', '', '', 'C', '<p class=\"MsoNormal\" xss=removed><span lang=\"IN\" xss=removed>Tangkai : Kelopak : Bunga = Pelepah : Tangkai : Daun.</span></p>', 1593289634, 1593289634),
(870, 20, 18, 28, 0, '', '', '<p><em><strong><span class=\"fontstyle0\">ANALOGI</span></strong></em></p>\r\n<p><span class=\"fontstyle0\">BAYI : ANAK-ANAK : REMAJA (USM PKN STAN 2016)</span></p>', '<p><span class=\"fontstyle2\">Ulat : Kepompong : Kupu-kupu</span></p>', '<p><span class=\"fontstyle2\">Anak : Ayah : Kakek</span></p>', '<p><span class=\"fontstyle2\">Malam : Siang : Pagi</span></p>', '<p><span class=\"fontstyle2\">Besar : Sedang : Kecil</span></p>', '<p><span class=\"fontstyle2\">Beras : Nasi : Lontong</span> </p>', 4, -1, -1, -1, -1, 0, '', '', '', '', '', 'A', '<p class=\"MsoNormal\" xss=removed><strong><span lang=\"IN\" xss=removed>Bayi </span></strong><span lang=\"IN\" xss=removed>menua menjadi <strong>anak-anak </strong>kemudian menjadi <strong>remaja</strong>. <strong>Ulat </strong>berubah menjadi <strong>kepompong </strong>kemudian menjadi <strong>kupu-kupu</strong>.</span></p>', 1593289721, 1593289721),
(871, 20, 18, 28, 0, '', '', '<p><em><strong>ANALOGI</strong></em></p>\r\n<p><span class=\"fontstyle0\">KERTAS : KAYU : DIOLAH (USM PKN STAN 2016)</span></p>', '<p><span class=\"fontstyle2\">Donat : Tepung : Dimasak</span></p>', '<p><span class=\"fontstyle2\">Buku : Pedoman : Dokumentasi</span></p>', '<p><span class=\"fontstyle2\">Pensil : Kertas : Tinta</span></p>', '<p><span class=\"fontstyle2\">Kunci Jawaban : Gembok : Pintu</span></p>', '<p><span class=\"fontstyle2\">Kepala : Sakit : Pinggang</span> </p>', 4, -1, -1, -1, -1, 0, '', '', '', '', '', 'A', '<p class=\"MsoNormal\" xss=removed><strong><span lang=\"IN\" xss=removed>Kertas </span></strong><span lang=\"IN\" xss=removed>berasal dari <strong>kayu </strong>yang telah <strong>diolah </strong>sedemikian rupa. <strong>Donat </strong>berasal dari <strong>tepung </strong>yang <strong>dimasak </strong>sedemikian rupa.</span></p>', 1593289805, 1593289805),
(872, 20, 18, 43, 0, '', '', '<p><span class=\"fontstyle0\">12 + 32 +52 + 72 + 92 + ... + 172 + 192 =<br></span></p>', '<p><span class=\"fontstyle0\">1.000</span></p>', '<p><span class=\"fontstyle0\">1.010</span></p>', '<p><span class=\"fontstyle0\">1.020</span></p>', '<p><span class=\"fontstyle0\">1.030</span></p>', '<p><span class=\"fontstyle0\">1.040</span> </p>', -1, -1, -1, 4, -1, 0, '', '', '', '', '', 'D', '<p class=\"MsoListParagraphCxSpFirst\" xss=removed><span lang=\"IN\" xss=removed>a = 12</span></p>\r\n<p class=\"MsoListParagraphCxSpMiddle\" xss=removed><span lang=\"IN\" xss=removed>b = 20</span></p>\r\n<p class=\"MsoListParagraphCxSpMiddle\" xss=removed><span lang=\"IN\" xss=removed>192 = a + (n-1)b</span></p>\r\n<p class=\"MsoListParagraphCxSpMiddle\" xss=removed><span lang=\"IN\" xss=removed>192 = 12 + (n-1)20</span></p>\r\n<p class=\"MsoListParagraphCxSpMiddle\" xss=removed><span lang=\"IN\" xss=removed>192 = 12 + 20n – 20</span></p>\r\n<p class=\"MsoListParagraphCxSpMiddle\" xss=removed><span lang=\"IN\" xss=removed>192 + 8 = 20n</span></p>\r\n<p class=\"MsoListParagraphCxSpMiddle\" xss=removed><span lang=\"IN\" xss=removed>n = 10</span></p>\r\n<p class=\"MsoListParagraphCxSpMiddle\" xss=removed><span lang=\"IN\" xss=removed>Sn = n/2(a+Un)</span></p>\r\n<p class=\"MsoListParagraphCxSpLast\" xss=removed><span lang=\"IN\" xss=removed>= 10/2 (12+192) = 1020</span></p>', 1593455772, 1593455772);
INSERT INTO `tbl_soal_tryout` (`id_soal`, `dosen_id`, `matkul_id`, `id_soal_paket`, `bobot`, `file`, `tipe_file`, `soal`, `opsi_a`, `opsi_b`, `opsi_c`, `opsi_d`, `opsi_e`, `bobot_a`, `bobot_b`, `bobot_c`, `bobot_d`, `bobot_e`, `bobot_kosong`, `file_a`, `file_b`, `file_c`, `file_d`, `file_e`, `jawaban`, `pembahasan`, `created_on`, `updated_on`) VALUES
(873, 20, 18, 43, 0, '', '', '<p><span class=\"fontstyle0\">1 – 1/3 – 1/9 – 1/27 – 1/81 - ... = ...</span></p>', '<p>1/3</p>', '<p>1/9</p>', '<p>1/2</p>', '<p>0</p>', '<p>Tak terhingga</p>', -1, -1, 4, -1, -1, 0, '', '', '', '', '', 'C', '<p> </p>\r\n<p class=\"MsoListParagraphCxSpFirst\" xss=removed><span lang=\"IN\" xss=removed>1 – 1/3 - 1/9 - 1/27 - ....  </span><span lang=\"IN\" xss=removed>= 1 – ( 1/3 +1/9 + 1/27 + ......) </span></p>\r\n<p class=\"MsoListParagraphCxSpMiddle\" xss=removed><span lang=\"IN\" xss=removed>         </span></p>\r\n<p class=\"MsoListParagraphCxSpMiddle\" xss=removed><span lang=\"IN\" xss=removed>*) 1/3 + 1/9 + 1/27 + ....  </span><span lang=\"IN\" xss=removed>= a /(1-r) </span><span lang=\"IN\" xss=removed>= (1/3)/(1-1/3) </span><span lang=\"IN\" xss=removed>= ½ [deret geometri tak hingga]</span></p>\r\n<p class=\"MsoListParagraphCxSpLast\" xss=removed> </p>\r\n<p class=\"MsoListParagraphCxSpLast\" xss=removed><span lang=\"IN\" xss=removed>sehingga</span></p>\r\n<p class=\"MsoListParagraphCxSpLast\" xss=removed><span lang=\"IN\" xss=removed>1 – ( 1/3 +1/9 + 1/27 + ......) = 1 – ½ = ½</span></p>\r\n<p> </p>\r\n<p> </p>', 1593456227, 1593456227),
(874, 20, 18, 43, 0, '', '', '<p><span class=\"fontstyle0\">Jika jumlah x bertambah tiga kali lipat perharinya dan Y bertambah enam kali lipat setiap 3 hari, maka jumlah x dan jumlah Y dalam seminggu adalah...</span></p>', '<p><span class=\"fontstyle0\">X < Y></p>', '<p><span class=\"fontstyle0\">X = Y</span></p>', '<p><span class=\"fontstyle0\">X + Y = 22</span></p>', '<p><span class=\"fontstyle0\">X > Y</span></p>', '<p><span class=\"fontstyle0\">Hubungan tidak dapat ditentukan.</span> </p>', -1, -1, -1, 4, -1, 0, '', '', '', '', '', 'D', '<p class=\"MsoListParagraphCxSpFirst\" xss=removed><span lang=\"IN\" xss=removed>diketahui x = 3 kali lipat perhari dan y = 6 kali lipat per 3 hari. </span></p>\r\n<p class=\"MsoListParagraphCxSpMiddle\" xss=removed><span lang=\"IN\" xss=removed>Maka jumlah x dan y seminggu adalah ,</span></p>\r\n<p class=\"MsoListParagraphCxSpMiddle\" xss=removed><span lang=\"IN\" xss=removed>X = 3 / hari * 7 = 21 <span xss=removed> </span></span></p>\r\n<p class=\"MsoListParagraphCxSpMiddle\" xss=removed><span lang=\"IN\" xss=removed>Y = 6/3hari * 7 = 14 . </span></p>\r\n<p class=\"MsoListParagraphCxSpLast\" xss=removed><span lang=\"IN\" xss=removed>Jadi, X > Y.</span></p>', 1593456343, 1593456343),
(875, 20, 18, 43, 0, '', '', '<p><span class=\"fontstyle0\">Berapa rata-rata barisan aritmatika kelipatan empat mulai dari enam belas sampai dengan empat puluh delapan ...</span> </p>', '<p>32</p>', '<p>23</p>', '<p>56</p>', '<p>27</p>', '<p>41</p>', 4, -1, -1, -1, -1, 0, '', '', '', '', '', 'A', '<p class=\"MsoListParagraphCxSpFirst\" xss=removed><span lang=\"IN\" xss=removed>Un = 16 + (n-1) 4</span></p>\r\n<p class=\"MsoListParagraphCxSpMiddle\" xss=removed><span lang=\"IN\" xss=removed>48 = 12 + 4n </span></p>\r\n<p class=\"MsoListParagraphCxSpMiddle\" xss=removed><span lang=\"IN\" xss=removed>36 = 4n maka n = 9 </span></p>\r\n<p class=\"MsoListParagraphCxSpLast\" xss=removed><span lang=\"IN\" xss=removed>S9 = (9/2) (2. 16 + 8 . 4 ) = (9/2) (64) = 288 .</span></p>\r\n<p class=\"MsoListParagraphCxSpLast\" xss=removed><span lang=\"IN\" xss=removed>Jadi, rata-rata= 288/9 = 32</span></p>', 1593456432, 1593456432),
(876, 20, 18, 43, 0, '', '', '<p><span class=\"fontstyle0\">Suku ke – 3 barisan geometri sama<br>dengan suku ke-6 barisan aritmatika.<br>Suku pertama kedua barisan tersebut adalah 6. Jika rasio (r) barisan geometri sama dengan (b) beda barisan aritmatika, dan keduanya adalah bilangan positif, maka suku ke- 4 barisan geometri dikurangi suku ke-13 barisan aritmatika adalah...</span> </p>', '<p>- 3,75</p>', '<p>2,50</p>', '<p>- 4,2</p>', '<p>- 1,5</p>', '<p>1,5</p>', 4, -1, -1, -1, -1, 0, '', '', '', '', '', 'A', '<p>www</p>\r\n<p><span xss=removed>aG = aA = 6</span></p>\r\n<p><span xss=removed>b = r</span></p>\r\n<p><span lang=\"IN\" xss=removed>U3G = U6A </span><span lang=\"IN\" xss=removed>à</span><span lang=\"IN\" xss=removed>6r<sup>2</sup> = 6 + 5r</span></p>\r\n<p> </p>\r\n<p class=\"MsoListParagraphCxSpMiddle\" xss=removed><span lang=\"IN\" xss=removed><span xss=removed>            </span></span><span lang=\"IN\" xss=removed><span xss=removed>à</span></span><span lang=\"IN\" xss=removed> 6r<sup>2</sup> – 5r – 6 = 0</span></p>\r\n<p class=\"MsoListParagraphCxSpMiddle\" xss=removed><span lang=\"IN\" xss=removed><span xss=removed>            </span></span><span lang=\"IN\" xss=removed><span xss=removed>à</span></span><span lang=\"IN\" xss=removed>(3r + <span xss=removed> </span>2)(2r – 3) = 0</span></p>\r\n<p class=\"MsoListParagraphCxSpMiddle\" xss=removed><span lang=\"IN\" xss=removed><span xss=removed>            </span></span><span lang=\"IN\" xss=removed><span xss=removed>à</span></span><span lang=\"IN\" xss=removed> r = 3/2 atau -2/3 dipilih 3/2 karena positif</span></p>\r\n<p class=\"MsoListParagraphCxSpMiddle\" xss=removed><span lang=\"IN\" xss=removed>U4G - U13A = ar<sup>3</sup> – (a+12b)</span></p>\r\n<p class=\"MsoListParagraphCxSpMiddle\" xss=removed><span lang=\"IN\" xss=removed><span xss=removed>     </span>= 6(3/2)<sup>3</sup> – (6 + 12.3/2) </span></p>\r\n<p class=\"MsoListParagraphCxSpLast\" xss=removed><span lang=\"IN\" xss=removed><span xss=removed>     </span>=81/4 – 24 = – 3,75</span></p>\r\n<p> </p>', 1593456655, 1593456655),
(877, 20, 18, 43, 0, '', '', '<p><span class=\"fontstyle0\">Sebuah kelereng dijatuhkan dari ketinggian 140 m . setiap kali menyentuh tanah, kelereng akan memantul hingga tiga perempat dari ketinggian sebelumnya . Berapa ketinggian yang dicapai bola setelah pemantulan ke enam ...</span></p>', '<p><span class=\"fontstyle0\">71,25 m</span></p>', '<p><span class=\"fontstyle0\">331,8 dm</span></p>', '<p><span class=\"fontstyle0\">397 m</span></p>', '<p><span class=\"fontstyle0\">42 m</span></p>', '<p><span class=\"fontstyle0\">436 cm</span> </p>', -1, 4, -1, -1, -1, 0, '', '', '', '', '', 'B', '<p class=\"MsoListParagraph\" xss=removed><span lang=\"IN\" xss=removed>U5 = 140 x (3/4)5 = 140 x (243 /1024) =33, 18 m = 331,8 dm</span></p>', 1593456782, 1593456782),
(878, 20, 18, 43, 0, '', '', '<p><span class=\"fontstyle0\">Antara bilangan 20 dan 116 disisipi 11 bilangan. Bilangan ini bersama dua bilangan semula membentuk deret hitung. Jumlah deret hitung adalah.. (Soal USM STAN 2009)</span></p>', '<p>952</p>', '<p>884</p>', '<p>880</p>', '<p>816</p>', '', -1, 4, -1, -1, -1, 0, '', '', '', '', '', 'B', '<p><span xss=removed>Total ada 13 bilangan = maka</span></p>\r\n<p><span xss=removed>Sn = n/2(a+un) </span></p>\r\n<p xss=removed><span xss=removed>= 13/2 (20+116 </span></p>\r\n<p xss=removed><span xss=removed>= 884</span></p>', 1593456911, 1593456911),
(879, 20, 18, 43, 0, '', '', '<p><span class=\"fontstyle0\">Angka ketujuh dari sederetan angka adalah 45. Jika selisih angka kedua dengan angka pertama adalah -7. Berapakah nilai angka yang berada pada urutan keempat.. (Soal USM STAN 2004)</span></p>', '<p><span class=\"fontstyle0\">122</span></p>', '<p><span class=\"fontstyle0\">94</span></p>', '<p><span class=\"fontstyle0\">66</span></p>', '<p><span class=\"fontstyle0\">45</span> </p>', '', -1, -1, 4, -1, -1, 0, '', '', '', '', '', 'C', '<p class=\"MsoListParagraphCxSpFirst\" xss=removed><span lang=\"IN\" xss=removed>U7 = 45</span></p>\r\n<p class=\"MsoListParagraphCxSpMiddle\" xss=removed><span lang=\"IN\" xss=removed>b = -7</span></p>\r\n<p class=\"MsoListParagraphCxSpMiddle\" xss=removed><span xss=removed>a + 6b =45</span></p>\r\n<p class=\"MsoListParagraphCxSpMiddle\" xss=removed><span lang=\"IN\" xss=removed>a = 45 +42 = 87</span></p>\r\n<p class=\"MsoListParagraphCxSpMiddle\" xss=removed><span lang=\"IN\" xss=removed>U4 = a + 3b = 87 + 3 (-7)</span></p>\r\n<p class=\"MsoListParagraphCxSpLast\" xss=removed><span lang=\"IN\" xss=removed>= 87 – 21 = 66</span></p>', 1593457061, 1593457061),
(880, 20, 18, 43, 0, '', '', '<p><span class=\"fontstyle0\">Dalam suatu deretan bilangan, setiap bilangan setelah bilangan pertama adalah 1/3 dari bilangan yang mendahuluinya. Jika bilangan ke-5 dalam deretan tersebut adalah 3, berapakah bilangan ke-2? (Soal USM STAN 2002)<br></span></p>', '<p><span class=\"fontstyle0\">1/3</span></p>', '<p><span class=\"fontstyle0\">1</span></p>', '<p><span class=\"fontstyle0\">27</span></p>', '<p><span class=\"fontstyle0\">81</span> </p>', '', -1, -1, -1, 4, -1, 0, '', '', '', '', '', 'D', '<p class=\"MsoListParagraphCxSpFirst\" xss=removed><span lang=\"IN\" xss=removed>r = 1/ 3</span></p>\r\n<p class=\"MsoListParagraphCxSpMiddle\" xss=removed><span lang=\"IN\" xss=removed>U5 = 3</span></p>\r\n<p class=\"MsoListParagraphCxSpMiddle\" xss=removed><span lang=\"IN\" xss=removed>ar<sup>4</sup> = 3</span></p>\r\n<p class=\"MsoListParagraphCxSpMiddle\" xss=removed><span lang=\"IN\" xss=removed>a (1/ 3)<sup>4</sup>= 3</span></p>\r\n<p class=\"MsoListParagraphCxSpMiddle\" xss=removed><span lang=\"IN\" xss=removed>a.1/81 = 3</span></p>\r\n<p class=\"MsoListParagraphCxSpMiddle\" xss=removed><span lang=\"IN\" xss=removed>a = 243</span></p>\r\n<p class=\"MsoListParagraphCxSpMiddle\" xss=removed><span lang=\"IN\" xss=removed>U2=?</span></p>\r\n<p class=\"MsoListParagraphCxSpLast\" xss=removed><span lang=\"IN\" xss=removed>U2 = ar = 243 x 1/3 = 81</span></p>', 1593460490, 1593460490),
(881, 20, 18, 43, 0, '', '', '<p><span class=\"fontstyle0\">Suku ke-3 suatu barisan geometri sama dengan suku ke-7 barisan aritmatika. Suku pertama kedua barisan tersebut adalah 4. Jika rasio (r) barisan geometri sama dengan beda (b) barisan aritmatika, dan keduanya adalah bilangan bulat, maka suku ke-6 barisan geometri dikurangi suku ke-15 barisan aritmatika adalah … (Soal USM STAN 2013)</span></p>', '<p><span class=\"fontstyle0\">96</span></p>', '<p><span class=\"fontstyle0\">98</span></p>', '<p><span class=\"fontstyle0\">100</span></p>', '<p><span class=\"fontstyle0\">102</span></p>', '<p><span class=\"fontstyle0\">104</span> </p>', 4, -1, -1, -1, -1, 0, '', '', '', '', '', 'A', '<p class=\"MsoListParagraphCxSpFirst\" xss=removed><span lang=\"IN\" xss=removed>Suku ke 3 geometri = suku ke 7 geometri</span></p>\r\n<p class=\"MsoListParagraphCxSpMiddle\" xss=removed><span lang=\"IN\" xss=removed>ar<sup>2</sup> = a+6b</span></p>\r\n<p class=\"MsoListParagraphCxSpMiddle\" xss=removed><span lang=\"IN\" xss=removed>4r<sup>2</sup> = 4 + 6b </span><span lang=\"IN\" xss=removed>ß</span><span lang=\"IN\" xss=removed> ingat rasio dan beda sama</span></p>\r\n<p class=\"MsoListParagraphCxSpMiddle\" xss=removed><span lang=\"IN\" xss=removed>ar<sup>2</sup> – 6r – 4 = 0</span></p>\r\n<p class=\"MsoListParagraphCxSpMiddle\" xss=removed><span lang=\"IN\" xss=removed>(ar + 2) (r-2) = 0</span></p>\r\n<p class=\"MsoListParagraphCxSpMiddle\" xss=removed><span lang=\"IN\" xss=removed>r = -1/2 dan r = 2</span></p>\r\n<p class=\"MsoListParagraphCxSpMiddle\" xss=removed><span lang=\"IN\" xss=removed>suku ke-6 geometri dikurangi suku ke-15 aritmatika</span></p>\r\n<p class=\"MsoListParagraphCxSpLast\" xss=removed><span lang=\"IN\" xss=removed>4r5 – (a+14b)</span></p>\r\n<p><span lang=\"IN\" xss=removed>= 4 (2)5 – (4 + 14(2)) = 128-32 = 96</span></p>', 1593460616, 1593460616),
(882, 20, 18, 43, 0, '', '', '<p><span class=\"fontstyle0\">15, 8, 45, 64, 75, 512... (Soal USM STAN 2010)</span></p>', '<p>105</p>', '<p>128</p>', '<p>600</p>', '<p>4096</p>', '', 4, -1, -1, -1, -1, 0, '', '', '', '', '', 'A', '<p class=\"MsoListParagraphCxSpFirst\" xss=removed><span lang=\"IN\" xss=removed>Suku genap = x8</span></p>\r\n<p class=\"MsoListParagraphCxSpLast\" xss=removed><span lang=\"IN\" xss=removed>Suku ganjil = +30</span></p>', 1593460704, 1593460704),
(883, 20, 18, 43, 0, '', '', '<p><span class=\"fontstyle0\">0, 4, 10, 18, 28, …. (Soal USM STAN 2010)</span></p>', '<p>40, 54</p>', '<p>40, 48</p>', '<p>38, 54</p>', '<p>38, 48</p>', '', 4, -1, -1, -1, -1, 0, '', '', '', '', '', 'A', '<p><span lang=\"IN\" xss=removed>+4 , +6, +8, +10, +12, +14</span></p>', 1593460816, 1593460816),
(884, 20, 18, 43, 0, '', '', '<p><span class=\"fontstyle0\">11, 25, 55, 125, 275, 625, …. (Soal USM STAN 2010)</span></p>', '<p>1225</p>', '<p>1375</p>', '<p>3025</p>', '<p>3125</p>', '', -1, 4, -1, -1, -1, 0, '', '', '', '', '', 'B', '<p class=\"MsoListParagraph\" xss=removed><span lang=\"IN\" xss=removed>Suku ganjil : x5</span></p>\r\n<p><span lang=\"IN\" xss=removed>Suku genap : 5<sup>1</sup> , 5<sup>2</sup>, 5<sup>3</sup>, dst</span></p>', 1593460907, 1593460907),
(885, 20, 18, 43, 0, '', '', '<p><span class=\"fontstyle0\">2, 4, 4, 7, 8, 10.. (Soal USM STAN 2010)</span></p>', '<p><span class=\"fontstyle0\">15, 13</span></p>', '<p><span class=\"fontstyle0\">16, 14</span></p>', '<p><span class=\"fontstyle0\">15, 14</span></p>', '<p><span class=\"fontstyle0\">16, 13</span> </p>', '', -1, -1, -1, 4, -1, 0, '', '', '', '', '', 'D', '<p class=\"MsoListParagraph\" xss=removed><span lang=\"IN\" xss=removed>Suku ganjil = x2</span></p>\r\n<p><span lang=\"IN\" xss=removed>Suku genap = +3</span></p>', 1593460998, 1593460998),
(886, 20, 18, 43, 0, '', '', '<p><span class=\"fontstyle0\">½, ½, ¾. 1 ¼, 2, …. (Soal USM STAN 2010)</span></p>', '<p><span class=\"fontstyle0\">3</span></p>', '<p><span class=\"fontstyle0\">3 ¼</span></p>', '<p><span class=\"fontstyle0\">3 ¾</span></p>', '<p><span class=\"fontstyle0\">4</span> </p>', '', -1, 4, -1, -1, -1, 0, '', '', '', '', '', 'B', '<p class=\"MsoListParagraphCxSpFirst\" xss=removed><span lang=\"IN\" xss=removed>Suku 1 + suku 2 = suku 3</span></p>\r\n<p class=\"MsoListParagraphCxSpLast\" xss=removed><span lang=\"IN\" xss=removed>Suku 2 + suku 3 = suku 4 dst</span></p>', 1593461086, 1593461086),
(887, 20, 18, 43, 0, '', '', '<p><span class=\"fontstyle0\">5, 7, 50, 49, 500, 343,… (Soal USM STAN 2009)</span></p>', '<p><span class=\"fontstyle0\">2401</span></p>', '<p><span class=\"fontstyle0\">3500</span></p>', '<p><span class=\"fontstyle0\">4900</span></p>', '<p><span class=\"fontstyle0\">5000</span></p>', '', -1, -1, -1, 4, -1, 0, '', '', '', '', '', 'D', '<p class=\"MsoListParagraphCxSpFirst\" xss=removed><span lang=\"IN\" xss=removed>Suku ganjil : x10</span></p>\r\n<p class=\"MsoListParagraphCxSpLast\" xss=removed><span lang=\"IN\" xss=removed>Suku genap : 7<sup>1 </sup>, 7<sup>2</sup>, 7<sup>3</sup>, dst</span></p>', 1593461557, 1593461557),
(888, 20, 18, 43, 0, '', '', '<p><span class=\"fontstyle0\">10, 30, 32, 16, 48, 50… (Soal USM STAN 2008)</span></p>', '<p><span class=\"fontstyle0\">58</span></p>', '<p><span class=\"fontstyle0\">25</span></p>', '<p><span class=\"fontstyle0\">32</span></p>', '<p><span class=\"fontstyle0\">18</span> </p>', '', -1, 4, -1, -1, -1, 0, '', '', '', '', '', 'B', '<p class=\"MsoListParagraphCxSpFirst\" xss=removed><span lang=\"IN\" xss=removed>Suku 1 x 3 = suku 2</span></p>\r\n<p class=\"MsoListParagraphCxSpMiddle\" xss=removed><span lang=\"IN\" xss=removed>Suku 2 + 2 = suku 3</span></p>\r\n<p class=\"MsoListParagraphCxSpMiddle\" xss=removed><span lang=\"IN\" xss=removed>Suku 3 : 2 = suku 4</span></p>\r\n<p class=\"MsoListParagraphCxSpLast\" xss=removed><span lang=\"IN\" xss=removed>Suku 4 x 3 = suku 5 dst</span></p>', 1593461645, 1593461645),
(889, 20, 18, 43, 0, '', '', '<p><span class=\"fontstyle0\">P, Q, 9, 16, 25, 36, 49, ..Berapa P dan Q? (Soal USM STAN 2001)</span></p>', '<p><span class=\"fontstyle0\">0 dan 2</span></p>', '<p><span class=\"fontstyle0\">1 dan 4</span></p>', '<p><span class=\"fontstyle0\">2 dan 5</span></p>', '<p><span class=\"fontstyle0\">3 dan 6</span> </p>', '', -1, 4, -1, -1, -1, 0, '', '', '', '', '', 'B', '<p class=\"MsoListParagraphCxSpFirst\" xss=removed><span lang=\"IN\" xss=removed>Suku 1 = 1<sup>2</sup></span></p>\r\n<p class=\"MsoListParagraphCxSpMiddle\" xss=removed><span lang=\"IN\" xss=removed>Suku 2 = 2<sup>2</sup></span></p>\r\n<p class=\"MsoListParagraphCxSpMiddle\" xss=removed><span lang=\"IN\" xss=removed>Suku 3 = 3<sup>2</sup></span></p>\r\n<p class=\"MsoListParagraphCxSpLast\" xss=removed><span lang=\"IN\" xss=removed>Dst</span></p>', 1593461772, 1593461772),
(890, 20, 18, 43, 0, '', '', '<p><span class=\"fontstyle0\">3, 4, 9, 32, ..., ...</span></p>', '<p><span class=\"fontstyle0\">120 dan 765</span></p>', '<p><span class=\"fontstyle0\">155 dan 476</span></p>', '<p><span class=\"fontstyle0\">88 dan 934</span></p>', '<p><span class=\"fontstyle0\">155 dan 924</span></p>', '<p><span class=\"fontstyle0\">64 dan 156</span> </p>', -1, -1, -1, 4, -1, 0, '', '', '', '', '', 'D', '<p class=\"MsoListParagraphCxSpFirst\" xss=removed><span lang=\"IN\" xss=removed>3<span xss=removed>          </span>4<span xss=removed>           </span>9<span xss=removed>           </span>32<span xss=removed>           </span>155<span xss=removed>           </span>924</span></p>\r\n<p class=\"MsoListParagraphCxSpMiddle\" xss=removed><span lang=\"IN\" xss=removed><span xss=removed>  </span>x2 -2<span xss=removed>     </span>x3 -3<span xss=removed>     </span>x4 -4<span xss=removed>      </span><strong>x5 -5<span xss=removed>        </span>x6-6</strong></span></p>\r\n<p class=\"MsoListParagraphCxSpMiddle\" xss=removed> </p>\r\n<p class=\"MsoListParagraphCxSpMiddle\" xss=removed><span xss=removed>selanjutnyapolanya: x5 -5 dan x6 -6</span></p>', 1593461889, 1593461889),
(891, 20, 18, 43, 0, '', '', '<p><span class=\"fontstyle0\">4, 7, 5, 6 , 11, 10, 10, 16, 16, ... , ...</span></p>', '<p><span class=\"fontstyle0\">16 17</span></p>', '<p><span class=\"fontstyle0\">16 16</span></p>', '<p><span class=\"fontstyle0\">15 21</span></p>', '<p><span class=\"fontstyle0\">15 22</span></p>', '<p><span class=\"fontstyle0\">16 22</span> </p>', -1, -1, -1, -1, 4, 0, '', '', '', '', '', 'E', '<p class=\"MsoListParagraphCxSpFirst\" xss=removed><span lang=\"IN\" xss=removed>4 7 5 6 11 10 10 16 16</span></p>\r\n<p class=\"MsoListParagraphCxSpMiddle\" xss=removed><span lang=\"IN\" xss=removed>Pola 1 (4 6 10 …)</span></p>\r\n<p class=\"MsoListParagraphCxSpMiddle\" xss=removed><span lang=\"IN\" xss=removed>polanya: +2, +4, maka selanjutnya +6</span></p>\r\n<p class=\"MsoListParagraphCxSpMiddle\" xss=removed><span lang=\"IN\" xss=removed>Suku ke-2, (7 11 16)</span></p>\r\n<p class=\"MsoListParagraphCxSpMiddle\" xss=removed><span lang=\"IN\" xss=removed>Polanya: +4, +5 +6</span></p>\r\n<p class=\"MsoListParagraphCxSpMiddle\" xss=removed><span lang=\"IN\" xss=removed>Suku ke-3, (5 10 …)</span></p>\r\n<p class=\"MsoListParagraphCxSpLast\" xss=removed><span lang=\"IN\" xss=removed>Polanya: +5, selanjutnya +6</span></p>', 1593462145, 1593462145),
(892, 20, 18, 43, 0, '', '', '<p><span class=\"fontstyle0\">E H J K O N ...</span></p>', '<p>T</p>', '<p>U</p>', '<p>V</p>', '<p>R</p>', '<p>S</p>', 4, -1, -1, -1, -1, 0, '', '', '', '', '', 'A', '<p class=\"MsoListParagraphCxSpFirst\" xss=removed><span lang=\"IN\" xss=removed>Jawaban A. E H J K O N T</span></p>\r\n<p class=\"MsoListParagraphCxSpFirst\" xss=removed><span xss=removed>E J O T : polanya +5</span></p>\r\n<p class=\"MsoListParagraphCxSpFirst\" xss=removed><span xss=removed>H K N … : polanya +3</span></p>', 1593462247, 1593462247),
(893, 20, 18, 43, 0, '', '', '<p><span class=\"fontstyle0\">I K M L K M O ... ....</span></p>', '<p><span class=\"fontstyle0\">N Q</span></p>', '<p><span class=\"fontstyle0\">N R</span></p>', '<p><span class=\"fontstyle0\">Q S</span></p>', '<p><span class=\"fontstyle0\">M N</span></p>', '<p><span class=\"fontstyle0\">N M</span> </p>', -1, -1, -1, -1, 4, 0, '', '', '', '', '', 'E', '<p class=\"MsoListParagraphCxSpFirst\" xss=removed><span lang=\"IN\" xss=removed>I <span xss=removed>      </span>K <span xss=removed>    </span><span xss=removed>  </span>M <span xss=removed>      </span>L <span xss=removed>      </span>K <span xss=removed>      </span>M <span xss=removed>     </span>O <span xss=removed>     </span>N <span xss=removed>     </span>M</span></p>\r\n<p class=\"MsoListParagraphCxSpMiddle\" xss=removed><span lang=\"IN\" xss=removed><span xss=removed>  </span>+2 <span xss=removed>   </span>+2 <span xss=removed>      </span>-1 <span xss=removed>    </span>-1 <span xss=removed>     </span>+2 <span xss=removed>     </span>+2</span></p>\r\n<p class=\"MsoListParagraphCxSpLast\" xss=removed> </p>\r\n<p class=\"MsoListParagraphCxSpLast\" xss=removed><span lang=\"IN\" xss=removed>Dua suku selanjutnya -1, -1</span></p>', 1593462362, 1593462362),
(894, 20, 18, 43, 0, '', '', '<p><span class=\"fontstyle0\">B Y D T G Q L  ... ...</span></p>', '<p><span class=\"fontstyle0\">O S</span></p>', '<p><span class=\"fontstyle0\">O T</span></p>', '<p><span class=\"fontstyle0\">P R</span></p>', '<p><span class=\"fontstyle0\">P T</span></p>', '<p><span class=\"fontstyle0\">Q S</span> </p>', 4, -1, -1, -1, -1, 0, '', '', '', '', '', 'A', '<p class=\"MsoListParagraphCxSpFirst\" xss=removed><span lang=\"IN\" xss=removed>B<span xss=removed>     </span>Y<span xss=removed>    </span>D<span xss=removed>     </span>T<span xss=removed>     </span>G<span xss=removed>     </span>Q<span xss=removed>     </span>L</span></p>\r\n<p class=\"MsoListParagraphCxSpMiddle\" xss=removed><span lang=\"IN\" xss=removed>2<span xss=removed>    </span>25<span xss=removed>   </span>4<span xss=removed>    </span>20<span xss=removed>     </span>7<span xss=removed>     </span>17<span xss=removed>    </span>12</span></p>\r\n<p class=\"MsoListParagraphCxSpMiddle\" xss=removed><span lang=\"IN\" xss=removed> </span></p>\r\n<p class=\"MsoListParagraphCxSpMiddle\" xss=removed><span lang=\"IN\" xss=removed>POLA 1 ; +2 +3 +5 +7 (B, D, G, L)</span></p>\r\n<p class=\"MsoListParagraphCxSpMiddle\" xss=removed><span lang=\"IN\" xss=removed>POLA 2 : -5 - 3 – 2 (Y, T, Q)</span></p>\r\n<p class=\"MsoListParagraphCxSpLast\" xss=removed><span lang=\"IN\" xss=removed>Jadi jawabannya adalah 15 dan 19 yaitu O dan S</span></p>', 1593462456, 1593462456),
(895, 20, 18, 43, 0, '', '', '<p><span class=\"fontstyle0\">E, E, E, I, E, M… (Soal USM STAN 2010)</span></p>', '<p>E</p>', '<p>K</p>', '<p>N</p>', '<p>Q</p>', '', 4, -1, -1, -1, -1, 0, '', '', '', '', '', 'A', '<p class=\"MsoListParagraphCxSpFirst\" xss=removed><span lang=\"IN\" xss=removed>Suku ganjil = selalu 5</span></p>\r\n<p class=\"MsoListParagraphCxSpLast\" xss=removed><span lang=\"IN\" xss=removed>Suku genap = +5</span></p>', 1593462516, 1593462516),
(896, 20, 18, 43, 0, '', '', '<p><span class=\"fontstyle0\">X, W, U, V, T, S, Q, R, P, O.. (Soal USM STAN 2010)</span></p>', '<p><span class=\"fontstyle0\">M, N</span></p>', '<p><span class=\"fontstyle0\">N, M</span></p>', '<p><span class=\"fontstyle0\">M, L</span></p>', '<p><span class=\"fontstyle0\">L, M</span> </p>', '', -1, 4, -1, -1, -1, 0, '', '', '', '', '', 'B', '<p class=\"MsoListParagraphCxSpFirst\" xss=removed><span lang=\"IN\" xss=removed>X, W, U, V, T, S, Q, R, P, O, M, N</span></p>\r\n<p class=\"MsoListParagraphCxSpMiddle\" xss=removed><span lang=\"IN\" xss=removed>U, V </span><span lang=\"IN\" xss=removed><span xss=removed>à</span></span><span lang=\"IN\" xss=removed> bila mengikuti urutan harusnya V, U</span></p>\r\n<p class=\"MsoListParagraphCxSpMiddle\" xss=removed><span lang=\"IN\" xss=removed>Q, R </span><span lang=\"IN\" xss=removed><span xss=removed>à</span></span><span lang=\"IN\" xss=removed> bila mengikuti urutan harusnya R, Q</span></p>\r\n<p class=\"MsoListParagraphCxSpLast\" xss=removed><span lang=\"IN\" xss=removed>M, N </span><span lang=\"IN\" xss=removed><span xss=removed>à</span></span><span lang=\"IN\" xss=removed> bila mengikuti urutan harusnya N, M</span></p>', 1593462603, 1593462603),
(897, 20, 18, 43, 0, '', '', '<p><span class=\"fontstyle0\">X, Y, K, X, U, L, X, Q,… (Soal USM STAN 2009)</span></p>', '<p>M</p>', '<p>N</p>', '<p>V</p>', '<p>X</p>', '', 4, -1, -1, -1, -1, 0, '', '', '', '', '', 'A', '<p class=\"MsoListParagraphCxSpFirst\" xss=removed><span lang=\"IN\" xss=removed>Suku 1 : 24 -- Suku 2 : 25 – Suku 3 = 11</span></p>\r\n<p class=\"MsoListParagraphCxSpMiddle\" xss=removed><span lang=\"IN\" xss=removed>Suku 4 : 24 -- Suku 5 : 21 – Suku 6 = 12</span></p>\r\n<p class=\"MsoListParagraphCxSpLast\" xss=removed><span lang=\"IN\" xss=removed>Suku 7 : 24 – Suku 8 : 17 – Suku 9 = 13</span></p>', 1593462709, 1593462709),
(898, 20, 18, 43, 0, '', '', '<p><span class=\"fontstyle0\">A, B, C, E, H (Soal USM STAN 2009)</span></p>', '<p><span class=\"fontstyle0\">M, T</span></p>', '<p><span class=\"fontstyle0\">N, T</span></p>', '<p><span class=\"fontstyle0\">N, U</span></p>', '<p><span class=\"fontstyle0\">M, U</span> </p>', '', 4, -1, -1, -1, -1, 0, '', '', '', '', '', 'A', '<p class=\"MsoListParagraphCxSpFirst\" xss=removed><span lang=\"IN\" xss=removed>A = 1</span></p>\r\n<p class=\"MsoListParagraphCxSpMiddle\" xss=removed><span lang=\"IN\" xss=removed>B = 2</span></p>\r\n<p class=\"MsoListParagraphCxSpMiddle\" xss=removed><span lang=\"IN\" xss=removed>C = 3</span></p>\r\n<p class=\"MsoListParagraphCxSpMiddle\" xss=removed><span lang=\"IN\" xss=removed>E = 5</span></p>\r\n<p class=\"MsoListParagraphCxSpMiddle\" xss=removed><span lang=\"IN\" xss=removed>H = 8</span></p>\r\n<p class=\"MsoListParagraphCxSpMiddle\" xss=removed><span lang=\"IN\" xss=removed>Suku 1 + suku 2 = suku 3</span></p>\r\n<p class=\"MsoListParagraphCxSpMiddle\" xss=removed><span lang=\"IN\" xss=removed>Suku 2 + suku 3 = suku 4 dst</span></p>\r\n<p class=\"MsoListParagraphCxSpMiddle\" xss=removed><span lang=\"IN\" xss=removed>5 + 8 = 13 (M)</span></p>\r\n<p class=\"MsoListParagraphCxSpLast\" xss=removed><span lang=\"IN\" xss=removed>8 + 13 = 21 (U)</span></p>', 1593462782, 1593462782),
(899, 20, 18, 43, 0, '', '', '<p><span class=\"fontstyle0\">A, B, D, G, K,.... (Soal USM STAN 2009)</span></p>', '<p><span class=\"fontstyle0\">O, V</span></p>', '<p><span class=\"fontstyle0\">O, U</span></p>', '<p><span class=\"fontstyle0\">P, U</span></p>', '<p><span class=\"fontstyle0\">P, V</span> </p>', '', 4, -1, -1, -1, -1, 0, '', '', '', '', '', 'A', '<p class=\"MsoListParagraphCxSpFirst\" xss=removed><span lang=\"IN\" xss=removed>A = 1</span></p>\r\n<p class=\"MsoListParagraphCxSpMiddle\" xss=removed><span lang=\"IN\" xss=removed>B = 2</span></p>\r\n<p class=\"MsoListParagraphCxSpMiddle\" xss=removed><span lang=\"IN\" xss=removed>D = 4</span></p>\r\n<p class=\"MsoListParagraphCxSpMiddle\" xss=removed><span lang=\"IN\" xss=removed>G = 7</span></p>\r\n<p class=\"MsoListParagraphCxSpMiddle\" xss=removed><span lang=\"IN\" xss=removed>K = 11</span></p>\r\n<p class=\"MsoListParagraphCxSpMiddle\" xss=removed><span lang=\"IN\" xss=removed>Suku 1 (1) + 1 = Suku 2 (2)</span></p>\r\n<p class=\"MsoListParagraphCxSpMiddle\" xss=removed><span lang=\"IN\" xss=removed>Suku 2 (2) + 2 = Suku 3 (4)</span></p>\r\n<p class=\"MsoListParagraphCxSpMiddle\" xss=removed><span lang=\"IN\" xss=removed>Suku 3 (4) + 3 = Suku 4 (7)</span></p>\r\n<p class=\"MsoListParagraphCxSpLast\" xss=removed><span lang=\"IN\" xss=removed>Dst</span></p>', 1593462881, 1593462881),
(900, 20, 18, 43, 0, '', '', '<p><span class=\"fontstyle0\">D, Y, N, Y, X... (Soal USM STAN 2009)</span></p>', '<p>A</p>', '<p>S</p>', '<p>W</p>', '<p>Y</p>', '', 4, -1, -1, -1, -1, 0, '', '', '', '', '', 'A', '<p class=\"MsoListParagraph\" xss=removed><span lang=\"IN\" xss=removed>Suku genap selalu Y. Abaikan saja suku ganjil nya</span></p>', 1593462947, 1593462947),
(901, 20, 18, 43, 0, '', '', '<p><span class=\"fontstyle0\">Q, E, N, G, K, I… (Soal USM STAN 2010)</span></p>', '<p>H</p>', '<p>J</p>', '<p>L</p>', '<p>M</p>', '', 4, -1, -1, -1, -1, 0, '', '', '', '', '', 'A', '<p class=\"MsoListParagraphCxSpFirst\" xss=removed><span lang=\"IN\" xss=removed>Q – N – K = melompat 2 ke belakang</span></p>\r\n<p class=\"MsoListParagraphCxSpLast\" xss=removed><span lang=\"IN\" xss=removed>E – G – I = melompat 1 ke depan</span></p>', 1593462994, 1593462994);

-- --------------------------------------------------------

--
-- Table structure for table `tb_soal`
--

CREATE TABLE `tb_soal` (
  `id_soal` int(11) NOT NULL,
  `dosen_id` int(11) NOT NULL,
  `matkul_id` int(11) NOT NULL,
  `id_soal_paket` int(10) NOT NULL,
  `bobot` int(11) NOT NULL,
  `file` varchar(255) NOT NULL,
  `tipe_file` varchar(50) NOT NULL,
  `soal` longtext NOT NULL,
  `opsi_a` longtext NOT NULL,
  `opsi_b` longtext NOT NULL,
  `opsi_c` longtext NOT NULL,
  `opsi_d` longtext NOT NULL,
  `opsi_e` longtext NOT NULL,
  `file_a` varchar(255) NOT NULL,
  `file_b` varchar(255) NOT NULL,
  `file_c` varchar(255) NOT NULL,
  `file_d` varchar(255) NOT NULL,
  `file_e` varchar(255) NOT NULL,
  `jawaban` varchar(5) NOT NULL,
  `created_on` int(11) NOT NULL,
  `updated_on` int(11) DEFAULT NULL,
  `jenis` enum('tryout','kuis','latihan') NOT NULL,
  `id_bab` int(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tb_soal`
--

INSERT INTO `tb_soal` (`id_soal`, `dosen_id`, `matkul_id`, `id_soal_paket`, `bobot`, `file`, `tipe_file`, `soal`, `opsi_a`, `opsi_b`, `opsi_c`, `opsi_d`, `opsi_e`, `file_a`, `file_b`, `file_c`, `file_d`, `file_e`, `jawaban`, `created_on`, `updated_on`, `jenis`, `id_bab`) VALUES
(21, 6, 7, 0, 1, '', '', '<p>soal 2 bab 2 nomor 1</p>', '<p>1</p>', '<p>2</p>', '<p>3</p>', '<p>4</p>', '<p>5</p>', '', '', '', '', '', 'D', 1585137699, 1585141394, 'latihan', 11),
(22, 6, 7, 0, 1, '', '', '<p>soal 2 bab 2 nomor 2</p>', '<p>a</p>', '<p>d</p>', '<p>f</p>', '<p>c</p>', '<p>x</p>', '', '', '', '', '', 'E', 1585141372, 1585141372, 'latihan', 11),
(23, 6, 7, 0, 1, '', '', '<p>soal 2 bab 2 nomor 3</p>', '<p>d</p>', '<p>a</p>', '<p>g</p>', '<p>x</p>', '<p>h</p>', '', '', '', '', '', 'C', 1585141418, 1585141418, 'latihan', 11),
(24, 6, 7, 0, 1, '', '', '<p>soal 1 bab 1 nomor 1</p>', '<p>d</p>', '<p>a</p>', '<p>g</p>', '<p>v</p>', '<p>c</p>', '', '', '', '', '', 'E', 1585141442, 1585147819, 'latihan', 9),
(25, 6, 7, 0, 1, '', '', '<p>soal 1 bab 1 nomor 2</p>', '<p>dd</p>', '<p>aa</p>', '<p>dds</p>', '<p>a</p>', '<p>g</p>', '', '', '', '', '', 'C', 1585141473, 1585147805, 'latihan', 9),
(26, 6, 7, 0, 1, '', '', '<p>soal 1 bab 1 nomor 3</p>', '<p>a</p>', '<p>g</p>', '<p>b</p>', '<p>d</p>', '<p>c</p>', '', '', '', '', '', 'A', 1585141490, 1585147790, 'latihan', 9),
(27, 6, 7, 0, 1, '', '', '<p>aaa</p><p data-f-id=\"pbf\" xss=removed>Powered by <a href=\"https://www.froala.com/wysiwyg-editor?pb=1\" title=\"Froala Editor\">Froala Editor</a></p>', '<p>a</p><p data-f-id=\"pbf\" xss=removed>Powered by <a href=\"https://www.froala.com/wysiwyg-editor?pb=1\" title=\"Froala Editor\">Froala Editor</a></p>', '<p>v</p><p data-f-id=\"pbf\" xss=removed>Powered by <a href=\"https://www.froala.com/wysiwyg-editor?pb=1\" title=\"Froala Editor\">Froala Editor</a></p>', '<p>c</p><p data-f-id=\"pbf\" xss=removed>Powered by <a href=\"https://www.froala.com/wysiwyg-editor?pb=1\" title=\"Froala Editor\">Froala Editor</a></p>', '<p>d</p><p data-f-id=\"pbf\" xss=removed>Powered by <a href=\"https://www.froala.com/wysiwyg-editor?pb=1\" title=\"Froala Editor\">Froala Editor</a></p>', '<p>e</p><p data-f-id=\"pbf\" xss=removed>Powered by <a href=\"https://www.froala.com/wysiwyg-editor?pb=1\" title=\"Froala Editor\">Froala Editor</a></p>', '', '', '', '', '', 'A', 1585897871, 1585897871, 'kuis', 14),
(28, 6, 7, 0, 1, '', '', '<p>Soal 1</p><p data-f-id=\"pbf\" xss=removed>Powered by <a href=\"https://www.froala.com/wysiwyg-editor?pb=1\" title=\"Froala Editor\">Froala Editor</a></p>', '<p>a</p><p data-f-id=\"pbf\" xss=removed>Powered by <a href=\"https://www.froala.com/wysiwyg-editor?pb=1\" title=\"Froala Editor\">Froala Editor</a></p>', '<p>b</p><p data-f-id=\"pbf\" xss=removed>Powered by <a href=\"https://www.froala.com/wysiwyg-editor?pb=1\" title=\"Froala Editor\">Froala Editor</a></p>', '<p>c</p><p data-f-id=\"pbf\" xss=removed>Powered by <a href=\"https://www.froala.com/wysiwyg-editor?pb=1\" title=\"Froala Editor\">Froala Editor</a></p>', '<p>d</p><p data-f-id=\"pbf\" xss=removed>Powered by <a href=\"https://www.froala.com/wysiwyg-editor?pb=1\" title=\"Froala Editor\">Froala Editor</a></p>', '<p>e</p><p data-f-id=\"pbf\" xss=removed>Powered by <a href=\"https://www.froala.com/wysiwyg-editor?pb=1\" title=\"Froala Editor\">Froala Editor</a></p>', '', '', '', '', '', 'A', 1585898115, 1585898115, 'kuis', 14),
(29, 6, 7, 0, 1, '', '', '<p>soal latihan 1</p><p data-f-id=\"pbf\" xss=removed>Powered by <a href=\"https://www.froala.com/wysiwyg-editor?pb=1\" title=\"Froala Editor\">Froala Editor</a></p>', '<p>a</p><p data-f-id=\"pbf\" xss=removed>Powered by <a href=\"https://www.froala.com/wysiwyg-editor?pb=1\" title=\"Froala Editor\">Froala Editor</a></p>', '<p>v</p><p data-f-id=\"pbf\" xss=removed>Powered by <a href=\"https://www.froala.com/wysiwyg-editor?pb=1\" title=\"Froala Editor\">Froala Editor</a></p>', '<p>d</p><p data-f-id=\"pbf\" xss=removed>Powered by <a href=\"https://www.froala.com/wysiwyg-editor?pb=1\" title=\"Froala Editor\">Froala Editor</a></p>', '<p>d</p><p data-f-id=\"pbf\" xss=removed>Powered by <a href=\"https://www.froala.com/wysiwyg-editor?pb=1\" title=\"Froala Editor\">Froala Editor</a></p>', '<p>g</p><p data-f-id=\"pbf\" xss=removed>Powered by <a href=\"https://www.froala.com/wysiwyg-editor?pb=1\" title=\"Froala Editor\">Froala Editor</a></p>', '', '', '', '', '', 'A', 1585899269, 1585899269, 'latihan', 14),
(30, 9, 17, 0, 1, '9b24a3e1eeaa7c691261de87da931af9.jpg', 'image/jpeg', '<p>alat apakah ini ?</p><p data-f-id=\"pbf\" xss=removed>Powered by <a href=\"https://www.froala.com/wysiwyg-editor?pb=1\" title=\"Froala Editor\">Froala Editor</a></p>', '<p>alat kedokteran</p><p data-f-id=\"pbf\" xss=removed>Powered by <a href=\"https://www.froala.com/wysiwyg-editor?pb=1\" title=\"Froala Editor\">Froala Editor</a></p>', '<p>alat kematian</p><p data-f-id=\"pbf\" xss=removed>Powered by <a href=\"https://www.froala.com/wysiwyg-editor?pb=1\" title=\"Froala Editor\">Froala Editor</a></p>', '<p>alat bengkel</p><p data-f-id=\"pbf\" xss=removed>Powered by <a href=\"https://www.froala.com/wysiwyg-editor?pb=1\" title=\"Froala Editor\">Froala Editor</a></p>', '<p>alat kehidupan</p><p data-f-id=\"pbf\" xss=removed>Powered by <a href=\"https://www.froala.com/wysiwyg-editor?pb=1\" title=\"Froala Editor\">Froala Editor</a></p>', '<p data-f-id=\"pbf\" xss=removed>Powered by <a href=\"https://www.froala.com/wysiwyg-editor?pb=1\" title=\"Froala Editor\">Froala Editor</a></p>', '', '', '', '', '', 'A', 1586486149, 1586486149, 'kuis', 20),
(31, 9, 17, 0, 1, '69e3933c85901b4f56e5c0e47cd66570.jpg', 'image/jpeg', '<p>gambar apa ini?</p><p data-f-id=\"pbf\" xss=removed>Powered by <a href=\"https://www.froala.com/wysiwyg-editor?pb=1\" title=\"Froala Editor\">Froala Editor</a></p>', '<p>semar</p><p data-f-id=\"pbf\" xss=removed>Powered by <a href=\"https://www.froala.com/wysiwyg-editor?pb=1\" title=\"Froala Editor\">Froala Editor</a></p>', '<p>gareng</p><p data-f-id=\"pbf\" xss=removed>Powered by <a href=\"https://www.froala.com/wysiwyg-editor?pb=1\" title=\"Froala Editor\">Froala Editor</a></p>', '<p>petruk</p><p data-f-id=\"pbf\" xss=removed>Powered by <a href=\"https://www.froala.com/wysiwyg-editor?pb=1\" title=\"Froala Editor\">Froala Editor</a></p>', '<p>gareng</p><p data-f-id=\"pbf\" xss=removed>Powered by <a href=\"https://www.froala.com/wysiwyg-editor?pb=1\" title=\"Froala Editor\">Froala Editor</a></p>', '<p data-f-id=\"pbf\" xss=removed>Powered by <a href=\"https://www.froala.com/wysiwyg-editor?pb=1\" title=\"Froala Editor\">Froala Editor</a></p>', '', '', '', '', '', 'A', 1586487606, 1586487606, 'latihan', 20),
(32, 11, 18, 0, 4, '', '', '<p>gg</p>', '<p>d</p>', '<p>g</p>', '<p>g</p>', '<p>g</p>', '<p>g</p>', '', '', '', '', '', 'B', 1586612111, 1586612111, 'kuis', 22),
(33, 11, 18, 0, 1, '', '', '<p>eee</p>', '<p>d</p>', '<p>f</p>', '<p>f</p>', '<p>f</p>', '<p>f</p>', '', '', '', '', '', 'B', 1586621230, 1586621230, 'latihan', 21),
(34, 12, 23, 0, 1, '', '', '<p>sdh</p>', '<p>g</p>', '<p>hg</p>', '<p>jhg</p>', '<p>hjg</p>', '<p>hjg</p>', '', '', '', '', '', 'B', 1586787680, 1586787680, 'kuis', 21),
(35, 12, 23, 0, 1, '', '', '<p>kjhkj</p>', '<p>kjh</p>', '<p>jhkj</p>', '<p>kjhkj</p>', '<p>kjhkj</p>', '<p>hjk</p>', '', '', '', '', '', 'D', 1586787986, 1586787986, 'latihan', 21);

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(11) UNSIGNED NOT NULL,
  `ip_address` varchar(45) NOT NULL,
  `username` varchar(100) DEFAULT NULL,
  `password` varchar(255) NOT NULL,
  `email` varchar(254) DEFAULT NULL,
  `activation_selector` varchar(255) DEFAULT NULL,
  `activation_code` varchar(255) DEFAULT NULL,
  `forgotten_password_selector` varchar(255) DEFAULT NULL,
  `forgotten_password_code` varchar(255) DEFAULT NULL,
  `forgotten_password_time` int(11) UNSIGNED DEFAULT NULL,
  `remember_selector` varchar(255) DEFAULT NULL,
  `remember_code` varchar(255) DEFAULT NULL,
  `created_on` int(11) UNSIGNED NOT NULL,
  `last_login` int(11) UNSIGNED DEFAULT NULL,
  `active` tinyint(1) UNSIGNED DEFAULT NULL,
  `first_name` varchar(50) DEFAULT NULL,
  `last_name` varchar(50) DEFAULT NULL,
  `company` varchar(100) DEFAULT NULL,
  `phone` varchar(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `ip_address`, `username`, `password`, `email`, `activation_selector`, `activation_code`, `forgotten_password_selector`, `forgotten_password_code`, `forgotten_password_time`, `remember_selector`, `remember_code`, `created_on`, `last_login`, `active`, `first_name`, `last_name`, `company`, `phone`) VALUES
(1, '127.0.0.1', 'Administrator', '$2y$12$ttOYZQ/XiD/ia4MFbrnmjOQ4gFJPW.zHYNXqBuOMi320r9gSqhfFy', 'admin@bersamabelajar.com', NULL, '', NULL, NULL, NULL, NULL, NULL, 1268889823, 1593505504, 1, 'Admin', 'Istrator', 'ADMIN', '0'),
(31, '103.119.141.244', '12121212', '$2y$10$y8P.FZn4.t8p00uTofiDguXjauAH0AW29ossKiZ2rnTKEY..sWM9q', 'bersamabelajar0411@gmail.com', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1586947438, 1591762698, 1, 'Bersama', 'Belajar', NULL, NULL),
(32, '103.119.141.58', '00000003', '$2y$10$DJHaeJg29mTffaQfZfC5KOMk1ysVcB8B9IzgwxAD3K/87KTKRxyLm', 'yuli@gmail.com', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1589077762, 1591762731, 1, 'Yuli', 'Yuli', NULL, NULL),
(34, '103.119.141.58', '00000005', '$2y$10$CVluLZ2pBMQmO4IzPBXWjulLyOuxB5CD5RBOIKHVMqbeeR/FGZ4bq', 'ika@gmail.com', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1589081335, 1590868526, 1, 'ika', 'ika', NULL, NULL),
(47, '103.119.140.145', '20200001', '$2y$10$RGRwgOeABUSPF19Mc/XJEukcW8Sk8fyrpDFQGg8uAB34uHX4JPfk2', 'nursilmialfi@gmail.com', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1591825710, 1592188625, 1, 'Alfi', 'Ubaedi', NULL, NULL),
(48, '103.119.140.145', '20200002', '$2y$10$hryXVw6fuM3IJzGDELqZbee4/61GcH4GbTVJK7CFcHc.mDr9EqqWi', 'berylcholif.8b@gmail.com', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1591825932, 1593342466, 1, 'Beryl', 'Cholif', NULL, NULL),
(49, '103.119.140.145', '20200003', '$2y$10$AVldnkCZn6ixckor0aWB0OblnlD7HJz6q2D61p4HWNMGP.iMbZYyi', 'churotulismi@gmail.com', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1591825935, 1592143451, 1, 'Churotul', 'Ismi', NULL, NULL),
(50, '103.119.140.145', '20200004', '$2y$10$GGS/d2luL8Z7nGuyYYL0yuvRwnbMAq/mdaCzfBYYY4LAIulF5sinW', 'fadel2989@gmail.com', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1591825938, 1592593661, 1, 'FADEL', 'R', NULL, NULL),
(51, '103.119.140.145', '20200005', '$2y$10$UigBN0JKODHvwWrYUfkTXOsVNp4KcuxmW57XLVW2Erp1IdukC.Y7C', 'nadasalma735@gmail.com', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1591825941, 1592589591, 1, 'SALMA', 'RENADA', NULL, NULL),
(52, '103.119.140.145', '20200006', '$2y$10$PdIUGVacmZ6vSgNsDXF9j.MQmsdjhzLRN32JA6l2aoTd.ODpEdXXq', 'mr0809805@gmail.com', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1591825944, NULL, 1, 'Muhammad', 'Ridwan', NULL, NULL),
(54, '103.119.140.145', '10000001', '$2y$10$BWl0tCB1z/iP7QMWgNAYM.3xH5LZXlPISfyjgEootgMeDYVVO4Tuq', 'pungkimuh1@gmail.com', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1591826057, 1593453721, 1, 'Muh', 'Setiawan', NULL, NULL),
(55, '103.119.140.145', '20200007', '$2y$10$LOlRJi2lPPOVU/Nz4Sp7eej6oyHJ937269EBngAHVg0KK2GDvws0y', 'yusnitanitaa@gmail.com', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1591826127, 1591977019, 1, 'Yusnita', 'Rahmadiani', NULL, NULL),
(56, '103.119.140.119', '10000002', '$2y$10$8nhGyMY2LSEugxZ1.7KRouYt2cVtBwRq0tzGRnLnbtLlDp5cG170K', 'pungkimuh2@gmail.com', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1591977220, 1592718566, 1, 'Muh', 'Setiawan', NULL, NULL),
(57, '103.119.140.119', '10000005', '$2y$10$gg2lXV2z2Gr5XSxhimDfHOpeu5pydGDc/FlqVS1ilhjAo7YQqSBLu', 'muhpungkinur@gmail.com', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1591977467, NULL, 1, 'Muh', 'Nur', NULL, NULL),
(58, '103.119.140.119', '10000003', '$2y$10$kit86xYnnfxF6Epc7m0hXOBWShICrs/a7.CauqoiEZUlvU3lM38fu', 'Pungkimuh3@gmail.com', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1592032483, NULL, 1, 'Muh', 'Setiawan', NULL, NULL),
(59, '103.119.140.119', '10000004', '$2y$10$84E6ufc9.9TwEXwOZFRvZebJpyV7Xh6001xF72fuC1bxGpgXKdhMS', 'pungkimuh4@gmail.com', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1592032505, NULL, 1, 'Muh', 'Setiawan', NULL, NULL),
(60, '103.119.141.206', '11111111', '$2y$10$HWe.V40qp6P.gNj1obiqoO8wedrch4fJir7YlEvG5d7wo7PzSOigy', 'muhpungki.pajak@gmail.com', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1592289809, 1592291367, 1, 'Atya', 'Uzumaki', NULL, NULL),
(61, '103.119.141.206', 'Astriatom35', '$2y$10$1u/bxzCQAN9F02j5rjzSFeJf7rExHPP9wBN2.T6emKevdH5dzz.qe', 'penagihan419@gmail.com', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1592290631, NULL, 1, 'Rizky', 'Ramadhan', NULL, NULL),
(62, '103.119.141.206', '22222222', '$2y$10$hTVT/mEUnE9uiIfn1rPHM.xDPwp7Wx5fUb/FJJWf6cqD1XzcecMMS', 'orangesky@gmail.com', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1592292613, 1592327249, 1, 'Orange', 'Orange', NULL, NULL),
(63, '103.119.141.206', '33333333', '$2y$10$a5jDHMgaRAs8diYGCnmH2.UIY1fDxDncVvs9FGDYHs2J5bzhxKezi', '333@gmail.com', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1592297143, 1593418285, 1, 'sku', 'sku', NULL, NULL),
(64, '114.124.197.68', '44444444', '$2y$10$u7Ytc5LKI1N.rVY9UKoVYO6pi8tdk8lJeEsCy01eOkL.uvDKCnK76', '444@gmail.com', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1592635012, 1592635044, 1, 'NIta', 'NIta', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `users_groups`
--

CREATE TABLE `users_groups` (
  `id` int(11) UNSIGNED NOT NULL,
  `user_id` int(11) UNSIGNED NOT NULL,
  `group_id` mediumint(8) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `users_groups`
--

INSERT INTO `users_groups` (`id`, `user_id`, `group_id`) VALUES
(3, 1, 1),
(7, 5, 3),
(8, 6, 2),
(9, 7, 3),
(10, 8, 3),
(16, 14, 2),
(19, 17, 3),
(21, 19, 2),
(27, 25, 2),
(28, 26, 3),
(31, 29, 2),
(33, 31, 3),
(34, 32, 2),
(35, 33, 3),
(36, 34, 2),
(38, 36, 2),
(39, 37, 2),
(40, 38, 2),
(41, 39, 2),
(49, 47, 3),
(50, 48, 3),
(51, 49, 3),
(52, 50, 3),
(53, 51, 3),
(54, 52, 3),
(56, 54, 2),
(57, 55, 3),
(58, 56, 2),
(59, 57, 2),
(60, 58, 2),
(61, 59, 2),
(62, 60, 3),
(63, 61, 3),
(64, 62, 3),
(65, 63, 3),
(66, 64, 3);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `bab`
--
ALTER TABLE `bab`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `dosen`
--
ALTER TABLE `dosen`
  ADD PRIMARY KEY (`id_dosen`),
  ADD UNIQUE KEY `email` (`email`),
  ADD UNIQUE KEY `nip` (`nip`),
  ADD KEY `matkul_id` (`matkul_id`);

--
-- Indexes for table `groups`
--
ALTER TABLE `groups`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `h_kuis`
--
ALTER TABLE `h_kuis`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `h_latihan`
--
ALTER TABLE `h_latihan`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `h_ujian`
--
ALTER TABLE `h_ujian`
  ADD PRIMARY KEY (`id`),
  ADD KEY `ujian_id` (`ujian_id`),
  ADD KEY `mahasiswa_id` (`mahasiswa_id`);

--
-- Indexes for table `jenis_soal`
--
ALTER TABLE `jenis_soal`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `jurusan`
--
ALTER TABLE `jurusan`
  ADD PRIMARY KEY (`id_jurusan`);

--
-- Indexes for table `jurusan_matkul`
--
ALTER TABLE `jurusan_matkul`
  ADD PRIMARY KEY (`id`),
  ADD KEY `jurusan_id` (`jurusan_id`),
  ADD KEY `matkul_id` (`matkul_id`);

--
-- Indexes for table `kabupaten`
--
ALTER TABLE `kabupaten`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `kelas`
--
ALTER TABLE `kelas`
  ADD PRIMARY KEY (`id_kelas`),
  ADD KEY `jurusan_id` (`jurusan_id`);

--
-- Indexes for table `kelas_dosen`
--
ALTER TABLE `kelas_dosen`
  ADD PRIMARY KEY (`id`),
  ADD KEY `kelas_id` (`kelas_id`),
  ADD KEY `dosen_id` (`dosen_id`);

--
-- Indexes for table `login_attempts`
--
ALTER TABLE `login_attempts`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `mahasiswa`
--
ALTER TABLE `mahasiswa`
  ADD PRIMARY KEY (`id_mahasiswa`);

--
-- Indexes for table `materi`
--
ALTER TABLE `materi`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `matkul`
--
ALTER TABLE `matkul`
  ADD PRIMARY KEY (`id_matkul`);

--
-- Indexes for table `m_kuis`
--
ALTER TABLE `m_kuis`
  ADD PRIMARY KEY (`id_kuis`);

--
-- Indexes for table `m_latihan`
--
ALTER TABLE `m_latihan`
  ADD PRIMARY KEY (`id_latihan`);

--
-- Indexes for table `m_ujian`
--
ALTER TABLE `m_ujian`
  ADD PRIMARY KEY (`id_ujian`),
  ADD KEY `matkul_id` (`matkul_id`),
  ADD KEY `dosen_id` (`dosen_id`);

--
-- Indexes for table `paket`
--
ALTER TABLE `paket`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `pendaftaran`
--
ALTER TABLE `pendaftaran`
  ADD PRIMARY KEY (`id`) USING BTREE;

--
-- Indexes for table `provinsi`
--
ALTER TABLE `provinsi`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `soal_paket`
--
ALTER TABLE `soal_paket`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_kelas`
--
ALTER TABLE `tbl_kelas`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_mapel`
--
ALTER TABLE `tbl_mapel`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_program` (`id_program`);

--
-- Indexes for table `tbl_program_belajar`
--
ALTER TABLE `tbl_program_belajar`
  ADD PRIMARY KEY (`id`) USING BTREE;

--
-- Indexes for table `tbl_soal_tryout`
--
ALTER TABLE `tbl_soal_tryout`
  ADD PRIMARY KEY (`id_soal`) USING BTREE;

--
-- Indexes for table `tb_soal`
--
ALTER TABLE `tb_soal`
  ADD PRIMARY KEY (`id_soal`),
  ADD KEY `matkul_id` (`matkul_id`),
  ADD KEY `dosen_id` (`dosen_id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `uc_activation_selector` (`activation_selector`),
  ADD UNIQUE KEY `uc_forgotten_password_selector` (`forgotten_password_selector`),
  ADD UNIQUE KEY `uc_remember_selector` (`remember_selector`),
  ADD UNIQUE KEY `uc_email` (`email`) USING BTREE;

--
-- Indexes for table `users_groups`
--
ALTER TABLE `users_groups`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `uc_users_groups` (`user_id`,`group_id`),
  ADD KEY `fk_users_groups_users1_idx` (`user_id`),
  ADD KEY `fk_users_groups_groups1_idx` (`group_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `bab`
--
ALTER TABLE `bab`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=75;

--
-- AUTO_INCREMENT for table `dosen`
--
ALTER TABLE `dosen`
  MODIFY `id_dosen` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=25;

--
-- AUTO_INCREMENT for table `groups`
--
ALTER TABLE `groups`
  MODIFY `id` mediumint(8) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `h_kuis`
--
ALTER TABLE `h_kuis`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `h_latihan`
--
ALTER TABLE `h_latihan`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `h_ujian`
--
ALTER TABLE `h_ujian`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;

--
-- AUTO_INCREMENT for table `jenis_soal`
--
ALTER TABLE `jenis_soal`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `jurusan`
--
ALTER TABLE `jurusan`
  MODIFY `id_jurusan` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;

--
-- AUTO_INCREMENT for table `jurusan_matkul`
--
ALTER TABLE `jurusan_matkul`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `kabupaten`
--
ALTER TABLE `kabupaten`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=588;

--
-- AUTO_INCREMENT for table `kelas`
--
ALTER TABLE `kelas`
  MODIFY `id_kelas` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=28;

--
-- AUTO_INCREMENT for table `kelas_dosen`
--
ALTER TABLE `kelas_dosen`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=23;

--
-- AUTO_INCREMENT for table `login_attempts`
--
ALTER TABLE `login_attempts`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `mahasiswa`
--
ALTER TABLE `mahasiswa`
  MODIFY `id_mahasiswa` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=37;

--
-- AUTO_INCREMENT for table `materi`
--
ALTER TABLE `materi`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=75;

--
-- AUTO_INCREMENT for table `matkul`
--
ALTER TABLE `matkul`
  MODIFY `id_matkul` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `m_kuis`
--
ALTER TABLE `m_kuis`
  MODIFY `id_kuis` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `m_latihan`
--
ALTER TABLE `m_latihan`
  MODIFY `id_latihan` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `m_ujian`
--
ALTER TABLE `m_ujian`
  MODIFY `id_ujian` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- AUTO_INCREMENT for table `paket`
--
ALTER TABLE `paket`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=131;

--
-- AUTO_INCREMENT for table `pendaftaran`
--
ALTER TABLE `pendaftaran`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=33;

--
-- AUTO_INCREMENT for table `provinsi`
--
ALTER TABLE `provinsi`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=38;

--
-- AUTO_INCREMENT for table `soal_paket`
--
ALTER TABLE `soal_paket`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=48;

--
-- AUTO_INCREMENT for table `tbl_kelas`
--
ALTER TABLE `tbl_kelas`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;

--
-- AUTO_INCREMENT for table `tbl_mapel`
--
ALTER TABLE `tbl_mapel`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=24;

--
-- AUTO_INCREMENT for table `tbl_program_belajar`
--
ALTER TABLE `tbl_program_belajar`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT for table `tbl_soal_tryout`
--
ALTER TABLE `tbl_soal_tryout`
  MODIFY `id_soal` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=902;

--
-- AUTO_INCREMENT for table `tb_soal`
--
ALTER TABLE `tb_soal`
  MODIFY `id_soal` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=36;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=65;

--
-- AUTO_INCREMENT for table `users_groups`
--
ALTER TABLE `users_groups`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=67;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `jurusan_matkul`
--
ALTER TABLE `jurusan_matkul`
  ADD CONSTRAINT `jurusan_matkul_ibfk_1` FOREIGN KEY (`jurusan_id`) REFERENCES `jurusan` (`id_jurusan`),
  ADD CONSTRAINT `jurusan_matkul_ibfk_2` FOREIGN KEY (`matkul_id`) REFERENCES `matkul` (`id_matkul`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
